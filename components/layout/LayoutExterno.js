import React from 'react';
import Head from 'next/head';
import { Global, css } from '@emotion/core';


import HeaderExterno from './HeaderExterno';

const LayoutExterno = (props) => {



    return (
        <>
            <Global
                styles={css`
                :root{
                    --gris: #3d3d3d;
                }
                html{
                    box-sizing: border-box;
                }
                *, *:before, *:after{
                    box-sizing: inherit;
                } 
                .contenedor{
                    margin: 0 4rem;
                }
                h1{
                    font-weight: 700;
                }
                .btn-lg{
                    font-size: 1rem;
                }
                
                a:link {
                    color: #f7ba00;
                }
                /* visited link */
                a:visited {
                    color: #f7ba00;
                }
                .logo-naranja{
                    color: #f7ba00;
                }
                .btn-primary{
                    background-color: #e5ac02;
                    border: #e5ac02;
                    
                    
                    &:hover{
                        /* background-color: #d19d02; */
                        background-color: #b28603;
                    }
                    &:focus{
                        background-color: #b28603;
                    }
                    &:disabled{
                        background-color: #b28603;
                    }
                }
                .centered {
                height: 100vh; /* Magic here */
                display: flex;
                justify-content: center;
                align-items: center;
                }
                .btn-secondary{
                    background-color: transparent;
                    border: transparent;
                    outline: none;
                    color: #e5ac02;
                    &:hover{
                        /* background-color: #d19d02; */
                        background-color: transparent;
                        color: #b28603
                    }
                    &:focus{
                        background-color: transparent;
                        color: #b28603
                    }
                    &:disabled{
                        background-color: transparent;
                        color: #b28603
                    }
                }
                .prohibido{
                    cursor: not-allowed;
                }
                .titulo{
                    font-weight: 700;
                    text-transform: uppercase;
                    color:#3e3f3a;
                }
                .cabecera{
                    /* font-size: 1rem; */
                    &:hover{
                        cursor: pointer;
                    }
                }
                .btn-exportar{
                    background-color: #047513;
                    padding: 0.75rem;
                    font-size: 0.85rem;
                    /* margin-left: 20rem; */
                    margin-bottom: 2rem;
                    margin-top: 2rem;
                    max-height:46px;
                    &:link{
                        color: whitesmoke;
                    }
                    &:hover{
                        /* background-color: #096615; */
                        background-color: #085912;
                    }
                }
                .sobre{
                    &:hover{
                        cursor: pointer;
                    }
                }

                

                /* Comportamiento Responsive del Header */
                @media (max-width: 992px){
                    #navbarColor01{
                        justify-content: center;
                        text-align: center;
                        ul{
                            margin-right: 7%;
                        }

                        div p{
                            display: none;
                        }
                        #opcionesDelUsuario{
                            justify-content: center;
                        }
                    }

                    nav img{
                        margin: 0 auto;
                        
                    }    

                    .tablaScroll{
                        overflow: scroll;
                        width: 100%;
                        height: 500px;
                    }
                }
            
            `}


            />
            <Head>
                <title>Lawyou</title>
                {/* <link rel="icon" href="/favicon.ico" /> */}
                <link rel="shortcut icon" href="../static/favicon/favicon.ico" />
                <link rel="stylesheet" href="https://bootswatch.com/4/sandstone/bootstrap.min.css" />
                {/* <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> */}
                {/* <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;700&family=Open+Sans:wght@400;700&display=swap" rel="stylesheet" />  */}
            </Head>

            <HeaderExterno />


            <main>
                <div className="contenedor">
                    {props.children}
                </div>
            </main>

            {/* JS de Bootstrap */}
            <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossOrigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossOrigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossOrigin="anonymous"></script>
        </>
    )
};

export default LayoutExterno;