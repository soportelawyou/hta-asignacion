import React, { useContext, useEffect, useState } from 'react';
// import Link from 'next/link';
import Link from './Link';
import { css } from '@emotion/core';
import { OpcionesUsuario } from '../ui/OpcionesUsuario';
import styled from '@emotion/styled';

import { FirebaseContext } from '../../firebase/index';
import firebase from '../../firebase/index';

const Logo = styled.img`
    width:10rem;
    &:hover{
        cursor:pointer;
    }
`;

const HeaderExterno = () => {

    // State local para usuario
    const [tipoUsuario, guardarTipoUsuario] = useState('');

    const { usuario, firebase } = useContext(FirebaseContext);

    // Funcion para buscar al usuario
    const buscarUsuario = async () => {
        await firebase.db.collection("usuarios").where("email", "==", usuario.email).onSnapshot(manejarSnapShot);
    }

    function manejarSnapShot(snapshot) {
        const usuarioBD = snapshot.docs.map(doc => {
            return {
                id: doc.id,
                ...doc.data()
            }
        })
        if (usuarioBD[0] !== undefined) {
            guardarTipoUsuario(usuarioBD[0].rol)
        }
    }

    useEffect(() => {
        let montado = true;
        if (usuario !== undefined && usuario !== null) {
            buscarUsuario();
        }
        return () => montado = false;
    }, [usuario])

    const clickCerrarSesion = () => {
        firebase.cerrarSesion();
    }



    return (
        <header
            css={css`
                margin-bottom: 3rem;
                width: 100%;
                .navbar-brand{
                    font-size: 2rem;
                }
                .nav-link{
                    font-size: 1rem;
                    margin-right: 1rem;
                }
                

            `}
        >
            <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
                <Link href="/linkIncorporacion">
                    {/* <a className="navbar-brand ml-5">La<span className="logo-naranja">wy</span>ou</a> */}
                    <Logo src="/static/img/lawyou-blanco-transp.png" />
                </Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
            </nav>
        </header>


    );
}

export default HeaderExterno;