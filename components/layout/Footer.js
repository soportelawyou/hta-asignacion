import React from 'react';
// import Link from 'next/link';
import {Label,Div,Label1} from '../ui/FooterStyle'


const Footer = () => {


    return (
        <>
            <Div className="row">
                <Div className="col-12">
                   <Label>© Lawyou 2020. Todos los derechos reservados.</Label>
                    <a href="https://lawyoulegal.com/terminos-y-condiciones/" target="_blank"><Label1> Terminos y condiciones</Label1></a>
                    <a href="https://lawyoulegal.com/politica-anti-spam/" target="_blank"><Label> Política de Cookies</Label></a>
                    <a href="https://lawyoulegal.com/politica-de-privacidad-y-proteccion-de-datos/" target="_blank"><Label> Política de privacidad y proteccion de datos</Label></a>
                    <Label> V: 1.0.2</Label>
                </Div>
            </Div>
        </>
    );
}

export default Footer;