import React from 'react';
import Link from 'next/link';

const Terceros = ({ terceros }) => {

    const { tipo, nombre, apellidos, id, idLawyou, dni, direccion,idioma, calle, provincia, ciudad, comunidadAutonoma, cp, contadorAsuntos, contadorRechazados,numeroTelefono,activo } = terceros;
    
    return (
        <tr className="text-center">
            <th className="p-3 align-middle">
                <Link href="/terceros/[id]" as={`/terceros/${id}`}>
                    <a href={`/terceros/${id}`}><span className="mr-1">{nombre}</span><span>{apellidos}</span></a>
                </Link>
            </th> 
            <th className="p-3 align-middle">{tipo}</th>
            <th className="p-3 align-middle">{dni}</th>
            <th className="p-3 align-middle">{numeroTelefono}</th>
            <th className="p-3 align-middle">{direccion}</th>
            {idioma === undefined ? (<th></th>) :
                (
                    <th className="p-3 align-middle">{idioma.map((idioma, index) => {
                        return (<span key={`especialidad${index}`}>{(index ? ', ' : '') + idioma}</span>)
                    })}</th>
                )}
            <th className="p-3 align-middle">{activo}</th>
        </tr>


    );

}   

export default Terceros;