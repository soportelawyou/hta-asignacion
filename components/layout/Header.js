import React, { useContext, useEffect, useState } from 'react';
// import Link from 'next/link';
import Link from '../layout/Link';
import { css } from '@emotion/core';
import { OpcionesUsuario } from '../ui/OpcionesUsuario';
import styled from '@emotion/styled';

import { FirebaseContext } from '../../firebase/index';
import firebase from '../../firebase/index';

const Logo = styled.img`
    width:10rem;
    &:hover{
        cursor:pointer;
    }
`;

const Header = () => {

    // State local para usuario
    const [tipoUsuario, guardarTipoUsuario] = useState('');

    const { usuario, firebase } = useContext(FirebaseContext);

    // Funcion para buscar al usuario
    const buscarUsuario = async () => {
        await firebase.db.collection("usuarios").where("email", "==", usuario.email).onSnapshot(manejarSnapShot);
    }

    function manejarSnapShot(snapshot) {
        const usuarioBD = snapshot.docs.map(doc => {
            return {
                id: doc.id,
                ...doc.data()
            }
        })
        if (usuarioBD[0] !== undefined) {
            guardarTipoUsuario(usuarioBD[0].rol)
        }
    }

    useEffect(() => {
        let montado = true;
        if (usuario !== undefined && usuario !== null) {
            buscarUsuario();
        }
        return () => montado = false;
    }, [usuario])

    const clickCerrarSesion = () => {
        firebase.cerrarSesion();
    }



    return (
        <header
            css={css`
                margin-bottom: 3rem;
                width: 100%;
                .navbar-brand{
                    font-size: 2rem;
                }
                .nav-link{
                    font-size: 1rem;
                    margin-right: 1rem;
                }
                

            `}
        >
            <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
                <Link href="/">
                    {/* <a className="navbar-brand ml-5">La<span className="logo-naranja">wy</span>ou</a> */}
                    <Logo src="/static/img/lawyou-blanco-transp.png" />
                </Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse justify-content-between" id="navbarColor01">
                    {usuario && (
                        <div className="justify-content-start">
                            <ul className="navbar-nav ml-5">
                                <li className="nav-item" >
                                    <Link href="/">
                                        <a className="nav-link">Oportunidades</a>
                                    </Link>
                                </li>
                                <li className="nav-item" >
                                    <Link href="/abogados">
                                        <a className="nav-link">Abogados</a>
                                    </Link>
                                </li>
                                <li className="nav-item" >
                                    <Link href="/terceros">
                                        <a className="nav-link">Terceros</a>
                                    </Link>
                                </li>

                                {tipoUsuario === "superAdmin" && (
                                    <li className="nav-item" >
                                        <Link href="/usuarios">
                                            <a className="nav-link">Usuarios</a>
                                        </Link>
                                    </li>
                                )}
                                {/* <li className="nav-item" >
                                    <Link href="/preguntas">
                                        <a className="nav-link">Preguntas</a>
                                    </Link>
                                </li> */}


                            </ul>
                        </div>
                    )}

                    <OpcionesUsuario id="opcionesDelUsuario">
                        {usuario ? (
                            <>
                                <li className="nav-item" >
                                    <p className="align-center ">Hola {usuario.displayName}</p>
                                </li>
                                <button
                                    className="btn btn-primary btn-lg"
                                    onClick={() => clickCerrarSesion()}
                                >Cerrar Sesión</button>
                            </>
                        ) : (

                                <>
                                    <div className="justify-content-end">
                                        <Link href="/iniciar-sesion">
                                            <button className="btn btn-primary btn-lg mr-2">Iniciar Sesión</button>
                                        </Link>
                                        <Link href="/registro">
                                            <button className="btn btn-outline-secondary btn-lg">Registrarse</button>
                                        </Link>
                                    </div>
                                </>
                            )}
                    </OpcionesUsuario>
                </div>






            </nav>
        </header>


    );
}

export default Header;