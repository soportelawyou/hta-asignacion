import React, { useState, useEffect } from 'react';
import {format} from 'date-fns';

const SeleccionarAbogadoCita = ({ abogado, seleccionarRadio, abogadoAsignado}) => {

    const [clase, guardarClase] = useState('');

    const { id,nombre,apellidos,} = abogado;

    return (
        <>
        <tr className= {`text-center ${clase}`}>
                {/* <th className="p-3 col-1 align-middle"> */}
                    {abogadoAsignado === id ? (
                        <th className="p-3 col-1 align-middle">
                        <input
                            type="radio"
                            name="abogadoAsignado"
                            value={id}
                            onChange={(e) => console.log(seleccionarRadio(e.target.value))}
                            defaultChecked
                        />
                        </th>

                    ) :
                        (
                            <th className="p-3 col-1 align-middle">
                            <input
                                type="radio"
                                name="abogadoAsignado"
                                value={id}
                                // onChange={(e) => seleccionarRadio(e.target.value)}
                                onChange={(e) => console.log(seleccionarRadio(e.target.value))}
                            />
                            </th>
                        )}

                {/* </th> */}
                <th className="p-3 align-middle">
                    <span className="mr-1">{nombre}</span><span>{apellidos}</span>
                </th>
            </tr>
           

        </>
    );
}

export default SeleccionarAbogadoCita;