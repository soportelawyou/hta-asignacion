import React from 'react';
import Link from 'next/link';

const Abogado = ({ abogado }) => {

    const { tipo, nombre, apellidos, id, idLawyou, dni, especialidadesAbogadoPrincipal,subEspecialidadesAbogadosPrincipal, direccion, calle, provincia, ciudad, comunidadAutonoma, cp, activo, contadorAsuntos,contadorAceptados, idioma, contadorRechazados, numeroTelefonoPrincipal } = abogado;

    return (
        <tr className="text-center">
            <th className="p-3 align-middle">
                <Link href="/abogados/[id]" as={`/abogados/${id}`}>
                    <a href={`/abogados/${id}`}><span className="mr-1">{nombre}</span><span>{apellidos}</span></a>
                </Link>
            </th>
            <th className="p-3 align-middle">{tipo}</th>
            <th className="p-3 align-middle">{especialidadesAbogadoPrincipal.map((especialidad, index) => {
                return (<span key={`especialidad${index}`}>{(index ? ', ' : '') + especialidad}</span>)
            })}</th>
                        <th className="p-3 align-middle">{subEspecialidadesAbogadosPrincipal.map((especialidad, index) => {
                return (<span key={`especialidad${index}`}>{(index ? ', ' : '') + especialidad}</span>)
            })}</th>
            <th className="p-3 align-middle">{numeroTelefonoPrincipal}</th>
            <th className="p-3 align-middle">{direccion}</th>
            <th className="p-3 align-middle">{contadorAsuntos}</th>
            <th className="p-3 align-middle">{contadorAceptados}</th>
            <th className="p-3 align-middle">{contadorRechazados}</th>
            <th className="p-3 align-middle">{activo}</th>
            {idioma === undefined ? (<th></th>) :
                (
                    <th className="p-3 align-middle">{idioma.map((idioma, index) => {
                        return (<span key={`especialidad${index}`}>{(index ? ', ' : '') + idioma}</span>)
                    })}</th>
                )}

        </tr>


    );
}

export default Abogado;