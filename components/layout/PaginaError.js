import React from 'react';

const PaginaError = ({msg}) => {
    return ( 
        <div className="text-center mt-2">
            <h1>{msg}</h1>
        </div>
     );
}
 
export default PaginaError;