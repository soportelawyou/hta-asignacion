import React, { useState, useContext } from 'react';

import { Aceptado, Rechazado, Administrador } from '../ui/Estados';
import { BotonEliminar } from '../ui/BotonEliminar'
import { BotonEditar } from '../ui/Visual'
import { useRouter } from 'next/router';
import { FirebaseContext } from '../../firebase/index';
import Swal from 'sweetalert2';
import {MyVerticallyCenteredModal} from '../../pages/usuarios/[id]'

const SeleccionarUsuario = ({ usuario }) => {

    const [modalShow, setModalShow] = React.useState(false);
    const router = useRouter();

    const { firebase } = useContext(FirebaseContext);

    const { id, nombre, email } = usuario;

    const controlarPermisos = async (usuario) => {
        if (usuario.admitido) {
            await firebase.db.collection('usuarios').doc(usuario.id).update({ admitido: false })
        } else {
            await firebase.db.collection('usuarios').doc(usuario.id).update({ admitido: true })
        }

    }

    const cambioPermisos = (usuario) => {
        if (usuario !== undefined) {
            controlarPermisos(usuario);
        }
    }

    

    return (
        <>
            <tr key={usuario.email}>
                <td className="p-3 align-middle text-center">{usuario.nombre}</td>
                <td className="p-3 align-middle text-center">{usuario.email}</td>
                <td className="p-3 align-middle text-center">{usuario.rol}</td>
                {usuario.rol === "superAdmin" ? (
                    <td className="p-3 align-middle text-center">
                        <Administrador
                            className="sobre"
                        >Administrador</Administrador>
                    </td>
                ) : (
                        usuario.admitido ? (
                            <td className="p-3 align-middle text-center">
                                <Aceptado
                                    className="sobre"
                                    onClick={() => cambioPermisos(usuario)}
                                >Autorizado</Aceptado>
                            </td>
                        ) : (
                                <td className="p-3 align-middle text-center">
                                    <Rechazado
                                        className="sobre"
                                        onClick={() => cambioPermisos(usuario)}
                                    >Desautorizado</Rechazado>
                                </td>
                            )
                    )}
                <td className="p-3 align-middle text-center">                   
                    <BotonEditar
                    onClick={() => setModalShow(true)}>
                        Editar
                    </BotonEditar>
                    <MyVerticallyCenteredModal
                    show={modalShow}
                    id={id}
                    onHide={() => setModalShow(false)}
                    />
                </td>


            </tr>


        </>
    );
}

export default SeleccionarUsuario;