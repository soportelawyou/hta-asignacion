import React, { useContext } from 'react';
import { BotonEditar,BotonEliminar } from '../ui/Visual'
import {MyVerticallyCenteredModal} from'../../pages/preguntas/[id]';
import { FirebaseContext } from '../../firebase/index';
import Swal from 'sweetalert2';
import router from 'next/router';


const Preguntas = ({ pregunta }) => {

    
// las variables iniciadas
    const [modalShow, setModalShow] = React.useState(false);


    //verificar usuario

    const { usuario, firebase } = useContext(FirebaseContext);

  //Sacar los datos de la variable Desconstruct
    const { id,titulo, tipo, descripcion } = pregunta;

    const eliminarPreguntas = async () => {
        if (!usuario) {
            return router.push("/login");
        }

        // Modal de SWAL
        Swal.fire({
            title: '¿Estás seguro?',
            text: "No podrás deshacer esta acción!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, eliminar!',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value) {
                try {
                    firebase.db.collection('preguntas').doc(id).delete();
                    Swal.fire({
                        icon: 'success',
                        title: 'La pregunta ha sido eliminado!',
                        showConfirmButton: false,
                        timer: 1200
                    })
                    setTimeout(() => {
                        router.push("/preguntas")
                    }, 1200);
                } catch (error) {
                    console.log(error)
                }

            }
        })

    }

    return (
        <tr className="text-center">
            <th className="p-3 align-middle">{titulo}</th>
            <th className="p-3 align-middle">{tipo}</th>
            <th className="p-3 align-middle">{descripcion}</th>
            <th className="p-3 align-middle text-center">                   
                    <BotonEditar
                    onClick={() => setModalShow(true)}>
                        Editar
                    </BotonEditar>
                    <MyVerticallyCenteredModal
                    show={modalShow}
                    id={id}
                    onHide={() => setModalShow(false)}
                    />
                     <BotonEliminar
                    onClick={() => eliminarPreguntas()}>
                        Eliminar
                    </BotonEliminar>
                </th>

        </tr>


    );
}

export default Preguntas;