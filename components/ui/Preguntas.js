export const Generales = ["¿Que desea Conseguir? ¿Qué desea Obtener?"]

// Preguntas específicas
export const Erte = {
    Text1: "¿Cuál es la actividad de la empresa?",
    Text2: "¿Nombre de la empresa?",
    Text3: "¿Dónde (lugar) se realiza la actividad? (¿Casas? ¿Calle? ¿Empresas?)",
    DespProv1: "¿En qué provincia se encuentra?", // Desplegable Provincias
    Text4: "¿Cuántos empleados tiene?", // Tipo Numerico
    Text5: "¿A cuántos le quiere hacer el ERTE?", // Tipo Numerico (En caso de mas de 25 mensaje de avisar al departamento)
    SiNo1: "¿Está dentro de las del RD? FUERZA MAYOR (FM)"
}

// SIN SUBESPECIALIDAD
export const Bancario = {
    SiNo1: "¿Ha recibido una carta del banco o financiera?",
    Text1: "¿Qué pone en esa carta?", // Si la anterior es afirmativa
    Text2: "¿La carta esta a su nombre o más gente?",
    Text3: "¿Qué plazo pone?",
    SiNo2: "¿Tiene contrato o papeles relacionado?",
    Text4: "¿Qué tipo de contrato o papeles relacionado tiene?", // Si la anterior es afirmativa
    Text5: "¿Sabe el total de la deuda o cuanto asciende?"
}

export const Despidos = {
    SiNo1: "¿Le han dado la carta de despido?",
    Text1: "¿Qué fecha pone?", // Tipo Fecha
    Text2: "¿Qué motivo pone?",
    Text3: "¿A que se dedica la empresa?"
}

export const NegligenciaMedica = {
    Text1: "¿De que le iban a operar?",
    SiNo1: "¿Firmó algún tipo de consentimiento?",
    SiNo2: "¿Tenía la operación diferentes partes?",
    Text2: "¿De qué partes consistía?", // Si la anterior es afirmativa
    SiNo3: "¿Tiene usted informes médicos?",
    SiNo4: "¿Tiene historiales clínicos?",
    Text3: "¿Fecha de los hechos?", // Tipo fecha
    Text4: "¿Dónde se dio el hecho?"
}

export const Herencia = {
    // Personal
    Titulo1: "1-Preguntas de Carácter Personal",
    Text1: "¿Fecha de defunción?", // Tipo fecha
    SiNo1: "¿Era viudo el fallecido? ¿Era viuda la fallecida?",
    Text2: "¿Cuántos hermanos sois?", // Tipo numerico
    Text3: "¿Existe buena relación?", // Si la anterior es mayor que cero
    Text4: "¿Nacionalidad?",
    Text5: "¿Residencia?",
    Text6: "¿Último domicilio del difunto?",
    SiNo2: "¿Están todos los hermanos vivos?",
    SiNo3: "¿Tenía hijos?",
    Text7: "¿Cuántos hijos?", // Tipo numerico y Si la anterior es afirmativa
    SiNo4: "¿Son menores?", // Si la anterior2 es afirmativa
    SiNo5: "¿Hay testamento?",
    // Patrimonial
    Titulo2: "2-Preguntas de Carácter Patrimonial",
    SiNo6: "¿Existen bienes inmuebles?",
    SiNo7: "¿Tenéis una vivienda en común?",
    SiNo8: "¿Era vuestra vivienda principal?", // Si la anterior es afirmativa
    SiNo9: "¿Tenéis alguna segunda vivienda propiedad en alquiler?", // Si la anterior2 es afirmativa
    SiNo10: "¿Te puedes cambiar de casa?" // Si la anterior3 es afirmativa
}

// SIN SUBESPECIALIDAD
export const Divorcio = {
    Text1: "¿Cuántos meses lleváis casados o en pareja?",
    SiNo1: "¿Tenéis hijos en común?",
    SiNo2: "¿Son mayores de edad?", // Si la anterior es afirmativa
    Text2: "¿Que nacionalidad tenéis?",
    Text3: "¿Dónde tenéis el domicilio común?",
    Text4: "¿Trabajáis los dos? ¿Estáis en el paro? ¿En qué trabajáis?",
    SiNo3: "¿Tenéis bienes en común?",
    SiNo4: "¿Tenéis buena relación?",
    SiNo5: "¿Tienes buena relación con los padres de la otra parte?",
    SiNo6: "¿Tenéis coche?",
    SiNo7: "¿Lo compartís?", // Si la anterior es afirmativa
    SiNo8: "¿Tenéis una vivienda en común?",
    SiNo9: "¿Era vuestra vivienda principal?", // Si la anterior es afirmativa
    SiNo10: "¿Tenéis alguna segunda vivienda?",
    SiNo11: "¿Se encuentra en propiedad o alquiler?", // Si la anterior es afirmativa
    SiNo12: "¿Tenéis deudas?",
    SiNo13: "¿Bancos o frente a 3º?", // Si la anterior es afirmativa
    SiNo14: "¿Te puedes cambiar de casa?"
}

// SIN SUBESPECIALIDAD
export const Desahucio = {
    // Personal
    Titulo1: "1-Preguntas de Carácter Personal",
    Text1: "¿Cuál es la renta mensual del arrendamiento?",
    Text2: "¿Cómo se paga la renta?",
    Text3: "¿Cuántas rentas debe el arrendatario?",
    SiNo1: "¿Debe el inquilino alguna cantidad además de la renta?",
    Text4: "¿A cuánto asciende la cantidad que debe además de la renta?", // Si la anterior es afirmativa
    Text5: "¿Cuál es el valor catastral de la finca arrendada?",
    Text6: "¿El último importe del IBI?",
    SiNo2: "¿Ha sido el inquilino requerido de pago?",
    SiNo3: "¿El requerimiento ha sido judicial?",
    Text7: "¿Ha sido extrajudicial o fehaciente?", // Si la anterior es afirmativa
    // Patrimonial
    Titulo2: "2-Preguntas de Carácter Patrimonial",
    SiNo4: "¿Tiene contrato de arrendamiento?",
    Text8: "¿Cuándo firmó el contrato de arrendamiento?", // Tipo fecha
    Text9: "¿Qué duración tiene el contrato de arrendamiento?", // Tipo fecha
    SiNo5: "¿Es la primera vez que tiene problemas con el inquilino?",
    SiNo6: "¿Conoce el motivo del impago?",
    Text10: "¿Cuál es el motivo del impago?", // Si la anterior es afirmativa
    SiNo7: "¿Es el propietario de la finca?",
    Text11: "¿Cuántas personas viven en la vivienda alquilada objeto de desahucio?",
    SiNo8: "¿Son todos los individuos que viven en la vivienda familia?",
    SiNo9: "¿Hay menores en la vivienda?"
}

export const LeySegundaOportunidad = {
    Text1: "¿Sabe a cuanto asciende la deuda total?",
    SiNo1: "¿Ha hecho uso de esta ley en los últimos años?",
    SiNo2: "¿Ha tenido antecedentes penales por delito contra Hacienda, Seguridad Social?",
    Text2: "¿Qué tipo de delitos?", // Si la anterior es afirmativa
    SiNo3: "¿Ha alcanzado algún acuerdo de pagos con sus deudores extrajudicialmente en los últimos 5 años?",
    Text3: "¿Qué tipo de acuerdo ha alcanzado con sus deudores?", // Si la anterior es afirmativa
    SiNo4: "¿Ha rechazado alguna oferta de empleo de SEPE en los últimos 4 años?"
}

export const ResponsabilidadPatrimonialEstado = {
    Text1: "¿A qué se dedica su negocio?",
    Text2: "¿A cuánto ascienden sus perdidas debido al cierre de su negocio?",
    Text3: "¿Cuánto calcula que ha dejado de ganar?",
    SiNo1: "¿Hay empleados afectados?",
    SiNo2: "¿Ha realizado un ERTE?",
    SiNo3: "¿Sigue abierto el negocio?"
}

// SIN SUBESPECIALIDAD
export const Fijezas = {
    Text1: "¿Cuánto tiempo lleva trabajando con un contrato o nombramiento temporal o encadenamiento de contratos?",
    SiNo1: "¿Ha sacado o no su plaza?",
    SiNo2: "¿Tenemos sentencia del TS?", // Si la anterior negativa
    SiNo3: "¿Es plaza de un funcionario?",
    SiNo4: "¿Tiene una sentencia de un juzgado de lo contencioso?", // Si la anterior es afirmativa
    SiNo5: "¿Tenemos sentencia del juez?",
    SiNo6: "¿Ha tenido opción de poder sacarla en propiedad?",
    SiNo7: "¿Se ha puesto en contacto con la administración?",
    Text2: "¿Qué le han comentado?" // Si la anterior es afirmativa
}

export const PropiedadIntelectual = {
    SiNo1: "¿Está registrada la patente o la marca?",
    Text1: "¿Qué quiere conseguir? ¿Registrar la marca?",
    SiNo2: "¿Alguien más la está usando?",
    Text2: "¿Quién la está usando y como se ha enterado?"
}

// SIN SUBESPECIALIDAD
export const ViciosOcultos = {
    SiNo1: "¿Había un contrato de compraventa?",
    Text1: "¿A quién le compró el coche? ¿A un particular o a una empresa?",
    SiNo2: "¿Tiene recibos?",
    Text2: "¿Cuándo salieron los vicios?",
    Text3: "¿Cuándo se las comunicó al vendedor?"
}