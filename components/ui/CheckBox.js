import styled from '@emotion/styled';

export const InputCheckBox = styled.input`
    border: 1px solid #e1e1e1;
    margin-left: 3rem;
    margin-top:1rem;
    padding: 1rem;
    min-width: 25px;
    height:25px;
`;

export const CheckBoxLabel = styled.label`
    padding: 1rem;
    min-width: 50px;
    height:48px;
`;
