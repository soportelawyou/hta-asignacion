import styled from '@emotion/styled';

export const BotonEliminar = styled.button`
border-radius:1rem;
margin-top: 0.25rem;
margin-bottom:0.25rem;
margin-left:1rem;
font-size: 1rem;
`;