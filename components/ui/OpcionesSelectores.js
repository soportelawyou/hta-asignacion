
export const provincias = [
    { value: 'Álava', label: 'Álava' },
    { value: 'Albacete', label: 'Albacete' },
    { value: 'Alicante', label: 'Alicante' },
    { value: 'Almería', label: 'Almería' },
    { value: 'Asturias', label: 'Asturias' },
    { value: 'Ávila', label: 'Ávila' },
    { value: 'Badajoz', label: 'Badajoz' },
    { value: 'Barcelona', label: 'Barcelona' },
    { value: 'Burgos', label: 'Burgos' },
    { value: 'Cáceres', label: 'Cáceres' },
    { value: 'Cádiz', label: 'Cádiz' },
    { value: 'Cantabria', label: 'Cantabria' },
    { value: 'Castellón', label: 'Castellón' },
    { value: 'Ceuta', label: 'Ceuta' },
    { value: 'Ciudad Real', label: 'Ciudad Real' },
    { value: 'Córdoba', label: 'Córdoba' },
    { value: 'A Coruña', label: 'A Coruña' },
    { value: 'Cuenca', label: 'Cuenca' },
    { value: 'Girona', label: 'Girona' },
    { value: 'Granada', label: 'Granada' },
    { value: 'Guadalajara', label: 'Guadalajara' },
    { value: 'Guipúzcoa', label: 'Guipúzcoa' },
    { value: 'Huelva', label: 'Huelva' },
    { value: 'Huesca', label: 'Huesca' },
    { value: 'Baleares', label: 'Baleares' },
    { value: 'Jaén', label: 'Jaén' },
    { value: 'León', label: 'León' },
    { value: 'Lleida', label: 'Lleida' },
    { value: 'Lugo', label: 'Lugo' },
    { value: 'Madrid', label: 'Madrid' },
    { value: 'Málaga', label: 'Málaga' },
    { value: 'Melilla', label: 'Melilla' },
    { value: 'Murcia', label: 'Murcia' },
    { value: 'Navarra', label: 'Navarra' },
    { value: 'Ourense', label: 'Ourense' },
    { value: 'Palencia', label: 'Palencia' },
    { value: 'Las Palmas', label: 'Las Palmas' },
    { value: 'Pontevedra', label: 'Pontevedra' },
    { value: 'La Rioja', label: 'La Rioja' },
    { value: 'Salamanca', label: 'Salamanca' },
    { value: 'Segovia', label: 'Segovia' },
    { value: 'Sevilla', label: 'Sevilla' },
    { value: 'Soria', label: 'Soria' },
    { value: 'Tarragona', label: 'Tarragona' },
    { value: 'Santa Cruz de Tenerife', label: 'Santa Cruz de Tenerife' },
    { value: 'Teruel', label: 'Teruel' },
    { value: 'Toledo', label: 'Toledo' },
    { value: 'Valencia', label: 'Valencia' },
    { value: 'Valladolid', label: 'Valladolid' },
    { value: 'Vizcaya', label: 'Vizcaya' },
    { value: 'Zamora', label: 'Zamora' },
    { value: 'Zaragoza', label: 'Zaragoza' },
];

export const tipos = [
    { value: 'Asociado', label: 'Asociado' },
    { value: 'Asociado Plus', label: 'Asociado Plus' },
    { value: 'Colaborador', label: 'Colaborador' },
    { value: 'Socio A', label: 'Socio A' },
    { value: 'Socio AC', label: 'Socio AC' },
    { value: 'Socio C', label: 'Socio C' },
    { value: 'Socio F', label: 'Socio F' },
    { value: 'Smarin', label: 'Smarin' },
    { value: 'Lawyou', label: 'Lawyou' },
    { value: 'Smarin', label: 'Smarin' },
    { value: 'Cliente General', label: 'Cliente General'},
    { value: 'Prescriptor Maresila', label: 'Prescriptor Maresila' },
    { value: 'Prescriptor Massini', label: 'Prescriptor Massini' },
    { value: 'maiolegal', label: 'maiolegal' }
    
]

export const comunidades = [

    { value: 'Andalucía', label: 'Andalucía' },
    { value: 'Aragón', label: 'Aragón' },
    { value: 'Asturias', label: 'Asturias' },
    { value: 'Cantabria', label: 'Cantabria' },
    { value: 'Castilla la Mancha', label: 'Castilla la Mancha' },
    { value: 'Castilla León', label: 'Castilla León' },
    { value: 'Cataluña', label: 'Cataluña' },
    { value: 'Ceuta', label: 'Ceuta' },
    { value: 'Extremadura', label: 'Extremadura' },
    { value: 'Galicia', label: 'Galicia' },
    { value: 'Baleares', label: 'Baleares' },
    { value: 'Canarias', label: 'Canarias' },
    { value: 'La Rioja', label: 'La Rioja' },
    { value: 'Madrid', label: 'Madrid' },
    { value: 'Melilla', label: 'Melilla' },
    { value: 'Murcia', label: 'Murcia' },
    { value: 'Navarra', label: 'Navarra' },
    { value: 'País Vasco', label: 'País Vasco' },
    { value: 'Valencia', label: 'Valencia' }
]



export const estados = [
    { value: "Cita Aceptada", label: "Cita Aceptada" },
    { value: "Cita Gestionada", label: "Cita Gestionada" },
    { value: "Presupuesto Aceptado", label: "Presupuesto Aceptado" },
    { value: "Presupuesto Gestionado", label: "Presupuesto Gestionado" },
    { value: "Pendiente Asignar", label: "Pendiente de Asignar" },
    { value: "No Contestado", label: "No Contestado" },
    { value: "Rechazada por Cliente", label: "Rechazada por Cliente" },
    { value: "Rechazada por Abogado", label: "Rechazada por Abogado" },
]

export const comunidadesConProvincias = [
    { com: 'Andalucía', prov: ['Almería', 'Córdoba', 'Granada', 'Huelva', 'Jaén', 'Málaga', 'Sevilla', 'Cádiz'] },
    { com: 'Aragón', prov: ['Huesca', 'Teruel', 'Zaragoza'] },
    { com: 'Asturias', prov: ['Asturias'] },
    { com: 'Cantabria', prov: ['Cantabria'] },
    { com: 'Castilla la Mancha', prov: ['Albacete', 'Ciudad Real', 'Cuenca', 'Guadalajara', 'Toledo'] },
    { com: 'Castilla León', prov: ['Ávila', 'Burgos', 'León', 'Palencia', 'Salamanca', 'Segovia', 'Soria', 'Valladolid', 'Zamora'] },
    { com: 'Cataluña', prov: ['Barcelona', 'Girona', 'Lleida', 'Tarragona'] },
    { com: 'Ceuta', prov: ['Ceuta'] },
    { com: 'Extremadura', prov: ['Badajoz', 'Cáceres'] },
    { com: 'Galicia', prov: ['A Coruña', 'Lugo', 'Ourense', 'Pontevedra'] },
    { com: 'Baleares', prov: ['Baleares'] },
    { com: 'Canarias', prov: ['Las Palmas', 'Santa Cruz de Tenerife'] },
    { com: 'La Rioja', prov: ['La Rioja'] },
    { com: 'Madrid', prov: ['Madrid'] },
    { com: 'Melilla', prov: ['Melilla'] },
    { com: 'Murcia', prov: ['Murcia'] },
    { com: 'Navarra', prov: ['Navarra'] },
    { com: 'País Vasco', prov: ['Álava', 'Guipuzcoa', 'Vizcaya'] },
    { com: 'Valencia', prov: ['Alicante', 'Castellón', 'Valencia'] }
]

export const motivosRechazoAbogado = [
    {value: "No es su especialidad. Desconocimiento.", label: "No es su especialidad. Desconocimiento."},
    {value: "Exceso de trabajo", label: "Exceso de trabajo"},
    {value: "Motivos personales", label: "Motivos personales"},
    {value: "Objección de conciencia", label: "Objección de conciencia"},
    {value: "Otro", label: "Otro"},
]

export const motivosRechazoCliente = [
    {value: "Datos de origen erróneos", label: "Datos de origen erróneos"},
    {value: "Ya tiene abogado", label: "Ya tiene abogado"},
    {value: "Servicio rechazado por precio", label: "Servicio rechazado por precio"},
    {value: "Inviabilidad del asunto", label: "Inviabilidad del asunto"},
    {value: "Servicio rechazado, no sabemos el motivo", label: "Servicio rechazado, no sabemos el motivo"},
    {value: "Pasado directo abogad@ y perdido", label: "Pasado directo abogad@ y perdido"},
    {value: "Otro", label: "Otro"},
]

export const tipoterceros = [
    {value: "Procurador" , label: "Procurador"},
    {value:"Peritos", label: "Peritos"}
]

export const actividad = [
    {value: "Activo" , label: "Activo"},
    {value:"Inactivo", label: "Inactivo"}
]

export const roles = [
    {value: "Estandar" , label: "Estandar"},
    {value:"Administrador", label: "Administrador"},
    {value:"superAdmin", label: "superAdmin"}
]

export const idiomas = [
    {value: "Español" , label: "Español"},
    {value:"Ingles", label: "Ingles"},
    {value:"Frances", label: "Frances"},
    {value:"Aleman", label: "Aleman"},
    {value:"Euskera", label: "Euskera"},
    {value:"Catalan", label: "Catalan"}
]

export const tipoPreguntas = [
    { value: 'Texto', label: 'Texto' },
    { value: 'Desplegable', label: 'Desplegable' },
    { value: 'Numeros', label: 'Numeros' },
    { value: 'Fecha', label: 'Fecha' },
    { value: 'Si/No', label: 'Si/No' },
   
]

//Especialidades

export const especialidades = [
    { value: "Administrativo", label: "Administrativo" },
    { value: "Civil", label: "Civil" },
    { value: 'Penal', label: 'Penal' },
    { value: 'Comunitario', label: 'Comunitario' },
    { value: 'Compliance', label: 'Compliance' },
    { value: 'Constitucional', label: 'Constitucional' },
    { value: 'Bancario', label: 'Bancario' },
    { value: 'Mercantil', label: 'Mercantil' },
    { value: 'Fiscal_y_Tributario', label: 'Fiscal_y_Tributario' },
    { value: 'Laboral', label: 'Laboral' },
    { value: 'Societario', label: 'Societario' },
    { value: 'Extranjeria', label: 'Extranjeria' },
    { value: 'Internacional', label: 'Internacional' },
    { value: 'Nuevas_Tecnologias', label: 'Nuevas_Tecnologias' },


]

//SubEspecialidades

export const Civil = [
    { value: 'Matrimonio', label: 'Matrimonio' },
    { value: 'Familia', label: 'Familia' },
    { value: 'Tutela y Acogimiento', label: 'Tutela y Acogimiento' },
    { value: 'Proteccion Juridica Del Mayor', label: 'Proteccion Juridica Del Mayor' },
    { value: 'Division de Bienes Comunes', label: 'Division de Bienes Comunes' },
    { value: 'Herencia y Sucesiones', label: 'Herencia y Sucesiones' },
    { value: 'Reclamaciones', label: 'Reclamaciones' },
    { value: 'Propiedad Horizontal', label: 'Propiedad Horizontal' },
    { value: 'Contratos (Civil)', label: 'Contratos (Civil)' },
    { value: 'Monitorios', label: 'Monitorios' },
    { value: 'Ejecucion de titulos judiciales', label: 'Ejecucion de titulos judiciales' },
    { value: 'Discapacidad', label: 'Discapacidad' },
    { value: 'Propiedad Intelectual', label: 'Propiedad Intelectual' },
    { value: 'Propiedad Industrial', label: 'Propiedad Industrial' },
    { value: 'Embargo', label: 'Embargo' },
    { value: 'Subastas', label: 'Subastas' },
    { value: 'Consumidores y Usuarios', label: 'Consumidores y Usuarios' },
    { value: 'Trafico (Civil)', label: 'Trafico (Civil)' },
    { value: 'Accidentes de Origen no trafico', label: 'Accidentes de Origen no Trafico' },
    { value: 'Arbitraje y Mediacion', label: 'Arbitraje y Mediacion' },
    { value: 'Incapacidad Civil', label: 'incapacidad Civil' },
    { value: 'Derecho de honor e intimidad', label: 'Derecho de honor e intimidad' },
    { value: 'Derecho Vasco', label: 'Derecho Vasco' },
    { value: 'Derecho de Imagen', label: 'Derecho de Imagen' },
    { value: 'Derecho Real', label: 'Derecho Real' },
    { value: 'Seguros', label: 'Seguros' },
    { value: 'Responsabilidad Profesional', label: 'Responsabilidad Profesional' },
    { value: 'Negligencia Medica Civil', label: 'Negligencia Medica Civil' },
    { value: 'Procesos Administrativos', label: 'Procesos Administrativos' },
    { value: 'Procesos Judiciales', label: 'Procesos Judiciales' },
    { value: 'Derecho Agrario', label: 'Derecho Agrario' },
    { value: 'Derecho Deportivo (Civil)', label: 'Derecho Deportivo (Civil)' },
    { value: 'Derecho Militar', label: 'Derecho Militar' },
    { value: 'Derecho Maritimo', label: 'Derecho Maritimo' },
    { value: 'Derecho Medio Ambiental', label: 'Derecho Medio Ambiental' },
    { value: 'Derecho de Telecomunicaciones', label: 'Derecho de Telecomunicaciones' },
    { value: 'Derecho de Transporte', label: 'Derecho de Transporte' },
    { value: 'Demandas', label: 'Demandas' },
    { value: 'Procesal Civil', label: 'Procesal Civil' }
]

export const Penal = [
    { value: 'Trafico (Penal)', label: 'Trafico (Penal)' },
    { value: 'Menores', label: 'Menores' },
    { value: 'Violencia de Genero', label: 'Violencia de Genero' },
    { value: 'Economico', label: 'Economico' },
    { value: 'Delitos y Faltas', label: 'Delitor y Faltas' },
    { value: 'Responsabilidad Penal de Empresa', label: 'Responsabilidad Penal de Empresa' },
    { value: 'Amenazas', label: 'Amenazas' },
    { value: 'Asociaciones Cannabicas (Penal)', label: 'Asociaciones Cannabicas (Penal)' },
    { value: 'Delitos Fiscal', label: 'Delitos Fiscal' },
    { value: 'Aprobacion Indebida', label: 'Aprobacion Indebida' },
    { value: 'Asistencia a Detenidos', label: 'Asistencia a Detenidos' },
    { value: 'Asistencia a Juicio', label: 'Asistencia a Juicio' },
    { value: 'Derecho Educativo Penal', label: 'Derecho Educativo Penal' },
    { value: 'Derecho Penitenciario', label: 'Derecho Penitenciario' },
    { value: 'Derecho Penal Ordinario', label: 'Derecho Penal Ordinario' },
    { value: 'Procedimientos Concursales (Penal)', label: 'Procedimientos Concursales (Penal)' },
    { value: 'Derecho Penal Especial', label: 'Derecho Penal Especial' },
    { value: 'Procedimiento Abreviado', label: 'Procedimiento Abreviado' },
    { value: 'Procesal Penal', label: 'Procesal Penal' },
    { value: 'Drogas', label: 'Drogas'},
    { value: 'Usurpacion', label: 'Usurpacion' },
    { value: 'Alcoholemias', label: 'Alcoholemias' },
    { value: 'Imputaciones', label: 'Imputaciones' },
    { value: 'Compliance Penal (Penal)', label: 'Compliance Penal (Penal)' },
    { value: 'Querellas', label: 'Querellas' },
    { value: 'Derecho Militar Penal', label: 'Derecho Militar Penal' },
    { value: 'Derecho Maritimo penal', label: 'Derecho Maritimo penal' },
    { value: 'Derecho Ambiental Penal', label: 'Derecho Ambiental Penal' },
    { value: 'Derecho Deportivo (Penal)', label: 'Derecho Deportivo (Penal)' },
    { value: 'Derecho Transporte Penal', label: 'Derecho Transporte Penal' }
]

export const Administrativo = [
    { value: 'Multas', label: 'Multas'},
    { value: 'Contencioso Administrativo', label: 'Contencioso Administrativo'},
    { value: 'Urbanismo', label: 'Urbanismo'},
    { value: 'Licencias y Permisos', label: 'Licencias y Permisos'},
    { value: 'Derecho Turistico', label: 'Derecho Turistico'},
    { value: 'Derecho Publico General', label: 'Derecho Publico General'},
    { value: 'Derecho Educativo', label: 'Derecho Educativo'},
    { value: 'Empleo Publico', label: 'Empleo Publico'},
    { value: 'Expropiaciones', label: 'Expropiaciones'},
    { value: 'Informes Periciales', label: 'Informes Periciales'},
    { value: 'Plusvalias', label: 'Plusvalias'},
    { value: 'Procedimiento de Incorporacion', label: 'Procedimiento de Incorporacion'},
    { value: 'Procedimiento administrativo sancionador', label: 'Procedimiento administrativo sancionador'},
    { value: 'Procedimiento administrativo tributarios ', label: 'Procedimiento administrativo tributarios '},
    { value: 'Proteccion de Datos', label: 'Proteccion de Datos'},
    { value: 'Reclamaciones a la administracion publica', label: 'Reclamaciones a la administracion publica'},
    { value: 'Responsabilidad Contractual', label: 'Responsabilidad Contractual'},
    { value: 'Responsabilidad Patrimonial', label: 'Responsabilidad Patrimonial'},
    { value: 'Negligencia Medica Administrativo', label: 'Negligencia Medica Administrativo'},
    { value: 'Derecho Militar Administrativo', label: 'Derecho Militar Administrativo'},
    { value: 'Entidades Publicas', label: 'Entidades Publicas'},
    { value: 'Asociaciones Cannabicas (Administrativo)', label: 'Asociaciones Cannabicas (Administrativo)'},
    { value: 'Subvenciones', label: 'Subvenciones'},
    { value: 'Derecho Maritimo Administrativo', label: 'Derecho Maritimo Administrativo' },
    { value: 'Derecho Ambiental Administrativo', label: 'Derecho Ambiental Administrativo' },
    { value: 'Derecho Deportivo Administrativo', label: 'Derecho Deportivo Administrativo' },
    { value: 'Derecho Transporte Administrativo', label: 'Derecho Transporte Administrativo' },
    { value: 'Derecho de Telecomunicaciones Administrativo', label: 'Derecho de Telecomunicaciones Administrativo'}
]

export const Comunitario = [
    { value: 'Derecho Europeo', label: 'Derecho Europeo'},
]

export const Compliance = [
    { value: 'Compliance Penal (Compliance)', label: 'Compliance Penal (Compliance)'},
    { value: 'Penal Juristas', label: 'Penal Juristas'},
    { value: 'Adquisiciones', label: 'Adquisiciones'},
    { value: 'Administrativo Concursal', label: 'Administrativo Concursal'}
]

export const Constitucional = [
    { value: 'Derecho a la Educacion', label: 'Derecho a la Educacion'},
    { value: 'Derechos Fundamental', label: 'Derechos Fundamentales'},
    { value: 'Recurso de Amparo', label: 'Recurso de Amparo'}
]

export const Bancario = [
    { value: 'Contratacion de Productos Financieros', label: 'Contratacion de Productos Financieros'},
    { value: 'Ley de Segunda Oportunidad', label: 'Ley de Segunda Oportunidad'},
    { value: 'Clausulas Abusivas', label: 'Clausulas Abusivas'},
    { value: 'Golden Divisa', label: 'Golden Divisa'},
    { value: 'Recuperaciones', label: 'Recuperaciones'}
]

export const Mercantil = [
    { value: 'Mediacion Mercantil', label: 'Mediacion Mercantil'},
    { value: 'Compraventa', label: 'Compraventa'},
    { value: 'Procesos Cambiarios', label: 'Procesos Cambiarios'},
    { value: 'Procesal Mercantil', label: 'Procesal Mercantil'}
]

export const Fiscal_y_Tributario = [
    { value: 'Asesoria Fiscal', label: 'Asesoria Fiscal'},
    { value: 'Asesoria Contable', label: 'Asesoria Contable'},
    { value: 'IVI', label: 'IVI'},
    { value: 'IS', label: 'IS'},
    { value: 'IRPF', label: 'IRPF'},
    { value: 'Gestion Tributaria', label: 'Gestion Tributaria'},
    { value: 'Derechos Financieros', label: 'Derechos Financieros'},
    { value: 'Inspecciones Fiscales', label: 'Inspecciones Fiscales'},
    { value: 'Tributacion Estatal', label: 'Tributacion Estatal'},
    { value: 'Tributacion Local', label: 'Tributacion Local'},
    { value: 'Tributacion Autonomico', label: 'Tributacion Autonomico'}
]

export const Laboral =[
    { value: 'Seguridad Social', label: 'Seguridad Social'},
    { value: 'Educativo Laboral', label: 'Educativo Laboral'},
    { value: 'Incapacidades', label: 'Incapacidades'},
    { value: 'Altas y Bajas Medicas', label: 'Altas y Bajas Medicas'},
    { value: 'Jubilacion', label: 'Jubilacion'},
    { value: 'Excedencia', label: 'Excedencia'},
    { value: 'Contratos (Laboral)', label: 'Contratos (Laboral)'},
    { value: 'Condiciones Laborales', label: 'Condiciones Laborales'},
    { value: 'Despidos', label: 'Despidos'},
    { value: 'Asesoria', label: 'Asesoria'},
    { value: 'Cotizacion', label: 'Cotizacion'},
    { value: 'Accidentes', label: 'Accidentes'},
    { value: 'ERE', label: 'ERE'},
    { value: 'ERTE', label: 'ERTE'},
    { value: 'Fiscal Laboral', label: 'Fiscal Laboral'}
    
]

export const Societario = [
    { value: 'Reunion de Juntas', label: 'Reunion de Juntas'},
    { value: 'Modificaciones de Estatutos', label: 'Modificaciones de Estatutos'},
    { value: 'Procedimientos Concursales (Societario)', label: 'Procedimientos Concursales (Societario)'},
    { value: 'Asesoramiento de PYMES y Autonomos', label: 'Asesoramiento de PYMES y Autonomos'},
    { value: 'Perito Administrado Concursal', label: 'Perito Administrado Concursal'}
]

export const Extranjeria = [
    { value: 'Expediente de Expulsiones', label: 'Expediente de Expulsiones'},
    { value: 'Global Mobility', label: 'Global Mobility'},
    { value: 'Tramites Judiciales', label: 'Tramites Judiciales'},
    { value: 'Asesoramiento', label: 'Asesoramiento'},
    { value: 'Nacionalidad y Residencia', label: 'Nacionalidad y Residencia'}
]

export const Internacional = [
    { value: 'Derecho Internacional Publico', label: 'Derecho Internacional Publico'},
    { value: 'Derecho Internacional Privado', label: 'Derecho Internacional Privado'},
    { value: 'Contratacion Internacional', label: 'Contratacion Internacional'},
    { value: 'Tributacion Internacional', label: 'Tributacion Internacional'}
]

export const Nuevas_Tecnologias = [
    { value: 'Marketing Digital', label: 'Marketing Digital'},
    { value: 'Redes Sociales', label: 'Redes Sociales'},
    { value: 'RGPF WEB', label: 'RGPF WEB'},
    { value: 'E-Commerce', label: 'E-Commerce'}
]

export const aTodo = [
    { value: 'Matrimonio', label: 'Matrimonio' },
    { value: 'Familia', label: 'Familia' },
    { value: 'Tutela y Acogimiento', label: 'Tutela y Acogimiento' },
    { value: 'Proteccion Juridica Del Mayor', label: 'Proteccion Juridica Del Mayor' },
    { value: 'Division de Bienes Comunes', label: 'Division de Bienes Comunes' },
    { value: 'Herencia y Sucesiones', label: 'Herencia y Sucesiones' },
    { value: 'Reclamaciones', label: 'Reclamaciones' },
    { value: 'Propiedad Horizontal', label: 'Propiedad Horizontal' },
    { value: 'Contratos', label: 'Contratos' },
    { value: 'Monitorios', label: 'Monitorios' },
    { value: 'Ejecucion de titulos judiciales', label: 'Ejecucion de titulos judiciales' },
    { value: 'Discapacidad', label: 'Discapacidad' },
    { value: 'Propiedad Intelectual', label: 'Propiedad Intelectual' },
    { value: 'Propiedad Industrial', label: 'Propiedad Industrial' },
    { value: 'Embargo', label: 'Embargo' },
    { value: 'Subastas', label: 'Subastas' },
    { value: 'Consumidores y Usuarios', label: 'Consumidores y Usuarios' },
    { value: 'Trafico', label: 'Trafico' },
    { value: 'Accidentes de Origen no trafico', label: 'Accidentes de Origen no Trafico' },
    { value: 'Arbitraje y Mediacion', label: 'Arbitraje y Mediacion' },
    { value: 'Incapacidad Civil', label: 'incapacidad Civil' },
    { value: 'Derecho de honor e intimidad', label: 'Derecho de honor e intimidad' },
    { value: 'Derecho Vasco', label: 'Derecho Vasco' },
    { value: 'Derecho de Imagen', label: 'Derecho de Imagen' },
    { value: 'Derecho Real', label: 'Derecho Real' },
    { value: 'Seguros', label: 'Seguros' },
    { value: 'Responsabilidad Profesional', label: 'Responsabilidad Profesional' },
    { value: 'Negligencia Medica Civil', label: 'Negligencia Medica Civil' },
    { value: 'Procesos Administrativos', label: 'Procesos Administrativos' },
    { value: 'Procesos Judiciales', label: 'Procesos Judiciales' },
    { value: 'Derecho Agrario', label: 'Derecho Agrario' },
    { value: 'Derecho Deportivo', label: 'Derecho Deportivo' },
    { value: 'Derecho Militar', label: 'Derecho Militar' },
    { value: 'Derecho Maritimo', label: 'Derecho Maritimo' },
    { value: 'Derecho Medio Ambiental', label: 'Derecho Medio Ambiental' },
    { value: 'Derecho de Telecomunicaciones', label: 'Derecho de Telecomunicaciones' },
    { value: 'Derecho de Transporte', label: 'Derecho de Transporte' },
    { value: 'Demandas', label: 'Demandas' },
    { value: 'Procesal Civil', label: 'Procesal Civil' },
    { value: 'Trafico', label: 'Trafico' },
    { value: 'Menores', label: 'Menores' },
    { value: 'Violencia de Genero', label: 'Violencia de Genero' },
    { value: 'Economico', label: 'Economico' },
    { value: 'Delitos y Faltas', label: 'Delitor y Faltas' },
    { value: 'Responsabilidad Penal de Empresa', label: 'Responsabilidad Penal de Empresa' },
    { value: 'Amenazas', label: 'Amenazas' },
    { value: 'Asociaciones Cannabicas', label: 'Asociaciones Cannabicas' },
    { value: 'Delitos Fiscal', label: 'Delitos Fiscal' },
    { value: 'Aprobacion Indebida', label: 'Aprobacion Indebida' },
    { value: 'Asistencia a Detenidos', label: 'Asistencia a Detenidos' },
    { value: 'Asistencia a Juicio', label: 'Asistencia a Juicio' },
    { value: 'Derecho Educativo Penal', label: 'Derecho Educativo Penal' },
    { value: 'Derecho Penitenciario', label: 'Derecho Penitenciario' },
    { value: 'Derecho Penal Ordinario', label: 'Derecho Penal Ordinario' },
    { value: 'Procedimientos Concursales', label: 'Procedimientos Concursales' },
    { value: 'Derecho Penal Especial', label: 'Derecho Penal Especial' },
    { value: 'Procedimiento Abreviado', label: 'Procedimiento Abreviado' },
    { value: 'Procesal Penal', label: 'Procesal Penal' },
    { value: 'Drogas', label: 'Drogas'},
    { value: 'Usurpacion', label: 'Usurpacion' },
    { value: 'Alcoholemias', label: 'Alcoholemias' },
    { value: 'Imputaciones', label: 'Imputaciones' },
    { value: 'Compliance Penal', label: 'Compliance Penal' },
    { value: 'Querellas', label: 'Querellas' },
    { value: 'Derecho Militar Penal', label: 'Derecho Militar Penal' },
    { value: 'Derecho Maritimo penal', label: 'Derecho Maritimo penal' },
    { value: 'Derecho Ambiental Penal', label: 'Derecho Ambiental Penal' },
    { value: 'Derecho Deportivo', label: 'Derecho Deportivo' },
    { value: 'Derecho Transporte Penal', label: 'Derecho Transporte Penal' },
    { value: 'Multas', label: 'Multas'},
    { value: 'Contencioso Administrativo', label: 'Contencioso Administrativo'},
    { value: 'Urbanismo', label: 'Urbanismo'},
    { value: 'Licencias y Permisos', label: 'Licencias y Permisos'},
    { value: 'Derecho Turistico', label: 'Derecho Turistico'},
    { value: 'Derecho Publico General', label: 'Derecho Publico General'},
    { value: 'Derecho Educativo', label: 'Derecho Educativo'},
    { value: 'Empleo Publico', label: 'Empleo Publico'},
    { value: 'Expropiaciones', label: 'Expropiaciones'},
    { value: 'Informes Periciales', label: 'Informes Periciales'},
    { value: 'Plusvalias', label: 'Plusvalias'},
    { value: 'Procedimiento de Incorporacion', label: 'Procedimiento de Incorporacion'},
    { value: 'Procedimiento administrativo sancionador', label: 'Procedimiento administrativo sancionador'},
    { value: 'Procedimiento administrativo tributarios ', label: 'Procedimiento administrativo tributarios '},
    { value: 'Proteccion de Datos', label: 'Proteccion de Datos'},
    { value: 'Reclamaciones a la administracion publica', label: 'Reclamaciones a la administracion publica'},
    { value: 'Responsabilidad Contractual', label: 'Responsabilidad Contractual'},
    { value: 'Responsabilidad Patrimonial', label: 'Responsabilidad Patrimonial'},
    { value: 'Negligencia Medica Administrativo', label: 'Negligencia Medica Administrativo'},
    { value: 'Derecho Militar Administrativo', label: 'Derecho Militar Administrativo'},
    { value: 'Entidades Publicas', label: 'Entidades Publicas'},
    { value: 'Asociaciones Cannabicas', label: 'Asociaciones Cannabicas'},
    { value: 'Subvenciones', label: 'Subvenciones'},
    { value: 'Derecho Maritimo Administrativo', label: 'Derecho Maritimo Administrativo' },
    { value: 'Derecho Ambiental Administrativo', label: 'Derecho Ambiental Administrativo' },
    { value: 'Derecho Deportivo Administrativo', label: 'Derecho Deportivo Administrativo' },
    { value: 'Derecho Transporte Administrativo', label: 'Derecho Transporte Administrativo' },
    { value: 'Derecho de Telecomunicaciones Administrativo', label: 'Derecho de Telecomunicaciones Administrativo'},
    { value: 'Derecho Europeo', label: 'Derecho Europeo'},
    { value: 'Compliance Penal', label: 'Compliance Penal'},
    { value: 'Penal Juristas', label: 'Penal Juristas'},
    { value: 'Adquisiciones', label: 'Adquisiciones'},
    { value: 'Administrativo Concursal', label: 'Administrativo Concursal'},
    { value: 'Derecho a la Educacion', label: 'Derecho a la Educacion'},
    { value: 'Derechos Fundamental', label: 'Derechos Fundamentales'},
    { value: 'Recurso de Amparo', label: 'Recurso de Amparo'},
    { value: 'Contratacion de Productos Financieros', label: 'Contratacion de Productos Financieros'},
    { value: 'Ley de Segunda Oportunidad', label: 'Ley de Segunda Oportunidad'},
    { value: 'Clausulas Abusivas', label: 'Clausulas Abusivas'},
    { value: 'Golden Divisa', label: 'Golden Divisa'},
    { value: 'Recuperaciones', label: 'Recuperaciones'},
    { value: 'Mediacion Mercantil', label: 'Mediacion Mercantil'},
    { value: 'Compraventa', label: 'Compraventa'},
    { value: 'Procesos Cambiarios', label: 'Procesos Cambiarios'},
    { value: 'Procesal Mercantil', label: 'Procesal Mercantil'},
    { value: 'Asesoria Fiscal', label: 'Asesoria Fiscal'},
    { value: 'Asesoria Contable', label: 'Asesoria Contable'},
    { value: 'IVI', label: 'IVI'},
    { value: 'IS', label: 'IS'},
    { value: 'IRPF', label: 'IRPF'},
    { value: 'Gestion Tributaria', label: 'Gestion Tributaria'},
    { value: 'Derechos Financieros', label: 'Derechos Financieros'},
    { value: 'Inspecciones Fiscales', label: 'Inspecciones Fiscales'},
    { value: 'Tributacion Estatal', label: 'Tributacion Estatal'},
    { value: 'Tributacion Local', label: 'Tributacion Local'},
    { value: 'Tributacion Autonomico', label: 'Tributacion Autonomico'},
    { value: 'Seguridad Social', label: 'Seguridad Social'},
    { value: 'Educativo Laboral', label: 'Educativo Laboral'},
    { value: 'Incapacidades', label: 'Incapacidades'},
    { value: 'Altas y Bajas Medicas', label: 'Altas y Bajas Medicas'},
    { value: 'Jubilacion', label: 'Jubilacion'},
    { value: 'Excedencia', label: 'Excedencia'},
    { value: 'Contratos', label: 'Contratos'},
    { value: 'Condiciones Laborales', label: 'Condiciones Laborales'},
    { value: 'Despidos', label: 'Despidos'},
    { value: 'Asesoria', label: 'Asesoria'},
    { value: 'Cotizacion', label: 'Cotizacion'},
    { value: 'Accidentes', label: 'Accidentes'},
    { value: 'ERE', label: 'ERE'},
    { value: 'ERTE', label: 'ERTE'},
    { value: 'Fiscal Laboral', label: 'Fiscal Laboral'},
    { value: 'Reunion de Juntas', label: 'Reunion de Juntas'},
    { value: 'Modificaciones de Estatutos', label: 'Modificaciones de Estatutos'},
    { value: 'Procedimientos Concursales', label: 'Procedimientos Concursales'},
    { value: 'Asesoramiento de PYMES y Autonomos', label: 'Asesoramiento de PYMES y Autonomos'},
    { value: 'Perito Administrado Concursal', label: 'Perito Administrado Concursal'},
    { value: 'Expediente de Expulsiones', label: 'Expediente de Expulsiones'},
    { value: 'Global Mobility', label: 'Global Mobility'},
    { value: 'Tramites Judiciales', label: 'Tramites Judiciales'},
    { value: 'Asesoramiento', label: 'Asesoramiento'},
    { value: 'Nacionalidad y Residencia', label: 'Nacionalidad y Residencia'},
    { value: 'Derecho Internacional Publico', label: 'Derecho Internacional Publico'},
    { value: 'Derecho Internacional Privado', label: 'Derecho Internacional Privado'},
    { value: 'Contratacion Internacional', label: 'Contratacion Internacional'},
    { value: 'Tributacion Internacional', label: 'Tributacion Internacional'},
    { value: 'Marketing Digital', label: 'Marketing Digital'},
    { value: 'Redes Sociales', label: 'Redes Sociales'},
    { value: 'RGPF WEB', label: 'RGPF WEB'},
    { value: 'E-Commerce', label: 'E-Commerce'}
]

//Aplicativo

export const Tipo_Servicio = [
    { value: 'Propio', label: 'Propio' },
    { value: 'A Compartir', label: 'A Compartir' },
    { value: 'A Derivar', label: 'A Derivar' }
]

export const Servicio = [
    { value: 'Estrategia de Defensa', label: 'Estrategia de Defensa' },
    { value: 'Consulta General', label: 'Consulta General' },
    { value: 'Análisis del Caso', label: 'Análisis del Caso' },
    { value: 'Ejecución de Estrategia de Defensa', label: 'Ejecución de Estrategia de Defensa' }
]

export const Tipo = [
    { value: 'Cita', label: 'Cita' },
    { value: 'Presupuesto', label: 'Presupuesto' }

]

export const SiNo = [
    {value: 'Sí', label: 'Sí'},
    {value: 'No', label: 'No'}
]

// Canal y subcanal
export const Canal = [
    { value: 'ANA MARÍA SANCHEZ', label: 'ANA MARÍA SANCHEZ' },
    { value: 'LAWYOU PRESCRIPTOR', label: 'LAWYOU PRESCRIPTOR' },
    { value: 'ABOGADO.ORG', label: 'ABOGADO.ORG' },
    { value: 'SO', label: 'SO' },
    { value: 'JJ', label: 'JJ' },
    { value: 'ABBO-P', label: 'ABBO-P' },
    { value: 'LAWYOU BLOG', label: 'LAWYOU BLOG' },
    { value: 'CLUB LAWYOU', label: 'CLUB LAWYOU' },
    { value: 'EASY OFFER', label: 'EASY OFFER' },
    { value: 'PP', label: 'PP' },
    { value: 'ELA LY', label: 'ELA LY' },
    { value: 'LAWYOU CHAT', label: 'LAWYOU CHAT' },
    { value: 'STAR OF SERVICE', label: 'STAR OF SERVICE' },
    { value: 'VCITA LABORAL', label: 'VCITA LABORAL' },
    { value: 'LAWYOU B2B', label: 'LAWYOU B2B' },
    { value: 'CHATBOT SERVICEFORM', label: 'CHATBOT SERVICEFORM' },
    { value: 'LAWYOU WASSAP', label: 'LAWYOU WASSAP' },
    { value: 'FCB', label: 'FCB' },
    { value: 'ABBO-LY', label: 'ABBO-LY' },
    { value: 'ABBO HUGO', label: 'ABBO HUGO' },
    { value: 'SOCIO', label: 'SOCIO' },
    { value: 'SOCIALONE', label: 'SOCIALONE' },
    { value: 'VCITA AUDITORIA', label: 'VCITA AUDITORIA' },
    { value: 'LEXDIR', label: 'LEXDIR' },
    { value: 'CRONOSHARE', label: 'CRONOSHARE' },
    { value: 'ARI', label: 'ARI' },
    { value: 'DATAWORK', label: 'DATAWORK' },
    { value: 'WEBS ABOGADOS', label: 'WEBS ABOGADOS' },
    { value: 'LAWYOU', label: 'LAWYOU' },
]

export const SubCanal = [
    { value: 'FCB MULTAS COVID', label: 'FCB MULTAS COVID' },
    { value: 'FCB PLUSVALIA', label: 'FCB PLUSVALIA' },
    { value: 'BLOG ALCOHOLEMIA', label: 'BLOG ALCOHOLEMIA' },
    { value: 'WEB JAVIER SEGURA', label: 'WEB JAVIER SEGURA' },
    { value: 'BLOG NEGLIGENCIAS MEDICAS', label: 'BLOG NEGLIGENCIAS MEDICAS' },
    { value: 'FCB DIVORCIO', label: 'FCB DIVORCIO' },
    { value: 'ELA 2º OPORTUNIDAD', label: 'ELA 2º OPORTUNIDAD' },
    { value: 'BLOG FIBROMIALGIA', label: 'BLOG FIBROMIALGIA' },
    { value: 'FCB 2º OPORTUNIDAD', label: 'FCB 2º OPORTUNIDAD' },
    { value: 'BLOG MILITAR', label: 'BLOG MILITAR' },
    { value: 'FCB HERENCIA', label: 'FCB HERENCIA' },
    { value: 'FCB TARJETAS REVOLVING', label: 'FCB TARJETAS REVOLVING' },
    { value: 'B2B ADEGI', label: 'B2B ADEGI' },
    { value: 'B2B ACENOMA', label: 'B2B ACENOMA' },
    { value: 'Prescriptor Sandro', label: 'Prescriptor Sandro' },
    { value: 'BLOG PLUSVALIAS', label: 'BLOG PLUSVALIAS' },
    { value: 'FCB DESPIDOS', label: 'FCB DESPIDOS' },
    { value: 'Prescriptor MASSINI POINT', label: 'Prescriptor MASSINI POINT' },
    { value: 'BLOG IMPUGNAR ALTA MEDICA', label: 'BLOG IMPUGNAR ALTA MEDICA' },
    { value: 'Prescriptor ANA MARIA SANCHEZ', label: 'Prescriptor ANA MARIA SANCHEZ' },
]