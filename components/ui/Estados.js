import styled from '@emotion/styled';

export const PendienteAsignar = styled.span`
    padding: 0.4rem 0.6rem;
    background-color: #0054b5;
    border-radius: 1rem;
    color: ivory;
`;

export const Aceptado = styled.span`
    padding: 0.4rem 0.6rem;
    background-color: #0ba103;
    border-radius: 1rem;
    color: ivory;
`;

export const Rechazado = styled.span`
    padding: 0.4rem 0.6rem;
    background-color: #cc330c;
    border-radius: 1rem;
    color: ivory;
`;

export const NoContestado = styled.span`
    padding: 0.4rem 0.6rem;
    background-color: #cc330c;
    border-radius: 1rem;
    color: ivory;
`;

export const PendienteAceptar = styled.span`
    padding: 0.4rem 0.6rem;
    background-color: orange;
    border-radius: 1rem;
    color: ivory;
`;

export const Administrador = styled.span`
    padding: 0.4rem 0.6rem;
    background-color: #dba527;
    border-radius: 1rem;
    color: ivory;
`;