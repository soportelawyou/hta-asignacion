import styled from '@emotion/styled';

export const InputText = styled.input`
    border: 1px solid #e1e1e1;
    margin-left: 3rem;
    padding: 1rem;
    min-width: 300px;
    height:48px;
`;


export const InputTextOportunidades = styled.input`
    border: 1px solid #e1e1e1;
    padding: 1rem;
    min-width: 300px;
    height:48px;
`;

export const InputSubmit = styled.button`
    height: 3rem;
    width: 3rem;
    display: block;
    background-size: 3rem;
    background-image: url('/static/img/buscar.png');
    background-repeat: no-repeat;
    border: 1px solid #e1e1e1;
    /* position:absolute; */
    right: 1rem;
    top: 1px;
    background-color: #e9ecef;
    text-indent: -9999px;
    margin-top: 0.5rem;
    &:hover{
        cursor: pointer;
    }
`;

export const InputSubmitSugerencias = styled.button`
    height: 3rem;
    width: 3rem;
    display: block;
    background-size: 3rem;
    background-image: url('/static/img/buscar.png');
    background-repeat: no-repeat;
    border: 1px solid #e1e1e1;
    /* position:absolute; */
    right: 1rem;
    top: 1px;
    background-color: #e9ecef;
    text-indent: -9999px;
 
`;