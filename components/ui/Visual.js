import styled from "@emotion/styled";

export const Divp1 = styled.div`
margin-top: 4rem;
`;

export const DivScroll = styled.div`
overflow: scroll; 
  margin-top: 2rem;
`;

export const Divp2 = styled.div`
  margin-left: 2rem;
`;

export const Divp3 = styled.div`
  margin-left: 8rem;
  margin-top: -1.5rem;
`;
export const Divp4 = styled.div`
  margin-top: 4rem;
`;
export const Divp5 = styled.div`
  margin-bottom: 4rem;
`;

export const DivFloat = styled.div`
  position: fixed;
  width: 12rem;
`;

export const Label = styled.label`
  font-size: 1.5rem;
  font-weight: bold;
  margin-bottom: 0.25rem;

`;
export const LabelRojo = styled.label`
  font-size: 1.2rem;
  font-weight: bold;
  margin-bottom: 0.25rem;
  color:red;
`;
export const LabelVerde = styled.label`
  font-size: 1.2rem;
  font-weight: bold;
  margin-bottom: 0.25rem;
  color:green;
`;

export const Label3 = styled.label`
  font-weight: bold;
  margin-top: 3rem;
`;

export const Label4 = styled.label`
  font-weight: bold;
`;

export const Label1 = styled.label`
  font-size: 1.1vw;
  font-weight: bold;
  margin-top: 0.2rem;
  margin-bottom: 0.25rem;
  text-transform: uppercase;
  &:hover {
    cursor: pointer;
    background-color: lightgray;
  }
`;

export const Label1Seleccionado = styled.label`
  font-size: 1.2vw;
  font-weight: bold;
  margin-top: 3rem;
  margin-bottom: 0.25rem;
  text-transform: uppercase;
  color: green;
  &:hover {
    cursor: pointer;
    background-color: lightgray;
  }
`;

export const Label2 = styled.label`
  font-size: 1.1vw;
  font-weight: bold;
  margin-top: 0.2rem;
  margin-bottom: 0.25rem;
  text-transform: uppercase;
  &:hover {
    cursor: pointer;
    background-color: lightgray;
  }
`;
export const Label2Seleccionado = styled.label`
  font-size: 1.2vw;
  font-weight: bold;
  margin-top: 0.5rem;
  margin-bottom: 0.25rem;
  text-transform: uppercase;
  color: green;
  &:hover {
    cursor: pointer;
    background-color: lightgray;
  }
`;

export const Label2Bloqueado = styled.label`
  font-size: 1.2rem;
  font-weight: bold;
  margin-top: 0.5rem;
  margin-bottom: 0.25rem;
  color: red;
  text-transform: uppercase;
  &:hover {
    cursor: pointer;
    background-color: lightgray;
  }
`;

export const LabelBotones = styled.label`
  font-size: 1.25rem;
  font-weight: bold;
  margin-bottom: 0.25rem;
`;
export const Span1 = styled.span`
  font-size: 1.1rem;
  margin-bottom: 0.25rem;
`;
export const SpanActivo = styled.span`
  padding: 0.4rem 0.6rem;
  background-color: #0ba103;
  border-radius: 1rem;
  color: ivory;
`;

export const SpanInactivo = styled.span`
  padding: 0.4rem 0.6rem;
  background-color: #cc330c;
  border-radius: 1rem;
  color: ivory;
`;

export const BotonEditar = styled.button`
  border-radius: 1rem;
  margin-top: 0.25rem;
  margin-bottom: 0.25rem;
  margin-left: 1rem;
  font-size: 1rem;
  border: 1px solid grey;
  background-color: transparent;

  &:hover {
    /* background-color: #d19d02; */
    background-color: #f5f5f5;
    border: 1px solid grey;
  }
  &:focus {
    background-color: #f5f5f5;
    border: 1px solid grey;
  }
  &:disabled {
    background-color: #f5f5f5;
    border: 1px solid grey;
  }
`;

export const BotonEliminar = styled.button`
  border-radius: 1rem;
  margin-top: 0.25rem;
  margin-bottom: 0.25rem;
  margin-left: 1rem;
  font-size: 1rem;
  border: 1px solid grey;
  background-color: #fe0000;

  &:hover {
    /* background-color: #d19d02; */
    background-color: #bc0202;
    border: 1px solid grey;
  }
  &:focus {
    background-color: #bc0202;
    border: 1px solid grey;
  }
  &:disabled {
    background-color: #bc0202;
    border: 1px solid grey;
  }
`;

export const BotonTodo = styled.button`
  margin-top: 0.25rem;
  margin-bottom: 0.25rem;
  width: 100%;
  height: 100%;
  color: black;
  background-color: transparent;
  border: transparent;
  &:hover {
    /* background-color: #d19d02; */
    background-color: #f5f5f5;
  }
  &:focus {
    background-color: #f5f5f5;
  }
  &:disabled {
    background-color: #f5f5f5;
  }
`;

export const P1 = styled.p`
  margin-top: 2rem;
  font-size: 0.8rem;
`;


export const Filtro = styled.button`
  height: 1.5rem;
  width: 1.5rem;
  margin-left: 1rem;
  margin-bottom: 1rem;
  display: block;
  background-size: 1.5rem;
  background-image: url("/static/img/Filtro.png");
  background-repeat: no-repeat;
  border: 0px solid #e1e1e1;
  background-color: transparent;
  /* position:absolute; */
  right: 3rem;
  top: 3px;
  text-indent: 30px;
  margin-top: 0.5rem;
  &:hover {
    cursor: pointer;
    background-color: transparent;
  }
`;

export const BotonOportunidad1 = styled.button`
  margin-top: 2rem;
  margin-left: 1rem;
  font-size: 1rem;
  font-weight: bold;
  width: 100%;
  height: 100%;
  color: black;
  background-color: transparent;
  border: transparent;
  &:hover {
    /* background-color: #d19d02; */
    background-color: #f5f5f5;
  }
  &:focus {
    background-color: #f5f5f5;
  }
  &:disabled {
    background-color: #f5f5f5;
  }
`;

export const BotonOportunidad2 = styled.button`
  margin-top: 2rem;
  margin-left: 1rem;
  font-size: 1rem;
  width: 100%;
  height: 100%;
  font-weight: bold;
  color: black;
  background-color: transparent;
  border: transparent;
  &:hover {
    /* background-color: #d19d02; */
    background-color: #f5f5f5;
  }
  &:focus {
    background-color: #f5f5f5;
  }
  &:disabled {
    background-color: #f5f5f5;
  }
`;

export const BotonMas = styled.button `
  background-color: transparent;
  color: black;
  border: none;
  display: block;
  background-size: 1.5rem;
  background-image: url("/static/img/Cruz.png");
  background-repeat: no-repeat;
  text-indent:30px;
`;

export const LabelPregunta = styled.label`
  margin-top: 1rem;
  margin-left: 5rem;
  font-weight: bold;
  font-size: 1.2rem;
  margin-bottom: 2rem;
`;

export const LabelRespuesta = styled.label`
  margin-left: 2rem;
  font-size: 1rem;
  margin-bottom: 0.25rem;
`;

export const H2Alerta = styled.h2`
  color:red;
  font-style:bold;
`;
