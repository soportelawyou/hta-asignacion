import styled from '@emotion/styled';


export const ContenedorFormulario = styled.div`
    width: 500px;
    margin: 0 auto;
    justify-content: center;
    padding: 1rem;
    
    fieldset{
        /* border: 1px solid #e1e1e1; */
        
    }

    .form-group {
        display: flex;
        align-items: center;
        line-height: 2.5rem;
        font-size: 1rem;
        
        
        
    }
    .form-group label {
        flex:0 0 150px;
        display: inline;
        padding-top: 0.5rem;
        font-size: 1rem;
        
    }
    button{
        width: 100%;
        margin: 0 auto;
        font-size: 1rem;
        margin-top: 2rem;
    }
    .alert{
       padding:0.5rem 0.5rem;
       
    }
`;


export const ContenedorFormularioLg = styled.div`
    width: 100%;
    margin: 1rem auto;
    justify-content: center;
    padding: 1rem;
    font-size: 1rem;

    fieldset{
        border: 2px solid #e1e1e1;
        padding: 1rem 1rem 2rem 1rem;
        small{
            padding-left: 0.5rem;
            color: #909090;
        }
        
    }
    legend{
        width:250px;
        padding-left:1.5rem;
        font-weight: bold;
    }
    fieldset:last-of-type{
       margin-top:2rem;
       legend{
        width: 280px;
        
        }
        .abajo{
            margin-top: 3rem;
        }
       
    }
    .abajo{
        margin: 2rem auto;

    }
    
`;

export const BotonCancelar = styled.button`
    margin: 2rem;
    width: 25%;
    font-size: 1rem;
    height: 3rem;
`;

export const BotonAceptar = styled.button`
    font-size: 1rem;
    width: 25%;
    margin: 2rem;
    height: 3rem;
`;

export const Span = styled.span`
    font-size: 1rem;
    width: 25%;
    margin: 2rem;
    height: 3rem;
    margin-bottom: 1rem;
`;

