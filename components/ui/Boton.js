import styled from '@emotion/styled';

export const Boton = styled.button`
    padding: 0.75rem;
    margin-top: 2rem;
    margin-bottom: 4rem;
    margin-left:1rem;
    font-size: 0.85rem;
 
`;

export const Boton2 = styled.button`
    padding: 0.75rem;
    margin-top: 2rem;
    margin-bottom: 4rem;
    margin-left:45rem;
    font-size: 0.85rem;
    background-color:green;
                     &:hover{
                        /* background-color: #d19d02; */
                        background-color: #116303;
                        
                    }
                    &:focus{
                        background-color: #116303;
                       
                    }
                    &:disabled{
                        background-color: #116303;
                       
                    }
 
`;

export const BotonTransparente = styled.button`
    font-size: 1rem;
    width: 25%;
    margin: 2rem;
    height: 3rem;
`;

export const Input = styled.input`
    margin-bottom: 3rem;
    margin-top: 2rem;
`;

export const Radio = styled.input`
    margin-left: 1.5rem;
    margin-right: .5rem;
`;

export const BotonRemove = styled.button`
    padding: 0.75rem;
    margin-top: 2rem;
    margin-bottom: 4rem;
    margin-left:1rem;
    font-size: 0.85rem;
    background-color:transparent;
    color: black;
    border-color:black;
 
`;