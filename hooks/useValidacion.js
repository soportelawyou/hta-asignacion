import React, {useState, useEffect} from 'react';
import firebase from '../firebase/index';

const useValidacion = (stateInicial, validar, funcion) => {

    const [valores, guardarValores] = useState(stateInicial);
    const [errores, guardarErrores] = useState({});
    const [submitForm, guardarSubmitForm] = useState(false);

    
    // Destructuring para trabajar con el DNI
    const {dni} = valores;
 
    useEffect(()=>{
        if(submitForm){
            const noErrores = Object.keys(errores).length === 0;
            if(noErrores){
                funcion();
            }
            guardarSubmitForm(false);
        }
    },[errores]);

    //************ Duplicado DNI**************** */
    
    const [testAbogados, guardarTestAbogados] = useState([]);
    const [testTerceros, guardarTestTerceros] = useState([]);

    useEffect(() => {
        const obtenerAbogados = () => {
            firebase.db.collection('abogados').onSnapshot(manejarSnapShot)
            firebase.db.collection('terceros').onSnapshot(manejarSnapShot2)
        }
        obtenerAbogados();
    }, [],[])

    function manejarSnapShot(snapshot) {
        const abogadosDB = snapshot.docs.map(doc => {
            return {
                id: doc.id,
                ...doc.data()
            }
        })

        guardarTestAbogados(abogadosDB);
    }

    function manejarSnapShot2(snapshot) {
        const tercerosDB = snapshot.docs.map(doc => {
            return {
                id: doc.id,
                ...doc.data()
            }
        })

        guardarTestTerceros(tercerosDB);
    }

    useEffect(() => {
        let encontrado=false
        if (testAbogados.length !== 0) {
            if(testAbogados.find(tes => tes.dni === dni) !== undefined){
                // console.log('duplicado')
                guardarValores({
                    ...valores,
                    dniDuplicado: true
                })
                return
                
            }
            else if (testTerceros.find(tes => tes.dni === dni) !== undefined && encontrado === false){
                // console.log('Duplicado')
                guardarValores({
                    ...valores,
                    dniDuplicado: true
                })
                return
                
            }
            guardarValores({
                ...valores,
                dniDuplicado: false
            })
            
        }
    }, [dni])

    


    //***************************** */

    useEffect(()=>{
        guardarValores(stateInicial)
    },[stateInicial])

    //***************************** */


    // Funcion que se ejecuta cuando se escribe algo
    const handleChange = e => {
        guardarValores({
            ...valores,
            [e.target.name]: e.target.value
        })
    }

    // Funcion que se ejecuta cuando se hace submit
    const handleSubmit = e => {
        e.preventDefault();
        const erroresValidacion = validar(valores);
        guardarErrores(erroresValidacion);
        guardarSubmitForm(true);
    }

       // Se ejecuta cuando se selecciona una provincia
       const handleChangeProvincia = (opcion) => {
        guardarValores({
            ...valores,
            provincia: opcion.value
        })
        // console.log(`Opcion seleccionada:`, opcion.value);
    };

      // Se ejecuta cuando se selecciona un tipo
      const handleChangeTipo = (opcion) => {
        guardarValores({
            ...valores,
            tipo: opcion.value
        })
    };

    const handleChangeActividad = (opcion) => {
        guardarValores({
            ...valores,
            activo: opcion.value
        })
    };

/*       // Se ejecuta cuando se selecciona un tipo
      const handleChangeComunidad = (opcion) => {
        guardarValores({
            ...valores,
            comunidadAutonoma: opcion.value
        })
    }; */
    const handleChangeCiudad = (opcion) => {
        guardarValores({
            ...valores,
            ciudad: opcion.value
        })
    };

    const handleChangePais = (opcion) => {
        guardarValores({
            ...valores,
            pais: opcion.value
        })
    };

      // Se ejecuta cuando se selecciona una especialidad
      const handleChangeEspecialidadPrincipal = (opciones) => {
        if (opciones === null) {
            opciones = [];
          }
        let resultado = opciones.map(especialidad => especialidad.value);
        guardarValores({
            ...valores,
            especialidadesAbogadoPrincipal: resultado
        })
    };

    // Se Ejecuta cuando se selecciona una subespecialidad
    const handleChangeSubEspecialidadPrincipal = (opciones) => {
        if (opciones === null) {
            opciones = [];
          }
        let resultado = opciones.map(subEspecialidades => subEspecialidades.value);
        guardarValores({
            ...valores,
            subEspecialidadesAbogadosPrincipal: resultado
        })
    };

    const handleChangeEspecialidadSecundario = (opciones) => {
        if (opciones === null) {
            opciones = [];
          }
        let resultado = opciones.map(especialidad => especialidad.value);
        guardarValores({
            ...valores,
            especialidadesAbogadoSecundario: resultado
        })
    };

    // Se Ejecuta cuando se selecciona una subespecialidad
    const handleChangeSubEspecialidadSecundario = (opciones) => {
        if (opciones === null) {
            opciones = [];
          }
        let resultado = opciones.map(subEspecialidades => subEspecialidades.value);
        guardarValores({
            ...valores,
            subEspecialidadesAbogadosSecundario: resultado
        })
    };

    const handleChangeEspecialidadDeseados = (opciones) => {
        if (opciones === null) {
            opciones = [];
          }
        let resultado = opciones.map(especialidad => especialidad.value);
        guardarValores({
            ...valores,
            especialidadesAbogadoDeseados: resultado
        })
    };

    // Se Ejecuta cuando se selecciona una subespecialidad
    const handleChangeSubEspecialidadDeseados = (opciones) => {
        if (opciones === null) {
            opciones = [];
          }
        let resultado = opciones.map(subEspecialidades => subEspecialidades.value);
        guardarValores({
            ...valores,
            subEspecialidadesAbogadosDeseados: resultado
        })
    };

    const handleChangeIdioma = (opciones) => {
        if (opciones === null) {
            opciones = [];
          }
        let resultado = opciones.map(idioma => idioma.value);
        guardarValores({
            ...valores,
            idioma: resultado
        })
    };
   
   

    return {
        valores,
        errores,
        submitForm,
        handleSubmit,
        handleChange,
        handleChangeProvincia,
        handleChangeTipo,
        handleChangeEspecialidadPrincipal,
        handleChangeSubEspecialidadPrincipal,
        handleChangeEspecialidadSecundario,
        handleChangeSubEspecialidadSecundario,
        handleChangeEspecialidadDeseados,
        handleChangeSubEspecialidadDeseados,
        handleChangeActividad,
        handleChangeIdioma,
        
        handleChangePais,
        handleChangeCiudad
    };
}
 
export default useValidacion;