import React, {useState, useEffect, useContext} from 'react';
import {FirebaseContext} from '../firebase/index';

const useContadorOportunidades = () => {

    const [contador, guardarContador] = useState(0);
    const {firebase} = useContext(FirebaseContext);

    useEffect(()=>{
        let montado = true;
        const obtenerContador = () => {
            firebase.db.collection('contadorOportunidades').doc('NYEfyyPx5x5qa17wE3ah').onSnapshot(manejarSnapShot)
            // .onSnapshot(function(doc) {
            //     console.log("Current data: ", doc.data());
            // });
        }
        if(montado){
            obtenerContador()
        }
        return()=>montado = false;
    },[])

    function manejarSnapShot(doc){
        const contadorDB = doc.data()
        guardarContador(contadorDB);
    }

    return {
        contador
    };
}
 
export default useContadorOportunidades;

// const contadorOportunidades = await firebase.db.collection('contadorOportunidades').doc('OKxqPbwf4oBCh6NpfQUU')
        // console.log(contadorOportunidades.contador)