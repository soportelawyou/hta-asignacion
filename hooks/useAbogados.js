import React, { useState, useEffect, useContext } from 'react';
import { FirebaseContext } from '../firebase/index';

const useAbogados = (orden) => {

    const [abogados, guardarAbogados] = useState([]);
    const { firebase } = useContext(FirebaseContext);

    useEffect(() => {
        let montado = true;
        const obtenerAbogados = () => {
            firebase.db.collection('abogados').orderBy(orden, 'asc').onSnapshot(manejarSnapShot)
        }
        if (montado) {
            obtenerAbogados()
        }
        return () => montado = false;
    }, [])

    function manejarSnapShot(snapshot) {
        const abogadosDB = snapshot.docs.map(doc => {

            return {
                id: doc.id,
                ...doc.data()
            }
        })
        guardarAbogados(abogadosDB);
    }

    return {
        abogados
    };
}

export default useAbogados;