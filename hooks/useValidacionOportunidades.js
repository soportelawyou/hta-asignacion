import React, { useState, useEffect } from 'react';
import firebase from '../firebase/index';

const useValidacionOportunidades = (stateInicial, validar, funcion) => {

    const [valores, guardarValores] = useState(stateInicial);
    const [errores, guardarErrores] = useState({});
    const [submitForm, guardarSubmitForm] = useState(false);

    useEffect(() => {
        if (submitForm) {
            const noErrores = Object.keys(errores).length === 0;
            if (noErrores) {
                funcion();
            }
            guardarSubmitForm(false);
        }
    }, [errores]);


    //***************************** */

    useEffect(() => {
        guardarValores(stateInicial)
    }, [stateInicial])

    //***************************** */

    // Funcion que se ejecuta cuando se escribe algo
    const validateEmail = (email) => {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    const handleChange = e => {
        /*if (e.target.name === "email") {
            const clientData = {};
            var email = e.target.value;

            if (validateEmail(email)) {
                var myHeaders = new Headers();
                var cuerpo = {
                    "filterGroups": [
                        {
                            "filters": [
                                {
                                    "propertyName": "email",
                                    "operator": "EQ",
                                    "value": email
                                }
                            ]
                        }
                    ],
                    "limit": 100,
                    "properties": [ "email","phone","firstname","lastname" ]
                }
                myHeaders.append("Content-Type", "application/json");
                const proxyurl = "https://cors-anywhere.herokuapp.com/";
                const url = `https://api.hubapi.com/crm/v3/objects/contacts/search?hapikey=17ffea9d-bfe1-420d-a87c-e0a49d5133eb`; // site that doesn’t send Access-Control-*
               
                fetch(proxyurl + url, {
                    method: 'POST',
                    mode: 'cors',
                    headers: myHeaders,
                    body: JSON.stringify(cuerpo)
        
                }).then((resp) => resp.json())
                    .then(function(data) {
                        clientData.email = email;
                        console.log(data.results[0].properties.phone)
                        if (data.status != "error") {
                            clientData.nombreCliente = data.results[0].properties.firstname;
                            clientData.apellidoCliente = data.results[0].properties.lastname;
                            clientData.telefonoCliente = data.results[0].properties.phone ? data.results[0].properties.phone : "";
                        }
                        guardarValores({
                            ...valores,
                            ...clientData
                        })
                    })
                    .catch(function(error) {
                        console.log(error);
                    }); 
                
            } else {
                guardarValores({
                    ...valores,
                    nombreCliente: "",
                    telefonoCliente: "",
                    nif: "",
                    [e.target.name]: e.target.value
                })
            }
        } else {*/
            guardarValores({
                ...valores,
                [e.target.name]: e.target.value
            })
        //}
    }

    // Funcion que se ejecuta cuando se hace submit
    const handleSubmit = e => {
        e.preventDefault();
        const erroresValidacion = validar(valores);
        guardarErrores(erroresValidacion);
        guardarSubmitForm(true);
    }

    // Se ejecuta cuando se selecciona una provincia
    const handleChangeProvincia = (opcion) => {
        guardarValores({
            ...valores,
            provinciaOportunidad: opcion.value
        })
        // console.log(`Opcion seleccionada:`, opcion.value);
    };

    // Se ejecuta cuando se selecciona un tipo
    const handleChangeComunidad = (opcion) => {
        guardarValores({
            ...valores,
            comunidadAutonoma: opcion.value
        })
    };

    // Se ejecuta cuando se selecciona una especialidad
    const handleChangeSubEspecialidad = (opcion) => {
        guardarValores({
            ...valores,
            subEspecialidad: opcion.value
        })
    };

    const handleChangeAbogadoAsignado = (opcion) => {
        console.log(opcion)
        guardarValores({
            ...valores,
            abogadoAsignado: opcion
        })
    }

    const handleChangeRespuestasPreguntas = (opcion) => {
        console.log(opcion)
        console.log("He entrado a Respuestas y Preguntas")
        guardarValores({
            ...valores,
            respuestasPreguntas: opcion
        })
    }

    const nombreAbogado = (nombre) => {
        console.log('NOMBRE Y APELLIDOS: ', nombre)
        guardarValores({
            ...valores,
            nombreAbogadoAsignado: nombre
        })
    }

    // const mailAbogado = (mail) =>{
    //     console.log('Mail: ',mail)
    //     guardarValores({
    //         ...valores,
    //         emailAbogadoAsignado: mail
    //     })
    // }

    const handleChangeServicio = (opcion) => {
        console.log(opcion);
        guardarValores({
            ...valores,
            servicio: opcion.value
        })
    }

    const handleChangePrecioServicio = (opcion) => {
        console.log(opcion);
        guardarValores({
            ...valores,
            precioServicio: opcion.value
        })
    }

    const handleChangeTipoServicio = (opcion) => {
        console.log(opcion);
        guardarValores({
            ...valores,
            tipoServicio: opcion.value
        })
    }

    const handleChangeTipo = (opcion) => {
        guardarValores({
            ...valores,
            tipo: opcion.value
        })
    }
    const handleChangePais = (opcion) => {
        guardarValores({
            ...valores,
            pais: opcion.value
        })
    };

    const handleChangeCiudad = (opcion) => {
        guardarValores({
            ...valores,
            ciudad: opcion.value
        })
    };

    const handleChangeSiNo = (opcion, num) => {
        switch(num){
            case 1:
                guardarValores({
                    ...valores,
                    respuestaSiNo1: opcion.value
                });
                break;
            case 2:
                guardarValores({
                    ...valores,
                    respuestaSiNo2: opcion.value
                });
                break;
            case 3:
                guardarValores({
                    ...valores,
                    respuestaSiNo3: opcion.value
                });
                break;
            case 4:
                guardarValores({
                    ...valores,
                    respuestaSiNo4: opcion.value
                });
                break;
            case 5:
                guardarValores({
                    ...valores,
                    respuestaSiNo5: opcion.value
                });
                break;
            case 6:
                guardarValores({
                    ...valores,
                    respuestaSiNo6: opcion.value
                });
                break;
            case 7:
                guardarValores({
                    ...valores,
                    respuestaSiNo7: opcion.value
                });
                break;
            case 8:
                guardarValores({
                    ...valores,
                    respuestaSiNo8: opcion.value
                });
                break;
            case 9:
                guardarValores({
                    ...valores,
                    respuestaSiNo9: opcion.value
                });
                break;
            case 10:
                guardarValores({
                    ...valores,
                    respuestaSiNo10: opcion.value
                });
                break;
            case 11:
                guardarValores({
                    ...valores,
                    respuestaSiNo11: opcion.value
                });
                break;
            case 12:
                guardarValores({
                    ...valores,
                    respuestaSiNo12: opcion.value
                });
                break;
            case 13:
                guardarValores({
                    ...valores,
                    respuestaSiNo13: opcion.value
                });
                break;
            case 14:
                guardarValores({
                    ...valores,
                    respuestaSiNo14: opcion.value
                });
                break;
            default:
                break;
        }
    }

    const handleChangePrProvincia = (opcion) => {
        guardarValores({
            ...valores,
            respuestaProvin: opcion.value
        })
    }

    const handleChangeCanal = (opcion) => {
        guardarValores({
            ...valores,
            canal: opcion.value
        })
    }

    const handleChangeSubCanal = (opcion) => {
        guardarValores({
            ...valores,
            subcanal: opcion.value
        })
    }

    return {
        valores,
        errores,
        submitForm,
        handleSubmit,
        handleChange,
        handleChangeProvincia,
        handleChangeComunidad,
        handleChangeSubEspecialidad,
        handleChangeAbogadoAsignado,
        nombreAbogado,
        handleChangePais,
        handleChangeCiudad,
        handleChangeTipo,
        handleChangeRespuestasPreguntas,
        handleChangeServicio,
        handleChangePrecioServicio,
        handleChangeTipoServicio,
        handleChangeSiNo,
        handleChangePrProvincia,
        handleChangeCanal,
        handleChangeSubCanal,
    };
}

export default useValidacionOportunidades;