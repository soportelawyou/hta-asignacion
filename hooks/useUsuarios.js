import React, { useState, useEffect, useContext } from 'react';
import { FirebaseContext } from '../firebase/index';

const useUsuarios = (orden) => {

    const [usuarios, guardarUsuarios] = useState([]);
    const { firebase } = useContext(FirebaseContext);

    useEffect(() => {
        let montado = true;
        const obtenerUsuarios = () => {
            firebase.db.collection('usuarios').orderBy(orden, 'asc').onSnapshot(manejarSnapShot)
        }
        if (montado) {
            obtenerUsuarios()
        }
        return () => montado = false;
    }, [])

    function manejarSnapShot(snapshot) {
        const usuariosDB = snapshot.docs.map(doc => {

            return {
                id: doc.id,
                ...doc.data()
            }
        })
        guardarUsuarios(usuariosDB);
    }

    return {
        usuarios
    };
}

export default useUsuarios;