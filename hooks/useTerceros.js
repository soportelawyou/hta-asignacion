import React, { useState, useEffect, useContext } from 'react';
import { FirebaseContext } from '../firebase/index';

const useTerceros = (orden) => {

    const [terceros, guardarTerceros] = useState([]);
    const { firebase } = useContext(FirebaseContext);

    useEffect(() => {
        let montado = true;
        const obtenerTerceros = () => {
            firebase.db.collection('terceros').orderBy(orden, 'asc').onSnapshot(manejarSnapShot)
        }
        if (montado) {
            obtenerTerceros()
        }
        return () => montado = false;
    }, [])

    function manejarSnapShot(snapshot) {
        const tercerosDB = snapshot.docs.map(doc => {

            return {
                id: doc.id,
                ...doc.data()
            }
        })
        guardarTerceros(tercerosDB);
    }

    return {
        terceros
    };
}

export default useTerceros;