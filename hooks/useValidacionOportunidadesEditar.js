import React, {useState, useEffect} from 'react';
import firebase from '../firebase/index';

const useValidacionOportunidadesEditar = (stateInicial, validar, funcion) => {
    

    const [valores, guardarValores] = useState(stateInicial);
    const [errores, guardarErrores] = useState({});
    const [submitForm, guardarSubmitForm] = useState(false);

    useEffect(()=>{
        if(submitForm){
            const noErrores = Object.keys(errores).length === 0;
            if(noErrores){
                funcion();
            }
            guardarSubmitForm(false);
        }
    },[errores]);

    
    //***************************** */

    useEffect(()=>{
        guardarValores(stateInicial)
        
    },[stateInicial])

    //***************************** */
    
    useEffect(()=>{
        nombreAbogado()
    },[valores.abogadoAsignado])


    async function nombreAbogado() {
        if (valores.abogadoAsignado) {
            const abogadoSeleccionado = await firebase.db.collection('abogados').doc(valores.abogadoAsignado).get()
            guardarValores({
                ...valores,
                nombreAbogadoAsignado: `${abogadoSeleccionado.data().nombre} ${abogadoSeleccionado.data().apellidos}`}
            )
        }
    }

    // Funcion que se ejecuta cuando se escribe algo
    const handleChange = e => {
        guardarValores({
            ...valores,
            [e.target.name]: e.target.value
        })
    }

    // Funcion que se ejecuta cuando se hace submit
    const handleSubmit = e => {
        e.preventDefault();
        const erroresValidacion = validar(valores);
        guardarErrores(erroresValidacion);
        guardarSubmitForm(true);
    }

       // Se ejecuta cuando se selecciona una provincia
       const handleChangeProvincia = (opcion) => {
        guardarValores({
            ...valores,
            provinciaOportunidad: opcion.value
        })
        // console.log(`Opcion seleccionada:`, opcion.value);
    };

      // Se ejecuta cuando se selecciona un tipo
      const handleChangeComunidad = (opcion) => {
        guardarValores({
            ...valores,
            comunidadAutonoma: opcion.value
        })
    };

      // Se ejecuta cuando se selecciona una especialidad
      const handleChangeEspecialidad = (opcion) => {
        guardarValores({
            ...valores,
            especialidad: opcion.value
        })
    };

    const handleChangeSubEspecialidad = (opcion) => {
        guardarValores({
            ...valores,
            subEspecialidad: opcion.value
        })
    };
   
    const handleChangeAbogadoAsignado = (opcion) =>{
        console.log(opcion)
        guardarValores({
            ...valores,
            abogadoAsignado: opcion
        })
    }
   
    const handleChangeEstado = (opcion) =>{
        console.log(opcion)
        guardarValores({
            ...valores,
            estado: opcion.value
        })
    }

     // Se ejecuta cuando se selecciona un motivo de rechazo
     const handleChangeMotivo = (opcion) => {
        guardarValores({
            ...valores,
            motivoRechazo: opcion.value
        })
    };

    const handleChangeServicio = (opcion) => {
        console.log(opcion);
        guardarValores({
            ...valores,
            servicio: opcion.value
        })
    }

    const handleChangePrecioServicio = (opcion) => {
        console.log(opcion);
        guardarValores({
            ...valores,
            precioServicio: opcion.value
        })
    }

    const handleChangeTipoServicio = (opcion) => {
        console.log(opcion);
        guardarValores({
            ...valores,
            tipoServicio: opcion.value
        })
    }

    const handleChangeTipo = (opcion) => {
        console.log(opcion);
        guardarValores({
            ...valores,
            tipo: opcion.value
        })
    }
    const handleChangePais = (opcion) => {
        guardarValores({
            ...valores,
            pais: opcion.value
        })
    };

    const handleChangeCiudad = (opcion) => {
        guardarValores({
            ...valores,
            ciudad: opcion.value
        })
    };

    const handleChangeSiNo = (opcion, num) => {
        switch(num){
            case 1:
                guardarValores({
                    ...valores,
                    respuestaSiNo1: opcion.value
                });
                break;
            case 2:
                guardarValores({
                    ...valores,
                    respuestaSiNo2: opcion.value
                });
                break;
            case 3:
                guardarValores({
                    ...valores,
                    respuestaSiNo3: opcion.value
                });
                break;
            case 4:
                guardarValores({
                    ...valores,
                    respuestaSiNo4: opcion.value
                });
                break;
            case 5:
                guardarValores({
                    ...valores,
                    respuestaSiNo5: opcion.value
                });
                break;
            case 6:
                guardarValores({
                    ...valores,
                    respuestaSiNo6: opcion.value
                });
                break;
            case 7:
                guardarValores({
                    ...valores,
                    respuestaSiNo7: opcion.value
                });
                break;
            case 8:
                guardarValores({
                    ...valores,
                    respuestaSiNo8: opcion.value
                });
                break;
            case 9:
                guardarValores({
                    ...valores,
                    respuestaSiNo9: opcion.value
                });
                break;
            case 10:
                guardarValores({
                    ...valores,
                    respuestaSiNo10: opcion.value
                });
                break;
            case 11:
                guardarValores({
                    ...valores,
                    respuestaSiNo11: opcion.value
                });
                break;
            case 12:
                guardarValores({
                    ...valores,
                    respuestaSiNo12: opcion.value
                });
                break;
            case 13:
                guardarValores({
                    ...valores,
                    respuestaSiNo13: opcion.value
                });
                break;
            case 14:
                guardarValores({
                    ...valores,
                    respuestaSiNo14: opcion.value
                });
                break;
            default:
                break;
        }
    }

    const handleChangePrProvincia = (opcion) => {
        guardarValores({
            ...valores,
            respuestaProvin: opcion.value
        })
    }

    const handleChangeCanal = (opcion) => {
        guardarValores({
            ...valores,
            canal: opcion.value
        })
    }

    const handleChangeSubCanal = (opcion) => {
        guardarValores({
            ...valores,
            subcanal: opcion.value
        })
    }
    const handleChangeRespuestasPreguntas = (opcion) => {
        console.log(opcion)
        console.log("He entrado a Respuestas y Preguntas")
        guardarValores({
            ...valores,
            respuestasPreguntas: opcion
        })
    }



    return {
        valores,
        errores,
        submitForm,
        handleSubmit,
        handleChange,
        handleChangeProvincia,
        handleChangeComunidad,
        handleChangeSubEspecialidad,
        handleChangeEspecialidad,
        handleChangeAbogadoAsignado,
        handleChangeEstado,
        handleChangeMotivo,
        handleChangePais,
        handleChangeCiudad,
        handleChangeTipo,
        handleChangeRespuestasPreguntas,
        handleChangeServicio,
        handleChangePrecioServicio,
        handleChangeTipoServicio,
        handleChangeSiNo,
        handleChangePrProvincia,
        handleChangeCanal,
        handleChangeSubCanal,
        
    };
}
 
export default useValidacionOportunidadesEditar;