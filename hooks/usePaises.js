import React, { useState, useEffect, useContext } from 'react';
import { Convertir } from '../pages/api/Funciones';

const usePaises = () => {
    const [Auth, guardarAuth] = useState('')
    const [Paises, guardarPaises] = useState([]);

    useEffect(() => {
        fetch('https://www.universal-tutorial.com/api/getaccesstoken', {
            method: ('GET'),
            headers: {
                "Accept": "application/json",
                "api-token": "FoDgxAIYOO9kHc5qdXTNvDk10t-zGQCh8bRAm4xhXzgbkdh7iGuqoExwdIaqnBnjT64",
                "user-email": "soportelawyou@gmail.com",
                "Connection": "close"
            },
        })
            .then(response => response.json())
            .then(json => {
                let token = json.auth_token;
                guardarAuth(token)
                return fetch('https://www.universal-tutorial.com/api/countries/', {
                    method: ('GET'),
                    headers: {
                        "Authorization": `Bearer ${token}`,
                        "Accept": "application/json",
                        "Connection": "close"
                    },
                })
                    .then(response => response.json())
                    .then(json => { guardarPaises(Convertir(json)) })
            })
    }, '')

    return {
        Paises,
        Auth
    };
}

export default usePaises;