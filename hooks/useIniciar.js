import React, { useState, useEffect, useContext } from 'react';

const useIniciar = () => {

    const [auth, guardarAuth] = useState('');

    useEffect(() => {
        fetch('https://www.universal-tutorial.com/api/getaccesstoken', {
            method: ('GET'),
            headers: {
                "Accept": "application/json",
                "api-token": "FoDgxAIYOO9kHc5qdXTNvDk10t-zGQCh8bRAm4xhXzgbkdh7iGuqoExwdIaqnBnjT64",
                "user-email": "soportelawyou@gmail.com",
                "Connection": "close"
            },
        })
            .then(response => response.json())
            .then(json => { guardarAuth(json.auth_token) })
    }, '')

    return {
        auth
    };
}

export default useIniciar;