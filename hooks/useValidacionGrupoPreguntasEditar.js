import React, {useState, useEffect} from 'react';
import firebase from '../firebase/index';

const useValidacionGrupoPreguntasEditar = (stateInicial, validar, funcion) => {

    const [valores, guardarValores] = useState(stateInicial);
    const [errores, guardarErrores] = useState({});
    const [submitForm, guardarSubmitForm] = useState(false);

    useEffect(()=>{
        if(submitForm){
            const noErrores = Object.keys(errores).length === 0;
            if(noErrores){
                funcion();
            }
            guardarSubmitForm(false);
        }
    },[errores]);

    
    //***************************** */

    useEffect(()=>{
        guardarValores(stateInicial)
    },[stateInicial])

    //***************************** */

    // Funcion que se ejecuta cuando se escribe algo
    const handleChange = e => {
        guardarValores({
            ...valores,
            [e.target.name]: e.target.value
        })
    }

    // Funcion que se ejecuta cuando se hace submit
    const handleSubmit = e => {
        e.preventDefault();
        const erroresValidacion = validar(valores);
        guardarErrores(erroresValidacion);
        guardarSubmitForm(true);
    }

    //Funcion para cambiar la actividad
    const handleChangePreguntas = (opciones) => {
        if (opciones === null) {
            opciones = [];
          }
        let resultado = opciones.map(preguntas => preguntas.value);
        guardarValores({
            ...valores,
            preguntasSeleccionadas: resultado
        })
    };


   

    return {
        valores,
        errores,
        handleSubmit,
        handleChange,
        handleChangePreguntas
       
    };
}
 
export default useValidacionGrupoPreguntasEditar;