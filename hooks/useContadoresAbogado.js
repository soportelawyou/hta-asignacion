import React, {useState, useEffect, useContext} from 'react';
import {FirebaseContext} from '../firebase/index';

const useContadorAbogado = (id) => {

    const [abogado, guardarAbogado] = useState(0);
    const {firebase} = useContext(FirebaseContext);

    useEffect(()=>{
        let montado = true;
        const obtenerContador = () => {
            firebase.db.collection('abogados').doc(id).onSnapshot(manejarSnapShot)
            // .onSnapshot(function(doc) {
            //     console.log("Current data: ", doc.data());
            // });
        }
        if(montado){
            obtenerContador()
        }
        return()=>montado = false;
    },[id])

    function manejarSnapShot(doc){
        const abogadoBD = doc.data()
        guardarAbogado(abogadoBD);
    }

    return {
        abogado
    };
}
 
export default useContadorAbogado;