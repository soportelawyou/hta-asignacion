import app from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

import firebaseConfig from './config';

class Firebase {
    constructor() {
        if (!app.apps.length) {
            app.initializeApp(firebaseConfig);
        }
        this.auth = app.auth();
        this.db = app.firestore();
    }

    // Registra un usuario
    async registrar(nombre, email, password) {
        const nuevoUsuario = await this.auth.createUserWithEmailAndPassword(email, password);
        console.log("UID! ", this.auth.uid)

        return await nuevoUsuario.user.updateProfile({
            displayName: nombre,
            id: this.auth.uid
        })
    }

    async actualizar(nombre){
       
        app.auth().currentUser.user.updateProfile({
            displayName:nombre
        })
    }




    // Iniciar sesion del usuario
    async login(email, password) {
        return this.auth.signInWithEmailAndPassword(email, password);
    }

    // Cerrar sesión del usuario
    async cerrarSesion() {
        await this.auth.signOut();
    }
};

const firebase = new Firebase();

export default firebase;