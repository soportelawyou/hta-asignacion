const validarEditarTerceros = (valores) => {

    let errores = {};
    // Validar Nombre
    if(!valores.nombre){
        errores.nombre = "El nombre es obligatorio"
    }

    // Validar Apellidos
    if(!valores.apellidos){
        errores.apellidos = "Los apellidos son obligatorios"
    }

    // Validar tipo
    if(!valores.tipo){
        errores.tipo = "Hay que seleccionar el tipo de abogado"
    }

    // Validar DNI
    if(!valores.dni){
        errores.dni = "El DNI es obligatorio"
    }else if((!/[0-9]{8}[a-zA-Z]{1}$/i.test(valores.dni)) ){
        errores.dni ="El DNI no tiene el formato válido"
    }

    // Validar Calle
    if(!valores.calle){
        errores.calle = "El nombre de la calle es obligatorio"
    }

    // Validar Ciudad
    if(!valores.ciudad){
        errores.ciudad = "La ciudad es obligatoria"
    }

    // Validar CP
    if(!valores.cp){
        errores.cp = "El CP es obligatorio"
    }

    // Validar Provincia
    if(!valores.provincia){
        errores.provincia = "La Provincia es obligatoria"
    }

    // Validar Comunidad Autonoma
    if(!valores.comunidadAutonoma){
        errores.comunidadAutonoma = "La Comunidad Autonoma es obligatoria"
    }

    // Validar que el DNI no lo tiene otro usuario
    if(valores.dniDuplicado){
        errores.dniDuplicado = "El DNI ya existe"
    }


    // Validar Email
    if(!valores.emailTerceros){
        errores.emailTerceros = "El mail es obligatorio"
    }else if(!/((?!\.)[\w-_.]*[^.])(@\w+)(\.\w+(\.\w+)?[^.\W])$/.test(valores.emailTerceros)){
        errores.emailTerceros = "El formato del mail es incorrecto"

    }

    if(!valores.numeroTelefono){
        errores.numeroTelefono = "El Numero de Telefono es obligatorio"
    }

    
    if(!valores.idLawyou){
        errores.idLawyou = "El ID es obligatorio"
    }

    if(valores.idioma.length === 0){
        errores.idioma = "Tienes que selecionar alguna idioma"
    }

    return errores;
};

export default validarEditarTerceros;