const validarCrearCuenta = (valores) => {
    
    let errores = {};
    
    // Validamos nombre usuario
    if(!valores.nombre){
        errores.nombre = "El nombre es obligatorio";
    }

    // Validamos el mail
    if(!valores.email){
        errores.email = "El email es obligatorio";
    }else if(!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(valores.email)){
        errores.email = "Email no válido";
    }else if(!/^[a-zA-Z0-9_.+-]+@(?:(?:[a-zA-Z0-9-]+\.)?[a-zA-Z]+\.)?(lawyoulegal)\.com$/.test(valores.email)){
        errores.email = "Email no válido, consulta con el administrador"
    }
    

    // Validar Password
    if(!valores.password){
        errores.password = "El password es obligatorio";
    }else if(valores.password.length < 6){
        errores.password = "El password debe ser de al menos 6 caracteres";
    }

    // Checamos que las dos contraseñas coincidan
    if(!valores.repetirPassword){
        errores.repetirPassword = "Es obligatorio repetir la contraseña";
    }else if(valores.repetirPassword.trim() !== valores.password){
        errores.repetirPassword = "Las contraseñas no coinciden";
    }

    return errores;
}
 
export default validarCrearCuenta;