const validarEditarUsuarios = (valores) => {

    let errores = {};

    // Validar Nombre
    if(!valores.nombre){
        errores.nombre = "El nombre es obligatorio"
    }

    // Validar Apellidos
    return errores;
};

export default validarEditarUsuarios;