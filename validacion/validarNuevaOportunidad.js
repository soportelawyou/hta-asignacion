
const validarNuevaOportunidad = (valores) => {

    let errores = {};
    
    // Validar Nombre Cliente
    // if(!valores.nombreCliente){
    //     errores.nombreCliente = "El nombre es obligatorio"
    // }

    // Validar Telefono
    // if(!valores.telefonoCliente){
    //     errores.telefonoCliente = "El teléfono es obligatorio"
    // }else if(!/^[+]{0,1}[0-9]{9,12}$/g.test((valores.telefonoCliente.replace(/ /g, "")))){
    //     console.log(valores.telefonoCliente)
    //     errores.telefonoCliente ="El teléfono no tiene un formato válido. Ej: (+34) 987654321 | Prefijo opcional"
    // }

    // Validar Especialidad
    if(!valores.subEspecialidad){
        errores.subEspecialidad = "La especialidad es obligatoria"
    }

    // Validar Provincia
    if(!valores.provinciaOportunidad){
        errores.provinciaOportunidad = "La provincia es obligatoria"
    }

    // Validar Abogado Asignado
    // if(!valores.abogadoAsignado){
    //     errores.abogadoAsignado = "Tienes que seleccionar un abogado"
    // }

    // Validar estado
    /*if(!valores.estado){
        errores.estado = "El estado es obligatorio"
    }*/

    // if(!valores.email){
    //     errores.email = "El email es obligatorio"
    // }
    if(!valores.tipo){
        errores.tipo = "El tipo de servicio que busca el cliente es obligatorio"
    }
 


    return errores;
}
 
export default validarNuevaOportunidad;