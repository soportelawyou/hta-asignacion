const validarNuevoAbogado = (valores) => {

    let errores = {};
    // Validar Nombre
    if (!valores.nombre) {
        errores.nombre = "El nombre es obligatorio"
    }

    // Validar Apellidos
    if (!valores.apellidos) {
        errores.apellidos = "Los apellidos son obligatorios"
    }

    // Validar tipo
    if (!valores.tipo) {
        errores.tipo = "Hay que seleccionar el tipo de abogado"
    }

    // Validar DNI
    if (!valores.dni) {
        errores.dni = "El DNI es obligatorio"
    } else if ((!/[0-9]{8}[a-zA-Z]{1}$/i.test(valores.dni))) {
        errores.dni = "El DNI no tiene el formato válido"
    }
    //Validar Cif

    // Validar Calle
    if (!valores.calle) {
        errores.calle = "El nombre de la calle es obligatorio"
    }

    // Validar Ciudad
    if (!valores.ciudad) {
        errores.ciudad = "La ciudad es obligatoria"
    }

    // Validar CP
    if (!valores.cp) {
        errores.cp = "El CP es obligatorio"
    }

    // Validar Provincia
    if (!valores.provincia) {
        errores.provincia = "La Provincia es obligatoria"
    }

    // Validar Comunidad Autonoma
    if (!valores.pais) {
        errores.pais = "La Comunidad Autonoma es obligatoria"
    }

    // Validar que el DNI no lo tiene otro usuario
    if (valores.dniDuplicado) {
        errores.dniDuplicado = "El DNI ya existe"
    }

    // Validar que haya especialidades
    if (valores.especialidadesAbogadoPrincipal.length === 0) {
        errores.especialidades = "Tienes que selecionar alguna especialidad"
    }

    if (valores.subEspecialidadesAbogadosPrincipal.length === 0) {
        errores.subEspecialidades = "Tienes que selecionar alguna especialidad"
    }


    if (valores.idioma.length === 0) {
        errores.idioma = "Tienes que selecionar alguna idioma"
    }

    // Validar Email
    if (!valores.emailAbogado) {
        errores.emailAbogado = "El mail es obligatorio"
    } else if (!/((?!\.)[\w-_.]*[^.])(@\w+)(\.\w+(\.\w+)?[^.\W])$/.test(valores.emailAbogado)) {
        errores.emailAbogado = "El formato del mail es incorrecto"

    }

    if (valores.emailAbogadoSecundario) {
        if (!/((?!\.)[\w-_.]*[^.])(@\w+)(\.\w+(\.\w+)?[^.\W])$/.test(valores.emailAbogado)) {
            errores.emailAbogadoSecundario = "El Formato del email es incorrecto"
        }
    }


    if (!valores.numeroTelefonoPrincipal) {
        errores.numeroTelefonoPrincipal = "El Numero de Telefono es obligatorio"
    }


    if (!valores.idLawyou) {
        errores.idLawyou = "El ID es obligatorio"
    }

    if (!valores.activo) {
        errores.activo = "Es obligatorio el Estado del abogado"
    }
    if (valores.entidadLegal == "Persona Jurídica") {
        if (!valores.entidadSocial) {
            errores.entidadSocial = "Es obligatorio el Estado del abogado"
        }
        if (!valores.cif) {
            errores.cif = "El CIF es obligatorio"
        } else if ((!/[0-9]{8}[a-zA-Z]{1}$/i.test(valores.cif))) {
            errores.cif = "El CIF no tiene el formato válido"
        }
    }

    return errores;
};

export default validarNuevoAbogado;