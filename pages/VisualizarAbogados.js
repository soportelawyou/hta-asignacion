import React, { useContext, useState, useEffect } from 'react';
import Link from 'next/link';
import Select from 'react-select';
import { useRouter } from 'next/router';
import Swal from 'sweetalert2';
import { divp1 } from '../components/ui/Visual'
import PaginaError from '../components/layout/PaginaError';
import Layout from '../components/layout/Layout';
import { ContenedorFormularioLg, BotonCancelar, BotonAceptar } from '../components/ui/Formulario';
import { BotonEliminar } from '../components/ui/BotonEliminar';
import usePaises from "../hooks/usePaises";
import axios from 'axios'
import {
    provincias,
    tipos,
    comunidades,
    especialidades,
    actividad,
    idiomas,
    Civil,
    Penal,
    Laboral,
    Societario,
    Extranjeria,
    Internacional,
    Nuevas_Tecnologias,
    Mercantil,
    Fiscal_y_Tributario,
    Bancario,
    Constitucional,
    Compliance,
    Comunitario,
    Administrativo
} from '../components/ui/OpcionesSelectores';

// Validaciones
import useValidacionEditar from '../hooks/useValidacionEditar';
import validarEditarAbogado from '../validacion/validarEditarAbogado';
import { Modal, Button } from 'react-bootstrap';
import { FirebaseContext } from '../firebase/index';
import { BotonMas } from '../components/ui/Visual';



export function MyVerticallyCenteredModal(props) {

    // State del componente
    const [abogado, guardarAbogado] = useState({});
    const [inputList, setInputList] = useState([{ Provincia: "", Ciudad: [] }]);
    const [error, guardarError] = useState(false);
    const [consultarDB, guardarConsultarDB] = useState(true);
    const [rol, guardarRol] = useState("");
    const [cargando, guardarCargando] = useState(false);
    const [opcionesProvincias, guardarOpcionesProvincias] = useState([]);
    const [opcionesCiudades, guardarOpcionesCiudades] = useState([]);
    // Routing para obtener el ID actual
    const router = useRouter();
    var id = props.id;
    // Context de Firebase
    const { firebase, usuario } = useContext(FirebaseContext);
    const { Paises, Auth } = usePaises()

    useEffect(() => {
        // Declaramos variable para trackear
        let estaCancelado = false;
        // console.log('useEffect [ID].js')

        if (id && consultarDB) {
            const obtenerAbogado = async () => {
                const abogadoQuery = await firebase.db.collection('abogados').doc(id);
                const abogado = await abogadoQuery.get();

                // Se ejecuta solo si esta montado
                if (!estaCancelado) {
                    if (abogado.exists) {
                        guardarAbogado(abogado.data());
                    } else {
                        guardarError(true);
                    }
                    guardarConsultarDB(false);
                }

            }
            obtenerAbogado();
        }
        return () => {
            // Ponemos la variable a true para evitar que intente actualizar un state inexistente
            estaCancelado = true;
        }
    }, [id, abogado])

    // handle input change
    const handleInputChange = (e, index, name) => {

        const list = [...lugaresEjercer];
        if (name.indexOf("Ciudad") != -1) {
            let resultados = e.map(res => res.value)
            list[index][name] = resultados;

        }
        else {
            axios(`https://www.universal-tutorial.com/api/cities/${e.value}`, {
                "method": 'GET',
                "headers": {
                    "Authorization": `Bearer ${Auth}`,
                    "Accept": "application/json",
                    "Connection": "close"
                },
            })
                .then(response => guardarOpcionesCiudades(Convertir(response.data)))

            list[index][name] = e.value;
        }
        lugaresEjercer=list
        console.log(list)
    };

    // handle click event of the Remove button
    const handleRemoveClick = index => {
        const list = [...inputList];
        list.splice(index, 1);
        setInputList(list);
    };

    // handle click event of the Add button
    const handleAddClick = () => {
        setInputList([...inputList, { Provincia: "", Ciudad: [] }]);
    };


    const busqueda = async (email) => {
        await firebase.db.collection('usuarios').where("email", "==", email).onSnapshot(manejarSnapShot);
    }

    function manejarSnapShot(snapshot) {
        const usuarioBD = snapshot.docs.map(doc => {
            return {
                id: doc.id,
                ...doc.data()
            }
        })
        if (usuarioBD[0] !== undefined) {
            // console.log("USUARIO DB", usuarioBD[0])
            console.log("entre en el guardar usuarios");
            console.log(usuarioBD)
            guardarRol(usuarioBD[0].rol);
        }
        guardarCargando(false)
    }

    useEffect(() => {
        guardarCargando(true)
        if (usuario !== undefined && usuario !== null) {
            console.log(usuario.email)
            busqueda(usuario.email)
        }
    }, [usuario])

    useEffect(() => {
        axios(`https://www.universal-tutorial.com/api/states/${pais}`, {
            "method": 'GET',
            "headers": {
                "Authorization": `Bearer ${Auth}`,
                "Accept": "application/json",
                "Connection": "close"
            },
        })
            .then(response => guardarOpcionesProvincias(Convertir(response.data)))


    }, [pais]);

    useEffect(() => {
        axios(`https://www.universal-tutorial.com/api/cities/${provincia}`, {
            "method": 'GET',
            "headers": {
                "Authorization": `Bearer ${Auth}`,
                "Accept": "application/json",
                "Connection": "close"
            },
        })
            .then(response => guardarOpcionesCiudades(Convertir(response.data)))


    }, [provincia]);


    // if (Object.keys(producto).length === 0) return <p>Cargando...</p>

    const dniOriginal = abogado.dni;
    const [grupoSubEspecialidades, guardarGrupoSubEspecialidades] = useState([]);

    const { valores, errores, handleSubmit, handleChange, handleChangeTipo, handleChangeProvincia, handleChangePais, handleChangeCiudad, handleChangeComunidad, handleChangeEspecialidad, handleChangeSubEspecialidad, handleChangeActividad, handleChangeIdioma } = useValidacionEditar(abogado, dniOriginal, validarEditarAbogado, editarAbogado);


    // const { tipo, nombre, apellidos, idLawyou, dni, direccion, calle, provincia, ciudad, comunidadAutonoma, cp, creado, creador, contadorAsuntos, contadorRechazados } = abogado;


    // Destructuring de los valores
    var { tipo, nombre, comunidadAutonoma, apellidos, entidadLegal,creado,creador, comentarios, idLawyou, lugaresEjercer, dni, calle, entidadSocial, provincia, especialidadesAbogado, subEspecialidadesAbogados, ciudad, pais, cp, activo, contadorAsuntos, contadorAceptados, emailAbogado, emailAbogadoSecundario, numeroTelefonoPrincipal, numeroTelefonoSecundario, contadorRechazados, ultimaOportunidad, idioma } = valores;



    const valorProvincia = { label: provincia, value: provincia }
    const valorCiudad = { label: ciudad, value: ciudad }
    const valorTipo = tipos.find(tip => tip.value === tipo);
    const valorComunidad = comunidades.find(comunidad => comunidad.value === comunidadAutonoma);
    const valoractividad = actividad.find(actividad => actividad.value === activo);
    // const valorEspecialidades = especialidades.filter(especialidad => especialidad.value === especialidadesAbogado);
    const valorEspecialidades = [];
    const valorPais = { label: pais, value: pais }
    if (especialidadesAbogado !== undefined) {
        // console.log(especialidadesAbogado)
        for (let i = 0; i < especialidadesAbogado.length; i++) {
            valorEspecialidades.push(especialidades.find(especialidad => especialidad.value === especialidadesAbogado[i]))
        }
    }
    const valorSubEspecialidades = [];
    if (subEspecialidadesAbogados !== undefined) {
        // console.log(especialidadesAbogado)
        for (let i = 0; i < subEspecialidadesAbogados.length; i++) {
            let aProbar = Civil.find(subEspecialidad => subEspecialidad.value === subEspecialidadesAbogados[i])
            if (aProbar != undefined) { valorSubEspecialidades.push(aProbar) }
            aProbar = Penal.find(subEspecialidad => subEspecialidad.value === subEspecialidadesAbogados[i])
            if (aProbar != undefined) { valorSubEspecialidades.push(aProbar) }
            aProbar = Laboral.find(subEspecialidad => subEspecialidad.value === subEspecialidadesAbogados[i])
            if (aProbar != undefined) { valorSubEspecialidades.push(aProbar) }
            aProbar = Societario.find(subEspecialidad => subEspecialidad.value === subEspecialidadesAbogados[i])
            if (aProbar != undefined) { valorSubEspecialidades.push(aProbar) }
            aProbar = Extranjeria.find(subEspecialidad => subEspecialidad.value === subEspecialidadesAbogados[i])
            if (aProbar != undefined) { valorSubEspecialidades.push(aProbar) }
            aProbar = Internacional.find(subEspecialidad => subEspecialidad.value === subEspecialidadesAbogados[i])
            if (aProbar != undefined) { valorSubEspecialidades.push(aProbar) }
            aProbar = Nuevas_Tecnologias.find(subEspecialidad => subEspecialidad.value === subEspecialidadesAbogados[i])
            if (aProbar != undefined) { valorSubEspecialidades.push(aProbar) }
            aProbar = Mercantil.find(subEspecialidad => subEspecialidad.value === subEspecialidadesAbogados[i])
            if (aProbar != undefined) { valorSubEspecialidades.push(aProbar) }
            aProbar = Fiscal_y_Tributario.find(subEspecialidad => subEspecialidad.value === subEspecialidadesAbogados[i])
            if (aProbar != undefined) { valorSubEspecialidades.push(aProbar) }
            aProbar = Bancario.find(subEspecialidad => subEspecialidad.value === subEspecialidadesAbogados[i])
            if (aProbar != undefined) { valorSubEspecialidades.push(aProbar) }
            aProbar = Constitucional.find(subEspecialidad => subEspecialidad.value === subEspecialidadesAbogados[i])
            if (aProbar != undefined) { valorSubEspecialidades.push(aProbar) }
            aProbar = Compliance.find(subEspecialidad => subEspecialidad.value === subEspecialidadesAbogados[i])
            if (aProbar != undefined) { valorSubEspecialidades.push(aProbar) }
            aProbar = Comunitario.find(subEspecialidad => subEspecialidad.value === subEspecialidadesAbogados[i])
            if (aProbar != undefined) { valorSubEspecialidades.push(aProbar) }
            aProbar = Administrativo.find(subEspecialidad => subEspecialidad.value === subEspecialidadesAbogados[i])
            if (aProbar != undefined) { valorSubEspecialidades.push(aProbar) }
        }

    }

    const valorIdioma = [];
    if (idioma !== undefined) {
        // console.log(especialidadesAbogado)
        for (let i = 0; i < idioma.length; i++) {
            valorIdioma.push(idiomas.find(idio => idio.value === idioma[i]))
        }
    }
    else {
        idioma = [];
    }

    //Validacion
    console.log(numeroTelefonoPrincipal)
    if (contadorAceptados === undefined) {
        contadorAceptados = 0;
    }
    if (numeroTelefonoPrincipal === undefined) {
        numeroTelefonoPrincipal = '';
    }
    if (numeroTelefonoSecundario === undefined) {
        numeroTelefonoSecundario = '';
    }
    if (emailAbogadoSecundario === undefined) {
        emailAbogadoSecundario = '';
    }
    if (activo === undefined) {
        activo = '';
    }

    useEffect(() => {
        var asubespecialidades = []
        if (especialidadesAbogado !== undefined) {
            if (especialidadesAbogado.length != 0) {
                especialidadesAbogado.map(e => {
                    if (e == "Civil") asubespecialidades = asubespecialidades.concat(Civil)
                    else if (e == "Penal") asubespecialidades = asubespecialidades.concat(Penal)
                    else if (e == "Societario") asubespecialidades = asubespecialidades.concat(Societario)
                    else if (e == "Laboral") asubespecialidades = asubespecialidades.concat(Laboral)
                    else if (e == "Extranjeria") asubespecialidades = asubespecialidades.concat(Extranjeria)
                    else if (e == "Internacional") asubespecialidades = asubespecialidades.concat(Internacional)
                    else if (e == "Nuevas_Tecnologias") asubespecialidades = asubespecialidades.concat(Nuevas_Tecnologias)
                    else if (e == "Mercantil") asubespecialidades = asubespecialidades.concat(Mercantil)
                    else if (e == "Fiscal_y_Tributario") asubespecialidades = asubespecialidades.concat(Fiscal_y_Tributario)
                    else if (e == "Bancario") asubespecialidades = asubespecialidades.concat(Bancario)
                    else if (e == "Constitucional") asubespecialidades = asubespecialidades.concat(Constitucional)
                    else if (e == "Compliance") asubespecialidades = asubespecialidades.concat(Compliance)
                    else if (e == "Comunitario") asubespecialidades = asubespecialidades.concat(Comunitario)
                    else if (e == "Administrativo") asubespecialidades = asubespecialidades.concat(Administrativo)

                })
                guardarGrupoSubEspecialidades(asubespecialidades.sort())
            }
        }
    }, [especialidadesAbogado])


    async function editarAbogado() {
        if (!usuario) {
            return router.push('/iniciar-sesion');
        }

        // Creamos el objeto de abogado
        const abogado = {
            tipo,
            nombre,
            apellidos,
            idLawyou,
            dni,
            calle,
            provincia,
            ciudad,
            emailAbogado,
            emailAbogadoSecundario,
            numeroTelefonoPrincipal,
            numeroTelefonoSecundario,
            direccion: `${calle}, ${ciudad}, ${provincia}, ${comunidadAutonoma} - ${cp}`,
            comunidadAutonoma,
            cp,
            activo,
            idioma,
            especialidadesAbogado,
            actualizado: Date.now(),
            actualizaUsuario: {
                id: usuario.uid,
                nombre: usuario.displayName
            },
            creado,
            creador,
            lugaresEjercer,
            contadorAsuntos,
            contadorAceptados,
            contadorRechazados,
            ultimaOportunidad
        };

        try {
            if (abogado) {
                // Actualiza en la bbdd
                await firebase.db.collection('abogados').doc(id).update(abogado);
                console.log('Editando abogado', abogado)
            }
            Swal.fire({
                icon: 'success',
                title: 'El abogado se editó correctamente!',
                showConfirmButton: false,
                timer: 1200
            })

            router.push('/abogados')
            window.location.reload(true);
        } catch (error) {
            console.log(error)
            guardarError(error)
        }


    };
    return (
        <Modal
            {...props}
            size="xl"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    <h1> Editar Abogado <span>{nombre} {apellidos}</span></h1>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div>

                    {error ? <PaginaError msg="Vaya... Parece que ha habido un error al editar el abogado, vuelve a intentarlo por favor"></PaginaError> : Object.keys(valores).length === 0 ? <p>Cargando...</p> : (

                        usuario ? (
                            <>
                                <form
                                    onSubmit={handleSubmit}
                                >
                                    <ContenedorFormularioLg>
                                        <fieldset>
                                            <legend>Datos Personales </legend>
                                            <div className="col-12 d-flex mt-2">
                                                <div className="col-4">
                                                    <label htmlFor="tipo">Tipo *</label>
                                                    <Select
                                                        onChange={handleChangeTipo}
                                                        options={tipos}
                                                        name="tipo"
                                                        placeholder="Selecciona Tipo"
                                                        defaultValue={valorTipo}
                                                    />
                                                </div>
                                                <div className="col-4">
                                                    <label htmlFor="idLawyou">ID Lawyou *</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="idLawyou"
                                                        onChange={handleChange}
                                                        defaultValue={idLawyou}
                                                        placeholder="Ej. A050"
                                                    />
                                                </div>
                                                <div className="col-4">
                                                    <label htmlFor="dni">DNI *</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="dni"
                                                        onChange={handleChange}
                                                        defaultValue={dni}
                                                        placeholder="DNI del Abogado"
                                                    />
                                                    <small>Ej. 12345678A</small>
                                                    {errores.dni && <p className="alert alert-danger text-center p-2 mt-2">{errores.dni}</p>}
                                                    {errores.dniDuplicado && <p className="alert alert-danger text-center p-2 mt-2">{errores.dniDuplicado}</p>}
                                                </div>

                                            </div>

                                            <div className="col-12 d-flex abajo">
                                                <div className="col-4">
                                                    <label htmlFor="nombre">Nombre *</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="nombre"
                                                        onChange={handleChange}
                                                        defaultValue={nombre}
                                                        placeholder="Nombre del abogado"
                                                    />
                                                    {errores.nombre && <p className="alert alert-danger text-center p-2 mt-2">{errores.nombre}</p>}
                                                </div>
                                                <div className="col-4">
                                                    <label htmlFor="apellidos">Apellidos *</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="apellidos"
                                                        onChange={handleChange}
                                                        defaultValue={apellidos}
                                                        placeholder="Apellidos del abogado"
                                                    />
                                                    {errores.apellidos && <p className="alert alert-danger text-center p-2 mt-2">{errores.apellidos}</p>}
                                                </div>



                                            </div>
                                            <div className="col-12 d-flex abajo">
                                                <div className="col-4">
                                                    <label htmlFor="especialidades">Especialidades *</label>
                                                    <Select
                                                        name="especialidades"
                                                        options={especialidades}
                                                        isMulti
                                                        onChange={handleChangeEspecialidad}
                                                        placeholder="Selecciona Especialidades"
                                                        defaultValue={valorEspecialidades}
                                                    />
                                                    {errores.especialidades && <p className="alert alert-danger text-center p-2 mt-2">{errores.especialidades}</p>}
                                                </div>


                                                <div className="col-4">
                                                    <label htmlFor="subEspecialidades">Sub-Especialidades *</label>
                                                    <Select
                                                        name="subEspecialidades"
                                                        options={grupoSubEspecialidades}
                                                        isMulti
                                                        onChange={handleChangeSubEspecialidad}
                                                        placeholder="Selecciona Especialidades"
                                                        defaultValue={valorSubEspecialidades}
                                                    />
                                                    {errores.subEspecialidades && <p className="alert alert-danger text-center p-2 mt-2">{errores.subEspecialidades}</p>}
                                                </div>
                                            </div>
                                            <div className="col-12 d-flex abajo">
                                                <div className="col-4">
                                                    <label htmlFor="numeroTelefonoPrincipal">Numero Telefono Principal *</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="numeroTelefonoPrincipal"
                                                        onChange={handleChange}
                                                        defaultValue={numeroTelefonoPrincipal}
                                                        placeholder="Numero de Telefono Principal"
                                                    />
                                                    {errores.numeroTelefonoPrincipal && <p className="alert alert-danger text-center p-2 mt-2">{errores.numeroTelefonoPrincipal}</p>}
                                                </div>

                                                <div className="col-4">
                                                    <label htmlFor="numeroTelefonoSecundario">Numero Telefono Secundario</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="numeroTelefonoSecundario"
                                                        onChange={handleChange}
                                                        defaultValue={numeroTelefonoSecundario}
                                                        placeholder="Numero de Telefono Secundario"
                                                    />
                                                    {errores.numeroTelefonoSecundario && <p className="alert alert-danger text-center p-2 mt-2">{errores.numeroTelefonoSecundario}</p>}
                                                </div>
                                                <div className="col-4">
                                                    <label htmlFor="idioma"> Idioma *</label>
                                                    <Select
                                                        name="idioma"
                                                        options={idiomas}
                                                        isMulti
                                                        className="basic-multi-select"
                                                        classNamePrefix="select"
                                                        onChange={handleChangeIdioma}
                                                        placeholder="Selecciona Idioma"
                                                        defaultValue={valorIdioma}
                                                    />
                                                    {errores.idioma && <p className="alert alert-danger text-center p-2 mt-2">{errores.idioma}</p>}
                                                </div>

                                            </div>
                                            <div className="col-12 d-flex abajo">
                                                <div className="col-4">
                                                    <label htmlFor="emailAbogadoLawyou">Email Lawyou *</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="emailAbogadoLawyou"
                                                        onChange={handleChange}
                                                        defaultValue={emailAbogado}
                                                        placeholder="Email Lawyou"
                                                    />
                                                    {errores.emailAbogado && <p className="alert alert-danger text-center p-2 mt-2">{errores.emailAbogado}</p>}
                                                </div>

                                                <div className="col-4">
                                                    <label htmlFor="emailAbogadoSecundario">Email Secundario</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="emailAbogadoSecundario"
                                                        onChange={handleChange}
                                                        defaultValue={emailAbogadoSecundario}
                                                        placeholder="Email Secundario"
                                                    />
                                                    {errores.emailAbogadoSecundario && <p className="alert alert-danger text-center p-2 mt-2">{errores.emailAbogadoSecundario}</p>}
                                                </div>

                                            </div>
                                        </fieldset>

                                        <fieldset className="mt-5">
                                            <legend>Datos Facturación</legend>
                                            {entidadLegal === "Persona Jurídica" ? (
                                                <div className="col-12 d-flex mt-2 justify-content-between">
                                                    <div className="col-6">
                                                        <label htmlFor="entidadSocial">Entidad Social *</label>
                                                        <input
                                                            type="text"
                                                            className="form-control"
                                                            name="entidadSocial"
                                                            onChange={handleChange}
                                                            value={entidadSocial}
                                                            defaultValue={entidadSocial}
                                                            placeholder="Ej. Sevilla"
                                                        />
                                                        {errores.entidadSocial && <p className="alert alert-danger text-center p-2 mt-2">{errores.entidadSocial}</p>}
                                                    </div>
                                                </div>
                                            ) : null}

                                            <div className="col-12 d-flex mt-2 justify-content-between">
                                                <div className="col-4">
                                                    <label htmlFor="Pais">Pais *</label>
                                                    <Select
                                                        onChange={handleChangePais}
                                                        options={Paises}
                                                        defaultValue={valorPais}
                                                        name="provincia"
                                                        placeholder="Selecciona Pais"
                                                    />
                                                    {errores.pais && <p className="alert alert-danger text-center p-2 mt-2">{errores.pais}</p>}
                                                </div>
                                                <div className="col-4">
                                                    <label htmlFor="Provincia">Provincia *</label>
                                                    <Select
                                                        onChange={handleChangeProvincia}
                                                        options={opcionesProvincias}
                                                        defaultValue={valorProvincia}
                                                        name="provincia"
                                                        placeholder="Selecciona Pais"
                                                    />
                                                    {errores.provincia && <p className="alert alert-danger text-center p-2 mt-2">{errores.provincia}</p>}
                                                </div>
                                                <div className="col-4">
                                                    <label htmlFor="ciudad">Ciudad *</label>
                                                    <Select
                                                        onChange={handleChangeCiudad}
                                                        options={opcionesCiudades}
                                                        name="ciudad"
                                                        defaultValue={valorCiudad}
                                                        placeholder="Selecciona Pais"
                                                    />
                                                    {errores.ciudad && <p className="alert alert-danger text-center p-2 mt-2">{errores.ciudad}</p>}
                                                </div>
                                            </div>
                                            <div className="col-12 d-flex mt-2 justify-content-between">
                                                <div className="col-6">
                                                    <label htmlFor="calle">Calle *</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="calle"
                                                        onChange={handleChange}
                                                        value={calle}
                                                        placeholder="Ej. Calle de Miramon Nº9, 3-A"
                                                    />
                                                    {errores.calle && <p className="alert alert-danger text-center p-2 mt-2">{errores.calle}</p>}
                                                </div>

                                                <div className="col-4">
                                                    <label htmlFor="cp">Código Postal *</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="cp"
                                                        onChange={handleChange}
                                                        value={cp}
                                                        placeholder="Ej. 20532"
                                                    />
                                                    {errores.cp && <p className="alert alert-danger text-center p-2 mt-2">{errores.cp}</p>}
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset className="mt-5">
                                            <legend> Zona Geografica de Actuación </legend>
                                            <div className="col-12 d-flex mt-2 justify-content-between">
                                                <div className="col-12">
                                                    <BotonMas
                                                        type="button"
                                                        onClick={handleAddClick}
                                                    > Añadir Localización
                                                </BotonMas>
                                                </div>
                                            </div>

                                            {lugaresEjercer.map((x, i) => {
                                                return (
                                                    <>
                                                        <div className="col-12 d-flex mt-2 justify-content-between">
                                                            <div className="col-4">
                                                                <label htmlFor="Provincia">Provincia</label>
                                                                <Select
                                                                    options={opcionesProvincias}
                                                                    name="provincia"
                                                                    placeholder="Introduce provincia"
                                                                    onChange={e => handleInputChange(e, i, `Provincia`)}
                                                                    defaultValue={{ value: x.Provincia, label: x.Provincia }}
                                                                />
                                                            </div>
                                                            {x.Ciudad !== undefined ? (
                                                                <div className="col-4">
                                                                    <label htmlFor="Ciudad">Ciudad</label>
                                                                    <Select
                                                                        options={opcionesCiudades}
                                                                        name="ciudad"
                                                                        isMulti
                                                                        className="basic-multi-select"
                                                                        classNamePrefix="select"
                                                                        placeholder="Introduce Ciudad"
                                                                        onChange={e => handleInputChange(e, i, `Ciudad`)}
                                                                        defaultValue={x.Ciudad.map(e => { return { value: e, label: e } })}
                                                                    />
                                                                </div>
                                                            ) : (
                                                                    <div className="col-4">
                                                                        <label htmlFor="Ciudad">Ciudad</label>
                                                                        <Select
                                                                            options={opcionesCiudades}
                                                                            name="ciudad"
                                                                            isMulti
                                                                            className="basic-multi-select"
                                                                            classNamePrefix="select"
                                                                            placeholder="Introduce Ciudad"
                                                                            onChange={e => handleInputChange(e, i, `Ciudad`)}
                                                                        />
                                                                    </div>)}

                                                            <div className="col-4">
                                                                {inputList.length !== 1 && <BotonRemove
                                                                    type="button"
                                                                    onClick={() => handleRemoveClick(i)}>Remove</BotonRemove>}
                                                            </div>
                                                        </div>
                                                    </>
                                                );
                                            })}


                                        </fieldset>

                                        <fieldset className="mt-5">
                                            <legend>Opciones</legend>
                                            <div className="col-12 d-flex mt-2 justify-content-between">
                                                <div className="col-4">
                                                    <label htmlFor="activo">Activo *</label>
                                                    <Select
                                                        onChange={handleChangeActividad}
                                                        options={actividad}
                                                        name="activo"
                                                        placeholder="actividad"
                                                        defaultValue={valoractividad}

                                                    />
                                                </div>
                                            </div>
                                        </fieldset>

                                    </ContenedorFormularioLg>
                                    <div className="col-12 d-flex mt-2 justify-content-between">
                                        <label> * marca los campos obligatorios</label>
                                    </div>

                                    <div className="col-12 d-flex justify-content-center">
                                        {error && <p className="alert alert-danger text-center p2 mt-2 mb-2">{error}</p>}
                                        <BotonAceptar
                                            className="btn btn-primary"
                                            type="submit"
                                        >Guardar Cambios</BotonAceptar>
                                    </div>
                                </form>



                            </>
                        ) :
                            <>
                                <PaginaError msg={`Tienes que estar logueado para acceder a esta sección.`}></PaginaError>
                                <div className="text-center">
                                    <Link href="/iniciar-sesion"><a><h1>Inicia Sesión</h1></a></Link>
                                </div>
                            </>

                    )}


                </div>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={props.onHide}>Close</Button>
            </Modal.Footer>
        </Modal >

    )
}
export default MyVerticallyCenteredModal;


