
import {Modal,Form,Button} from 'react-bootstrap';
import React, { useContext, useState } from 'react';
import Link from 'next/link';
import Select from 'react-select';
import Router from 'next/router';
import Swal from 'sweetalert2';

import PaginaError from '../components/layout/PaginaError';
import { ContenedorFormularioLg, BotonAceptar } from '../components/ui/Formulario';
import {
    provincias,
    tipoterceros,
    comunidades,
    actividad,
    idiomas
} from '../components/ui/OpcionesSelectores';

// Validaciones
import useValidacion from '../hooks/useValidacion';
import validarNuevoTerceros from '../validacion/validarNuevoTerceros';

import { FirebaseContext } from '../firebase/index';
import firebase from '../firebase/index';

const STATE_INICIAL = {
    tipo: '',
    nombre: '',
    apellidos: '',
    idLawyou: '',
    dni: '',
    calle: '',
    provincia: '',
    ciudad: '',
    comunidadAutonoma: '',
    cp: '',
    idioma: [],
    activo: '',
    emailterceros: '',
    numeroTelefono: '',
    dniDuplicado: false,
    contadorAsuntos: 0,
    contadorRechazados: 0,
    ultimaOportunidad: ''
}

export function MyVerticallyCenteredModal(props) {
    
    const [error, guardarError] = useState(false);

    const { valores, errores, handleSubmit, handleChange, handleChangeTipo, handleChangeProvincia, handleChangeComunidad,handleChangeActividad,handleChangeIdioma} = useValidacion(STATE_INICIAL, validarNuevoTerceros, nuevoTerceros);


    // Destructuring de los valores
    const { tipo, nombre, apellidos, idLawyou, dni, calle,idioma, provincia, ciudad, comunidadAutonoma, cp,activo, contadorAsuntos, emailTerceros, numeroTelefono, contadorRechazados, ultimaOportunidad } = valores;


    const { usuario } = useContext(FirebaseContext);


    async function nuevoTerceros() {
        if (!usuario) {
            return Router.push('/iniciar-sesion');
        }
        // const direccion = `${calle}, ${ciudad}, ${provincia}, ${comunidadAutonoma} - ${cp}`;

        // Creamos el objeto de abogado
        const tercero = {
            tipo,
            nombre,
            apellidos,
            idLawyou,
            dni,
            calle,
            provincia,
            ciudad,
            emailTerceros,
            numeroTelefono,
            direccion: `${calle}, ${ciudad}, ${provincia}, ${comunidadAutonoma} - ${cp}`,
            comunidadAutonoma,
            cp,
            activo,
            idioma,
            creado: Date.now(),
            creador: {
                id: usuario.uid,
                nombre: usuario.displayName
            },
            contadorAsuntos,
            contadorRechazados,
            ultimaOportunidad
        };

        try {
            await firebase.db.collection('terceros').add(tercero);
            Swal.fire({
                // position: 'top-end',
                icon: 'success',
                title: 'El ' + tipo + ' se creó correctamente!',
                showConfirmButton: false,
                timer: 1200
            })
            setTimeout(() => {
                Router.push('/terceros');
                window.location.reload(true);
            }, 1200);
        } catch (error) {
            console.log(error);
            guardarError(error);
        }
    };

    
    return (
      <Modal
        {...props}
        size="xl"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
           <h1>Nuevo Tercero</h1>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <div>
                {usuario ? (
                    <>
                        <form
                            onSubmit={handleSubmit}
                        >
                            <ContenedorFormularioLg>
                                <fieldset>
                                    <legend>Datos Personales</legend>
                                    <div className="col-12 d-flex mt-3">
                                        <div className="col-4">
                                            <label htmlFor="tipo">Tipo *</label>
                                            <Select
                                                onChange={handleChangeTipo}
                                                options={tipoterceros}
                                                name="tipo"
                                                placeholder="Selecciona Tipo"
                                            />
                                            {errores.tipo && <p className="alert alert-danger text-center p-2 mt-2">{errores.tipo}</p>}
                                        </div>
                                        <div className="col-4">
                                            <label htmlFor="idLawyou">ID Lawyou *</label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                name="idLawyou"
                                                onChange={handleChange}
                                                value={idLawyou}
                                                placeholder="Ej. A050"
                                            />
                                            {errores.idLawyou && <p className="alert alert-danger text-center p-2 mt-2">{errores.idLawyou}</p>}
                                        </div>
                                        <div className="col-4">
                                            <label htmlFor="dni">DNI *</label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                name="dni"
                                                onChange={handleChange}
                                                value={dni}
                                                placeholder="DNI del Tercero"
                                            />
                                            <small>Ej. 12345678A</small>
                                            {errores.dni && <p className="alert alert-danger text-center p-2 mt-2">{errores.dni}</p>}
                                            {errores.dniDuplicado && <p className="alert alert-danger text-center p-2 mt-2">{errores.dniDuplicado}</p>}
                                        </div>

                                    </div>

                                    <div className="col-12 d-flex abajo">
                                        <div className="col-4">
                                            <label htmlFor="nombre">Nombre *</label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                name="nombre"
                                                onChange={handleChange}
                                                value={nombre}
                                                placeholder="Nombre de Tercero"
                                            />
                                            {errores.nombre && <p className="alert alert-danger text-center p-2 mt-2">{errores.nombre}</p>}
                                        </div>
                                        <div className="col-4">
                                            <label htmlFor="apellidos">Apellidos *</label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                name="apellidos"
                                                onChange={handleChange}
                                                value={apellidos}
                                                placeholder="Apellidos del Tercero"
                                            />
                                            {errores.apellidos && <p className="alert alert-danger text-center p-2 mt-2">{errores.apellidos}</p>}
                                        </div>

                                    </div>

                                    <div className="col-12 d-flex abajo">
                                        
                                        <div className="col-4">
                                            <label htmlFor="numeroTelefono">Numero Telefono *</label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                name="numeroTelefono"
                                                onChange={handleChange}
                                                value={numeroTelefono}
                                                placeholder="Numero de telefono"
                                            />
                                            {errores.numeroTelefono && <p className="alert alert-danger text-center p-2 mt-2">{errores.numeroTelefono}</p>}
                                        </div>
                                        <div className="col-4">
                                            <label htmlFor="idioma"> Idioma *</label>
                                            <Select
                                                name="idioma"
                                                options={idiomas}
                                                isMulti
                                                className="basic-multi-select"
                                                classNamePrefix="select"
                                                onChange={handleChangeIdioma}
                                                placeholder="Selecciona Especialidades"
                                            />
                                            {errores.idioma && <p className="alert alert-danger text-center p-2 mt-2">{errores.idioma}</p>}
                                        </div>
                                    </div>
                                    <div className="col-12 d-flex abajo">
                                    <div className="col-4">
                                            <label htmlFor="emailTerceros">Email *</label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                name="emailTerceros"
                                                onChange={handleChange}
                                                value={emailTerceros}
                                                placeholder="Email"
                                            />
                                            {errores.emailTerceros && <p className="alert alert-danger text-center p-2 mt-2">{errores.emailTerceros}</p>}
                                        </div>
                                    </div>




                                </fieldset>

                                <fieldset className="mt-3">
                                    <legend>Dirección</legend>
                                    <div className="col-12 d-flex mt-2 justify-content-between">
                                        <div className="col-6">
                                            <label htmlFor="calle">Calle *</label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                name="calle"
                                                onChange={handleChange}
                                                value={calle}
                                                placeholder="Ej. Calle de Miramon Nº9, 3-A"
                                            />
                                            {errores.calle && <p className="alert alert-danger text-center p-2 mt-2">{errores.calle}</p>}
                                        </div>

                                        <div className="col-6">
                                            <label htmlFor="ciudad">Ciudad *</label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                name="ciudad"
                                                onChange={handleChange}
                                                value={ciudad}
                                                placeholder="Ej. Sevilla"
                                            />
                                            {errores.ciudad && <p className="alert alert-danger text-center p-2 mt-2">{errores.ciudad}</p>}
                                        </div>
                                    </div>

                                    <div className="col-12 d-flex  abajo">
                                        <div className="col-4">
                                            <label htmlFor="provincia">Provincia *</label>
                                            <Select
                                                onChange={handleChangeProvincia}
                                                options={provincias}
                                                name="provincia"
                                                placeholder="Selecciona Provincia"
                                            />
                                            {errores.provincia && <p className="alert alert-danger text-center p-2 mt-2">{errores.provincia}</p>}
                                        </div>
                                        <div className="col-4">
                                            <label htmlFor="comunidadAutonoma">Comunidad Autónoma *</label>
                                            <Select
                                                onChange={handleChangeComunidad}
                                                options={comunidades}
                                                name="comunidadAutonoma"
                                                placeholder="Selecciona Comunidad Autonoma"
                                            />
                                            {errores.comunidadAutonoma && <p className="alert alert-danger text-center p-2 mt-2">{errores.comunidadAutonoma}</p>}
                                        </div>
                                        <div className="col-4">
                                            <label htmlFor="cp">Código Postal</label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                name="cp"
                                                onChange={handleChange}
                                                value={cp}
                                                placeholder="Ej. 20532"
                                            />
                                            {errores.cp && <p className="alert alert-danger text-center p-2 mt-2">{errores.cp}</p>}
                                        </div>
                                    </div>
                                </fieldset>

                                <fieldset className="mt-3">
                                    <legend>Opciones</legend>
                                    <div className="col-12 d-flex mt-2 justify-content-between">
                                        <div className="col-4">
                                        <label htmlFor="activo">Activo: </label>
                                            <Select
                                                onChange={handleChangeActividad}
                                                options={actividad}
                                                name="activo"
                                                placeholder="actividad"
                                            />
                                            {errores.actividad && <p className="alert alert-danger text-center p-2 mt-2">{errores.actividad}</p>}
                                        </div>
                                    </div>
                                </fieldset>

                            </ContenedorFormularioLg>

                            <div className="col-12 d-flex justify-content-center">
                                {error && <p className="alert alert-danger text-center p2 mt-2 mb-2">{error}</p>}
                                <BotonAceptar
                                    className="btn btn-primary"
                                    type="submit"
                                >Crear</BotonAceptar>
                            </div>
                        </form>

                    </>
                ) :
                    <>
                        <PaginaError msg={`Tienes que estar logueado para acceder a esta sección.`}></PaginaError>
                        <div className="text-center">
                            <Link href="/iniciar-sesion"><a><h1>Inicia Sesión</h1></a></Link>
                        </div>
                    </>
                }
        </div>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }
  export default MyVerticallyCenteredModal;