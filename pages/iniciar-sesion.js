import React, { useState } from 'react';
import Layout from '../components/layout/Layout';
import Router from 'next/router';
import { css } from '@emotion/core';
import { ContenedorFormulario, Error } from '../components/ui/Formulario';
import firebase from '../firebase/index';

// Validaciones
import useValidacion from '../hooks/useValidacion';
import validarIniciarSesion from '../validacion/validarIniciarSesion';

const STATE_INICIAL = {
    email: '',
    password: '',
}

const IniciarSesion = () => {

    const [error, guardarError] = useState(false);

    const { valores, errores, handleSubmit, handleChange} = useValidacion(STATE_INICIAL, validarIniciarSesion, iniciarSesion);

    const {email, password} = valores;

   async function iniciarSesion(){
       try {
         console.log('intentando')
         await firebase.login(email, password);
         console.log('Estas dentro')
         Router.push('/');
       } catch (error) {
         console.log(error);
         if(error.code === "auth/wrong-password" || error.code === "auth/user-not-found"){
             guardarError("Usuario o contraseña incorrectos")
         }else{
            guardarError(error.message);

         }
       }
   }

    return (
        <div>
            <Layout>
                <>
                    <h1
                        css={css`
                            text-align: center;
                        `}
                    >Iniciar Sesión</h1>
                    <form
                        onSubmit={handleSubmit}
                        noValidate
                    >
                        <ContenedorFormulario >
                            <fieldset>
                               
                                <div className="form-group col-12">
                                    <label htmlFor="email">Email</label>
                                    <input
                                        type="email"
                                        className="form-control"
                                        placeholder="Introduce tu Email"
                                        id="email"
                                        name="email"
                                        value={email}
                                        onChange={handleChange}
                                    />
                                </div>

                                {errores.email && (
                                     <div className="form-group col-12">
                                         <label></label>
                                         <p className="alert alert-danger text-center w-100">{errores.email}</p>
                                     </div>)}

                                <div className="form-group col-12">
                                    <label htmlFor="password">Password</label>
                                    <input
                                        type="password"
                                        className="form-control"
                                        id="password"
                                        name="password"
                                        value={password}
                                        placeholder="Introduce tu Contraseña"
                                        onChange={handleChange}
                                    />
                                </div>
                                
                                {errores.password && (
                                     <div className="form-group col-12">
                                         <label></label>
                                         <p className="alert alert-danger text-center w-100">{errores.password}</p>
                                     </div>)}

                                {error && (
                                <div className="form-group col-12">
                                    <label></label>
                                    <p className="alert alert-danger text-center w-100">{error}</p>
                                </div>)}

                                <button
                                    type="submit"
                                    className="btn btn-primary btn-block"
                                >Iniciar Sesión</button>
                            </fieldset>
                            
                        </ContenedorFormulario>
                    </form>
                </>
            </Layout>
        </div>
    )
}

export default IniciarSesion;
