// React
import React, { useContext, useState, useEffect } from "react";
import Router from "next/router";
import Link from "next/link";
import Select from "react-select";
import Swal from "sweetalert2";
import emailjs from "emailjs-com";
// Components
import {
  ContenedorFormularioLg,
  BotonAceptar,
} from "../components/ui/Formulario";
import PaginaError from "../components/layout/PaginaError";
import {
  provincias,
  especialidades,
  comunidadesConProvincias,
  Tipo_Servicio,
  Servicio,
  aTodo,
  motivosRechazo,
  Tipo,
  Civil,
  Penal,
  Laboral,
  Societario,
  Extranjeria,
  Internacional,
  Nuevas_Tecnologias,
  Mercantil,
  Fiscal_y_Tributario,
  Bancario,
  Constitucional,
  Compliance,
  Comunitario,
  Administrativo,
  Precios,
  SiNo,
  Canal,
  SubCanal,
} from "../components/ui/OpcionesSelectores";
import { SinResultados } from "../components/ui/SinResultados";
import SeleccionarAbogado from "../components/layout/SeleccionarAbogado";
import SeleccionarAbogadoPresupuesto from "../components/layout/SeleccionarAbogadoPresupuesto";
import {
  InputTextOportunidades,
  InputSubmitSugerencias,
} from "../components/ui/Busqueda";
import { Radio } from "../components/ui/Boton";
import {
  Generales,
  Erte,
  Despidos,
  NegligenciaMedica,
  Herencia,
  Divorcio,
  Desahucio,
  LeySegundaOportunidad,
  ResponsabilidadPatrimonialEstado,
  Fijezas,
  PropiedadIntelectual,
  ViciosOcultos,
} from "../components/ui/Preguntas";
// Hooks
import useAbogadosSugerencia from "../hooks/useAbogadosSugerencia";
import useContadorOportunidades from "../hooks/useContadorOportunidades";
import usePaises from "../hooks/usePaises";
// Validaciones
import useValidacionOportunidades from "../hooks/useValidacionOportunidades";
import validarNuevaOportunidad from "../validacion/validarNuevaOportunidad";
// Firebase
import { FirebaseContext } from "../firebase/index";
// AXIOS libreria para llamada http
import axios from "axios";
// Funciones Propias
import { Convertir } from "./api/Funciones";
// Modal
import { Modal, Button, InputGroup } from "react-bootstrap";
// Estilos
import {
  Divp1,
  Label1,
  Label2,
  Label4,
  DivFloat,
  H2Alerta,
  DivScroll,
  LabelRojo,
  LabelVerde,
} from "../components/ui/Visual";

//Variable para el estado inicial de la oportunidad
const STATE_INICIAL = {
  idOportunidad: "",
  entidadLegal: "Persona Física",
  entidadSocial: "",
  nombreCliente: "",
  apellidoCliente: "",
  ultimaOportunidad: "",
  telefonoCliente: "",
  descripcion: "",
  especialidad: "",
  subEspecialidad: "",
  ciudad: "",
  email: "",
  dni: "",
  nif: "",
  cif: "",
  provinciaOportunidad: "",
  abogadoAsignado: "",
  abogadosSeleccionados: [],
  estado: "",
  numeroReasignaciones: 0,
  respuestasPreguntasGeneral: [],
  preguntasGeneral: [],
  abogadosRechazadores: [],
  servicio: "",
  precioServicio: 0,
  precioPresupuesto: 0,
  tipoServicio: "",
  descripcionOportunidad: "",
  registroLlamada: "",
  tipo: "",
  noContestadoFecha: "",
  noContestadoHora: "",
  logNoContestado: [],
  notasGestion: "",
  pais: "",
  nombreAbogadoAsignado: "",
  horaCita: "",
  fechaCita: "",
  canal: "",
  subcanal: "",
  respuestaText1: "",
  respuestaText2: "",
  respuestaText3: "",
  respuestaText4: "",
  respuestaText5: "",
  respuestaText6: "",
  respuestaText7: "",
  respuestaText8: "",
  respuestaText9: "",
  respuestaText10: "",
  respuestaText11: "",
  respuestaSiNo1: "",
  respuestaSiNo2: "",
  respuestaSiNo3: "",
  respuestaSiNo4: "",
  respuestaSiNo5: "",
  respuestaSiNo6: "",
  respuestaSiNo7: "",
  respuestaSiNo8: "",
  respuestaSiNo9: "",
  respuestaSiNo10: "",
  respuestaSiNo11: "",
  respuestaSiNo12: "",
  respuestaSiNo13: "",
  respuestaSiNo14: "",
  respuestaProvin: "",
  mEmail: false,
  mSms: false,
  mPasarela: false,
  mWhats: false,
  mSeguimiento: false,
  mEmailPresupuesto: false,
  mPreguntar: false,
  mLlamar: false,
};

export function MyVerticallyCenteredModal(props) {
  const [error, guardarError] = useState(false);
  const [cargado, guardarCargado] = useState(0);
  const [errorDB, guardarErrorDB] = useState("");
  const [contadorAbogado, guardarContadorAbogado] = useState(0);
  const [nombreClientec, guardarNombreCliente] = useState("");
  const [apellidoClientec, guardarApellidoCliente] = useState("");
  const [telefonoClientec, guardarTelefonoCliente] = useState("");
  const [dniClientec, guardarDniCliente] = useState("");

  const [contadorDB, guardarContadorDB] = useState(0);
  const [contadorPreguntas, guardarContadorPreguntas] = useState(1);
  const [ventanaMostrada, guardarVentanaMostrada] = useState(1);
  const [entidadLegal, guardarEntidadLegal] = useState("Persona Física");
  const [idOportunidad, guardarIdOportunidad] = useState("");
  const [comunidadDeProvincia, guardarComunidadDeProvincia] = useState("");
  const [abogadosPorComunidadPrincipal, guardarAbogadosPorComunidadPrincipal] = useState([]);
  const [abogadosPorComunidadSecundario, guardarAbogadosPorComunidadSecundario] = useState([]);
  const [sociosComunidadPrincipal, guardarSociosComunidadPrincipal] = useState([]);
  const [sociosComunidadSecundario, guardarSociosComunidadSecundario] = useState([]);
  const [
    listaAbogadosSeleccionados,
    guardarListaAbogadosSeleccionados,
  ] = useState([]);

  //CheckFields
  const [Checked, setChecked] = useState(false);
  const [CheckedSms, setCheckedSms] = useState(false);
  const [CheckedEmail, setCheckedEmail] = useState(false);
  const [CheckedApp, setCheckedApp] = useState(false);
  const [CheckedSeguimiento, setCheckedSeguimiento] = useState(false);
  const [CheckedPasarela, setCheckedPasarela] = useState(false);
  const [
    CheckedPreguntarPresupuesto,
    setCheckedPreguntarPresupuesto,
  ] = useState(false);
  const [CheckedEmailPresupuesto, setCheckedEmailPresupuesto] = useState(false);
  const [CheckedLlamar, setCheckedLlamar] = useState(false);
  const [CheckedGestionarCita, setCheckedGestionarCita] = useState(false);

  const [colaboradoresComunidadPrincipal, guardarColaboradoresComunidadPrincipal] = useState([]);
  const [colaboradoresComunidadSecundario, guardarColaboradoresComunidadSecundario] = useState([]);
  const [emailAbogadoAsignado, guardarEmailAbogadoAsignado] = useState("");
  const [sEspecialidad, guardarEspecialidad] = useState("");
  // FILTRADO
  const [
    abogadosSugeridosPrincipal,
    guardarAbogadosSugeridosPrincipal,
  ] = useState([]);
  const [
    colaboradoresSugeridosPrincipal,
    guardarColaboradoresSugeridosPrincipal,
  ] = useState([]);
  const [
    abogadosSugeridosSecundario,
    guardarAbogadosSugeridosSecundario,
  ] = useState([]);
  const [
    colaboradoresSugeridosSecundario,
    guardarColaboradoresSugeridosSecundario,
  ] = useState([]);
  const [sinSugerencias, guardarSinSugerencias] = useState(false);
  const [busquedaManual, guardarBusquedaManual] = useState(false);
  const [otrasSugerenciasPrincipal, guardarOtrasSugerenciasPrincipal] = useState([]);
  const [otrasSugerenciasSecundario, guardarOtrasSugerenciasSecundario] = useState([]);

  const { firebase, usuario } = useContext(FirebaseContext);
  const { abogados } = useAbogadosSugerencia("ultimaOportunidad");
  const { contador } = useContadorOportunidades();
  const { Paises, Auth } = usePaises();
  const [opcionesProvincias, guardarOpcionesProvincias] = useState([]);
  const [opcionesCiudades, guardarOpcionesCiudades] = useState([]);
  // const {provincia, especialidadesAbogado} = abogados;
  // if(abogados.length !== 0){

  //     console.log(abogados)
  // }

  // Valores por defecto
  const sDefaultServicio = {
    value: "Consulta General",
    label: "Consulta General",
  };
  const sDefaultTipoServicio = { value: "Propio", label: "Propio" };
  const sPrecio = { value: "60", label: "60" };
  const sPais = { value: "Spain", label: "Spain" };

  //Valores extraidos de la instancia principal de arriba
  const {
    valores,
    errores,
    handleSubmit,
    handleChange,
    handleChangeProvincia,
    handleChangeSubEspecialidad,
    handleChangeAbogadoAsignado,
    nombreAbogado,
    handleChangeTipo,
    handleChangeTipoServicio,
    handleChangeSiNo,
    handleChangePrProvincia,
    handleChangeCanal,
    handleChangeSubCanal,
    handleChangePais,
    handleChangeCiudad,
    handleChangeServicio,
  } = useValidacionOportunidades(
    STATE_INICIAL,
    validarNuevaOportunidad,
    crearNuevaOportunidad
  );
  var {
    entidadSocial,
    nombreCliente,
    abogadosSeleccionados,
    apellidoCliente,
    motivoRechazo,
    telefonoCliente,
    provincia,
    descripcion,
    subEspecialidad,
    ciudad,
    email,
    dni,
    nif,
    cif,
    especialidad,
    provinciaOportunidad,
    abogadoAsignado,
    abogadosSeleccionados,
    estado,
    nombreAbogadoAsignado,
    servicio,
    precioServicio,
    precioPresupuesto,
    tipoServicio,
    descripcionOportunidad,
    registroLlamada,
    tipo,
    pais,
    horaCita,
    reasignada,
    fechaCita,
    logNoContestado,
    noContestadoFecha,
    noContestadoHora,
    notasGestion,
    canal,
    subcanal,
    respuestaProvin,
    respuestaText1,
    respuestaText2,
    respuestaText3,
    respuestaText4,
    respuestaText5,
    respuestaText6,
    respuestaText7,
    respuestaText8,
    respuestaText9,
    respuestaText10,
    respuestaText11,
    respuestaSiNo1,
    respuestaSiNo2,
    respuestaSiNo3,
    respuestaSiNo4,
    respuestaSiNo5,
    respuestaSiNo6,
    respuestaSiNo7,
    respuestaSiNo8,
    respuestaSiNo9,
    respuestaSiNo10,
    respuestaSiNo11,
    respuestaSiNo12,
    respuestaSiNo13,
    respuestaSiNo14,
    mEmail,
    mSms,
    mPasarela,
    mWhats,
    mSeguimiento,
    mEmailPresupuesto,
    mPreguntar,
    mLlamar,
    mGestionarCita,
  } = valores;

  // const {router} = useRouter();

  useEffect(() => {
    let montado = true;
    console.log(montado);
    if (contador !== undefined) {
      if (montado) {
        guardarContadorDB(contador.contador);
        console.log("contadorDB", contador.contador);
      }
    }
    return () => (montado = false);
  }, [contador]);

  //Api PAISES

  useEffect(() => {
    axios(`https://www.universal-tutorial.com/api/states/${pais}`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${Auth}`,
        Accept: "application/json",
        Connection: "close",
      },
    }).then((response) => guardarOpcionesProvincias(Convertir(response.data)));
  }, [pais]);

  useEffect(() => {
    axios(
      `https://www.universal-tutorial.com/api/cities/${provinciaOportunidad}`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${Auth}`,
          Accept: "application/json",
          Connection: "close",
        },
      }
    ).then((response) => guardarOpcionesCiudades(Convertir(response.data)));
  }, [provinciaOportunidad]);

  // Sugerencias de SOCIOS y COLABORADORES
  useEffect(() => {
    let montado = true;
    if (montado && provinciaOportunidad !== "") {
      const comuna = comunidadesConProvincias.find((prov) =>
        prov.prov.includes(quitarTildes(provinciaOportunidad))
      );
      if (comuna === undefined) {
        guardarComunidadDeProvincia("");
      } else {
        console.log(comuna.com);
        guardarComunidadDeProvincia(comuna.com);
      }
    }

    if (abogados) {
      // console.log('abogados', abogados)
      const provinciaMinuscula = provinciaOportunidad.toLowerCase();
      const provinciaSinTildes = quitarTildes(provinciaMinuscula);
      //const ciudadMinuscula = ciudad.toLowerCase();
      //const ciudadSinTildes = quitarTildes(ciudadMinuscula);
      // const especialidadMinuscula = especialidad.toLowerCase();
      // const especialidadSinTildes = quitarTildes(especialidadMinuscula);
      const abogadosFiltradosPrincipal = abogados.filter((abogado) => {
        if (
          subEspecialidad !== "" &&
          provinciaOportunidad !== "" &&
          abogado.lugaresEjercer !== undefined
        ) {
          if (abogado.lugaresEjercer.length > 0) {
            for (let i = 0; i < abogado.lugaresEjercer.length; i++) {
              return (
                quitarTildes(
                  abogado.lugaresEjercer[i].Provincia.toLowerCase()
                ).includes(provinciaSinTildes) &&
                abogado.subEspecialidadesAbogadosPrincipal.includes(
                  subEspecialidad
                ) &&
                abogado.tipo !== "Colaborador" &&
                abogado.activo === "Activo"
                //&&  quitarTildes(abogado.ciudad.toLowerCase()).includes(ciudadSinTildes)
              );
            }
          }
        }
      });
      const abogadosFiltradosSecundario = abogados.filter((abogado) => {
        if (
          subEspecialidad !== "" &&
          provinciaOportunidad !== "" &&
          abogado.lugaresEjercer !== undefined
        ) {
          if (abogado.lugaresEjercer.length > 0) {
            for (let i = 0; i < abogado.lugaresEjercer.length; i++) {
              return (
                quitarTildes(
                  abogado.lugaresEjercer[i].Provincia.toLowerCase()
                ).includes(provinciaSinTildes) &&
                abogado.subEspecialidadesAbogadosSecundario.includes(
                  subEspecialidad
                ) &&
                abogado.tipo !== "Colaborador" &&
                abogado.activo === "Activo"
                //&&  quitarTildes(abogado.ciudad.toLowerCase()).includes(ciudadSinTildes)
              );
            }
          }
        }
      });
      const colaboradoresFiltradosPrincipal = abogados.filter((abogado) => {
        if (subEspecialidad !== "" && provinciaOportunidad !== "") {
          if (abogado.lugaresEjercer.length > 0) {
            for (let i = 0; i < abogado.lugaresEjercer.length; i++) {
              return (
                quitarTildes(
                  abogado.lugaresEjercer[i].Provincia.toLowerCase()
                ).includes(provinciaSinTildes) &&
                abogado.subEspecialidadesAbogadosPrincipal.includes(
                  subEspecialidad
                ) &&
                abogado.tipo === "Colaborador" &&
                abogado.activo === "Activo"
                //&&  quitarTildes(abogado.ciudad.toLowerCase()).includes(ciudadSinTildes)
              );
            }
          }
        }
      });

      const colaboradoresFiltradosSecundario = abogados.filter((abogado) => {
        if (subEspecialidad !== "" && provinciaOportunidad !== "") {
          if (abogado.lugaresEjercer.length > 0) {
            for (let i = 0; i < abogado.lugaresEjercer.length; i++) {
              return (
                quitarTildes(
                  abogado.lugaresEjercer[i].Provincia.toLowerCase()
                ).includes(provinciaSinTildes) &&
                abogado.subEspecialidadesAbogadosSecundario.includes(
                  subEspecialidad
                ) &&
                abogado.tipo === "Colaborador" &&
                abogado.activo === "Activo"
                //&&  quitarTildes(abogado.ciudad.toLowerCase()).includes(ciudadSinTildes)
              );
            }
          }
        }
      });
      if (montado) {
        guardarAbogadosSugeridosPrincipal(abogadosFiltradosPrincipal);
        guardarColaboradoresSugeridosPrincipal(colaboradoresFiltradosPrincipal);
        guardarAbogadosSugeridosSecundario(abogadosFiltradosSecundario);
        guardarColaboradoresSugeridosSecundario(
          colaboradoresFiltradosSecundario
        );
        if (
          subEspecialidad !== "" &&
          provinciaOportunidad !== "" &&
          abogadosFiltradosPrincipal.length === 0 &&
          colaboradoresFiltradosPrincipal.length === 0 &&
          abogadosFiltradosSecundario.length === 0 &&
          colaboradoresFiltradosSecundario.length === 0
        ) {
          guardarSinSugerencias(true);
          guardarBusquedaManual(true);
          return;
        }
        guardarSinSugerencias(false);
        guardarBusquedaManual(false);
      }
    }
    return () => (montado = false);
  }, [provinciaOportunidad, subEspecialidad, ciudad]);

  async function verContador() {
    if (abogadoAsignado.id) {
      const abogadoSeleccionado = await firebase.db
        .collection("abogados")
        .doc(abogadoAsignado.id)
        .get();
      guardarContadorAbogado(abogadoSeleccionado.data().contadorAsuntos);
      nombreAbogado(
        `${abogadoSeleccionado.data().nombre} ${abogadoSeleccionado.data().apellidos
        }`
      );
      guardarEmailAbogadoAsignado(abogadoSeleccionado.data().emailAbogado);
    }
  }

  useEffect(() => {
    Civil.map((e) => {
      if (e.value === subEspecialidad) {
        document.getElementById("iEspecialidad").value = "Civil";
        guardarEspecialidad("Civil");
      }
    });
    Penal.map((e) => {
      if (e.value === subEspecialidad) {
        document.getElementById("iEspecialidad").value = "Penal";
        guardarEspecialidad("Penal");
      }
    });
    Laboral.map((e) => {
      if (e.value === subEspecialidad) {
        document.getElementById("iEspecialidad").value = "Laboral";
        guardarEspecialidad("Laboral");
      }
    });
    Societario.map((e) => {
      if (e.value === subEspecialidad) {
        document.getElementById("iEspecialidad").value = "Societario";
        guardarEspecialidad("Societario");
      }
    });
    Extranjeria.map((e) => {
      if (e.value === subEspecialidad) {
        document.getElementById("iEspecialidad").value = "Extranjeria";
        guardarEspecialidad("Extranjeria");
      }
    });
    Internacional.map((e) => {
      if (e.value === subEspecialidad) {
        document.getElementById("iEspecialidad").value = "Internacional";
        guardarEspecialidad("Internacional");
      }
    });
    Nuevas_Tecnologias.map((e) => {
      if (e.value === subEspecialidad) {
        document.getElementById("iEspecialidad").value = "Nuevas_Tecnologias";
        guardarEspecialidad("Nuevas_Tecnologias");
      }
    });
    Mercantil.map((e) => {
      if (e.value === subEspecialidad) {
        document.getElementById("iEspecialidad").value = "Mercantil";
        guardarEspecialidad("Mercantil");
      }
    });
    Fiscal_y_Tributario.map((e) => {
      if (e.value === subEspecialidad) {
        document.getElementById("iEspecialidad").value = "Fiscal_y_Tributario";
        guardarEspecialidad("Fiscal_y_Tributario");
      }
    });
    Bancario.map((e) => {
      if (e.value === subEspecialidad) {
        document.getElementById("iEspecialidad").value = "Bancario";
        guardarEspecialidad("Bancario");
      }
    });
    Constitucional.map((e) => {
      if (e.value === subEspecialidad) {
        document.getElementById("iEspecialidad").value = "Constitucional";
        guardarEspecialidad("Constitucional");
      }
    });
    Compliance.map((e) => {
      if (e.value === subEspecialidad) {
        document.getElementById("iEspecialidad").value = "Compliance";
        guardarEspecialidad("Compliance");
      }
    });
    Comunitario.map((e) => {
      if (e.value === subEspecialidad) {
        document.getElementById("iEspecialidad").value = "Comunitario";
        guardarEspecialidad("Comunitario");
      }
    });
    Administrativo.map((e) => {
      if (e.value === subEspecialidad) {
        document.getElementById("iEspecialidad").value = "Administrativo";
        guardarEspecialidad("Administrativo");
      }
    });
  }, [subEspecialidad, provincia]);
  useEffect(() => {
    let montado = true;
    if (montado) {
      verContador();
    }
    return () => (montado = false);
  }, [abogadoAsignado]);

  const quitarTildes = (palabra) => {
    const letras = { á: "a", é: "e", í: "i", ó: "o", ú: "u" };
    palabra = palabra.replace(/[áéíóú]/g, (m) => letras[m]);
    return palabra;
  };

  // Funcion para checar el abogado seleccionado
  const seleccionarRadio = async (opcion, nombre, apellidos) => {
    handleChangeAbogadoAsignado(opcion);
    const aux = document.getElementById("abogadoAsignado");

    aux.value = `${nombre} ${apellidos}`;
  };

  const seleccionarCheckBox = async (opcion, accion, nombre, apellidos) => {
    const aux = listaAbogadosSeleccionados;
    if (accion == true) {
      aux.push({ id: opcion, nombre: nombre, apellidos: apellidos });
    } else {
      const oBorrar = aux.filter((e) => e.id === opcion);
      if (oBorrar.length > 0) {
        const index = aux.indexOf(oBorrar[0]);
        if (index > -1) {
          aux.splice(index, 1);
        }
      }
    }
    guardarListaAbogadosSeleccionados(aux);
  };
  // Funcion de filtrado manual
  // FILTRADO
  const [q, guardarQ] = useState("");
  const [resultados, guardarResultados] = useState([]);

  const [qComunidad, guardarQComunidad] = useState("");

  // Cuando no hay abogados en la misma provincia, Abogados Principal
  useEffect(() => {
    let montado = true;
    if (otrasSugerenciasPrincipal) {
      const busqueda = q.toLowerCase();
      const busquedaSinTildes = quitarTildes(busqueda);
      const abogadosFiltrados = otrasSugerenciasPrincipal.filter((abogado) => {
        return `${quitarTildes(abogado.nombre.toLowerCase())} ${quitarTildes(
          abogado.apellidos.toLowerCase()
        )}`.includes(busquedaSinTildes);
      });

      if (montado) {
        if (comunidadDeProvincia !== "") {
          const busqueda = qComunidad.toLowerCase();
          const busquedaSinTildes = quitarTildes(busqueda);
          console.log(otrasSugerenciasPrincipal)
          const abogadosComunidadAutonoma = otrasSugerenciasPrincipal.filter(
            (abogado) => {
              return (
                quitarTildes(abogado.comunidadAutonoma).includes(
                  quitarTildes(comunidadDeProvincia)
                ) &&
                `${quitarTildes(abogado.nombre.toLowerCase())} ${quitarTildes(
                  abogado.apellidos.toLowerCase()
                )}`.includes(busquedaSinTildes)
              );
            }
          );
          // Guardamos los abogados de la CCAA en state
          guardarAbogadosPorComunidadPrincipal(abogadosComunidadAutonoma);
        }
        guardarResultados(abogadosFiltrados);
      }
    }
    return () => (montado = false);
  }, [
    q,
    qComunidad,
    otrasSugerenciasPrincipal,
    comunidadDeProvincia,
    provinciaOportunidad,
  ]);

  // Cuando no hay abogados en la misma provincia, Abogados Secundario
  useEffect(() => {
    let montado = true;
    if (otrasSugerenciasPrincipal) {
      const busqueda = q.toLowerCase();
      const busquedaSinTildes = quitarTildes(busqueda);
      const abogadosFiltrados = otrasSugerenciasSecundario.filter((abogado) => {
        return `${quitarTildes(abogado.nombre.toLowerCase())} ${quitarTildes(
          abogado.apellidos.toLowerCase()
        )}`.includes(busquedaSinTildes);
      });

      if (montado) {
        if (comunidadDeProvincia !== "") {
          const busqueda = qComunidad.toLowerCase();
          const busquedaSinTildes = quitarTildes(busqueda);
          console.log(otrasSugerenciasSecundario)
          const abogadosComunidadAutonoma = otrasSugerenciasSecundario.filter(
            (abogado) => {
              return (
                quitarTildes(abogado.comunidadAutonoma).includes(
                  quitarTildes(comunidadDeProvincia)
                ) &&
                `${quitarTildes(abogado.nombre.toLowerCase())} ${quitarTildes(
                  abogado.apellidos.toLowerCase()
                )}`.includes(busquedaSinTildes)
              );
            }
          );
          // Guardamos los abogados de la CCAA en state
          guardarAbogadosPorComunidadSecundario(abogadosComunidadAutonoma);
        }
        guardarResultados(abogadosFiltrados);
      }
    }
    return () => (montado = false);
  }, [
    q,
    qComunidad,
    otrasSugerenciasSecundario,
    comunidadDeProvincia,
    provinciaOportunidad,
  ]);

  // Diferenciamos Socios de Colaboradores (CCAA) cuando no haya sugerencias
  useEffect(() => {
    let montado = true;
    if (montado && abogadosPorComunidadPrincipal.length !== 0) {
      const sociosFiltrados = abogadosPorComunidadPrincipal.filter((abogado) => {
        return abogado.tipo !== "Colaborador";
      });
      guardarSociosComunidadPrincipal(sociosFiltrados);

      const colaboradoresFiltrados = abogadosPorComunidadPrincipal.filter((abogado) => {
        return abogado.tipo === "Colaborador";
      });
      guardarColaboradoresComunidadPrincipal(colaboradoresFiltrados);
    } else {
      guardarSociosComunidadPrincipal([]);
      guardarColaboradoresComunidadPrincipal([]);
    }

    return () => (montado = false);
  }, [abogadosPorComunidadPrincipal]);

  useEffect(() => {
    let montado = true;
    if (montado && abogadosPorComunidadSecundario.length !== 0) {
      const sociosFiltrados = abogadosPorComunidadSecundario.filter((abogado) => {
        return abogado.tipo !== "Colaborador";
      });
      guardarSociosComunidadSecundario(sociosFiltrados);

      const colaboradoresFiltrados = abogadosPorComunidadSecundario.filter((abogado) => {
        return abogado.tipo === "Colaborador";
      });
      guardarColaboradoresComunidadSecundario(colaboradoresFiltrados);
    } else {
      guardarSociosComunidadSecundario([]);
      guardarColaboradoresComunidadSecundario([]);
    }

    return () => (montado = false);
  }, [abogadosPorComunidadSecundario]);


  //Buscar por provincia con principal
  const resultadosProvinciaPrincipal = async () => {
    await firebase.db
      .collection("abogados")
      .where("subEspecialidadesAbogadosPrincipal", "array-contains", subEspecialidad)
      .onSnapshot(manejarSnapShotPrincipal);
  };

  function manejarSnapShotPrincipal(snapshot) {
    const abogadosDB = snapshot.docs.map((doc) => {
      return {
        id: doc.id,
        ...doc.data(),
      };
    });
    // const abogadosOrdenados = abogadosDB.sort((a, b) => (a.contadorAsuntos > b.contadorAsuntos) ? 1 : -1)
    const abogadosOrdenados = abogadosDB.sort((a, b) =>
      a.ultimaOportunidad > b.ultimaOportunidad ? 1 : -1
    );
    guardarOtrasSugerenciasPrincipal(abogadosOrdenados);
  }

  //Buscar por provincia con Secundario
  const resultadosProvinciaSecundario = async () => {
    await firebase.db
      .collection("abogados")
      .where("subEspecialidadesAbogadosSecundario", "array-contains", subEspecialidad)
      .onSnapshot(manejarSnapShotSecundario);
  };

  function manejarSnapShotSecundario(snapshot) {
    const abogadosDB = snapshot.docs.map((doc) => {
      return {
        id: doc.id,
        ...doc.data(),
      };
    });
    // const abogadosOrdenados = abogadosDB.sort((a, b) => (a.contadorAsuntos > b.contadorAsuntos) ? 1 : -1)
    const abogadosOrdenados = abogadosDB.sort((a, b) =>
      a.ultimaOportunidad > b.ultimaOportunidad ? 1 : -1
    );
    guardarOtrasSugerenciasSecundario(abogadosOrdenados);
  }



  useEffect(() => {
    resultadosProvinciaPrincipal();
    resultadosProvinciaSecundario();
  }, [subEspecialidad]);


  useEffect(() => {
    let montado = true;
    if (contadorDB !== 0) {
      const fecha = new Date();
      const año = fecha.getFullYear();
      if (montado) {
        guardarIdOportunidad(`LY${año}${contadorDB}`);
      }
    }
    return () => (montado = false);
  }, [contadorDB]);

  const template_params = {
    reply_to: "servicios.lawyou@lawyoulegal.com",
    emailAbogado: emailAbogadoAsignado,
    destinatario: nombreAbogadoAsignado,
    from_name: "Lawyou",
    mensaje_html: `<ul>
							<li><b>Nombre Cliente</b>:  ${nombreCliente}</li>
							<li><b>Telefono</b>:  ${telefonoCliente}</li>
							<li><b>Especialidad</b>:  ${especialidad}</li>
							<li><b>Provincia</b>:  ${provinciaOportunidad}</li>
						</ul><br>
						<p>En caso de no haber confirmado tu disponibilidad, no olvides hacerlo llamando al 667 60 66 11 o respondiendo a este mail</p>`,
  };

  // Funcion que a partir de las variables de respuestas y la subespecialidad seleccionada crea un objeto con las preguntas y respuestas para importar al firebase
  function crearObjetoPreguntasYRespuestas(subes) {
    let prYRes = [];
    switch (subes) {
      case "ERTE":
        if (respuestaText1 !== "")
          prYRes.push({ Pregunta: Erte["Text1"], Respuesta: respuestaText1 });
        if (respuestaText2 !== "")
          prYRes.push({ Pregunta: Erte["Text2"], Respuesta: respuestaText2 });
        if (respuestaText3 !== "")
          prYRes.push({ Pregunta: Erte["Text3"], Respuesta: respuestaText3 });
        if (respuestaProvin !== "")
          prYRes.push({
            Pregunta: Erte["DespProv1"],
            Respuesta: respuestaProvin,
          });
        if (respuestaText4 !== "")
          prYRes.push({ Pregunta: Erte["Text4"], Respuesta: respuestaText4 });
        if (respuestaText5 !== "")
          prYRes.push({ Pregunta: Erte["Text5"], Respuesta: respuestaText5 });
        if (respuestaSiNo1 !== "")
          prYRes.push({ Pregunta: Erte["SiNo1"], Respuesta: respuestaSiNo1 });
        break;
      case "Despidos":
        if (respuestaSiNo1 !== "")
          prYRes.push({
            Pregunta: Despidos["SiNo1"],
            Respuesta: respuestaSiNo1,
          });
        if (respuestaText1 !== "")
          prYRes.push({
            Pregunta: Despidos["Text1"],
            Respuesta: respuestaText1,
          });
        if (respuestaText2 !== "")
          prYRes.push({
            Pregunta: Despidos["Text2"],
            Respuesta: respuestaText2,
          });
        if (respuestaText3 !== "")
          prYRes.push({
            Pregunta: Despidos["Text3"],
            Respuesta: respuestaText3,
          });
        break;
      case "Negligencia Medica Civil":
        if (respuestaText1 !== "")
          prYRes.push({
            Pregunta: NegligenciaMedica["Text1"],
            Respuesta: respuestaText1,
          });
        if (respuestaSiNo1 !== "")
          prYRes.push({
            Pregunta: NegligenciaMedica["SiNo1"],
            Respuesta: respuestaSiNo1,
          });
        if (respuestaSiNo2 !== "")
          prYRes.push({
            Pregunta: NegligenciaMedica["SiNo2"],
            Respuesta: respuestaSiNo2,
          });
        if (respuestaText2 !== "")
          prYRes.push({
            Pregunta: NegligenciaMedica["Text2"],
            Respuesta: respuestaText2,
          });
        if (respuestaSiNo3 !== "")
          prYRes.push({
            Pregunta: NegligenciaMedica["SiNo3"],
            Respuesta: respuestaSiNo3,
          });
        if (respuestaSiNo4 !== "")
          prYRes.push({
            Pregunta: NegligenciaMedica["SiNo4"],
            Respuesta: respuestaSiNo4,
          });
        if (respuestaText3 !== "")
          prYRes.push({
            Pregunta: NegligenciaMedica["Text3"],
            Respuesta: respuestaText3,
          });
        if (respuestaText4 !== "")
          prYRes.push({
            Pregunta: NegligenciaMedica["Text4"],
            Respuesta: respuestaText4,
          });
        break;
      case "Herencia y Sucesiones":
        if (respuestaText1 !== "")
          prYRes.push({
            Pregunta: Herencia["Text1"],
            Respuesta: respuestaText1,
          });
        if (respuestaSiNo1 !== "")
          prYRes.push({
            Pregunta: Herencia["SiNo1"],
            Respuesta: respuestaSiNo1,
          });
        if (respuestaText2 !== "")
          prYRes.push({
            Pregunta: Herencia["Text2"],
            Respuesta: respuestaText2,
          });
        if (respuestaText3 !== "")
          prYRes.push({
            Pregunta: Herencia["Text3"],
            Respuesta: respuestaText3,
          });
        if (respuestaText4 !== "")
          prYRes.push({
            Pregunta: Herencia["Text4"],
            Respuesta: respuestaText4,
          });
        if (respuestaText5 !== "")
          prYRes.push({
            Pregunta: Herencia["Text5"],
            Respuesta: respuestaText5,
          });
        if (respuestaText6 !== "")
          prYRes.push({
            Pregunta: Herencia["Text6"],
            Respuesta: respuestaText6,
          });
        if (respuestaSiNo2 !== "")
          prYRes.push({
            Pregunta: Herencia["SiNo2"],
            Respuesta: respuestaSiNo2,
          });
        if (respuestaSiNo3 !== "")
          prYRes.push({
            Pregunta: Herencia["SiNo3"],
            Respuesta: respuestaSiNo3,
          });
        if (respuestaText7 !== "")
          prYRes.push({
            Pregunta: Herencia["Text7"],
            Respuesta: respuestaText7,
          });
        if (respuestaSiNo4 !== "")
          prYRes.push({
            Pregunta: Herencia["SiNo4"],
            Respuesta: respuestaSiNo4,
          });
        if (respuestaSiNo5 !== "")
          prYRes.push({
            Pregunta: Herencia["SiNo5"],
            Respuesta: respuestaSiNo5,
          });
        if (respuestaSiNo6 !== "")
          prYRes.push({
            Pregunta: Herencia["SiNo6"],
            Respuesta: respuestaSiNo6,
          });
        if (respuestaSiNo7 !== "")
          prYRes.push({
            Pregunta: Herencia["SiNo7"],
            Respuesta: respuestaSiNo7,
          });
        if (respuestaSiNo8 !== "")
          prYRes.push({
            Pregunta: Herencia["SiNo8"],
            Respuesta: respuestaSiNo8,
          });
        if (respuestaSiNo9 !== "")
          prYRes.push({
            Pregunta: Herencia["SiNo9"],
            Respuesta: respuestaSiNo9,
          });
        if (respuestaSiNo10 !== "")
          prYRes.push({
            Pregunta: Herencia["SiNo10"],
            Respuesta: respuestaSiNo10,
          });
        break;
      case "Ley de Segunda Oportunidad":
        if (respuestaText1 !== "")
          prYRes.push({
            Pregunta: LeySegundaOportunidad["Text1"],
            Respuesta: respuestaText1,
          });
        if (respuestaSiNo1 !== "")
          prYRes.push({
            Pregunta: LeySegundaOportunidad["SiNo1"],
            Respuesta: respuestaSiNo1,
          });
        if (respuestaSiNo2 !== "")
          prYRes.push({
            Pregunta: LeySegundaOportunidad["SiNo2"],
            Respuesta: respuestaSiNo2,
          });
        if (respuestaText2 !== "")
          prYRes.push({
            Pregunta: LeySegundaOportunidad["Text2"],
            Respuesta: respuestaText2,
          });
        if (respuestaSiNo3 !== "")
          prYRes.push({
            Pregunta: LeySegundaOportunidad["SiNo3"],
            Respuesta: respuestaSiNo3,
          });
        if (respuestaText3 !== "")
          prYRes.push({
            Pregunta: LeySegundaOportunidad["Text3"],
            Respuesta: respuestaText3,
          });
        if (respuestaSiNo4 !== "")
          prYRes.push({
            Pregunta: LeySegundaOportunidad["SiNo4"],
            Respuesta: respuestaSiNo4,
          });
        break;
      case "Responsabilidad Patrimonial":
        if (respuestaText1 !== "")
          prYRes.push({
            Pregunta: ResponsabilidadPatrimonialEstado["Text1"],
            Respuesta: respuestaText1,
          });
        if (respuestaText2 !== "")
          prYRes.push({
            Pregunta: ResponsabilidadPatrimonialEstado["Text2"],
            Respuesta: respuestaText2,
          });
        if (respuestaText3 !== "")
          prYRes.push({
            Pregunta: ResponsabilidadPatrimonialEstado["Text3"],
            Respuesta: respuestaText3,
          });
        if (respuestaSiNo1 !== "")
          prYRes.push({
            Pregunta: ResponsabilidadPatrimonialEstado["SiNo1"],
            Respuesta: respuestaSiNo1,
          });
        if (respuestaSiNo2 !== "")
          prYRes.push({
            Pregunta: ResponsabilidadPatrimonialEstado["SiNo2"],
            Respuesta: respuestaSiNo2,
          });
        if (respuestaSiNo3 !== "")
          prYRes.push({
            Pregunta: ResponsabilidadPatrimonialEstado["SiNo3"],
            Respuesta: respuestaSiNo3,
          });
        break;
      case "Propiedad Intelectual":
        if (respuestaSiNo1 !== "")
          prYRes.push({
            Pregunta: PropiedadIntelectual["SiNo1"],
            Respuesta: respuestaSiNo1,
          });
        if (respuestaText1 !== "")
          prYRes.push({
            Pregunta: PropiedadIntelectual["Text1"],
            Respuesta: respuestaText1,
          });
        if (respuestaSiNo2 !== "")
          prYRes.push({
            Pregunta: PropiedadIntelectual["SiNo2"],
            Respuesta: respuestaSiNo2,
          });
        if (respuestaText2 !== "")
          prYRes.push({
            Pregunta: PropiedadIntelectual["Text2"],
            Respuesta: respuestaText2,
          });
        break;
      default:
        prYRes.push("No ha preguntas específicas");
    }
    return prYRes;
  }
  async function Comprobar() {
    console.log("Funciona")
    var myHeaders = new Headers();
    var cuerpo = {
      "filterGroups": [
        {
          "filters": [
            {
              "propertyName": "email",
              "operator": "EQ",
              "value": email
            }
          ]
        }
      ],
      "limit": 100,
      "properties": ["email", "phone", "firstname", "lastname","dni"]
    }
    myHeaders.append("Content-Type","application/json");
    myHeaders.append("Access-Control-Allow-Origin","*");
    const proxyurl = "https://cors-anywhere.herokuapp.com/";
    const url = `https://api.hubapi.com/crm/v3/objects/contacts/search?hapikey=17ffea9d-bfe1-420d-a87c-e0a49d5133eb`; // site that doesn’t send Access-Control-*

    fetch(proxyurl + url, {
      method: 'POST',
      mode: 'cors',
      headers: myHeaders,
      body: JSON.stringify(cuerpo)

    }).then((resp) => resp.json())
      .then(function (data) {
        if (data.results.length != 0) {
          guardarCargado(2)
          guardarNombreCliente(data.results[0].properties.firstname) 
          guardarApellidoCliente(data.results[0].properties.lastname) 
          guardarTelefonoCliente(data.results[0].properties.phone ? data.results[0].properties.phone : "")
          guardarDniCliente(data.results[0].properties.dni ? data.results[0].properties.dni : "")
          // El estado dos, cargara los datos y los pondra en readonly
          
        }
        else {
          //Podras introducir manualmente
          guardarCargado(1)
        }
      })
      .catch(function (error) {
        console.log(error);
      });

  }

  async function crearNuevaOportunidad() {
    if (!usuario) {
      return Router.push("/iniciar-sesion");
    }

    const preguntasYRespuestas = crearObjetoPreguntasYRespuestas(
      subEspecialidad
    );

    //estancia de la nueva oportunidad utilizando los valores conseguidos en el formulario.

    const oportunidad = {
      idOportunidad,
      entidadLegal,
      entidadSocial,
      nombreCliente,
      apellidoCliente,
      telefonoCliente,
      ciudad,
      email,
      dni,
      nif,
      cif,
      notasGestion,
      reasignada: false,
      especialidad,
      subEspecialidad,
      provinciaOportunidad,
      abogadoAsignado,
      abogadoAsignadoOriginal:abogadoAsignado,
      abogadosSeleccionados,
      estado: "",
      noContestadoFecha,
      noContestadoHora,
      logNoContestado,
      creado: Date.now(),
      creador: {
        id: usuario.uid,
        nombre: usuario.displayName,
      },
      cita: {
        Hora: horaCita,
        Fecha: fechaCita,
      },
      numeroReasignaciones: 0,
      nombreAbogadoAsignado,
      servicio,
      precioServicio,
      precioPresupuesto,
      tipoServicio,
      descripcionOportunidad,
      registroLlamada,
      tipo,
      pais,
      canales: {
        canal: canal,
        subcanal: subcanal,
      },
      preguntasYRespuestas,
      mEmail,
      mSms,
      mPasarela,
      mWhats,
      mSeguimiento,
      mEmailPresupuesto,
      mPreguntar,
      mLlamar,
      mGestionarCita:CheckedGestionarCita,
    };

    if (tipo === "Cita") {
      oportunidad.estado = "Cita Gestionada";
      oportunidad.mEmail = CheckedEmail;
      oportunidad.mSms = CheckedSms;
      oportunidad.mPasarela = CheckedPasarela;
      oportunidad.mWhats = CheckedApp;
      oportunidad.mSeguimiento = CheckedSeguimiento;
      if (oportunidad.AbogadoAsignado === "") {
        oportunidad.estado = "Pendiente Asignar";
      }
    } else {

      oportunidad.estado = "Presupuesto Gestionado";
      oportunidad.mEmailPresupuesto = CheckedEmailPresupuesto;
      oportunidad.mPreguntar = CheckedPreguntarPresupuesto;
      oportunidad.mLlamar = CheckedLlamar;
      oportunidad.mGestionarCita = CheckedGestionarCita;
      if (listaAbogadosSeleccionados.length === 0) {
        oportunidad.estado = "Pendiente Asignar";
      } else {
        console.log("Entre AL 2");
        oportunidad.abogadosSeleccionados = listaAbogadosSeleccionados;
        oportunidad.estado = "Presupuesto Gestionado";
      }
      if(CheckedGestionarCita){
        oportunidad.mEmail = CheckedEmail;
        oportunidad.mSms = CheckedSms;
        oportunidad.mPasarela = CheckedPasarela;
        oportunidad.mWhats = CheckedApp;
        oportunidad.mSeguimiento = CheckedSeguimiento;
        if (oportunidad.AbogadoAsignado === "") {
          oportunidad.estado = "Pendiente Asignar";
        }
      }
    }
    if(cargado=== 0 || cargado === 2){
      oportunidad.nombreCliente = nombreClientec;
      oportunidad.apellidoCliente = apellidoClientec;
      oportunidad.telefonoCliente = telefonoClientec;
    }


    //Comprobar abogado asignado, y si no hay ninguno asignado ponerle vacio por defecto
    if (Checked === true) {
      oportunidad.estado = "No Contestado";
      oportunidad.logNoContestado.push({
        Fecha: oportunidad.noContestadoFecha,
        Hora: oportunidad.noContestadoHora,
      });
    }

    // Comprobar si la entidad legal es persona fisica, el campo entidad social esté vacío
    if (entidadLegal !== "Persona Jurídica") {
      console.log("BORRANDO ENTIDAD SOCIAL");
      oportunidad.entidadSocial = "";
    }

    oportunidad.especialidad = sEspecialidad;

    if (oportunidad.servicio === "") {
      oportunidad.servicio = sDefaultServicio.value;
    }
    if (oportunidad.tipoServicio === "") {
      oportunidad.tipoServicio = sDefaultTipoServicio.value;
    }
    if (oportunidad.precioServicio === "") {
      oportunidad.precioServicio = sPrecio;
    }
    if (oportunidad.pais === "") {
      oportunidad.pais = "Spain";
    }

    try {
      // Guardar la oportunidad
      console.log("HE ENTRADO EN GUARDAR OPORTUNIDAD");
      await firebase.db.collection("oportunidades").add(oportunidad);
    } catch (error) {
      console.log(error);
      console.log("Error Al crear")
      guardarErrorDB(error);
    }
    //Guardar contador +1
    try {
      await firebase.db
        .collection("contadorOportunidades")
        .doc("NYEfyyPx5x5qa17wE3ah")
        .update({ contador: contadorDB + 1 });
    } catch (error) {
      console.log(error);
    }

    try {
      // Sumarle uno al contador del abogado asignado
      if (abogadoAsignado !== "") {
        await firebase.db
          .collection("abogados")
          .doc(abogadoAsignado)
          .update({
            contadorAsuntos: contadorAbogado + 1,
            ultimaOportunidad: Date.now(),
          });
        emailjs
          .send(
            "smtp_server",
            "template_kZfiJX4o",
            template_params,
            "user_xxs3Pu51cqPmbXqR7CsEW"
          )
          .then(
            (response) => {
              console.log("SUCCESS!", response.status, response.text);
            },
            (err) => {
              console.log("FAILED...", err);
            }
          );
      }
      Swal.fire({
        // position: 'top-end',
        icon: "success",
        title: "La oportunidad se creó correctamente!",
        showConfirmButton: false,
        timer: 1200,
      });
      setTimeout(() => {
        Router.push("/index");
        location.reload();
      }, 1200);
      console.log("todo OK");
    } catch (error) {
      console.log(error);
    }
  }

  // Funcion para navegar por selección
  function irASeleccion(n, id) {
    guardarVentanaMostrada(n);

    var selector = document.getElementById(id);
    if (typeof selector.scrollIntoView !== "undefined") {
      selector.scrollIntoView();
    }
  }

  return (
    <Modal {...props} size="xl">
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          <h1 className="titulo">Nueva Oportunidad</h1>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          {error ? (
            <PaginaError msg="Vaya... Parece que ha habido un error al editar el abogado, vuelve a intentarlo por favor."></PaginaError>
          ) : contadorDB === undefined ? (
            <p>Cargando...</p>
          ) : usuario ? (
            <>
              <form onSubmit={handleSubmit}>
                <Divp1 className="mb-2 row">
                  <div className="col-12 col-lg-3">
                    <DivFloat>
                      <div className="row">
                        {/* Pestaña 1 */}
                        <div className="col-12 d-flex">
                          <Label1
                            className="d-block"
                            onClick={() => irASeleccion(1, "ven1")}
                          >
                            1. Datos Cliente
                          </Label1>
                        </div>
                      </div>
                      <hr />

                      {/* Pestaña 2 */}
                      <div className="row">
                        <div className="col-12 d-flex">
                          <Label2
                            className="d-block"
                            onClick={() => irASeleccion(2, "ven2")}
                          >
                            2. Registro de la Llamada
                          </Label2>
                        </div>
                      </div>
                      <hr />

                      {/* Pestaña 3 */}
                      {subEspecialidad !== "" && provinciaOportunidad !== "" ? (
                        <>
                          <div className="row">
                            <div className="col-12 d-flex">
                              <Label2
                                className="d-block"
                                onClick={() => irASeleccion(3, "ven3")}
                              >
                                3. Preguntas Específicas
                              </Label2>
                            </div>
                          </div>
                          <hr />
                        </>
                      ) : null}

                      {/* Pestaña 4 */}
                      {subEspecialidad !== "" && provinciaOportunidad !== "" ? (
                        <>
                          <div className="row">
                            <div className="col-12 d-flex">
                              <Label2
                                className="d-block"
                                onClick={() => irASeleccion(4, "ven4")}
                              >
                                4. Seleccionar Abogados
                              </Label2>
                            </div>
                          </div>
                          <hr />
                        </>
                      ) : null}

                      {/* Pestaña 5 */}
                      {subEspecialidad !== "" && provinciaOportunidad !== "" ? (
                        <>
                          <div className="row">
                            <div className="col-12 d-flex">
                              <Label2
                                className="d-block"
                                onClick={() => irASeleccion(5, "ven5")}
                              >
                                5. Datos Oportunidad
                              </Label2>
                            </div>
                          </div>
                          <hr />
                        </>
                      ) : null}

                      {/* Pestaña 6 */}
                      {subEspecialidad !== "" && provinciaOportunidad !== "" ? (
                        <>
                          <div className="row">
                            <div className="col-12 d-flex">
                              <Label2
                                className="d-block"
                                onClick={() => irASeleccion(6, "ven6")}
                              >
                                6. Gestión
                              </Label2>
                            </div>
                          </div>
                          <hr />
                        </>
                      ) : null}
                      {/* Pestaña 7 */}
                      {CheckedGestionarCita ? (
                        <>
                          <div className="row">
                            <div className="col-12 d-flex">
                              <Label2
                                className="d-block"
                                onClick={() => irASeleccion(7, "ven7")}
                              >
                                7. Gestion de cita
                              </Label2>
                            </div>
                          </div>
                        </>
                      ) : null}
                    </DivFloat>
                  </div>

                  <DivScroll className="col-12 col-lg-9">
                    {/* Ventana Completa */}
                    {/* V1 */}
                    <ContenedorFormularioLg id="ven1">
                      <fieldset className="mt-3">
                        <legend className="pl-5">Datos Cliente</legend>
                        <div className="col-12 d-flex mt-4">
                          <div className="col-12">
                            <label htmlFor="entidadLegal">Entidad Legal:</label>
                            <label>
                              <Radio
                                type="radio"
                                defaultChecked
                                name="persona"
                                onClick={() => {
                                  guardarEntidadLegal("Persona Física");
                                }}
                                onChange={handleChange}
                              />
                              Persona Física
                            </label>
                            <label>
                              <Radio
                                type="radio"
                                name="persona"
                                onClick={() => {
                                  guardarEntidadLegal("Persona Jurídica");
                                }}
                                onChange={handleChange}
                              />
                              Persona Jurídica
                            </label>
                          </div>
                        </div>
                        <div className="col-12 d-flex mt-4">
                          <div className="col-8">
                            <div class="input-group mb-3">
                              <input
                                type="text"
                                class="form-control"
                                placeholder="Introduce un email de Cliente"
                                name="email"
                                onChange={handleChange}
                                value={email}
                                aria-label="Recipient's username"
                                aria-describedby="basic-addon2" />
                              <div class="input-group-append">
                                <button
                                  class="btn btn-outline-secondary"
                                  type="button"
                                  onClick={() => Comprobar()}
                                >Comprobar
                          </button>
                              </div>
                            </div>
                          </div>
                        </div>
                        {cargado === 0 ? (

                          <fieldset>
                            <legend>Cliente</legend>
                            <div className="col-12 d-flex mt-4">
                              <div className="col-4">
                                <label htmlFor="nombreCliente">Nombre</label>
                                <input
                                  readOnly
                                  type="text"
                                  className="form-control"
                                  name="nombreCliente"
                                  onChange={handleChange}
                                  value={nombreClientec}
                                  placeholder="Nombre del cliente"
                                />
                              </div>

                              <div className="col-4">
                                <label htmlFor="apellidoCliente">Apellido</label>
                                <input
                                  readOnly
                                  type="text"
                                  className="form-control"
                                  name="apellidoCliente"
                                  onChange={handleChange}
                                  value={apellidoClientec}
                                  placeholder="Apellido del cliente"
                                />
                              </div>
                              {entidadLegal === "Persona Jurídica" ? (
                                <div className="col-4">
                                  <label htmlFor="entidadSocial">
                                    Entidad Social
    </label>
                                  <input
                                    readOnly
                                    type="text"
                                    className="form-control"
                                    name="entidadSocial"
                                    onChange={handleChange}
                                    value={entidadSocial}
                                    placeholder="Nombre de la Entidad Social"
                                  />
                                </div>
                              ) : null}
                            </div>

                            <div className="col-12 d-flex abajo">
                              <div className="col-4">
                                <label htmlFor="telefonoCliente">
                                  Teléfono del Cliente
  </label>
                                <input
                                  readOnly
                                  type="text"
                                  className="form-control"
                                  name="telefonoCliente"
                                  onChange={handleChange}
                                  value={telefonoClientec}
                                  placeholder="Teléfono del cliente"
                                />
                              </div>
                            </div>
                          </fieldset>

                        ) : cargado === 1 ? (
                          <>
                          <div className="col-12 d-flex mt-4">
                            <LabelRojo> EL EMAIL NO ES CORRECTO O EL CLIENTE NO EXISTE</LabelRojo>
                            </div>
                          <fieldset>
                            <legend>Cliente</legend>
                            <div className="col-12 d-flex mt-4">
                              <div className="col-4">
                                <label htmlFor="nombreCliente">Nombre</label>
                                <input
                                  type="text"
                                  className="form-control"
                                  name="nombreCliente"
                                  onChange={handleChange}
                                  value={nombreCliente}
                                  placeholder="Nombre del cliente"
                                />
                              </div>

                              <div className="col-4">
                                <label htmlFor="apellidoCliente">Apellido</label>
                                <input
                                  type="text"
                                  className="form-control"
                                  name="apellidoCliente"
                                  onChange={handleChange}
                                  value={apellidoCliente}
                                  placeholder="Apellido del cliente"
                                />
                              </div>
                              {entidadLegal === "Persona Jurídica" ? (
                                <div className="col-4">
                                  <label htmlFor="entidadSocial">
                                    Entidad Social
                              </label>
                                  <input
                                    type="text"
                                    className="form-control"
                                    name="entidadSocial"
                                    onChange={handleChange}
                                    value={entidadSocial}
                                    placeholder="Nombre de la Entidad Social"
                                  />
                                </div>
                              ) : null}
                            </div>

                            <div className="col-12 d-flex abajo">
                              <div className="col-4">
                                <label htmlFor="telefonoCliente">
                                  Teléfono del Cliente
                            </label>
                                <input
                                  type="text"
                                  className="form-control"
                                  name="telefonoCliente"
                                  onChange={handleChange}
                                  value={telefonoCliente}
                                  placeholder="Teléfono del cliente"
                                />
                              </div>
                            </div>
                          </fieldset>
                          </>
                        ) : cargado === 2 ? (
                          <>
                          <div className="col-12 d-flex mt-4">
                            <LabelVerde> CLIENTE CARGADO CORRECTAMENTE</LabelVerde>
                            </div>

                          <fieldset>
                            <legend>Cliente</legend>
                            <div className="col-12 d-flex mt-4">
                              <div className="col-4">
                                <label htmlFor="nombreCliente">Nombre</label>
                                <input
                                  readOnly
                                  type="text"
                                  className="form-control"
                                  name="nombreCliente"
                                  onChange={handleChange}
                                  value={nombreClientec}
                                  placeholder="Nombre del cliente"
                                  id="nc2"
                                />
                              </div>

                              <div className="col-4">
                                <label htmlFor="apellidoCliente">Apellido</label>
                                <input
                                  readOnly
                                  type="text"
                                  className="form-control"
                                  name="apellidoCliente"
                                  onChange={handleChange}
                                  value={apellidoClientec}
                                  placeholder="Apellido del cliente"
                                  id="ac2"
                                />
                              </div>
                              {entidadLegal === "Persona Jurídica" ? (
                                <div className="col-4">
                                  <label htmlFor="entidadSocial">
                                    Entidad Social
  </label>
                                  <input
                                    readOnly
                                    type="text"
                                    className="form-control"
                                    name="entidadSocial"
                                    onChange={handleChange}
                                    placeholder="Nombre de la Entidad Social"
                                    id="es2"
                                  />
                                </div>
                              ) : null}
                            </div>

                            <div className="col-12 d-flex abajo">
                              <div className="col-4">
                                <label htmlFor="telefonoCliente">
                                  Teléfono del Cliente
</label>
                                <input
                                  readOnly
                                  type="text"
                                  className="form-control"
                                  name="telefonoCliente"
                                  onChange={handleChange}
                                  value={telefonoClientec}
                                  placeholder="Teléfono del cliente"
                                  id="tc2"
                                />
                              </div>
                            </div>
                          </fieldset>
                          </>
                        ):null}

                      </fieldset>
                    </ContenedorFormularioLg>

                    {/* V2 */}
                    <ContenedorFormularioLg id="ven2">
                      <fieldset className="mt-3">
                        <legend className="pl-5">Registro de la Llamada</legend>
                        <div className="col-12 d-flex mt-2">
                          <div className="col-4">
                            <label>Llamada No Contestada: </label>
                            <input
                              type="checkbox"
                              defaultChecked={Checked}
                              onChange={() => {
                                setChecked(!Checked);
                              }}
                            />
                          </div>
                        </div>
                        <div className="col-12 d-flex abajo">
                          <div className="col-8">
                            <label>Registro Llamada</label>
                            <textarea
                              className="form-control"
                              type="text"
                              name="registroLlamada"
                              onChange={handleChange}
                            />
                          </div>
                        </div>
                        <div className="col-12 d-flex abajo">
                          <div className="col-8">
                            <p>
                              <Label4> {Generales[0]} </Label4>
                            </p>
                            <Select
                              name="Tipo"
                              options={Tipo}
                              onChange={handleChangeTipo}
                              placeholder="Selecciona que tipo de servicio busca el cliente"
                            />
                            {errores.tipo && (
                              <p className="alert alert-danger text-center p-2 mt-2">
                                {errores.tipo}
                              </p>
                            )}
                          </div>
                        </div>
                        <div className="col-12 d-flex abajo">
                          <div className="col-8 ">
                            <label htmlFor="subEspecialidad">
                              SubEspecialidad de la Oportunidad *
                            </label>
                            <Select
                              name="subEspecialidad"
                              options={aTodo}
                              onChange={handleChangeSubEspecialidad}
                              placeholder="Selecciona Subespecialidad"
                            />

                            {errores.subEspecialidad && (
                              <p className="alert alert-danger text-center p-2 mt-2">
                                {errores.subEspecialidad}
                              </p>
                            )}
                          </div>
                          <div className="col-4 ">
                            <label htmlFor="especialidad">Especialidad</label>
                            <input
                              type="text"
                              className="form-control"
                              name="especialidad"
                              readOnly
                              id="iEspecialidad"
                              placeholder="Especialidad"
                            ></input>
                          </div>
                        </div>
                        <div className="col-12 d-flex mt-2 justify-content-between">
                          <div className="col-4">
                            <label htmlFor="Pais">Pais *</label>
                            <Select
                              onChange={handleChangePais}
                              options={Paises}
                              name="pais"
                              placeholder="Selecciona Pais"
                              defaultValue={sPais}
                            />
                            {errores.pais && (
                              <p className="alert alert-danger text-center p-2 mt-2">
                                {errores.pais}
                              </p>
                            )}
                          </div>
                          {pais == "Spain" || pais == "" ? (
                            <div className="col-4">
                              <label htmlFor="provincia">Provincia *</label>
                              <Select
                                onChange={handleChangeProvincia}
                                options={provincias}
                                name="provincia"
                                placeholder="Selecciona Provincia"
                              />
                              {errores.provincia && (
                                <p className="alert alert-danger text-center p-2 mt-2">
                                  {errores.provincia}
                                </p>
                              )}
                            </div>
                          ) : (
                              <div className="col-4">
                                <label htmlFor="provincia">Provincia *</label>
                                <Select
                                  onChange={handleChangeProvincia}
                                  options={opcionesProvincias}
                                  name="provincia"
                                  placeholder="Selecciona Provincia"
                                />
                                {errores.provincia && (
                                  <p className="alert alert-danger text-center p-2 mt-2">
                                    {errores.provincia}
                                  </p>
                                )}
                              </div>
                            )}
                          {/*}
                            <div className="col-4">
                              <label htmlFor="ciudad">Ciudad *</label>
                              <Select
                                onChange={handleChangeCiudad}
                                options={opcionesCiudades}
                                name="ciudad"
                                placeholder="Selecciona Ciudad"
                              />
                              {errores.ciudad && <p className="alert alert-danger text-center p-2 mt-2">{errores.ciudad}</p>}
                            </div>{*/}
                        </div>
                      </fieldset>
                    </ContenedorFormularioLg>
                    {/* V3 */}
                    {subEspecialidad !== "" ? (
                      <ContenedorFormularioLg id="ven3">
                        <fieldset className="mt-3">
                          <legend className="pl-5">
                            Preguntas Especificas
                          </legend>
                          {
                            subEspecialidad === "ERTE" ? ( //  ERTE
                              <div>
                                <div className="col-12 d-flex abajo">
                                  {" "}
                                  {/* Pregunta Text1 */}
                                  <div className="col-8">
                                    <label>{Erte["Text1"]}</label>
                                    <input
                                      name="respuestaText1"
                                      className="form-control"
                                      type="text"
                                      onChange={handleChange}
                                      value={respuestaText1}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex abajo">
                                  {" "}
                                  {/* Pregunta Text2 */}
                                  <div className="col-8">
                                    <label>{Erte["Text2"]}</label>
                                    <input
                                      name="respuestaText2"
                                      className="form-control"
                                      type="text"
                                      onChange={handleChange}
                                      value={respuestaText2}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex abajo">
                                  {" "}
                                  {/* Pregunta Text3 */}
                                  <div className="col-8">
                                    <label>{Erte["Text3"]}</label>
                                    <input
                                      name="respuestaText3"
                                      className="form-control"
                                      type="text"
                                      onChange={handleChange}
                                      value={respuestaText3}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex abajo">
                                  {" "}
                                  {/* Pregunta DespProv1 */}
                                  <div className="col-8">
                                    <label>{Erte["DespProv1"]}</label>
                                    <Select
                                      name="respuestaProvin"
                                      options={provincias}
                                      onChange={handleChangePrProvincia}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex abajo">
                                  {" "}
                                  {/* Pregunta Text4(num) */}
                                  <div className="col-8">
                                    <label>{Erte["Text4"]}</label>
                                    <input
                                      name="respuestaText4"
                                      className="form-control"
                                      type="number"
                                      min="0.01"
                                      step="0.01"
                                      onChange={handleChange}
                                      value={respuestaText4}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex abajo">
                                  {" "}
                                  {/* Pregunta Text5(num) */}
                                  <div className="col-8">
                                    <label>{Erte["Text5"]}</label>
                                    <input
                                      name="respuestaText5"
                                      className="form-control"
                                      type="number"
                                      min="0.01"
                                      step="0.01"
                                      onChange={handleChange}
                                      value={respuestaText5}
                                    />
                                    {parseInt(respuestaText5) >= 25 ? (
                                      <label>Avisar al departamento.</label>
                                    ) : null}
                                  </div>
                                </div>
                                <div className="col-12 d-flex abajo">
                                  {" "}
                                  {/* Pregunta SiNo1 */}
                                  <div className="col-8">
                                    <label>{Erte["SiNo1"]}</label>
                                    <Select
                                      name="respuestaSiNo1"
                                      options={SiNo}
                                      onChange={(event) =>
                                        handleChangeSiNo(event, 1)
                                      }
                                    />
                                  </div>
                                </div>
                              </div>
                            ) : subEspecialidad === "Despidos" ? ( // DESPIDOS
                              <div>
                                <div className="col-12 d-flex abajo">
                                  {" "}
                                  {/* Pregunta SiNo1 */}
                                  <div className="col-8">
                                    <label>{Despidos["SiNo1"]}</label>
                                    <Select
                                      name="respuestaSiNo1"
                                      options={SiNo}
                                      onChange={(event) =>
                                        handleChangeSiNo(event, 1)
                                      }
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex abajo">
                                  {" "}
                                  {/* Pregunta Text1 */}
                                  <div className="col-8">
                                    <label>{Despidos["Text1"]}</label>
                                    <input
                                      name="respuestaText1"
                                      className="form-control"
                                      type="date"
                                      onChange={handleChange}
                                      value={respuestaText1}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex abajo">
                                  {" "}
                                  {/* Pregunta Text2 */}
                                  <div className="col-8">
                                    <label>{Despidos["Text2"]}</label>
                                    <input
                                      name="respuestaText2"
                                      className="form-control"
                                      type="text"
                                      onChange={handleChange}
                                      value={respuestaText2}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex abajo">
                                  {" "}
                                  {/* Pregunta Text3 */}
                                  <div className="col-8">
                                    <label>{Despidos["Text3"]}</label>
                                    <input
                                      name="respuestaText3"
                                      className="form-control"
                                      type="text"
                                      onChange={handleChange}
                                      value={respuestaText3}
                                    />
                                  </div>
                                </div>
                              </div>
                            ) : subEspecialidad ===
                              "Negligencia Medica Civil" ? ( // NEGLIGENCIA MEDICA
                                    <div>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta Text1 */}
                                        <div className="col-8">
                                          <label>{NegligenciaMedica["Text1"]}</label>
                                          <input
                                            name="respuestaText1"
                                            className="form-control"
                                            type="text"
                                            onChange={handleChange}
                                            value={respuestaText1}
                                          />
                                        </div>
                                      </div>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta SiNo1 */}
                                        <div className="col-8">
                                          <label>{NegligenciaMedica["SiNo1"]}</label>
                                          <Select
                                            name="respuestaSiNo1"
                                            options={SiNo}
                                            onChange={(event) =>
                                              handleChangeSiNo(event, 1)
                                            }
                                          />
                                        </div>
                                      </div>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta SiNo2 */}
                                        <div className="col-8">
                                          <label>{NegligenciaMedica["SiNo2"]}</label>
                                          <Select
                                            name="respuestaSiNo2"
                                            options={SiNo}
                                            onChange={(event) =>
                                              handleChangeSiNo(event, 2)
                                            }
                                          />
                                        </div>
                                      </div>
                                      {respuestaSiNo2 === "Sí" ? (
                                        <div className="col-12 d-flex abajo">
                                          {" "}
                                          {/* Pregunta Text2 */}
                                          <div className="col-8">
                                            <label>
                                              {NegligenciaMedica["Text2"]}
                                            </label>
                                            <input
                                              name="respuestaText2"
                                              className="form-control"
                                              type="text"
                                              onChange={handleChange}
                                              value={respuestaText2}
                                            />
                                          </div>
                                        </div>
                                      ) : null}
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta SiNo3 */}
                                        <div className="col-8">
                                          <label>{NegligenciaMedica["SiNo3"]}</label>
                                          <Select
                                            name="respuestaSiNo3"
                                            options={SiNo}
                                            onChange={(event) =>
                                              handleChangeSiNo(event, 3)
                                            }
                                          />
                                        </div>
                                      </div>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta SiNo4 */}
                                        <div className="col-8">
                                          <label>{NegligenciaMedica["SiNo4"]}</label>
                                          <Select
                                            name="respuestaSiNo4"
                                            options={SiNo}
                                            onChange={(event) =>
                                              handleChangeSiNo(event, 4)
                                            }
                                          />
                                        </div>
                                      </div>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta Text3 */}
                                        <div className="col-8">
                                          <label>{NegligenciaMedica["Text3"]}</label>
                                          <input
                                            name="respuestaText3"
                                            className="form-control"
                                            type="date"
                                            onChange={handleChange}
                                            value={respuestaText3}
                                          />
                                        </div>
                                      </div>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta Text4 */}
                                        <div className="col-8">
                                          <label>{NegligenciaMedica["Text4"]}</label>
                                          <input
                                            name="respuestaText4"
                                            className="form-control"
                                            type="text"
                                            onChange={handleChange}
                                            value={respuestaText4}
                                          />
                                        </div>
                                      </div>
                                    </div>
                                  ) : subEspecialidad === "Herencia y Sucesiones" ? ( // HERENCIA
                                    <div>
                                      <p>
                                        <Label4>{Herencia["Titulo1"]}</Label4>
                                      </p>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta Text1 */}
                                        <div className="col-8">
                                          <label>{Herencia["Text1"]}</label>
                                          <input
                                            name="respuestaText1"
                                            className="form-control"
                                            type="date"
                                            onChange={handleChange}
                                            value={respuestaText1}
                                          />
                                        </div>
                                      </div>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta SiNo1 */}
                                        <div className="col-8">
                                          <label>{Herencia["SiNo1"]}</label>
                                          <Select
                                            name="respuestaSiNo1"
                                            options={SiNo}
                                            onChange={(event) =>
                                              handleChangeSiNo(event, 1)
                                            }
                                          />
                                        </div>
                                      </div>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta Text2 */}
                                        <div className="col-8">
                                          <label>{Herencia["Text2"]}</label>
                                          <input
                                            name="respuestaText2"
                                            className="form-control"
                                            type="number"
                                            min="0.01"
                                            step="0.01"
                                            onChange={handleChange}
                                            value={respuestaText2}
                                          />
                                        </div>
                                      </div>
                                      {parseInt(respuestaText2) > 0 ? (
                                        <div className="col-12 d-flex abajo">
                                          {" "}
                                          {/* Pregunta Text3 */}
                                          <div className="col-8">
                                            <label>{Herencia["Text3"]}</label>
                                            <input
                                              name="respuestaText3"
                                              className="form-control"
                                              type="text"
                                              onChange={handleChange}
                                              value={respuestaText3}
                                            />
                                          </div>
                                        </div>
                                      ) : null}
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta Text4 */}
                                        <div className="col-8">
                                          <label>{Herencia["Text4"]}</label>
                                          <input
                                            name="respuestaText4"
                                            className="form-control"
                                            type="text"
                                            onChange={handleChange}
                                            value={respuestaText4}
                                          />
                                        </div>
                                      </div>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta Text5 */}
                                        <div className="col-8">
                                          <label>{Herencia["Text5"]}</label>
                                          <input
                                            name="respuestaText5"
                                            className="form-control"
                                            type="text"
                                            onChange={handleChange}
                                            value={respuestaText5}
                                          />
                                        </div>
                                      </div>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta Text6 */}
                                        <div className="col-8">
                                          <label>{Herencia["Text6"]}</label>
                                          <input
                                            name="respuestaText6"
                                            className="form-control"
                                            type="text"
                                            onChange={handleChange}
                                            value={respuestaText6}
                                          />
                                        </div>
                                      </div>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta SiNo2 */}
                                        <div className="col-8">
                                          <label>{Herencia["SiNo2"]}</label>
                                          <Select
                                            name="respuestaSiNo2"
                                            options={SiNo}
                                            onChange={(event) =>
                                              handleChangeSiNo(event, 2)
                                            }
                                          />
                                        </div>
                                      </div>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta SiNo3 */}
                                        <div className="col-8">
                                          <label>{Herencia["SiNo3"]}</label>
                                          <Select
                                            name="respuestaSiNo3"
                                            options={SiNo}
                                            onChange={(event) =>
                                              handleChangeSiNo(event, 3)
                                            }
                                          />
                                        </div>
                                      </div>
                                      {respuestaSiNo3 === "Sí" ? (
                                        <div>
                                          <div className="col-12 d-flex abajo">
                                            {" "}
                                            {/* Pregunta Text7 */}
                                            <div className="col-8">
                                              <label>{Herencia["Text7"]}</label>
                                              <input
                                                name="respuestaText7"
                                                className="form-control"
                                                type="number"
                                                min="0.01"
                                                step="0.01"
                                                onChange={handleChange}
                                                value={respuestaText7}
                                              />
                                            </div>
                                          </div>
                                          <div className="col-12 d-flex abajo">
                                            {" "}
                                            {/* Pregunta SiNo4 */}
                                            <div className="col-8">
                                              <label>{Herencia["SiNo4"]}</label>
                                              <Select
                                                name="respuestaSiNo4"
                                                options={SiNo}
                                                onChange={(event) =>
                                                  handleChangeSiNo(event, 4)
                                                }
                                              />
                                            </div>
                                          </div>
                                        </div>
                                      ) : null}
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta SiNo5 */}
                                        <div className="col-8">
                                          <label>{Herencia["SiNo5"]}</label>
                                          <Select
                                            name="respuestaSiNo5"
                                            options={SiNo}
                                            onChange={(event) =>
                                              handleChangeSiNo(event, 5)
                                            }
                                          />
                                        </div>
                                      </div>
                                      <p>
                                        <Label4>{Herencia["Titulo2"]}</Label4>
                                      </p>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta SiNo6 */}
                                        <div className="col-8">
                                          <label>{Herencia["SiNo6"]}</label>
                                          <Select
                                            name="respuestaSiNo6"
                                            options={SiNo}
                                            onChange={(event) =>
                                              handleChangeSiNo(event, 6)
                                            }
                                          />
                                        </div>
                                      </div>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta SiNo7 */}
                                        <div className="col-8">
                                          <label>{Herencia["SiNo7"]}</label>
                                          <Select
                                            name="respuestaSiNo7"
                                            options={SiNo}
                                            onChange={(event) =>
                                              handleChangeSiNo(event, 7)
                                            }
                                          />
                                        </div>
                                      </div>
                                      {respuestaSiNo7 === "Sí" ? (
                                        <div>
                                          <div className="col-12 d-flex abajo">
                                            {" "}
                                            {/* Pregunta SiNo8 */}
                                            <div className="col-8">
                                              <label>{Herencia["SiNo8"]}</label>
                                              <Select
                                                name="respuestaSiNo8"
                                                options={SiNo}
                                                onChange={(event) =>
                                                  handleChangeSiNo(event, 8)
                                                }
                                              />
                                            </div>
                                          </div>
                                          <div className="col-12 d-flex abajo">
                                            {" "}
                                            {/* Pregunta SiNo9 */}
                                            <div className="col-8">
                                              <label>{Herencia["SiNo9"]}</label>
                                              <Select
                                                name="respuestaSiNo9"
                                                options={SiNo}
                                                onChange={(event) =>
                                                  handleChangeSiNo(event, 9)
                                                }
                                              />
                                            </div>
                                          </div>
                                          <div className="col-12 d-flex abajo">
                                            {" "}
                                            {/* Pregunta SiNo10 */}
                                            <div className="col-8">
                                              <label>{Herencia["SiNo10"]}</label>
                                              <Select
                                                name="respuestaSiNo10"
                                                options={SiNo}
                                                onChange={(event) =>
                                                  handleChangeSiNo(event, 10)
                                                }
                                              />
                                            </div>
                                          </div>
                                        </div>
                                      ) : null}
                                    </div>
                                  ) : subEspecialidad ===
                                    "Ley de Segunda Oportunidad" ? ( // LEY DE SEGUNDA OPORTUNIDAD
                                        <div>
                                          <div className="col-12 d-flex abajo">
                                            {" "}
                                            {/* Pregunta Text1 */}
                                            <div className="col-8">
                                              <label>
                                                {LeySegundaOportunidad["Text1"]}
                                              </label>
                                              <input
                                                name="respuestaText1"
                                                className="form-control"
                                                type="text"
                                                onChange={handleChange}
                                                value={respuestaText1}
                                              />
                                            </div>
                                          </div>
                                          <div className="col-12 d-flex abajo">
                                            <div className="col-8">
                                              <label>
                                                {LeySegundaOportunidad["SiNo1"]}
                                              </label>
                                              <Select
                                                name="respuestaSiNo1"
                                                options={SiNo}
                                                onChange={(event) =>
                                                  handleChangeSiNo(event, 1)
                                                }
                                              />
                                            </div>
                                          </div>
                                          <div className="col-12 d-flex abajo">
                                            <div className="col-8">
                                              <label>
                                                {LeySegundaOportunidad["SiNo2"]}
                                              </label>
                                              <Select
                                                name="respuestaSiNo2"
                                                options={SiNo}
                                                onChange={(event) =>
                                                  handleChangeSiNo(event, 2)
                                                }
                                              />
                                            </div>
                                          </div>
                                          {respuestaSiNo2 === "Sí" ? (
                                            <div className="col-12 d-flex abajo">
                                              <div className="col-8">
                                                <label>
                                                  {LeySegundaOportunidad["Text2"]}
                                                </label>
                                                <input
                                                  name="respuestaText2"
                                                  className="form-control"
                                                  type="text"
                                                  onChange={handleChange}
                                                  value={respuestaText2}
                                                />
                                              </div>
                                            </div>
                                          ) : null}
                                          <div className="col-12 d-flex abajo">
                                            <div className="col-8">
                                              <label>
                                                {LeySegundaOportunidad["SiNo3"]}
                                              </label>
                                              <Select
                                                name="respuestaSiNo3"
                                                options={SiNo}
                                                onChange={(event) =>
                                                  handleChangeSiNo(event, 3)
                                                }
                                              />
                                            </div>
                                          </div>
                                          {respuestaSiNo3 === "Sí" ? (
                                            <div className="col-12 d-flex abajo">
                                              <div className="col-8">
                                                <label>
                                                  {LeySegundaOportunidad["Text3"]}
                                                </label>
                                                <input
                                                  name="respuestaText3"
                                                  className="form-control"
                                                  type="text"
                                                  onChange={handleChange}
                                                  value={respuestaText3}
                                                />
                                              </div>
                                            </div>
                                          ) : null}
                                          <div className="col-12 d-flex abajo">
                                            <div className="col-8">
                                              <label>
                                                {LeySegundaOportunidad["SiNo4"]}
                                              </label>
                                              <Select
                                                name="respuestaSiNo3"
                                                options={SiNo}
                                                onChange={(event) =>
                                                  handleChangeSiNo(event, 3)
                                                }
                                              />
                                            </div>
                                          </div>
                                        </div>
                                      ) : subEspecialidad ===
                                        "Responsabilidad Patrimonial" ? ( // RESPONSABILIDAD PATRIMONIAL ESTADO
                                          <div>
                                            <div className="col-12 d-flex abajo">
                                              <div className="col-8">
                                                <label>
                                                  {
                                                    ResponsabilidadPatrimonialEstado[
                                                    "Text1"
                                                    ]
                                                  }
                                                </label>
                                                <input
                                                  name="respuestaText1"
                                                  className="form-control"
                                                  type="text"
                                                  onChange={handleChange}
                                                  value={respuestaText1}
                                                />
                                              </div>
                                            </div>
                                            <div className="col-12 d-flex abajo">
                                              <div className="col-8">
                                                <label>
                                                  {
                                                    ResponsabilidadPatrimonialEstado[
                                                    "Text2"
                                                    ]
                                                  }
                                                </label>
                                                <input
                                                  name="respuestaText2"
                                                  className="form-control"
                                                  type="text"
                                                  onChange={handleChange}
                                                  value={respuestaText2}
                                                />
                                              </div>
                                            </div>
                                            <div className="col-12 d-flex abajo">
                                              <div className="col-8">
                                                <label>
                                                  {
                                                    ResponsabilidadPatrimonialEstado[
                                                    "Text3"
                                                    ]
                                                  }
                                                </label>
                                                <input
                                                  name="respuestaText3"
                                                  className="form-control"
                                                  type="text"
                                                  onChange={handleChange}
                                                  value={respuestaText3}
                                                />
                                              </div>
                                            </div>
                                            <div className="col-12 d-flex abajo">
                                              <div className="col-8">
                                                <label>
                                                  {
                                                    ResponsabilidadPatrimonialEstado[
                                                    "SiNo1"
                                                    ]
                                                  }
                                                </label>
                                                <Select
                                                  name="respuestaSiNo1"
                                                  options={SiNo}
                                                  onChange={(event) =>
                                                    handleChangeSiNo(event, 1)
                                                  }
                                                />
                                              </div>
                                            </div>
                                            <div className="col-12 d-flex abajo">
                                              <div className="col-8">
                                                <label>
                                                  {
                                                    ResponsabilidadPatrimonialEstado[
                                                    "SiNo2"
                                                    ]
                                                  }
                                                </label>
                                                <Select
                                                  name="respuestaSiNo2"
                                                  options={SiNo}
                                                  onChange={(event) =>
                                                    handleChangeSiNo(event, 2)
                                                  }
                                                />
                                              </div>
                                            </div>
                                            <div className="col-12 d-flex abajo">
                                              <div className="col-8">
                                                <label>
                                                  {
                                                    ResponsabilidadPatrimonialEstado[
                                                    "SiNo3"
                                                    ]
                                                  }
                                                </label>
                                                <Select
                                                  name="respuestaSiNo3"
                                                  options={SiNo}
                                                  onChange={(event) =>
                                                    handleChangeSiNo(event, 3)
                                                  }
                                                />
                                              </div>
                                            </div>
                                          </div>
                                        ) : subEspecialidad === "Propiedad Intelectual" ? ( // PROPIEDAD INTELECTUAL
                                          <div>
                                            <div className="col-12 d-flex abajo">
                                              <div className="col-8">
                                                <label>
                                                  {PropiedadIntelectual["SiNo1"]}
                                                </label>
                                                <Select
                                                  name="respuestaSiNo1"
                                                  options={SiNo}
                                                  onChange={(event) =>
                                                    handleChangeSiNo(event, 1)
                                                  }
                                                />
                                              </div>
                                            </div>
                                            <div className="col-12 d-flex abajo">
                                              <div className="col-8">
                                                <label>
                                                  {PropiedadIntelectual["Text1"]}
                                                </label>
                                                <input
                                                  name="respuestaText1"
                                                  className="form-control"
                                                  type="text"
                                                  onChange={handleChange}
                                                  value={respuestaText1}
                                                />
                                              </div>
                                            </div>
                                            <div className="col-12 d-flex abajo">
                                              <div className="col-8">
                                                <label>
                                                  {PropiedadIntelectual["SiNo2"]}
                                                </label>
                                                <Select
                                                  name="respuestaSiNo2"
                                                  options={SiNo}
                                                  onChange={(event) =>
                                                    handleChangeSiNo(event, 2)
                                                  }
                                                />
                                              </div>
                                            </div>
                                            <div className="col-12 d-flex abajo">
                                              <div className="col-8">
                                                <label>
                                                  {PropiedadIntelectual["Text2"]}
                                                </label>
                                                <input
                                                  name="respuestaText2"
                                                  className="form-control"
                                                  type="text"
                                                  onChange={handleChange}
                                                  value={respuestaText2}
                                                />
                                              </div>
                                            </div>
                                          </div>
                                        ) : (
                                            <label>
                                              No hay preguntas específicas para esta
                                              subespecialidad.
                                            </label>
                                          ) // NO HAY PREGUNTAS
                          }
                        </fieldset>
                      </ContenedorFormularioLg>
                    ) : null}

                    {/* V4 */}
                    {tipo === "Cita" ? (
                      <>
                        <div id="ven4"></div>
                        {subEspecialidad !== "" &&
                          provinciaOportunidad !== "" ? (
                            <>
                              {/* Abogados Principal */}
                              {sinSugerencias != true ? (
                                <>
                                  <>
                                    <div className="col-12 d-flex abajo">
                                      <div className="col-8">
                                        <h2>Socios Principal Sugeridos</h2>
                                        <table
                                          className="table table-bordered table-hover"
                                        >
                                          <thead className="thead-dark text-center p-3">
                                            <tr className="m-2">
                                              <th className="align-middle">
                                                Seleccionar
                                          </th>
                                              <th className="align-middle">
                                                Nombre y Apellidos
                                          </th>
                                              <th className="align-middle">
                                                Comentarios
                                          </th>
                                              <th className="align-middle">
                                                Dirección
                                          </th>
                                              <th className="align-middle">
                                                Última Oportunidad
                                          </th>
                                            </tr>
                                          </thead>
                                          {abogadosSugeridosPrincipal.length !== 0 ? (
                                            <tbody>
                                              {abogadosSugeridosPrincipal.map(
                                                (abogado) => {
                                                  return (
                                                    <SeleccionarAbogado
                                                      key={abogado.id}
                                                      abogado={abogado}
                                                      seleccionarRadio={
                                                        seleccionarRadio
                                                      }
                                                    />
                                                  );
                                                }
                                              )}
                                            </tbody>
                                          ) : (
                                              <SinResultados className="text-center">
                                                <td colSpan="8">
                                                  La búsqueda no dió resultados
                                      </td>
                                              </SinResultados>)}
                                        </table>
                                      </div>
                                    </div>
                                  </>


                                  {/* Abogados Secundarios */}

                                  <>
                                    <div className="col-12 d-flex abajo">
                                      <div className="col-8">
                                        <h2>Socios Secundario Sugeridos</h2>
                                        <table
                                          className="table table-bordered table-hover"
                                        >
                                          <thead className="thead-dark text-center p-3">
                                            <tr className="m-2">
                                              <th className="align-middle">
                                                Seleccionar
                                          </th>
                                              <th className="align-middle">
                                                Nombre y Apellidos
                                          </th>
                                              <th className="align-middle">
                                                Comentarios
                                          </th>
                                              <th className="align-middle">
                                                Dirección
                                          </th>
                                              <th className="align-middle">
                                                Última Oportunidad
                                          </th>
                                            </tr>
                                          </thead>
                                          {abogadosSugeridosSecundario.length !== 0 ? (
                                            <tbody>
                                              {abogadosSugeridosSecundario.map(
                                                (abogado) => {
                                                  return (
                                                    <SeleccionarAbogado
                                                      key={abogado.id}
                                                      abogado={abogado}
                                                      seleccionarRadio={
                                                        seleccionarRadio
                                                      }
                                                    />
                                                  );
                                                }
                                              )}
                                            </tbody>
                                          ) : (
                                              <SinResultados className="text-center">
                                                <td colSpan="8">
                                                  La búsqueda no dió resultados
                                      </td>
                                              </SinResultados>
                                            )}
                                        </table>
                                      </div>
                                    </div>
                                  </>


                                  {/* Colaboradores Principales*/}

                                  <>
                                    <div className="col-12 d-flex abajo">
                                      <div className="col-8">
                                        <h2 className="mt-4">
                                          Colaboradores Sugeridos
                                </h2>
                                        <table className="table table-bordered table-hover w-100 ">
                                          <thead className="thead-dark text-center p-3">
                                            <tr className="m-2">
                                              <th className="align-middle">
                                                Seleccionar
                                      </th>
                                              <th className="align-middle">
                                                Nombre y Apellidos
                                      </th>
                                              <th className="align-middle">
                                                Comentarios
                                      </th>
                                              <th className="align-middle">
                                                Dirección
                                      </th>
                                              <th className="align-middle">
                                                Última Oportunidad
                                      </th>
                                            </tr>
                                          </thead>
                                          {colaboradoresSugeridosPrincipal.length !== 0 ? (
                                            <tbody>
                                              {colaboradoresSugeridosPrincipal.map(
                                                (abogado) => {
                                                  return (
                                                    <SeleccionarAbogado
                                                      key={abogado.id}
                                                      abogado={abogado}
                                                      seleccionarRadio={seleccionarRadio}
                                                    />
                                                  );
                                                }
                                              )}
                                            </tbody>
                                          ) : (
                                              <SinResultados className="text-center">
                                                <td colSpan="8">
                                                  La búsqueda no dió resultados
                                    </td>
                                              </SinResultados>
                                            )}
                                        </table>
                                      </div>
                                    </div>
                                  </>


                                  {/* Colaboradores Secundarios*/}

                                  <>
                                    <div className="col-12 d-flex abajo">
                                      <div className="col-8">
                                        <h2 className="mt-4">
                                          Colaboradores Sugeridos
                                </h2>
                                        <table className="table table-bordered table-hover w-100 ">
                                          <thead className="thead-dark text-center p-3">
                                            <tr className="m-2">
                                              <th className="align-middle">
                                                Seleccionar
                                      </th>
                                              <th className="align-middle">
                                                Nombre y Apellidos
                                      </th>
                                              <th className="align-middle">
                                                Comentarios
                                      </th>
                                              <th className="align-middle">
                                                Dirección
                                      </th>
                                              <th className="align-middle">
                                                Última Oportunidad
                                      </th>
                                            </tr>
                                          </thead>
                                          {colaboradoresSugeridosSecundario.length !== 0 ? (
                                            <tbody>
                                              {colaboradoresSugeridosSecundario.map(
                                                (abogado) => {
                                                  return (
                                                    <SeleccionarAbogado
                                                      key={abogado.id}
                                                      abogado={abogado}
                                                      seleccionarRadio={seleccionarRadio}
                                                    />
                                                  );
                                                }
                                              )}
                                            </tbody>
                                          ) : (
                                              <SinResultados className="text-center">
                                                <td colSpan="8">
                                                  La búsqueda no dió resultados
                                    </td>
                                              </SinResultados>
                                            )}
                                        </table>
                                      </div>
                                    </div>
                                  </>
                                </>
                              ) : null}


                              {/* Sin Sugerencias */}
                              {sinSugerencias && (
                                <>
                                  <H2Alerta className="text-center mb-3">
                                    No hay resultados de socios ni colaboradores
                                    en la provincia, elige otra opción:
                                </H2Alerta>

                                  <div>Selecciona</div>

                                  {/* Socios y Colaboradores de la CCAA */}
                                  <div className="d-flex justify-content-between mt-5">
                                    <h2 className="font-weight-bold">
                                      Comunidad Autonoma
                                  </h2>
                                    <div className="d-flex justify-content-between">
                                      <InputTextOportunidades
                                        type="text"
                                        value={qComunidad}
                                        onChange={(e) =>
                                          guardarQComunidad(e.target.value)
                                        }
                                        placeholder="Introduce el nombre del abogado..."
                                      // className="mt-2"
                                      />
                                      <InputSubmitSugerencias disabled>
                                        Buscar
                                    </InputSubmitSugerencias>
                                    </div>
                                  </div>

                                  <br />

                                  {/* <div className="d-flex mt-4 mb-4"> */}

                                  <div className="col-4">
                                    <label htmlFor="provincia">
                                      Cambiar Provincia:
                                  </label>
                                    <Select
                                      onChange={handleChangeProvincia}
                                      options={provincias}
                                      name="provincia"
                                      placeholder="Selecciona Provincia"
                                    />
                                    {errores.provincia && (
                                      <p className="alert alert-danger text-center p-2 mt-2">
                                        {errores.provincia}
                                      </p>
                                    )}
                                  </div>

                                  <br />

                                  <h2>Socios Principal</h2>
                                  <table className="table table-bordered table-hover w-100">
                                    <thead className="thead-dark text-center p-3">
                                      <tr className="m-2">
                                        <th className="align-middle">
                                          Seleccionar
                                      </th>
                                        <th className="align-middle">
                                          Nombre y Apellidos
                                      </th>
                                        <th className="align-middle">
                                          Comentarios
                                      </th>
                                        <th className="align-middle">
                                          Dirección
                                      </th>
                                        <th className="align-middle">
                                          Última Oportunidad
                                      </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      {sociosComunidadPrincipal.length === 0 ? (
                                        <SinResultados className="text-center">
                                          <td colSpan="8">
                                            La búsqueda no dió resultados
                                        </td>
                                        </SinResultados>
                                      ) : (
                                          sociosComunidadPrincipal.map((abogado) => {
                                            return (
                                              <SeleccionarAbogado
                                                key={abogado.id}
                                                abogado={abogado}
                                                seleccionarRadio={seleccionarRadio}
                                              />
                                            );
                                          })
                                        )}
                                    </tbody>
                                  </table>

                                  <h2>Socios Secundario</h2>
                                  <table className="table table-bordered table-hover w-100">
                                    <thead className="thead-dark text-center p-3">
                                      <tr className="m-2">
                                        <th className="align-middle">
                                          Seleccionar
                                      </th>
                                        <th className="align-middle">
                                          Nombre y Apellidos
                                      </th>
                                        <th className="align-middle">
                                          Comentarios
                                      </th>
                                        <th className="align-middle">
                                          Dirección
                                      </th>
                                        <th className="align-middle">
                                          Última Oportunidad
                                      </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      {sociosComunidadSecundario.length === 0 ? (
                                        <SinResultados className="text-center">
                                          <td colSpan="8">
                                            La búsqueda no dió resultados
                                        </td>
                                        </SinResultados>
                                      ) : (
                                          sociosComunidadSecundario.map((abogado) => {
                                            return (
                                              <SeleccionarAbogado
                                                key={abogado.id}
                                                abogado={abogado}
                                                seleccionarRadio={seleccionarRadio}
                                              />
                                            );
                                          })
                                        )}
                                    </tbody>
                                  </table>

                                  <h2>Colaboradores Principal</h2>
                                  <table className="table table-bordered table-hover w-100">
                                    <thead className="thead-dark text-center p-3">
                                      <tr className="m-2">
                                        <th className="align-middle">
                                          Seleccionar
                                      </th>
                                        <th className="align-middle">
                                          Nombre y Apellidos
                                      </th>
                                        <th className="align-middle">
                                          Comentarios
                                      </th>
                                        <th className="align-middle">
                                          Dirección
                                      </th>
                                        <th className="align-middle">
                                          Última Oportunidad
                                      </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      {colaboradoresComunidadPrincipal.length === 0 ? (
                                        <SinResultados className="text-center">
                                          <td colSpan="8">
                                            La búsqueda no dió resultados
                                        </td>
                                        </SinResultados>
                                      ) : (
                                          colaboradoresComunidadPrincipal.map((abogado) => {
                                            return (
                                              <SeleccionarAbogado
                                                key={abogado.id}
                                                abogado={abogado}
                                                seleccionarRadio={seleccionarRadio}
                                                abogadoAsignado={
                                                  listaAbogadosSeleccionados
                                                }
                                              />
                                            );
                                          })
                                        )}
                                    </tbody>
                                  </table>

                                  <h2>Colaboradores Secundario</h2>
                                  <table className="table table-bordered table-hover w-100">
                                    <thead className="thead-dark text-center p-3">
                                      <tr className="m-2">
                                        <th className="align-middle">
                                          Seleccionar
                                      </th>
                                        <th className="align-middle">
                                          Nombre y Apellidos
                                      </th>
                                        <th className="align-middle">
                                          Comentarios
                                      </th>
                                        <th className="align-middle">
                                          Dirección
                                      </th>
                                        <th className="align-middle">
                                          Última Oportunidad
                                      </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      {colaboradoresComunidadSecundario.length === 0 ? (
                                        <SinResultados className="text-center">
                                          <td colSpan="8">
                                            La búsqueda no dió resultados
                                        </td>
                                        </SinResultados>
                                      ) : (
                                          colaboradoresComunidadSecundario.map((abogado) => {
                                            return (
                                              <SeleccionarAbogado
                                                key={abogado.id}
                                                abogado={abogado}
                                                seleccionarRadio={seleccionarRadio}
                                                abogadoAsignado={
                                                  listaAbogadosSeleccionados
                                                }
                                              />
                                            );
                                          })
                                        )}
                                    </tbody>
                                  </table>

                                  {abogadoAsignado === ""
                                    ? (estado = "Pendiente Asignar")
                                    : null}
                                </>
                              )}

                              {/* Errores */}
                              {errores.abogadoAsignado ? (
                                <p className="alert alert-danger text-center p-2 mt-2">
                                  {errores.abogadoAsignado}
                                </p>
                              ) : null}

                              {error && (
                                <p className="alert alert-danger text-center p2 mt-2 mb-2">
                                  {error}
                                </p>
                              )}
                            </>
                          ) : null}
                      </>
                    ) : (
                        <>
                          <div id="ven4"></div>
                          {subEspecialidad !== "" &&
                            provinciaOportunidad !== "" ? (
                              <>
                                {/* Abogados Principal */}
                                {sinSugerencias != true ? (
                                  <>
                                    <>
                                      <div className="col-12 d-flex abajo">
                                        <div className="col-8">
                                          <h2>Socios Principal Sugeridos</h2>
                                          <table
                                            className="table table-bordered table-hover"
                                          >
                                            <thead className="thead-dark text-center p-3">
                                              <tr className="m-2">
                                                <th className="align-middle">
                                                  Seleccionar
                                          </th>
                                                <th className="align-middle">
                                                  Nombre y Apellidos
                                          </th>
                                                <th className="align-middle">
                                                  Comentarios
                                          </th>
                                                <th className="align-middle">
                                                  Dirección
                                          </th>
                                                <th className="align-middle">
                                                  Última Oportunidad
                                          </th>
                                              </tr>
                                            </thead>
                                            {abogadosSugeridosPrincipal.length !== 0 ? (
                                              <tbody>
                                                {abogadosSugeridosPrincipal.map(
                                                  (abogado) => {
                                                    return (
                                                      <SeleccionarAbogadoPresupuesto
                                                        key={abogado.id}
                                                        abogado={abogado}
                                                        abogadoAsignado={
                                                          listaAbogadosSeleccionados
                                                        }
                                                        seleccionarCheckBox={
                                                          seleccionarCheckBox
                                                        }
                                                      />
                                                    );
                                                  }
                                                )}
                                              </tbody>
                                            ) : (
                                                <SinResultados className="text-center">
                                                  <td colSpan="8">
                                                    La búsqueda no dió resultados
                                      </td>
                                                </SinResultados>)}
                                          </table>
                                        </div>
                                      </div>
                                    </>


                                    {/* Abogados Secundarios */}

                                    <>
                                      <div className="col-12 d-flex abajo">
                                        <div className="col-8">
                                          <h2>Socios Secundario Sugeridos</h2>
                                          <table
                                            className="table table-bordered table-hover"
                                          >
                                            <thead className="thead-dark text-center p-3">
                                              <tr className="m-2">
                                                <th className="align-middle">
                                                  Seleccionar
                                          </th>
                                                <th className="align-middle">
                                                  Nombre y Apellidos
                                          </th>
                                                <th className="align-middle">
                                                  Comentarios
                                          </th>
                                                <th className="align-middle">
                                                  Dirección
                                          </th>
                                                <th className="align-middle">
                                                  Última Oportunidad
                                          </th>
                                              </tr>
                                            </thead>
                                            {abogadosSugeridosSecundario.length !== 0 ? (
                                              <tbody>
                                                {abogadosSugeridosSecundario.map(
                                                  (abogado) => {
                                                    return (
                                                      <SeleccionarAbogadoPresupuesto
                                                        key={abogado.id}
                                                        abogado={abogado}
                                                        abogadoAsignado={
                                                          listaAbogadosSeleccionados
                                                        }
                                                        seleccionarCheckBox={
                                                          seleccionarCheckBox
                                                        }
                                                      />
                                                    );
                                                  }
                                                )}
                                              </tbody>
                                            ) : (
                                                <SinResultados className="text-center">
                                                  <td colSpan="8">
                                                    La búsqueda no dió resultados
                                      </td>
                                                </SinResultados>
                                              )}
                                          </table>
                                        </div>
                                      </div>
                                    </>


                                    {/* Colaboradores Principales*/}

                                    <>
                                      <div className="col-12 d-flex abajo">
                                        <div className="col-8">
                                          <h2 className="mt-4">
                                            Colaboradores Sugeridos
                                </h2>
                                          <table className="table table-bordered table-hover w-100 ">
                                            <thead className="thead-dark text-center p-3">
                                              <tr className="m-2">
                                                <th className="align-middle">
                                                  Seleccionar
                                      </th>
                                                <th className="align-middle">
                                                  Nombre y Apellidos
                                      </th>
                                                <th className="align-middle">
                                                  Comentarios
                                      </th>
                                                <th className="align-middle">
                                                  Dirección
                                      </th>
                                                <th className="align-middle">
                                                  Última Oportunidad
                                      </th>
                                              </tr>
                                            </thead>
                                            {colaboradoresSugeridosPrincipal.length !== 0 ? (
                                              <tbody>
                                                {colaboradoresSugeridosPrincipal.map(
                                                  (abogado) => {
                                                    return (
                                                      <SeleccionarAbogadoPresupuesto
                                                        key={abogado.id}
                                                        abogado={abogado}
                                                        abogadoAsignado={
                                                          listaAbogadosSeleccionados
                                                        }
                                                        seleccionarCheckBox={
                                                          seleccionarCheckBox
                                                        }
                                                      />
                                                    );
                                                  }
                                                )}
                                              </tbody>
                                            ) : (
                                                <SinResultados className="text-center">
                                                  <td colSpan="8">
                                                    La búsqueda no dió resultados
                                    </td>
                                                </SinResultados>
                                              )}
                                          </table>
                                        </div>
                                      </div>
                                    </>


                                    {/* Colaboradores Secundarios*/}

                                    <>
                                      <div className="col-12 d-flex abajo">
                                        <div className="col-8">
                                          <h2 className="mt-4">
                                            Colaboradores Sugeridos
                                </h2>
                                          <table className="table table-bordered table-hover w-100 ">
                                            <thead className="thead-dark text-center p-3">
                                              <tr className="m-2">
                                                <th className="align-middle">
                                                  Seleccionar
                                      </th>
                                                <th className="align-middle">
                                                  Nombre y Apellidos
                                      </th>
                                                <th className="align-middle">
                                                  Comentarios
                                      </th>
                                                <th className="align-middle">
                                                  Dirección
                                      </th>
                                                <th className="align-middle">
                                                  Última Oportunidad
                                      </th>
                                              </tr>
                                            </thead>
                                            {colaboradoresSugeridosSecundario.length !== 0 ? (
                                              <tbody>
                                                {colaboradoresSugeridosSecundario.map(
                                                  (abogado) => {
                                                    return (
                                                      <SeleccionarAbogadoPresupuesto
                                                        key={abogado.id}
                                                        abogado={abogado}
                                                        abogadoAsignado={
                                                          listaAbogadosSeleccionados
                                                        }
                                                        seleccionarCheckBox={
                                                          seleccionarCheckBox
                                                        }
                                                      />
                                                    );
                                                  }
                                                )}
                                              </tbody>
                                            ) : (
                                                <SinResultados className="text-center">
                                                  <td colSpan="8">
                                                    La búsqueda no dió resultados
                                    </td>
                                                </SinResultados>
                                              )}
                                          </table>
                                        </div>
                                      </div>
                                    </>
                                  </>
                                ) : null}


                                {/* Sin Sugerencias */}
                                {sinSugerencias && (
                                  <>
                                    <H2Alerta className="text-center mb-3">
                                      No hay resultados de socios ni colaboradores
                                      en la provincia, elige otra opción:
                                </H2Alerta>

                                    <div>Selecciona</div>

                                    {/* Socios y Colaboradores de la CCAA */}
                                    <div className="d-flex justify-content-between mt-5">
                                      <h2 className="font-weight-bold">
                                        Comunidad Autonoma
                                  </h2>
                                      <div className="d-flex justify-content-between">
                                        <InputTextOportunidades
                                          type="text"
                                          value={qComunidad}
                                          onChange={(e) =>
                                            guardarQComunidad(e.target.value)
                                          }
                                          placeholder="Introduce el nombre del abogado..."
                                        // className="mt-2"
                                        />
                                        <InputSubmitSugerencias disabled>
                                          Buscar
                                    </InputSubmitSugerencias>
                                      </div>
                                    </div>

                                    <br />

                                    {/* <div className="d-flex mt-4 mb-4"> */}

                                    <div className="col-4">
                                      <label htmlFor="provincia">
                                        Cambiar Provincia:
                                  </label>
                                      <Select
                                        onChange={handleChangeProvincia}
                                        options={provincias}
                                        name="provincia"
                                        placeholder="Selecciona Provincia"
                                      />
                                      {errores.provincia && (
                                        <p className="alert alert-danger text-center p-2 mt-2">
                                          {errores.provincia}
                                        </p>
                                      )}
                                    </div>

                                    <br />

                                    <h2>Socios Principal</h2>
                                    <table className="table table-bordered table-hover w-100">
                                      <thead className="thead-dark text-center p-3">
                                        <tr className="m-2">
                                          <th className="align-middle">
                                            Seleccionar
                                      </th>
                                          <th className="align-middle">
                                            Nombre y Apellidos
                                      </th>
                                          <th className="align-middle">
                                            Comentarios
                                      </th>
                                          <th className="align-middle">
                                            Dirección
                                      </th>
                                          <th className="align-middle">
                                            Última Oportunidad
                                      </th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        {sociosComunidadPrincipal.length === 0 ? (
                                          <SinResultados className="text-center">
                                            <td colSpan="8">
                                              La búsqueda no dió resultados
                                        </td>
                                          </SinResultados>
                                        ) : (
                                            sociosComunidadPrincipal.map((abogado) => {
                                              return (
                                                <SeleccionarAbogadoPresupuesto
                                                  key={abogado.id}
                                                  abogado={abogado}
                                                  abogadoAsignado={
                                                    listaAbogadosSeleccionados
                                                  }
                                                  seleccionarCheckBox={
                                                    seleccionarCheckBox
                                                  }
                                                />
                                              );
                                            })
                                          )}
                                      </tbody>
                                    </table>

                                    <h2>Socios Secundario</h2>
                                    <table className="table table-bordered table-hover w-100">
                                      <thead className="thead-dark text-center p-3">
                                        <tr className="m-2">
                                          <th className="align-middle">
                                            Seleccionar
                                      </th>
                                          <th className="align-middle">
                                            Nombre y Apellidos
                                      </th>
                                          <th className="align-middle">
                                            Comentarios
                                      </th>
                                          <th className="align-middle">
                                            Dirección
                                      </th>
                                          <th className="align-middle">
                                            Última Oportunidad
                                      </th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        {sociosComunidadSecundario.length === 0 ? (
                                          <SinResultados className="text-center">
                                            <td colSpan="8">
                                              La búsqueda no dió resultados
                                        </td>
                                          </SinResultados>
                                        ) : (
                                            sociosComunidadSecundario.map((abogado) => {
                                              return (
                                                <SeleccionarAbogadoPresupuesto
                                                  key={abogado.id}
                                                  abogado={abogado}
                                                  abogadoAsignado={
                                                    listaAbogadosSeleccionados
                                                  }
                                                  seleccionarCheckBox={
                                                    seleccionarCheckBox
                                                  }
                                                />
                                              );
                                            })
                                          )}
                                      </tbody>
                                    </table>

                                    <h2>Colaboradores Principal</h2>
                                    <table className="table table-bordered table-hover w-100">
                                      <thead className="thead-dark text-center p-3">
                                        <tr className="m-2">
                                          <th className="align-middle">
                                            Seleccionar
                                      </th>
                                          <th className="align-middle">
                                            Nombre y Apellidos
                                      </th>
                                          <th className="align-middle">
                                            Comentarios
                                      </th>
                                          <th className="align-middle">
                                            Dirección
                                      </th>
                                          <th className="align-middle">
                                            Última Oportunidad
                                      </th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        {colaboradoresComunidadPrincipal.length === 0 ? (
                                          <SinResultados className="text-center">
                                            <td colSpan="8">
                                              La búsqueda no dió resultados
                                        </td>
                                          </SinResultados>
                                        ) : (
                                            colaboradoresComunidadPrincipal.map((abogado) => {
                                              return (
                                                <SeleccionarAbogadoPresupuesto
                                                  key={abogado.id}
                                                  abogado={abogado}
                                                  seleccionarCheckBox={
                                                    seleccionarCheckBox
                                                  }
                                                  abogadoAsignado={
                                                    listaAbogadosSeleccionados
                                                  }
                                                />
                                              );
                                            })
                                          )}
                                      </tbody>
                                    </table>

                                    <h2>Colaboradores Secundario</h2>
                                    <table className="table table-bordered table-hover w-100">
                                      <thead className="thead-dark text-center p-3">
                                        <tr className="m-2">
                                          <th className="align-middle">
                                            Seleccionar
                                      </th>
                                          <th className="align-middle">
                                            Nombre y Apellidos
                                      </th>
                                          <th className="align-middle">
                                            Comentarios
                                      </th>
                                          <th className="align-middle">
                                            Dirección
                                      </th>
                                          <th className="align-middle">
                                            Última Oportunidad
                                      </th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        {colaboradoresComunidadSecundario.length === 0 ? (
                                          <SinResultados className="text-center">
                                            <td colSpan="8">
                                              La búsqueda no dió resultados
                                        </td>
                                          </SinResultados>
                                        ) : (
                                            colaboradoresComunidadSecundario.map((abogado) => {
                                              return (
                                                <SeleccionarAbogadoPresupuesto
                                                  key={abogado.id}
                                                  abogado={abogado}
                                                  seleccionarCheckBox={
                                                    seleccionarCheckBox
                                                  }
                                                  abogadoAsignado={
                                                    listaAbogadosSeleccionados
                                                  }
                                                />
                                              );
                                            })
                                          )}
                                      </tbody>
                                    </table>

                                    {abogadoAsignado === ""
                                      ? (estado = "Pendiente Asignar")
                                      : null}
                                  </>
                                )}

                                {/* Errores */}
                                {errores.abogadoAsignado ? (
                                  <p className="alert alert-danger text-center p-2 mt-2">
                                    {errores.abogadoAsignado}
                                  </p>
                                ) : null}

                                {error && (
                                  <p className="alert alert-danger text-center p2 mt-2 mb-2">
                                    {error}
                                  </p>
                                )}
                              </>

                            ) : null}
                        </>

                      )}

                    {/* V5 */}
                    {subEspecialidad !== "" && provinciaOportunidad !== "" ? (
                      <ContenedorFormularioLg id="ven5">
                        <fieldset className="mt-3">
                          <legend className="pl-8">Datos Oportunidad</legend>

                          <div className="col-12 d-flex abajo">
                            <div className="col-8">
                              <label>Datos de la oportunidad</label>
                              <textarea
                                className="form-control"
                                type="text"
                                name="descripcionOportunidad"
                                onChange={handleChange}
                                value={descripcionOportunidad}
                              />
                            </div>
                          </div>

                          <div className="col-12 d-flex abajo">
                            <div className="col-4">
                              <label htmlFor="idOportunidad">
                                ID de Oportunidad
                              </label>
                              <input
                                type="text"
                                className="form-control"
                                name="idOportunidad"
                                disabled
                                value={
                                  contadorDB !== undefined ? idOportunidad : ""
                                }
                              />
                            </div>
                          </div>
                          <fieldset>
                            <div className="col-12">
                              <Label4>Información del canal de entrada</Label4>
                            </div>
                            <div className="col-12 d-flex abajo">
                              <div className="col-5">
                                <label>Canal</label>
                                <Select
                                  name="canal"
                                  options={Canal}
                                  placeholder="Lawyou"
                                  onChange={handleChangeCanal}
                                />
                              </div>
                              <div className="col-5">
                                <label>SubCanal</label>
                                <Select
                                  name="subcanal"
                                  options={SubCanal}
                                  placeholder="Lawyou"
                                  onChange={handleChangeSubCanal}
                                />
                              </div>
                            </div>
                          </fieldset>
                        </fieldset>
                      </ContenedorFormularioLg>
                    ) : null}

                    {/*GESTION OPORTUNIDAD CITA O PRESUPUESTO VEN6 */}
                    {subEspecialidad !== "" && provinciaOportunidad !== "" ? (
                      <>
                        <div id="ven6"></div>{" "}
                        {/* Div vacio para redireccionar aqui */}
                        {tipo === "Cita" ? (
                          <ContenedorFormularioLg id="ven6">
                            <fieldset className="mt-3">
                              <legend className="pl-5">Gestion</legend>
                              <div className="col-12 d-flex abajo">
                                <div className="col-4">
                                  <label htmlFor="email">
                                    GESTION DE LA CITA
                                  </label>
                                </div>
                              </div>
                              <div className="col-12 d-flex abajo">
                                <div className="col-12">
                                  <label>Notas</label>
                                  <textarea
                                    className="form-control"
                                    type="text"
                                    name="notasGestion"
                                    onChange={handleChange}
                                  />
                                </div>
                              </div>
                              <div className="col-12 d-flex abajo">
                                <div className="col-4">
                                  <label>Fecha Cita</label>
                                  <input
                                    type="date"
                                    className="form-control"
                                    name="fechaCita"
                                    onChange={handleChange}
                                    value={fechaCita}
                                  />
                                </div>
                                <div className="col-4">
                                  <label>Hora Cita</label>
                                  <input
                                    type="time"
                                    className="form-control"
                                    name="horaCita"
                                    onChange={handleChange}
                                    value={horaCita}
                                  />
                                </div>
                              </div>
                              <div className="col-12 d-flex abajo">
                                <div className="col-4">
                                  <label htmlFor="email">Email</label>
                                  <input
                                    type="text"
                                    className="form-control"
                                    name="email"
                                    onChange={handleChange}
                                    value={email}
                                    placeholder="Email del Cliente"
                                  />

                                  {
                                    //errores.email && <p className="alert alert-danger text-center p-2 mt-2">{errores.email}</p>
                                  }
                                </div>
                                {entidadLegal === "Persona Física" ? (
                                  <div className="col-4">
                                    <label>DNI</label>
                                    <input
                                      type="text"
                                      className="form-control"
                                      name="dni"
                                      onChange={handleChange}
                                      value={dni}
                                      placeholder="DNI del Cliente"
                                    />
                                  </div>
                                ) : (
                                    <div className="col-4">
                                      <label>NIF</label>
                                      <input
                                        type="text"
                                        className="form-control"
                                        name="nif"
                                        onChange={handleChange}
                                        value={nif}
                                        placeholder="DNI del Cliente"
                                      />
                                    </div>
                                  )}
                              </div>

                              {entidadLegal === "Persona Jurídica" ? (
                                <div className="col-12 d-flex abajo">
                                  <div className="col-8">
                                    <label>CIF de la Empresa</label>
                                    <input
                                      type="text"
                                      className="form-control"
                                      name="cif"
                                      onChange={handleChange}
                                      value={cif}
                                      placeholder="CIF de la Entidad Social"
                                    />
                                  </div>
                                </div>
                              ) : null}
                              <div className="col-12 d-flex abajo">
                                <div className="col-4">
                                  <label htmlFor="servicio">Servicio *</label>
                                  <Select
                                    name="servicio"
                                    options={Servicio}
                                    placeholder="Servicios"
                                    defaultValue={sDefaultServicio}
                                  />
                                </div>
                                <div className="col-4">
                                  <label htmlFor="precio">
                                    Precio del Servicio (Sin IVA)
                                  </label>
                                  <InputGroup className="mb-6">
                                    <input
                                      type="number"
                                      min="0.01"
                                      step="0.01"
                                      className="col-6"
                                      name="precioServicio"
                                      onChange={handleChange}
                                      placeholder="Precio del servicio"
                                      defaultValue={sPrecio}
                                    />
                                    <InputGroup.Prepend>
                                      <InputGroup.Text>€</InputGroup.Text>
                                    </InputGroup.Prepend>
                                  </InputGroup>
                                </div>
                              </div>
                              <div className="col-12 d-flex abajo">
                                <div className="col-4">
                                  <label>Tipo de Servicio *</label>
                                  <Select
                                    name="tipoServicio"
                                    options={Tipo_Servicio}
                                    placeholder="Tipo de Servicio"
                                    defaultValue={sDefaultTipoServicio}
                                  />
                                </div>
                              </div>
                              <div className="col-12 d-flex abajo">
                                <div className="col-6">
                                  <label htmlFor="AbogadoAsignado">
                                    Abogado Asignado
                                  </label>
                                  <input
                                    readOnly
                                    type="text"
                                    className="form-control"
                                    name="abogadoAsignado"
                                    id="abogadoAsignado"
                                    onChange={handleChange}
                                    placeholder="Email del Cliente"
                                    value={abogadoAsignado.nombre}
                                  />
                                </div>
                              </div>
                              <div className="col-12 d-flex mt-2">
                                <div className="col-4">
                                  <label>MandarEmail: </label>
                                  <input
                                    type="checkbox"
                                    defaultChecked={CheckedEmail}
                                    onChange={() => {
                                      setCheckedEmail(!CheckedEmail);
                                    }}
                                  />
                                </div>
                              </div>
                              <div className="col-12 d-flex mt-2">
                                <div className="col-4">
                                  <label>Mandar SMS: </label>
                                  <input
                                    type="checkbox"
                                    defaultChecked={CheckedSms}
                                    onChange={() => {
                                      setCheckedSms(!CheckedSms);
                                    }}
                                  />
                                </div>
                              </div>
                              <div className="col-12 d-flex mt-2">
                                <div className="col-4">
                                  <label>Pasarela: </label>
                                  <input
                                    type="checkbox"
                                    defaultChecked={CheckedPasarela}
                                    onChange={() => {
                                      setCheckedPasarela(!CheckedPasarela);
                                    }}
                                  />
                                </div>
                              </div>
                              <div className="col-12 d-flex mt-2">
                                <div className="col-4">
                                  <label>WhatsApp: </label>
                                  <input
                                    type="checkbox"
                                    defaultChecked={CheckedApp}
                                    onChange={() => {
                                      setCheckedApp(!CheckedApp);
                                    }}
                                  />
                                </div>
                              </div>
                              <div className="col-12 d-flex mt-2">
                                <div className="col-4">
                                  <label>Tarea de Seguimiento: </label>
                                  <input
                                    type="checkbox"
                                    defaultChecked={CheckedSeguimiento}
                                    onChange={() => {
                                      setCheckedSeguimiento(
                                        !CheckedSeguimiento
                                      );
                                    }}
                                  />
                                </div>
                              </div>
                            </fieldset>
                          </ContenedorFormularioLg>
                        ) : (
                            <ContenedorFormularioLg id="ven6">
                              <fieldset className="mt-3">
                                <legend className="pl-5">Gestion</legend>
                                <div className="col-12 d-flex abajo">
                                  <div className="col-12">
                                    <label htmlFor="email">
                                      GESTION DEL PRESUPUESTO
                                  </label>
                                  </div>
                                </div>
                                <div className="col-12 d-flex abajo">
                                  <div className="col-12">
                                    <label>Notas</label>
                                    <textarea
                                      className="form-control"
                                      type="text"
                                      name="notasGestion"
                                      onChange={handleChange}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex abajo">
                                  <div className="col-8">
                                    <label htmlFor="precio">
                                      Presupuesto Orientativo
                                  </label>
                                    <InputGroup className="mb-6">
                                      <input
                                        type="number"
                                        min="0.01"
                                        step="0.01"
                                        className="col-10"
                                        name="precioPresupuesto"
                                        onChange={handleChange}
                                        placeholder="Precio del presupuesto"
                                      />
                                      <InputGroup.Prepend>
                                        <InputGroup.Text>€</InputGroup.Text>
                                      </InputGroup.Prepend>
                                    </InputGroup>
                                  </div>
                                </div>

                                {listaAbogadosSeleccionados.length !== 0 ? (
                                  <>
                                    <fieldset>
                                      {" "}
                                      <legend>Abogados Seleccionados</legend>
                                      {listaAbogadosSeleccionados.map(
                                        (e, index) => {
                                          return (
                                            <div className="col-12 d-flex abajo">
                                              <div className="col-6">
                                                <label htmlFor="abogadoSeleccionado">
                                                  Abogado Seleccionado {index + 1}
                                                </label>
                                                <p>
                                                  <strong>
                                                    {e.nombre} {e.apellidos}
                                                  </strong>
                                                </p>
                                              </div>
                                            </div>
                                          );
                                        }
                                      )}
                                    </fieldset>
                                  </>
                                ) : null}

                                <div className="col-12 d-flex abajo">
                                  <div className="col-4">
                                    <label htmlFor="servicio">Servicio *</label>
                                    <Select
                                      name="servicio"
                                      options={Servicio}
                                      placeholder="Servicios"
                                      defaultValue={sDefaultServicio}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex abajo">
                                  <div className="col-4">
                                    <label>Tipo de Servicio *</label>
                                    <Select
                                      name="tipoServicio"
                                      options={Tipo_Servicio}
                                      placeholder="Tipo de Servicio"
                                      defaultValue={sDefaultTipoServicio}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex abajo">
                                  <div className="col-4">
                                    <label htmlFor="email">Email</label>
                                    <input
                                      type="text"
                                      className="form-control"
                                      name="email"
                                      onChange={handleChange}
                                      value={email}
                                      placeholder="Email del Cliente"
                                    />

                                    {
                                      //errores.email && <p className="alert alert-danger text-center p-2 mt-2">{errores.email}</p>
                                    }
                                  </div>
                                  {entidadLegal === "Persona Física" ? (
                                    <div className="col-4">
                                      <label>DNI</label>
                                      <input
                                        type="text"
                                        className="form-control"
                                        name="dni"
                                        onChange={handleChange}
                                        value={dni}
                                        placeholder="DNI del Cliente"
                                      />
                                    </div>
                                  ) : (
                                      <div className="col-4">
                                        <label>nif</label>
                                        <input
                                          type="text"
                                          className="form-control"
                                          name="nif"
                                          onChange={handleChange}
                                          value={nif}
                                          placeholder="DNI del Cliente"
                                        />
                                      </div>
                                    )}
                                </div>

                                {entidadLegal === "Persona Jurídica" ? (
                                  <div className="col-12 d-flex abajo">
                                    <div className="col-8">
                                      <label>CIF de la Empresa</label>
                                      <input
                                        type="text"
                                        className="form-control"
                                        name="cif"
                                        onChange={handleChange}
                                        value={cif}
                                        placeholder="CIF de la Entidad Social"
                                      />
                                    </div>
                                  </div>
                                ) : null}
                                <div className="col-12 d-flex mt-2">
                                  <div className="col-12">
                                    <label>Preguntar Abogado Presupuesto: </label>
                                    <input
                                      type="checkbox"
                                      defaultChecked={CheckedPreguntarPresupuesto}
                                      onChange={() => {
                                        setCheckedPreguntarPresupuesto(
                                          !CheckedPreguntarPresupuesto
                                        );
                                      }}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex mt-2">
                                  <div className="col-12">
                                    <label>
                                      Enviar Email con el Presupuesto:{" "}
                                    </label>
                                    <input
                                      type="checkbox"
                                      defaultChecked={CheckedEmailPresupuesto}
                                      onChange={() => {
                                        setCheckedEmailPresupuesto(
                                          !CheckedEmailPresupuesto
                                        );
                                      }}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex mt-2">
                                  <div className="col-12">
                                    <label>
                                      Llamar al Cliente con el Presupuesto:{" "}
                                    </label>
                                    <input
                                      type="checkbox"
                                      defaultChecked={CheckedLlamar}
                                      onChange={() => {
                                        setCheckedLlamar(!CheckedLlamar);
                                      }}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex mt-2">
                                  <div className="col-12">
                                    <label>Gestionar Cita: </label>
                                    <input
                                      type="checkbox"
                                      defaultChecked={CheckedGestionarCita}
                                      onChange={() => {
                                        setCheckedGestionarCita(
                                          !CheckedGestionarCita
                                        );
                                      }}
                                    />
                                  </div>
                                </div>
                              </fieldset>
                            </ContenedorFormularioLg>
                          )}
                      </>
                    ) : null}

                    {CheckedGestionarCita ? (
                      <ContenedorFormularioLg id="ven7">
                        <fieldset className="mt-3">
                          <legend className="pl-5">Gestion de la cita</legend>
                          <div className="col-12 d-flex abajo">
                            <div className="col-12">
                              <label>Notas</label>
                              <textarea
                                className="form-control"
                                type="text"
                                name="notasGestion"
                                onChange={handleChange}
                              />
                            </div>
                          </div>
                          <div className="col-12 d-flex abajo">
                            <div className="col-4">
                              <label>Fecha Cita</label>
                              <input
                                type="date"
                                className="form-control"
                                name="fechaCita"
                                onChange={handleChange}
                                value={fechaCita}
                              />
                            </div>
                            <div className="col-4">
                              <label>Hora Cita</label>
                              <input
                                type="time"
                                className="form-control"
                                name="horaCita"
                                onChange={handleChange}
                                value={horaCita}
                              />
                            </div>
                          </div>
                          <div className="col-12 d-flex abajo">
                            <div className="col-4">
                              <label htmlFor="email">Email</label>
                              <input
                                type="text"
                                className="form-control"
                                name="email"
                                onChange={handleChange}
                                value={email}
                                placeholder="Email del Cliente"
                              />

                              {
                                //errores.email && <p className="alert alert-danger text-center p-2 mt-2">{errores.email}</p>
                              }
                            </div>
                            {entidadLegal === "Persona Física" ? (
                              <div className="col-4">
                                <label>DNI</label>
                                <input
                                  type="text"
                                  className="form-control"
                                  name="dni"
                                  onChange={handleChange}
                                  value={dni}
                                  placeholder="DNI del Cliente"
                                />
                              </div>
                            ) : (
                                <div className="col-4">
                                  <label>NIF</label>
                                  <input
                                    type="text"
                                    className="form-control"
                                    name="nif"
                                    onChange={handleChange}
                                    value={nif}
                                    placeholder="DNI del Cliente"
                                  />
                                </div>
                              )}
                          </div>

                          {entidadLegal === "Persona Jurídica" ? (
                            <div className="col-12 d-flex abajo">
                              <div className="col-8">
                                <label>CIF de la Empresa</label>
                                <input
                                  type="text"
                                  className="form-control"
                                  name="cif"
                                  onChange={handleChange}
                                  value={cif}
                                  placeholder="CIF de la Entidad Social"
                                />
                              </div>
                            </div>
                          ) : null}
                          <div className="col-12 d-flex abajo">
                            <div className="col-4">
                              <label htmlFor="servicio">Servicio *</label>
                              <Select
                                name="servicio"
                                options={Servicio}
                                placeholder="Servicios"
                                defaultValue={sDefaultServicio}
                              />
                            </div>
                            <div className="col-4">
                              <label htmlFor="precio">
                                Precio del Servicio (SIN IVA)
                              </label>
                              <InputGroup className="mb-6">
                                <input
                                  type="number"
                                  min="0.01"
                                  step="0.01"
                                  className="col-6"
                                  name="precioServicio"
                                  onChange={handleChange}
                                  placeholder="Precio del servicio"
                                  defaultValue={sPrecio}
                                />
                                <InputGroup.Prepend>
                                  <InputGroup.Text>€</InputGroup.Text>
                                </InputGroup.Prepend>
                              </InputGroup>
                            </div>
                          </div>
                          <div className="col-12 d-flex abajo">
                            <div className="col-4">
                              <label>Tipo de Servicio *</label>
                              <Select
                                name="tipoServicio"
                                options={Tipo_Servicio}
                                placeholder="Tipo de Servicio"
                                defaultValue={sDefaultTipoServicio}
                              />
                            </div>
                          </div>
                          <div className="col-12 d-flex abajo">
                            <div className="col-6">
                              <label htmlFor="AbogadoAsignado">Abogado Asignado</label>
                              <input
                                readOnly
                                type="text"
                                className="form-control"
                                name="abogadoAsignado"
                                id="abogadoAsignado"
                                onChange={handleChange}
                                placeholder="Email del Cliente"
                                value={abogadoAsignado.nombre}
                              />
                            </div>
                          </div>
                          <div className="col-12 d-flex mt-2">
                            <div className="col-4">
                              <label>MandarEmail: </label>
                              <input
                                type="checkbox"
                                defaultChecked={CheckedEmail}
                                onChange={() => {
                                  setCheckedEmail(!CheckedEmail);
                                }}
                              />
                            </div>
                          </div>
                          <div className="col-12 d-flex mt-2">
                            <div className="col-4">
                              <label>Mandar SMS: </label>
                              <input
                                type="checkbox"
                                defaultChecked={CheckedSms}
                                onChange={() => {
                                  setCheckedSms(!CheckedSms);
                                }}
                              />
                            </div>
                          </div>
                          <div className="col-12 d-flex mt-2">
                            <div className="col-4">
                              <label>Pasarela: </label>
                              <input
                                type="checkbox"
                                defaultChecked={CheckedPasarela}
                                onChange={() => {
                                  setCheckedPasarela(!CheckedPasarela);
                                }}
                              />
                            </div>
                          </div>
                          <div className="col-12 d-flex mt-2">
                            <div className="col-4">
                              <label>WhatsApp: </label>
                              <input
                                type="checkbox"
                                defaultChecked={CheckedApp}
                                onChange={() => {
                                  setCheckedApp(!CheckedApp);
                                }}
                              />
                            </div>
                          </div>
                          <div className="col-12 d-flex mt-2">
                            <div className="col-4">
                              <label>Tarea de Seguimiento: </label>
                              <input
                                type="checkbox"
                                defaultChecked={CheckedSeguimiento}
                                onChange={() => {
                                  setCheckedSeguimiento(!CheckedSeguimiento);
                                }}
                              />
                            </div>
                          </div>
                        </fieldset>
                      </ContenedorFormularioLg>
                    ) : null}

                    {Checked == true ? (
                      <>
                        {/* Boton Crear Llamada contestada*/}
                        <div className="col-12 d-flex justify-content-center">
                          <BotonAceptar
                            className="btn btn-primary"
                            type="button"
                            onClick={() => crearNuevaOportunidad()}
                          >
                            Crear Oportunidad
                          </BotonAceptar>
                        </div>
                      </>
                    ) : (
                        <>
                          {/* Boton Crear Llamada contestada*/}
                          <div className="col-12 d-flex justify-content-center">
                            <BotonAceptar
                              className="btn btn-primary"
                              type="submit"
                            >
                              Crear Oportunidad
                          </BotonAceptar>
                          </div>
                        </>
                      )}
                  </DivScroll>
                </Divp1>
              </form>
            </>
          ) : (
                  <>
                    <PaginaError
                      msg={`Tienes que estar logueado para acceder a esta sección.`}
                    ></PaginaError>
                    <div className="text-center">
                      <Link href="/iniciar-sesion">
                        <a>
                          <h1>Inicia Sesión</h1>
                        </a>
                      </Link>
                    </div>
                  </>
                )}
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}
export default MyVerticallyCenteredModal;
