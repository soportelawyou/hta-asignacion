import React, { useState, useEffect, useContext } from 'react';
import Router, { useRouter } from 'next/router';
import Link from 'next/link';
import { BotonAceptar } from '../components/ui/Formulario';
import PaginaError from '../components/layout/PaginaError';
import { Modal, Button } from 'react-bootstrap';
import useAbogadosSugerencia from '../hooks/useAbogadosSugerencia';
import { SinResultados } from '../components/ui/SinResultados';
import SeleccionarAbogado from '../components/layout/SeleccionarAbogado';
import { InputTextOportunidades, InputSubmit } from '../components/ui/Busqueda';
// Validaciones
import useValidacionOportunidadesEditar from '../hooks/useValidacionOportunidadesEditar';
import validarEditarOportunidad from '../validacion/validarEditarOportunidad';

// Firebase

import { FirebaseContext } from '../firebase/index';

//Estilo
import {
    Divp1
} from "../components/ui/Visual";

import {
    provincias,
    especialidades,
    comunidadesConProvincias,
    Tipo_Servicio,
    Servicio
} from "../components/ui/OpcionesSelectores";

export function MyVerticallyCenteredModal(props) {
    const id = props.id;
    const [oportunidad, guardarOportunidad] = useState({});
    const [comunidadDeProvincia, guardarComunidadDeProvincia] = useState("");
    const { abogados } = useAbogadosSugerencia("ultimaOportunidad");
    const { usuario, firebase } = useContext(FirebaseContext);
    const router = useRouter();
    const [error, guardarError] = useState(false);
    const [errorDB, guardarErrorDB] = useState([]);
    const [contadorAbogado, guardarContadorAbogado] = useState(0);
    const [contadorAceptadosAbogado, guardarContadorAceptadosAbogado] = useState(0);
    const [contadorDB, guardarContadorDB] = useState(0);
    const [consultarDB, guardarConsultarDB] = useState(true)
    const [abogadoAsignadoOriginal, guardarAbogadoAsignadoOriginal] = useState('');
    const [nombreAbogado, guardarNombreAbogado] = useState('');
    const [rol, guardarRol] = useState("");

    const [abogadosSugeridos, guardarAbogadosSugeridos] = useState([]);
    const [colaboradoresSugeridos, guardarColaboradoresSugeridos] = useState([]);
    const [sinSugerencias, guardarSinSugerencias] = useState(false);
    const [busquedaManual, guardarBusquedaManual] = useState(false);
    const [cargando, guardarCargando] = useState(false);
    const [otrasSugerencias, guardarOtrasSugerencias] = useState([]);
    const [abogadosPorComunidad, guardarAbogadosPorComunidad] = useState([]);

    const [sociosComunidad, guardarSociosComunidad] = useState([]);
    const [colaboradoresComunidad, guardarColaboradoresComunidad] = useState([]);
    const [sociosNacionales, guardarSociosNacionales] = useState([]);
    const [colaboradoresNacionales, guardarColaboradoresNacionales] = useState(
        []
    );


    useEffect(() => {
        // Declaramos variable para trackear
        let estaCancelado = false;
        // console.log('useEffect [ID].js')

        if (id && consultarDB) {
            const obtenerOportunidad = async () => {
                const oportunidadQuery = await firebase.db.collection('oportunidades').doc(id);
                const oportunidad = await oportunidadQuery.get();

                // Se ejecuta solo si esta montado
                if (!estaCancelado) {
                    if (oportunidad.exists) {
                        guardarOportunidad(oportunidad.data());
                    } else {
                        guardarError(true);
                    }
                    guardarConsultarDB(false);
                }

            }
            obtenerOportunidad();
        }
        return () => {
            // Ponemos la variable a true para evitar que intente actualizar un state inexistente
            estaCancelado = true;
        }
    }, [id, oportunidad])

    const busqueda = async (email) => {
        await firebase.db.collection('usuarios').where("email", "==", email).onSnapshot(manejarSnapShot);
    }

    function manejarSnapShot(snapshot) {
        const usuarioBD = snapshot.docs.map(doc => {
            return {
                id: doc.id,
                ...doc.data()
            }
        })
        if (usuarioBD[0] !== undefined) {
            // console.log("USUARIO DB", usuarioBD[0])
            console.log("entre en el guardar usuarios");
            console.log(usuarioBD)
            guardarRol(usuarioBD[0].rol);
        }
        guardarCargando(false)
    }

    useEffect(() => {
        guardarCargando(true)
        if (usuario !== undefined && usuario !== null) {
            console.log(usuario.email)
            busqueda(usuario.email)
        }
    }, [usuario])

    const { valores, errores, handleSubmit, handleChange, handleChangeProvincia, handleChangeComunidad, handleChangeEspecialidad, handleChangeAbogadoAsignado, handleChangeEstado, handleChangeMotivo } = useValidacionOportunidadesEditar(oportunidad, validarEditarOportunidad, editarOportunidad);
    var { nombreCliente, telefonoCliente, especialidad, ciudad, email, descripcion, idOportunidad, provinciaOportunidad, abogadoAsignado, estado, creado, creador, numeroReasignaciones, idOportunidadOriginal, rechazada, reasignada, abogadosRechazadores, nombreAbogadoAsignado, motivoRechazo, textoMotivoRechazo } = valores;


    // Sugerencias de SOCIOS y COLABORADORES
    useEffect(() => {
        let montado = true;
        if (montado && provinciaOportunidad !== "") {
            console.log("comunidadesConProvincias", comunidadesConProvincias);
            const comuna = comunidadesConProvincias.find((prov) =>
                prov.prov.includes(provinciaOportunidad)
            );
            if (comuna === undefined) {
                guardarComunidadDeProvincia("")
            }
            else {
                guardarComunidadDeProvincia(comuna.com);
            }
        }

        if (abogados) {
            // console.log('abogados', abogados)
            if (provinciaOportunidad !== undefined) {
                const provinciaMinuscula = provinciaOportunidad.toLowerCase();
                const provinciaSinTildes = quitarTildes(provinciaMinuscula);
                const ciudadMinuscula = ciudad.toLowerCase();
                const ciudadSinTildes = quitarTildes(ciudadMinuscula);
                // const especialidadMinuscula = especialidad.toLowerCase();
                // const especialidadSinTildes = quitarTildes(especialidadMinuscula);
                const abogadosFiltrados = abogados.filter((abogado) => {
                    if (especialidad !== "" && provinciaOportunidad !== "") {
                        return (
                            quitarTildes(abogado.provincia.toLowerCase()).includes(
                                provinciaSinTildes
                            ) &&
                            abogado.especialidadesAbogado.includes(especialidad) &&
                            abogado.tipo !== "Colaborador" &&
                            abogado.activo === "Activo" &&
                            quitarTildes(abogado.ciudad.toLowerCase()).includes(ciudadSinTildes)
                        );
                    }
                });
                const colaboradoresFiltrados = abogados.filter((abogado) => {
                    if (especialidad !== "" && provinciaOportunidad !== "") {
                        return (
                            quitarTildes(abogado.provincia.toLowerCase()).includes(
                                provinciaSinTildes
                            ) &&
                            abogado.especialidadesAbogado.includes(especialidad) &&
                            abogado.tipo === "Colaborador" &&
                            abogado.activo === "Activo" &&
                            quitarTildes(abogado.ciudad.toLowerCase()).includes(ciudadSinTildes)
                        );
                    }
                });
                if (montado) {
                    guardarAbogadosSugeridos(abogadosFiltrados);
                    console.log("sociosSugeridos", abogadosFiltrados);
                    guardarColaboradoresSugeridos(colaboradoresFiltrados);
                    console.log("colaboradoresSugeridos", colaboradoresFiltrados);
                    if (
                        especialidad !== "" &&
                        provinciaOportunidad !== "" &&
                        abogadosFiltrados.length === 0 &&
                        colaboradoresFiltrados.length === 0
                    ) {
                        guardarSinSugerencias(true);
                        guardarBusquedaManual(true);
                        return;
                    }
                    guardarSinSugerencias(false);
                    guardarBusquedaManual(false);
                }
            }
        }
        return () => (montado = false);
    }, [provinciaOportunidad, especialidad, ciudad]);

    const quitarTildes = (palabra) => {
        const letras = { á: "a", é: "e", í: "i", ó: "o", ú: "u" };
        palabra = palabra.replace(/[áéíóú]/g, (m) => letras[m]);
        return palabra;
    };

    // Funcion para checar el abogado seleccionado
    const seleccionarRadio = async (opcion) => {
        handleChangeAbogadoAsignado(opcion);
    };

    async function editarOportunidad() {
        if (!usuario) {
            return Router.push('/iniciar-sesion');
        }

        const oportunidad = {
            idOportunidad,
            nombreCliente,
            telefonoCliente,
            especialidad,
            ciudad,
            email,
            descripcion,
            provinciaOportunidad,
            abogadoAsignado,
            estado,
            creado,
            creador,
            actualizado: Date.now(),
            actualizaUsuario: {
                id: usuario.uid,
                nombre: usuario.displayName
            },
            nombreAbogadoAsignado: nombreAbogado
        }

        if (abogadoAsignadoOriginal !== abogadoAsignado && abogadoAsignadoOriginal !== '') {
            console.log('ORIGInAL', abogadoAsignadoOriginal)
            Swal.fire({
                icon: 'error',
                title: 'No se puede cambiar el abogado asignado directamente',
                text: 'Para ello, habría que rechazar la oportunidad y reasignarsela al abogado deseado',
                showConfirmButton: true,
            })
        } else {
            if (!usuario) {
                return router.push("/login");
            }


            // Updatear en firebase
            try {
                if (numeroReasignaciones >= 1) {
                    await firebase.db.collection('oportunidades').doc(id).update({ ...oportunidad, reasignada: true })
                    console.log("reasignada TRUE", oportunidad)
                } else {
                    await firebase.db.collection('oportunidades').doc(id).update(oportunidad)
                    console.log("Editando oportunidad...", oportunidad)
                }
            } catch (error) {
                console.log(error)
                guardarErrorDB({
                    ...errorDB,
                    error
                })
            }
            if (estado === "Aceptada") {
                if (!usuario) {
                    return router.push("/login");
                }
                console.log("Entre al aceptado")

                // Updatear en firebase
                try {
                    if (numeroReasignaciones >= 1) {
                        await firebase.db.collection('oportunidades').doc(id).update({ ...oportunidad, reasignada: true })
                        console.log("reasignada TRUE", oportunidad)
                    } else {
                        await firebase.db.collection('oportunidades').doc(id).update(oportunidad)
                        console.log("Editando oportunidad...", oportunidad)
                    }
                } catch (error) {
                    console.log(error)
                    guardarErrorDB({
                        ...errorDB,
                        error
                    })
                }

                try {
                    // Sumarle uno al contador del abogado asignado
                    await firebase.db.collection('abogados').doc(abogadoAsignado).update({ ultimaOportunidad: Date.now(), contadorAceptados: contadorAceptadosAbogado + 1 });
                    emailjs.send('smtp_server', 'template_kZfiJX4o', template_params, 'user_xxs3Pu51cqPmbXqR7CsEW')
                        .then((response) => {
                            console.log('SUCCESS!', response.status, response.text);
                        }, (err) => {
                            console.log('FAILED...', err);
                        });
                } catch (error) {
                    console.log(error)
                    guardarErrorDB({
                        ...errorDB,
                        error
                    })
                }
                Swal.fire({
                    icon: 'success',
                    title: 'La oportunidad se actualizó correctamente!',
                    showConfirmButton: false,
                    timer: 1200
                })
                router.push("/index")
                window.location.reload(true);

            } else if (estado === "Rechazada") {
                //Cuando se RECHAZAN
                if (!usuario) {
                    return router.push("/login");
                }


                // Updatear firebase OPORTUNIDAD 
                try {
                    if (textoMotivoRechazo !== undefined) {
                        await firebase.db.collection('oportunidades').doc(id).update({ ...oportunidad, rechazada: true, motivoRechazo: motivoRechazo, textoMotivoRechazo: textoMotivoRechazo })

                    } else {
                        await firebase.db.collection('oportunidades').doc(id).update({ ...oportunidad, rechazada: true, motivoRechazo: motivoRechazo })
                    }
                    console.log("Editando oportunidad...", oportunidad)
                } catch (error) {
                    console.log(error)
                    guardarErrorDB({
                        ...errorDB,
                        error
                    })
                }

                // Updatear firebase ABOGADO, Hay que sumarle una a la lista de rechazadas al abogado
                try {
                    await firebase.db.collection('abogados').doc(abogadoAsignado).update({ contadorRechazados: datosAbogado + 1 })
                } catch (error) {
                    console.log(error)
                }

                // Crear una oportunidad "gemela" con la coletilla -R + crear numeroReasignaciones  idOportunidad: idOportunidad + numeroReasignaciones.toString()
                // la oportunidad debe de tener el estado pendiente de asignar
                const numeroReasignacionesActual = numeroReasignaciones + 1
                console.log('numeroReasignaciones', numeroReasignaciones)
                console.log('numeroReasignacionesActual: ', numeroReasignacionesActual)

                if (numeroReasignaciones <= 0) {
                    let abogadosRechazadores = []
                    abogadosRechazadores.push(abogadoAsignado)
                    console.log('abogados RECHAZADORES: ', abogadosRechazadores)
                    const oportunidadClonada = {
                        idOportunidadOriginal: idOportunidad,
                        idOportunidad: `${idOportunidad}-R${numeroReasignacionesActual}`,
                        nombreCliente,
                        telefonoCliente,
                        descripcion,
                        email,
                        especialidad,
                        provinciaOportunidad,
                        ciudad,
                        abogadoAsignado: '',
                        estado: "Pendiente Asignar",
                        creado: Date.now(),
                        creador: {
                            id: usuario.uid,
                            nombre: usuario.displayName
                        },
                        numeroReasignaciones: numeroReasignacionesActual,
                        reasignada: false,
                        abogadosRechazadores,
                        nombreAbogadoAsignado
                    }
                    try {
                        await firebase.db.collection('oportunidades').add(oportunidadClonada)
                        console.log('oportunidadClonada', oportunidadClonada)

                    } catch (error) {
                        console.log(error)
                    }
                } else {
                    abogadosRechazadores.push(abogadoAsignado)
                    console.log('abogados RECHAZADORES: ', abogadosRechazadores)
                    const oportunidadClonada = {
                        idOportunidadOriginal,
                        idOportunidad: `${idOportunidadOriginal}-R${numeroReasignacionesActual}`,
                        nombreCliente,
                        telefonoCliente,
                        especialidad,
                        provinciaOportunidad,
                        abogadoAsignado: '',
                        estado: "Pendiente Asignar",
                        creado: Date.now(),
                        creador: {
                            id: usuario.uid,
                            nombre: usuario.displayName
                        },
                        numeroReasignaciones: numeroReasignacionesActual,
                        reasignada: false,
                        abogadosRechazadores,
                        nombreAbogadoAsignado: ''
                    }
                    try {
                        await firebase.db.collection('oportunidades').add(oportunidadClonada)
                        console.log('oportunidadClonada', oportunidadClonada)

                    } catch (error) {
                        console.log(error)
                    }
                }

                console.log('Rechazando..  TODO OK')

                // Tras esto, redireccionar y mostrar mensaje
                router.push('/');
            }

            else {
                try {
                    // Sumarle uno al contador del abogado asignado
                    await firebase.db.collection('abogados').doc(abogadoAsignado).update({ contadorAsuntos: contadorAbogado + 1, ultimaOportunidad: Date.now() });
                    emailjs.send('smtp_server', 'template_kZfiJX4o', template_params, 'user_xxs3Pu51cqPmbXqR7CsEW')
                        .then((response) => {
                            console.log('SUCCESS!', response.status, response.text);
                        }, (err) => {
                            console.log('FAILED...', err);
                        });
                } catch (error) {
                    console.log(error)
                    guardarErrorDB({
                        ...errorDB,
                        error
                    })
                }
            }
            Swal.fire({
                icon: 'success',
                title: 'La oportunidad se actualizó correctamente!',
                showConfirmButton: false,
                timer: 1200
            })
            router.push("/index")

        }
    }

    // Funcion de filtrado manual
    // FILTRADO
    const [q, guardarQ] = useState("");
    const [resultados, guardarResultados] = useState([]);

    const [qComunidad, guardarQComunidad] = useState("");

    const [
        respuestaPreguntasEspecialidadProvisional,
        guardarRespuestasPreguntasEspecialidadProvisional,
    ] = useState([]);
    const [
        respuestaPreguntasGeneralProvisional,
        guardarRespuestasPreguntasGeneralProvisional,
    ] = useState([]);

    // Cuando no hay abogados en la misma provincia
    useEffect(() => {
        let montado = true;
        console.log(montado);
        if (otrasSugerencias) {
            console.log("otrasSugerencias", otrasSugerencias);
            const busqueda = q.toLowerCase();
            const busquedaSinTildes = quitarTildes(busqueda);

            const abogadosFiltrados = otrasSugerencias.filter((abogado) => {
                return `${quitarTildes(abogado.nombre.toLowerCase())} ${quitarTildes(
                    abogado.apellidos.toLowerCase()
                )}`.includes(busquedaSinTildes);
            });

            if (montado) {
                if (comunidadDeProvincia !== "") {
                    const busqueda = qComunidad.toLowerCase();
                    const busquedaSinTildes = quitarTildes(busqueda);
                    const abogadosComunidadAutonoma = otrasSugerencias.filter(
                        (abogado) => {
                            return (
                                quitarTildes(abogado.comunidadAutonoma).includes(
                                    quitarTildes(comunidadDeProvincia)
                                ) &&
                                `${quitarTildes(abogado.nombre.toLowerCase())} ${quitarTildes(
                                    abogado.apellidos.toLowerCase()
                                )}`.includes(busquedaSinTildes)
                            );
                        }
                    );
                    console.log("abogadosPorComunidad", abogadosComunidadAutonoma);
                    // Guardamos los abogados de la CCAA en state
                    guardarAbogadosPorComunidad(abogadosComunidadAutonoma);
                }
                const sociosNacionalesFiltrados = abogadosFiltrados.filter(
                    (abogado) => {
                        return abogado.tipo !== "Colaborador";
                    }
                );
                const colaboradoresNacionalesFiltrados = abogadosFiltrados.filter(
                    (abogado) => {
                        return abogado.tipo === "Colaborador";
                    }
                );
                guardarSociosNacionales(sociosNacionalesFiltrados);
                guardarColaboradoresNacionales(colaboradoresNacionalesFiltrados);
                guardarResultados(abogadosFiltrados);
            }
            console.log("abogadosFiltrados especialidad", abogadosFiltrados);
        }
        return () => (montado = false);
    }, [
        q,
        qComunidad,
        otrasSugerencias,
        comunidadDeProvincia,
        provinciaOportunidad,
    ]);

    // Diferenciamos Socios de Colaboradores (CCAA) cuando no haya sugerencias
    useEffect(() => {
        let montado = true;
        if (montado && abogadosPorComunidad.length !== 0) {
            const sociosFiltrados = abogadosPorComunidad.filter((abogado) => {
                return abogado.tipo !== "Colaborador";
            });
            console.log("sociosFiltrados", sociosFiltrados);
            guardarSociosComunidad(sociosFiltrados);

            const colaboradoresFiltrados = abogadosPorComunidad.filter((abogado) => {
                return abogado.tipo === "Colaborador";
            });
            console.log("colaboradoresFiltrados", colaboradoresFiltrados);
            guardarColaboradoresComunidad(colaboradoresFiltrados);
        } else {
            guardarSociosComunidad([]);
            guardarColaboradoresComunidad([]);
        }

        return () => (montado = false);
    }, [abogadosPorComunidad]);

    const resultadosProvincia = async () => {
        if (especialidad !== undefined) {
            await firebase.db
                .collection("abogados")
                .where("especialidadesAbogado", "array-contains", especialidad)
                .onSnapshot(manejarSnapShot);
        }
    };

    useEffect(() => {
        resultadosProvincia();
    }, [especialidad]);

    function manejarSnapShot(snapshot) {
        const abogadosDB = snapshot.docs.map((doc) => {
            return {
                id: doc.id,
                ...doc.data(),
            };
        });
        // const abogadosOrdenados = abogadosDB.sort((a, b) => (a.contadorAsuntos > b.contadorAsuntos) ? 1 : -1)
        const abogadosOrdenados = abogadosDB.sort((a, b) =>
            a.ultimaOportunidad > b.ultimaOportunidad ? 1 : -1
        );
        guardarOtrasSugerencias(abogadosOrdenados);
    }

    return (
        <Modal
            {...props}
            size="xl"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            scrollable
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    <h1 className="titulo">Nueva Oportunidad</h1>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div>
                    {error ? (
                        <PaginaError msg="Vaya... Parece que ha habido un error al editar el abogado, vuelve a intentarlo por favor."></PaginaError>
                    ) : contadorDB === undefined ? (
                        <p>Cargando...</p>
                    ) : usuario ? (
                        <>
                            <form onSubmit={handleSubmit}>
                                <Divp1 className="mb-2 row">
                                    <div className="col-12 ">
                                        <>
                                            {/* Abogados */}
                                            {abogadosSugeridos.length !== 0 ? (
                                                <>
                                                    <h2>Socios Sugeridos</h2>
                                                    <table className="table table-bordered table-hover w-100 " id="ven5">
                                                        <thead className="thead-dark text-center p-3">
                                                            <tr className="m-2">
                                                                <th className="align-middle">
                                                                    Seleccionar Abogado
                                  </th>
                                                                <th className="align-middle">
                                                                    Nombre y Apellidos
                                  </th>
                                                                <th className="align-middle">Tipo</th>
                                                                <th className="align-middle">
                                                                    Especialidades
                                  </th>
                                                                <th className="align-middle">Dirección</th>
                                                                <th className="align-middle">
                                                                    Última Oportunidad
                                  </th>
                                                                <th className="align-middle">
                                                                    Asuntos PreAsignados
                                  </th>
                                                                <th className="align-middle">
                                                                    Asuntos Aceptados
                                  </th>
                                                                <th className="align-middle">
                                                                    Asuntos Rechazados
                                  </th>
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                            {abogadosSugeridos.map((abogado) => {
                                                                return (
                                                                    <SeleccionarAbogado
                                                                        key={abogado.id}
                                                                        abogado={abogado}
                                                                        seleccionarRadio={seleccionarRadio}
                                                                    />
                                                                );
                                                            })}
                                                        </tbody>
                                                    </table>
                                                </>
                                            ) : null}

                                            {/* Colaboradores */}
                                            {colaboradoresSugeridos.length !== 0 ? (
                                                <>
                                                    <h2 className="mt-4">Colaboradores Sugeridos</h2>
                                                    <table className="table table-bordered table-hover w-100 ">
                                                        <thead className="thead-dark text-center p-3">
                                                            <tr className="m-2">
                                                                <th className="align-middle">
                                                                    Seleccionar Abogado
                                  </th>
                                                                <th className="align-middle">
                                                                    Nombre y Apellidos
                                  </th>
                                                                <th className="align-middle">Tipo</th>
                                                                <th className="align-middle">
                                                                    Especialidades
                                  </th>
                                                                <th className="align-middle">Dirección</th>
                                                                <th className="align-middle">
                                                                    Última Oportunidad
                                  </th>
                                                                <th className="align-middle">
                                                                    Asuntos PreAsignados
                                  </th>
                                                                <th className="align-middle">
                                                                    Asuntos Aceptados
                                  </th>
                                                                <th className="align-middle">
                                                                    Asuntos Rechazados
                                  </th>
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                            {colaboradoresSugeridos.map((abogado) => {
                                                                return (
                                                                    <SeleccionarAbogado
                                                                        key={abogado.id}
                                                                        abogado={abogado}
                                                                        seleccionarRadio={seleccionarRadio}
                                                                    />
                                                                );
                                                            })}
                                                        </tbody>
                                                    </table>
                                                </>
                                            ) : null}

                                            {/* Sin Sugerencias */}
                                            {sinSugerencias && (
                                                <>
                                                    <h2 className="text-center mb-3">
                                                        No hay resultados de socios ni colaboradores en la
                                                        provincia, elige otra opción:
                            </h2>

                                                    {/* Socios y Colaboradores de la CCAA */}
                                                    <div className="d-flex justify-content-between mt-5">
                                                        <h2 className="font-weight-bold">
                                                            Comunidad Autonoma
                              </h2>
                                                        <div className="d-flex justify-content-between">
                                                            <InputTextOportunidades
                                                                type="text"
                                                                value={qComunidad}
                                                                onChange={(e) =>
                                                                    guardarQComunidad(e.target.value)
                                                                }
                                                                placeholder="Introduce el nombre del abogado..."
                                                            // className="mt-2"
                                                            />
                                                            <InputSubmitSugerencias disabled>
                                                                Buscar
                                </InputSubmitSugerencias>
                                                        </div>
                                                    </div>

                                                    <br />

                                                    {/* <div className="d-flex mt-4 mb-4"> */}

                                                    <h2>Socios</h2>
                                                    <table className="table table-bordered table-hover w-100">
                                                        <thead className="thead-dark text-center p-3">
                                                            <tr className="m-2">
                                                                <th className="align-middle">
                                                                    Seleccionar Abogado
                                  </th>
                                                                <th className="align-middle">
                                                                    Nombre y Apellidos
                                  </th>
                                                                <th className="align-middle">Tipo</th>
                                                                <th className="align-middle">
                                                                    Especialidades
                                  </th>
                                                                <th className="align-middle">Dirección</th>
                                                                <th className="align-middle">
                                                                    Última Oportunidad
                                  </th>
                                                                <th className="align-middle">
                                                                    Asuntos PreAsignados
                                  </th>
                                                                <th className="align-middle">
                                                                    Asuntos Aceptados
                                  </th>
                                                                <th className="align-middle">
                                                                    Asuntos Rechazados
                                  </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {sociosComunidad.length === 0 ? (
                                                                <SinResultados className="text-center">
                                                                    <td colSpan="8">
                                                                        La búsqueda no dió resultados
                                    </td>
                                                                </SinResultados>
                                                            ) : (
                                                                    sociosComunidad.map((abogado) => {
                                                                        return (
                                                                            <SeleccionarAbogado
                                                                                key={abogado.id}
                                                                                abogado={abogado}
                                                                                seleccionarRadio={seleccionarRadio}
                                                                            />
                                                                        );
                                                                    })
                                                                )}
                                                        </tbody>
                                                    </table>

                                                    <h2>Colaboradores</h2>
                                                    <table className="table table-bordered table-hover w-100">
                                                        <thead className="thead-dark text-center p-3">
                                                            <tr className="m-2">
                                                                <th className="align-middle">
                                                                    Seleccionar Abogado
                                  </th>
                                                                <th className="align-middle">
                                                                    Nombre y Apellidos
                                  </th>
                                                                <th className="align-middle">Tipo</th>
                                                                <th className="align-middle">
                                                                    Especialidades
                                  </th>
                                                                <th className="align-middle">Dirección</th>
                                                                <th className="align-middle">
                                                                    Última Oportunidad
                                  </th>
                                                                <th className="align-middle">
                                                                    Asuntos PreAsignados
                                  </th>
                                                                <th className="align-middle">
                                                                    Asuntos Aceptados
                                  </th>
                                                                <th className="align-middle">
                                                                    Asuntos Rechazados
                                  </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {colaboradoresComunidad.length === 0 ? (
                                                                <SinResultados className="text-center">
                                                                    <td colSpan="8">
                                                                        La búsqueda no dió resultados
                                    </td>
                                                                </SinResultados>
                                                            ) : (
                                                                    colaboradoresComunidad.map((abogado) => {
                                                                        return (
                                                                            <SeleccionarAbogado
                                                                                key={abogado.id}
                                                                                abogado={abogado}
                                                                                seleccionarRadio={seleccionarRadio}
                                                                            />
                                                                        );
                                                                    })
                                                                )}
                                                        </tbody>
                                                    </table>

                                                    {/* Socios y Colaboradores Nacionales */}
                                                    <div className="d-flex justify-content-between mt-5">
                                                        <h2 className="font-weight-bold">
                                                            Nivel Nacional
                              </h2>
                                                        <div className="d-flex justify-content-between">
                                                            <InputTextOportunidades
                                                                type="text"
                                                                value={q}
                                                                onChange={(e) => guardarQ(e.target.value)}
                                                                placeholder="Introduce el nombre del abogado..."
                                                            // className="mt-2"
                                                            />
                                                            <InputSubmitSugerencias disabled>
                                                                Buscar
                                </InputSubmitSugerencias>
                                                        </div>
                                                    </div>

                                                    <br />

                                                    {/* Tabla Socios NACIONALES */}
                                                    <h2>Socios</h2>
                                                    <table className="table table-bordered table-hover w-100">
                                                        <thead className="thead-dark text-center p-3">
                                                            <tr className="m-2">
                                                                <th className="align-middle">
                                                                    Seleccionar Abogado
                                  </th>
                                                                <th className="align-middle">
                                                                    Nombre y Apellidos
                                  </th>
                                                                <th className="align-middle">Tipo</th>
                                                                <th className="align-middle">
                                                                    Especialidades
                                  </th>
                                                                <th className="align-middle">Dirección</th>
                                                                <th className="align-middle">
                                                                    Última Oportunidad
                                  </th>
                                                                <th className="align-middle">
                                                                    Asuntos PreAsignados
                                  </th>
                                                                <th className="align-middle">
                                                                    Asuntos Aceptados
                                  </th>
                                                                <th className="align-middle">
                                                                    Asuntos Rechazados
                                  </th>
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                            {sociosNacionales.length === 0 ? (
                                                                <SinResultados className="text-center">
                                                                    <td colSpan="8">
                                                                        La búsqueda no dió resultados
                                    </td>
                                                                </SinResultados>
                                                            ) : (
                                                                    sociosNacionales.map((abogado) => {
                                                                        return (
                                                                            <SeleccionarAbogado
                                                                                key={abogado.id}
                                                                                abogado={abogado}
                                                                                seleccionarRadio={seleccionarRadio}
                                                                            />
                                                                        );
                                                                    })
                                                                )}
                                                        </tbody>
                                                    </table>

                                                    {/* Tabla Colaboradores NACIONALES */}
                                                    <h2>Colaboradores</h2>
                                                    <table className="table table-bordered table-hover w-100">
                                                        <thead className="thead-dark text-center p-3">
                                                            <tr className="m-2">
                                                                <th className="align-middle">
                                                                    Seleccionar Abogado
                                  </th>
                                                                <th className="align-middle">
                                                                    Nombre y Apellidos
                                  </th>
                                                                <th className="align-middle">Tipo</th>
                                                                <th className="align-middle">
                                                                    Especialidades
                                  </th>
                                                                <th className="align-middle">Dirección</th>
                                                                <th className="align-middle">
                                                                    Última Oportunidad
                                  </th>
                                                                <th className="align-middle">
                                                                    Asuntos PreAsignados
                                  </th>
                                                                <th className="align-middle">
                                                                    Asuntos Aceptados
                                  </th>
                                                                <th className="align-middle">
                                                                    Asuntos Rechazados
                                  </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {colaboradoresNacionales.length === 0 ? (
                                                                <SinResultados className="text-center">
                                                                    <td colSpan="8">
                                                                        La búsqueda no dió resultados
                                    </td>
                                                                </SinResultados>
                                                            ) : (
                                                                    colaboradoresNacionales.map((abogado) => {
                                                                        return (
                                                                            <SeleccionarAbogado
                                                                                key={abogado.id}
                                                                                abogado={abogado}
                                                                                seleccionarRadio={seleccionarRadio}
                                                                            />
                                                                        );
                                                                    })
                                                                )}
                                                        </tbody>
                                                    </table>

                                                    {abogadoAsignado === ""
                                                        ? (estado = "Pendiente Asignar")
                                                        : null}
                                                </>
                                            )}

                                            {/* Errores */}
                                            {errores.abogadoAsignado ? (
                                                <p className="alert alert-danger text-center p-2 mt-2">
                                                    {errores.abogadoAsignado}
                                                </p>
                                            ) : null}

                                            {error && (
                                                <p className="alert alert-danger text-center p2 mt-2 mb-2">
                                                    {error}
                                                </p>
                                            )}

                                            {/* Boton Crear */}
                                            <div className="col-12 d-flex justify-content-center">
                                                <BotonAceptar
                                                    className="btn btn-primary"
                                                    type="submit"
                                                >
                                                    Crear Oportunidad
                          </BotonAceptar>
                                            </div>
                                        </>
                                    </div>
                                </Divp1>
                            </form>
                        </>
                    ) : (
                                    <>
                                        <PaginaError
                                            msg={`Tienes que estar logueado para acceder a esta sección.`}
                                        ></PaginaError>
                                        <div className="text-center">
                                            <Link href="/iniciar-sesion">
                                                <a>
                                                    <h1>Inicia Sesión</h1>
                                                </a>
                                            </Link>
                                        </div>
                                    </>
                                )}
                </div>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={props.onHide}>Close</Button>
            </Modal.Footer>
        </Modal>
    );
}
export default MyVerticallyCenteredModal;

