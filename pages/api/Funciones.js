export function Convertir (aConvertir){

    try{
        var aDevolver=[]
        aConvertir.map(e =>{
            let sValor=Object.values(e)[0]
            aDevolver.push({label:`${sValor}`,value:`${sValor}`})
        })

        return aDevolver
    }
    catch (e){
        console.log("Error en Convertir")
    }
}