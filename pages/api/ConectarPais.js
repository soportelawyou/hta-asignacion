import { useState } from 'react';
import fetch from 'node-fetch';
import {Convertir} from '../api/Funciones';


export function iniciar() {
    const [auth, guardarAuth] = useState('')

    fetch('https://www.universal-tutorial.com/api/getaccesstoken', {
        method: ('GET'),
        headers: {
            "Accept": "application/json",
            "api-token": "FoDgxAIYOO9kHc5qdXTNvDk10t-zGQCh8bRAm4xhXzgbkdh7iGuqoExwdIaqnBnjT64",
            "user-email": "soportelawyou@gmail.com",
            "Connection": "close"
        },
    })
        .then(response => response.json())
        .then(json => { guardarAuth(json.auth_token) })

            const [paises, guardarPaises] = useState([])
        
        
        
            fetch('https://www.universal-tutorial.com/api/countries/', {
                method: ('GET'),
                headers: {
                    "Authorization": `Bearer ${auth}`,
                    "Accept": "application/json",
                    "Connection": "close"
                },
            })
                .then(response => response.json())
                .then(json => {guardarDevolver(Convertir(json))})

}

export function Paises(auth) {
    const [devolver, guardarDevolver] = useState([])



    fetch('https://www.universal-tutorial.com/api/countries/', {
        method: ('GET'),
        headers: {
            "Authorization": `Bearer ${auth}`,
            "Accept": "application/json",
            "Connection": "close"
        },
    })
        .then(response => response.json())
        .then(json => {guardarDevolver(Convertir(json))})

        return devolver;
}

export function Provincias(auth, pais) {
    const [devolver, guardarDevolver] = useState([])



    fetch(`https://www.universal-tutorial.com/api/states/${pais}`, {
        method: ('GET'),
        headers: {
            "Authorization": `Bearer ${auth}`,
            "Accept": "application/json",
            "Connection": "close"
        },
    })
        .then(response => response.json())
        .then(json => {guardarDevolver(Convertir(json))})
        return devolver;
}

export function Ciudades(auth, provincias) {
    const [devolver, guardarDevolver] = useState([])



    fetch(`https://www.universal-tutorial.com/api/cities/${provincias}`, {
        method: ('GET'),
        headers: {
            "Authorization": `Bearer ${auth}`,
            "Accept": "application/json",
            "Connection": "close"
        },
    })
        .then(response => response.json())
        .then(json => {guardarDevolver(Convertir(json))})
}



