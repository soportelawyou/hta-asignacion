import React, { useContext, useState, useEffect } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';


import { Divp1, Divp2,Label, Span1, BotonEditar, SpanInactivo, SpanActivo } from '../../components/ui/Visual'
import PaginaError from '../../components/layout/PaginaError';
import Layout from '../../components/layout/Layout';
import Swal from 'sweetalert2';
import Footer from '../../components/layout/Footer'

// Validaciones
import useValidacionEditar from '../../hooks/useValidacionEditar';
import validarEditarTerceros from '../../validacion/validarEditarTerceros';
import { MyVerticallyCenteredModal } from '../VisualizarTercero'
import { FirebaseContext } from '../../firebase/index';



const Terceros = () => {

    //Conprolar sl state del modal
    const [modalShow, setModalShow] = React.useState(false);
    // State del componente
    const [terceros, guardarTerceros] = useState({});
    const [error, guardarError] = useState(false);
    const [consultarDB, guardarConsultarDB] = useState(true);
    const [rol, guardarRol] = useState("");
    const [cargando, guardarCargando] = useState(false);

    // Routing para obtener el ID actual
    const router = useRouter();
    const { query: { id } } = router;

    // Context de Firebase
    const { firebase, usuario } = useContext(FirebaseContext);

    useEffect(() => {
        // Declaramos variable para trackear
        let estaCancelado = false;
        // console.log('useEffect [ID].js')

        if (id && consultarDB) {
            const obtenerTerceros = async () => {
                const tercerosQuery = await firebase.db.collection('terceros').doc(id);
                const terceros = await tercerosQuery.get();

                // Se ejecuta solo si esta montado
                if (!estaCancelado) {
                    if (terceros.exists) {
                        guardarTerceros(terceros.data());
                    } else {
                        guardarError(true);
                    }
                    guardarConsultarDB(false);
                }

            }
            obtenerTerceros();
        }
        return () => {
            // Ponemos la variable a true para evitar que intente actualizar un state inexistente
            estaCancelado = true;
        }
    }, [id, terceros])

    const busqueda = async (email) => {
        await firebase.db.collection('usuarios').where("email", "==", email).onSnapshot(manejarSnapShot);
    }

    function manejarSnapShot(snapshot) {
        const usuarioBD = snapshot.docs.map(doc => {
            return {
                id: doc.id,
                ...doc.data()
            }
        })
        if (usuarioBD[0] !== undefined) {
            // console.log("USUARIO DB", usuarioBD[0])
            console.log("entre en el guardar usuarios");
            console.log(usuarioBD)
            guardarRol(usuarioBD[0].rol);

        }
        guardarCargando(false)
    }

    useEffect(() => {
        guardarCargando(true)
        if (usuario !== undefined && usuario !== null) {
            console.log(usuario.email)
            busqueda(usuario.email)
        }
    }, [usuario])

    const eliminarTerceros = async () => {
        if (!usuario) {
            return router.push("/login");
        }

        // Modal de SWAL
        Swal.fire({
            title: '¿Estás seguro?',
            text: "No podrás deshacer esta acción!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, eliminar!',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value) {
                try {
                    firebase.db.collection('terceros').doc(id).delete();
                    Swal.fire({
                        icon: 'success',
                        title: 'El Tercero ha sido eliminado!',
                        showConfirmButton: false,
                        timer: 1200
                    })
                    setTimeout(() => {
                        router.push("/terceros")
                    }, 1200);
                } catch (error) {
                    console.log(error)
                }

            }
        })

    }


    // if (Object.keys(producto).length === 0) return <p>Cargando...</p>

    const dniOriginal = terceros.dni;

    const { valores} = useValidacionEditar(terceros, dniOriginal, validarEditarTerceros);


    // const { tipo, nombre, apellidos, idLawyou, dni, direccion, calle, provincia, ciudad, comunidadAutonoma, cp, creado, creador, contadorAsuntos, contadorRechazados } = abogado;


    // Destructuring de los valores
    const { tipo, nombre, apellidos, dni, direccion, activo, emailTerceros, numeroTelefono } = valores;

    // const valorEspecialidades = especialidades.filter(especialidad => especialidad.value === especialidadesAbogado);

    return (
        <div>
            <Layout>

                {error ? <PaginaError msg="Vaya... Parece que ha habido un error al editar el tercero, vuelve a intentarlo por favor"></PaginaError> : Object.keys(valores).length === 0 ? <p>Cargando...</p> : (

                    usuario ? (
                        <>
                            <div className="row">
                                <div className="col-12">
                                    <div className="row">
                                        <div className="col-12">
                                            <h1 className="display-5 my-2">Detalles <span>{nombre}</span> <span>{apellidos}</span> - Tercero</h1>
                                        </div>
                                    </div>
                                    <Divp1 className="mb-2 row">
                                        <div className="col-12 col-lg-4">
                                            <div className="row">
                                                <div className="col-12">
                                                    <Label className="d-block">Tipo</Label>
                                                    <Span1>{tipo}</Span1>
                                                </div>
                                            </div>
                                            <hr />
                                            <div className="row">
                                                <div className="col-12">
                                                    <Label className="d-block">Nombre y Apellidos</Label>
                                                    <Span1>{nombre} {apellidos}</Span1>
                                                </div>
                                            </div>
                                            <hr />
                                            <div className="row">
                                                <div className="col-12">
                                                    <Label className="d-block">DNI</Label>
                                                    <Span1>{dni}</Span1>
                                                </div>
                                            </div>
                                            <hr />
                                            <div className="row">
                                                <div className="col-12">
                                                    <Label className="d-block">Email</Label>
                                                    <Span1>{emailTerceros}</Span1>
                                                </div>
                                            </div>
                                            <hr />
                                            <div className="row">
                                                <div className="col-12">
                                                    <Label className="d-block">Telefono</Label>
                                                    <Span1>{numeroTelefono}</Span1>
                                                </div>
                                            </div>
                                            <hr />
                                            <div className="row">
                                                <div className="col-12">
                                                    <BotonEditar
                                                        className="col-4 btn"
                                                        onClick={() => setModalShow(true)}>
                                                        Editar Tercero
                                                    </BotonEditar>
                                                    {rol !== "Estandar" ? (
                                                        <BotonEditar
                                                            className="col-4 btn"
                                                            type="button"
                                                            onClick={() => eliminarTerceros()}
                                                        >Eliminar Tercero</BotonEditar>

                                                    ) : null}
                                                </div>
                                                <MyVerticallyCenteredModal
                                                    show={modalShow}
                                                    id={id}
                                                    onHide={() => setModalShow(false)}
                                                />
                                            </div>
                                            <hr />
                                        </div>
                                        <Divp2 className="con-12 col-lg-7">
                                            <div className="row">
                                                <div className="col-12">
                                                    <h2>Dirección</h2>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-12">
                                                    <span>{direccion}</span>
                                                </div>
                                            </div>
                                            <hr />
                                            <div className="row">
                                                <div className="col-12">
                                                    <h2>Estado</h2>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-12">
                                                    {activo === "Activo" ? (<SpanActivo>{activo}</SpanActivo>) : (<SpanInactivo>{activo}</SpanInactivo>)}

                                                </div>
                                            </div>
                                            <hr />
                                        </Divp2>
                                    </Divp1>


                                </div>
                            </div>
                        </>
                    ) :
                        <>
                            <PaginaError msg={`Tienes que estar logueado para acceder a esta sección.`}></PaginaError>
                            <div className="text-center">
                                <Link href="/iniciar-sesion"><a><h1>Inicia Sesión</h1></a></Link>
                            </div>
                        </>

                )}

                <Footer />
            </Layout>

        </div>
    )
}

export default Terceros;