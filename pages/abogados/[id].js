import React, { useContext, useState, useEffect } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import Swal from 'sweetalert2';
import PaginaError from '../../components/layout/PaginaError';
import Layout from '../../components/layout/Layout';
import Footer from '../../components/layout/Footer';
import {
    provincias,
    tipos,
    comunidades,
    especialidades,
    actividad
} from '../../components/ui/OpcionesSelectores';
import { Divp1, Divp2, Label, Span1, BotonEditar, SpanInactivo, SpanActivo } from '../../components/ui/Visual'
import { MyVerticallyCenteredModal } from '../VisualizarAbogados'

// Validaciones
import useValidacionEditar from '../../hooks/useValidacionEditar';
import validarEditarAbogado from '../../validacion/validarEditarAbogado';

import { FirebaseContext } from '../../firebase/index';



const Abogado = () => {

    const [modalShow, setModalShow] = React.useState(false);
    const [rol, guardarRol] = useState("");
    // State del componente
    const [abogado, guardarAbogado] = useState({});
    const [error, guardarError] = useState(false);
    const [consultarDB, guardarConsultarDB] = useState(true)

    // Routing para obtener el ID actual
    const router = useRouter();
    const { query: { id } } = router;

    // Context de Firebase
    const { firebase, usuario } = useContext(FirebaseContext);

    useEffect(() => {
        // Declaramos variable para trackear
        let estaCancelado = false;
        // console.log('useEffect [ID].js')

        if (id && consultarDB) {
            const obtenerAbogado = async () => {
                const abogadoQuery = await firebase.db.collection('abogados').doc(id);
                const abogado = await abogadoQuery.get();

                // Se ejecuta solo si esta montado
                if (!estaCancelado) {
                    if (abogado.exists) {
                        guardarAbogado(abogado.data());
                    } else {
                        guardarError(true);
                    }
                    guardarConsultarDB(false);
                }

            }
            obtenerAbogado();
        }
        return () => {
            // Ponemos la variable a true para evitar que intente actualizar un state inexistente
            estaCancelado = true;
        }
    }, [id, abogado])

    const eliminarAbogado = async () => {
        if (!usuario) {
            return router.push("/login");
        }

        // Modal de SWAL
        Swal.fire({
            title: '¿Estás seguro?',
            text: "No podrás deshacer esta acción!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, eliminar!',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value) {
                try {
                    firebase.db.collection('abogados').doc(id).delete();
                    Swal.fire({
                        icon: 'success',
                        title: 'El abogado ha sido eliminado!',
                        showConfirmButton: false,
                        timer: 1200
                    })
                    setTimeout(() => {
                        router.push("/abogados")
                    }, 1200);
                } catch (error) {
                    console.log(error)
                }

            }
        })

    }


    // if (Object.keys(producto).length === 0) return <p>Cargando...</p>

    const dniOriginal = abogado.dni;

    const { valores} = useValidacionEditar(abogado, dniOriginal, validarEditarAbogado, editarAbogado);


    // const { tipo, nombre, apellidos, idLawyou, dni, direccion, calle, provincia, ciudad, comunidadAutonoma, cp, creado, creador, contadorAsuntos, contadorRechazados } = abogado;


    // Destructuring de los valores
    const { tipo, nombre, apellidos, idLawyou, dni, direccion, calle, provincia, especialidadesAbogado,entidadLegal, ciudad, comunidadAutonoma, cp, activo, creado, creador, contadorAsuntos,contadorAceptados, contadorRechazados, ultimaOportunidad, emailAbogado, emailAbogadoSecundario, numeroTelefonoPrincipal, numeroTelefonoSecundario } = valores;

    async function editarAbogado() {
        if (!usuario) {
            return router.push('/iniciar-sesion');
        }

        // Creamos el objeto de abogado
        const abogado = {
            tipo,
            nombre,
            apellidos,
            idLawyou,
            dni,
            calle,
            provincia,
            ciudad,
            emailAbogado,
            emailAbogadoSecundario,
            numeroTelefonoPrincipal,
            numeroTelefonoSecundario,
            direccion: `${calle}, ${ciudad}, ${provincia}, ${comunidadAutonoma} - ${cp}`,
            comunidadAutonoma,
            cp,
            activo,
            especialidadesAbogado,
            actualizado: Date.now(),
            actualizaUsuario: {
                id: usuario.uid,
                nombre: usuario.displayName
            },
            creado,
            creador,
            contadorAsuntos,
            contadorAceptados,
            contadorRechazados,
            ultimaOportunidad
        };

        try {
            if (abogado) {
                // Actualiza en la bbdd
                await firebase.db.collection('abogados').doc(id).update(abogado);
                console.log('Editando abogado', abogado)
            }
            Swal.fire({
                icon: 'success',
                title: 'El abogado se editó correctamente!',
                showConfirmButton: false,
                timer: 1200
            })

            router.push('/abogados')
        } catch (error) {
            console.log(error)
            guardarError(error)
        }


    };

    return (
        <div>
            <Layout>

                {error ? <PaginaError msg="Vaya... Parece que ha habido un error al editar el tercero, vuelve a intentarlo por favor"></PaginaError> : Object.keys(valores).length === 0 ? <p>Cargando...</p> : (

                    usuario ? (
                        <>
                            <div className="row">
                                <div className="col-12">
                                    <div className="row">
                                        <div className="col-12">
                                            <h1 className="display-5 my-2">Detalles <span>{nombre}</span> <span>{apellidos}</span> - Abogado</h1>
                                        </div>
                                    </div>
                                    <Divp1 className="mb-2 row">
                                        <div className="col-12 col-lg-4">
                                            <div className="row">
                                                <div className="col-12">
                                                    <Label className="d-block">Tipo</Label>
                                                    <Span1>{tipo}</Span1>
                                                </div>
                                            </div>
                                            <hr />
                                            {entidadLegal==="Persona Física"?(
                                            <>
                                             <div className="row">
                                                <div className="col-12">
                                                    <Label className="d-block">Nombre y Apellidos</Label>
                                                    <Span1>{nombre} {apellidos}</Span1>
                                                </div>
                                            </div>
                                            <hr />
                                            </>):null}
                                            <div className="row">
                                                <div className="col-12">
                                                    {entidadLegal==="Persona Física"?(<Label className="d-block">DNI</Label>):(<Label className="d-block">NIF</Label>)}
                                                    <Span1>{dni}</Span1>
                                                </div>
                                            </div>
                                            <hr />
                                            <div className="row">
                                                <div className="col-12">
                                                    <Label className="d-block">Email Lawyou</Label>
                                                    <Span1>{emailAbogado}</Span1>
                                                </div>
                                                {emailAbogadoSecundario !== "" ? (<div className="col-12">
                                                    <Label className="d-block">Email Secundario</Label>
                                                    <Span1>{emailAbogadoSecundario}</Span1>
                                                </div>) : null}
                                            </div>
                                            <hr />
                                            <div className="row">
                                                <div className="col-12">
                                                    <Label className="d-block">Telefono</Label>
                                                    <Span1>{numeroTelefonoPrincipal}</Span1>
                                                </div>
                                                {numeroTelefonoSecundario !== "" ? (<div className="col-12">
                                                    <Label className="d-block">Telefono Secundario</Label>
                                                    <Span1>{numeroTelefonoSecundario}</Span1>
                                                </div>) : null}
                                            </div>
                                            <hr />
                                            <div className="row">
                                                <div className="col-12">
                                                    <BotonEditar
                                                        className="col-4 btn"
                                                        onClick={() => setModalShow(true)}>
                                                        Editar Abogado
                                                    </BotonEditar>
                                                    <MyVerticallyCenteredModal
                                                        show={modalShow}
                                                        id={id}
                                                        onHide={() => setModalShow(false)}
                                                    />
                                                    {rol !== "Estandar" ? (
                                                        <BotonEditar
                                                            className="col-4 btn"
                                                            type="button"
                                                            onClick={() => eliminarAbogado()}
                                                        >Eliminar Abogado</BotonEditar>

                                                    ) : null}
                                                </div>
                                            </div>
                                            <hr />
                                        </div>
                                        <Divp2 className="con-12 col-lg-7">
                                            <div className="row">
                                                <div className="col-12">
                                                    <h2>Dirección</h2>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-12">
                                                    <span>{direccion}</span>
                                                </div>
                                            </div>
                                            <hr />
                                            <div className="row">
                                                <div className="col-12">
                                                    <h2>Estado</h2>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-12">
                                                    {activo === "Activo" ? (<SpanActivo>{activo}</SpanActivo>) : (<SpanInactivo>{activo}</SpanInactivo>)}

                                                </div>
                                            </div>
                                            <hr />
                                            <div className="row">
                                                <div className="col-12">
                                                    <table className="table table-bordered table-hover w-100">
                                                        <thead className="thead-dark text-center p-3">
                                                            <tr className="m-2">
                                                                <th className="align-middle cabecera">Oportunidades Preasignadas</th>
                                                                <th className="align-middle cabecera">Oportunidades Aceptadas</th>
                                                                <th className="align-middle cabecera">Oportunidades Rechazadas</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr className="text-center">
                                                                <th className="p-3 align-middle">{contadorAsuntos}</th>
                                                                <th className="p-3 align-middle">{contadorAceptados}</th>
                                                                <th className="p-3 align-middle">{contadorRechazados}</th>
                                                            </tr>
                                                        </tbody>

                                                    </table>
                                                </div>
                                            </div>
                                        </Divp2>
                                    </Divp1>


                                </div>
                            </div>
                            <Footer />
                        </>
                    ) :
                        <>
                            <PaginaError msg={`Tienes que estar logueado para acceder a esta sección.`}></PaginaError>
                            <div className="text-center">
                                <Link href="/iniciar-sesion"><a><h1>Inicia Sesión</h1></a></Link>
                            </div>
                        </>

                )}

                
            </Layout>
        </div>
    )
}

export default Abogado;

