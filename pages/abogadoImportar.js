import React, { useContext, useState } from 'react';
import Link from 'next/link';
import Select from 'react-select';
import Router, {useRouter} from 'next/router';
import Swal from 'sweetalert2';
import { Modal, Button } from 'react-bootstrap';
import {Input} from '../components/ui/Boton'
import * as Papa from 'papaparse';

import PaginaError from '../components/layout/PaginaError';
import { ContenedorFormularioLg, BotonAceptar } from '../components/ui/Formulario';
import {
    tipoPreguntas,
} from '../components/ui/OpcionesSelectores';

// Validaciones
import useValidacionPreguntas from '../hooks/useValidacionPreguntas';
import validarNuevaPregunta from '../validacion/validarNuevaPregunta';

import { FirebaseContext } from '../firebase/index';
import firebase from '../firebase/index';

const DatosAbogados = [];

export function MyVerticallyCenteredModal2(props) {

    const router = useRouter();
    const {usuario, firebase} = useContext(FirebaseContext);

    // Divide un string en un array por las comas que tenga
    function dividirCampo(name){
        if(name !== "" && name !== undefined){
            return name.split(", ");
        }
        return "";
    }

    // Divide string localizacion en array con provincia, ciudad
    function dividirLocalizacion(name){
        let lugaresEjercer = [];
        if(name !== "" && name !== undefined){
            let provincias = name.split(", ");
            lugaresEjercer = provincias.map(e => {
                let ciudad = []
                return {
                    Ciudad: ciudad,
                    Provincia: e
                }
            })
        } else{
            let provVacio = "";
            let ciudVacio = [];
            lugaresEjercer.push({Provincia: provVacio, Ciudad: ciudVacio});
        }
        return lugaresEjercer;
    }

    // Crea una direccion a partir de los campos: calle, ciudad, provincia, ca, pais, cp
    function crearDireccion(calle, ciudad, provincia, pais, cp){
        let direccion = "";

        if(calle !== "" && calle !== undefined){
            direccion = direccion.concat(calle);
            if(ciudad !== "" && ciudad !== undefined) direccion = direccion.concat(", ", ciudad);
            if(provincia !== "" && provincia !== undefined) direccion = direccion.concat(", ", provincia);
            if(pais !== "" && pais !== undefined) direccion = direccion.concat(" (", pais, ")");
            if(cp !== "" && cp !== undefined) direccion = direccion.concat(" - ", cp);
        } 

        return direccion;
    }

    // Modifica el array de abogados, haciendo ajustes para que encajen con los del firebase
    function modificarArray(datosAbogados){ // Le pasamos un array, cada objeto del array es un abogado
        let nuevoDatos = datosAbogados.map(item => {
            let campos = Object.keys(item);

            let espe = dividirCampo(item[campos[5]]);
            let subEspe = dividirCampo(item[campos[6]]);
            let idio = dividirCampo(item[campos[9]]);
            let dir = crearDireccion(item[campos[17]], item[campos[15]], item[campos[14]], item[campos[12]], item[campos[16]]);
            let localizaciones = dividirLocalizacion(item[campos[19]]);

            return {
                idLawyou: item[campos[0]],
                tipo: item[campos[1]],
                dni: item[campos[2]],
                nombre: item[campos[3]],
                apellidos: item[campos[4]],
                especialidadesAbogado: espe,
                subEspecialidadesAbogados: subEspe,
                numeroTelefonoPrincipal: item[campos[7]], 
                numeroTelefonoSecundario: item[campos[8]],
                idioma: idio,
                emailAbogado: item[campos[10]],
                emailAbogadoSecundario: item[campos[11]],
                pais: item[campos[12]],
                comunidadAutonoma: item[campos[13]],
                provincia: item[campos[14]],
                ciudad: item[campos[15]],
                cp: item[campos[16]],
                calle: item[campos[17]],
                activo: item[campos[18]],
                lugaresEjercer: localizaciones,
                direccion: dir,
                creado: Date.now(),
                creador: {
                    id: usuario.uid,
                    nombre: usuario.displayName
                },
                ultimaOportunidad: "",
            }
        })

        return nuevoDatos;
    }

    // Lee un fichero csv y lo guarda en el firebase
    function leerFichero() {
        try{
            Papa.parse(document.getElementById('upload-csv').files[0], {
                delimiter: ";",
                header: true,
                encoding: "ISO-8859-1",
                complete: function(results) {
                    let nuevoArray = modificarArray(results.data);
                    nuevoArray.forEach(abog => {
                        if(abog['idLawyou'] !== "" && abog['idLawyou'] !== undefined){
                            // Añadimos el abogado al firebase
                            firebase.db.collection('abogados').add(abog);

                            // Cerramos el model y actualizamos la pagina abogados
                            props.onHide();
                            return router.push('/abogados');
                        }
                    })
                }
            })
        } catch(e){
            alert("No has seleccionado ningún archivo!");
        }
    }

    return (
        
        <Modal
            {...props}
            size="xl"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    <h1>IMPORTAR ABOGADOS</h1>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <div>
                <div>
                    <h2>Sube tu archivo CSV</h2>
                </div>
                <div>
                    <Input type="file" id="upload-csv" accept=".csv"></Input>
                </div>
                <div>
                    <Button id="btn-upload-csv" onClick={() => leerFichero()}>Importar</Button>
                </div>
            </div>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={props.onHide}>Close</Button>
            </Modal.Footer>
        </Modal>

    )
}
export default MyVerticallyCenteredModal2;