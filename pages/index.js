import React, { useContext, useState, useEffect } from 'react';
import Link from 'next/link';
import ReactTooltip from 'react-tooltip';
import { CSVLink } from 'react-csv';
import { format } from 'date-fns';

import Layout from '../components/layout/Layout';
import PaginaError from '../components/layout/PaginaError';
import { Boton } from '../components/ui/Boton';
import { InputTextOportunidades, InputSubmit } from '../components/ui/Busqueda';
import useOportunidades from '../hooks/useOportunidades'
import { SinResultados } from '../components/ui/SinResultados';
import DetallesOportunidad from '../components/layout/DetallesOportunidad';

import { MyVerticallyCenteredModal } from './nueva-oportunidad';
import { FirebaseContext } from '../firebase/index';
import Footer from '../components/layout/Footer'



const Home = () => {
  const [modalShow, setModalShow] = React.useState(false);
  const [oportunidadesOrdenadas, guardarOportunidadesOrdenadas] = useState([]);
  const [datosCSV, guardarDatosCSV] = useState([]);
  const [permiso, guardarPermiso] = useState(false);
  const [isAdmin, guardarRol] = useState(false);
  const [cargando, guardarCargando] = useState(false);
  const headersCSV = [
    { label: "idOportunidad", key: "id" },
    { label: "Nombre Cliente", key: "nombre" },
    { label: "Telefono", key: "telefono" },
    { label: "Estado", key: "estado" },
    { label: "Abogado Asignado", key: "abogado" },
    { label: "Fecha Oportunidad", key: "fechaOportunidad" },
    { label: "Especialidad", key: "especialidad" },
    { label: "Provincia", key: "provincia" },
    { label: "Gestionado", key: "gestionado" },
  ]

  const { usuario, firebase } = useContext(FirebaseContext);
  const { oportunidades } = useOportunidades('creado');

  // FILTRADO
  const [q, guardarQ] = useState('');
  const [resultados, guardarResultados] = useState([])
  ///////
  // buscar usuario
  const busqueda = async (email) => {
    await firebase.db.collection('usuarios').where("email", "==", email).onSnapshot(manejarSnapShot);
  }

  function manejarSnapShot(snapshot) {
    const usuarioBD = snapshot.docs.map(doc => {
      return {
        id: doc.id,
        ...doc.data()
      }
    })
    if (usuarioBD[0] !== undefined) {
      console.log("USUARIO DB", usuarioBD[0]);

      if (usuarioBD[0].rol === "superAdmin"){
        guardarRol(true);
      } else {
        guardarRol(false);
      }

      if (usuarioBD[0].admitido) {
        guardarPermiso(true);
      } else {
        guardarPermiso(false);
      }
    }
    guardarCargando(false)
  }

  useEffect(() => {
    guardarCargando(true)
    if (usuario !== undefined && usuario !== null) {
      console.log(usuario.email)
      console.log(usuario)
      busqueda(usuario.email)
    }
  }, [usuario])
  ///////

  // Funcion que filtra oportunidades por el rol del usuario
  function filtrarOportunidadesPorRol (opor, usuario){
    console.log(usuario);
    console.log(permiso);
    console.log(isAdmin);
    if (usuario.uid === "f16ZNfAGrHYnbLBVD8GDl3IoLFy2") {
      return opor;
    } else{
      return opor.filter(function(e) {
        return e['creador']['id'] == usuario.uid;
      });
    }
  }

  //Cargar oportunidades segun el rol del usuario
  useEffect(() => {
    let montado = true;
    if (oportunidades) {
      // console.log('OPOTUNIDADES: ', oportunidades)
      const busqueda = q.toLowerCase();
      const busquedaSinTildes = quitarTildes(busqueda);

      if (oportunidadesOrdenadas.length !== 0) {
        var oportunidadesPorRol = oportunidadesOrdenadas;
        // Usuario no null, filtramos por permisos del rol
        if (usuario !== null){
          oportunidadesPorRol = filtrarOportunidadesPorRol(oportunidadesOrdenadas, usuario);
        }
        const oportunidadesFiltradas = oportunidadesPorRol.filter(oportunidad => {
          return (
            quitarTildes(oportunidad.nombreCliente.toLowerCase()).includes(busquedaSinTildes) || quitarTildes(oportunidad.estado.toLowerCase()).includes(busquedaSinTildes)
            || quitarTildes(oportunidad.idOportunidad.toLowerCase()).includes(busquedaSinTildes) || quitarTildes(oportunidad.nombreAbogadoAsignado.toLowerCase()).includes(busquedaSinTildes)
          )
        })
        if (montado) {
          guardarResultados(oportunidadesFiltradas)
          guardarDatosCSV([])
          oportunidadesOrdenadas.map(oportunidad => {
            // const fila = `{id: ${oportunidad.idOportunidad}, nombre: ${oportunidad.nombreCliente}}`
            console.log({ id: oportunidad.idOportunidad, nombre: oportunidad.nombreCliente })
            return (
              guardarDatosCSV(
                datosCSV => [
                  ...datosCSV,
                  { id: oportunidad.idOportunidad, nombre: oportunidad.nombreCliente, telefono: oportunidad.telefonoCliente, estado: oportunidad.estado, abogado: oportunidad.nombreAbogadoAsignado, fechaOportunidad: format(oportunidad.creado, 'dd/MM/yyyy'), especialidad: oportunidad.especialidad, provincia: oportunidad.provinciaOportunidad, gestionado: oportunidad.creador.nombre }]
              )
            )
            // return(`{id: ${oportunidad.idOportunidad}, nombre: ${oportunidad.nombreCliente}}`)
          })

          console.log('DatosCSV', datosCSV);
        }
      } else {
        var oportunidadesPorRol = oportunidadesOrdenadas;
        // Usuario no null, filtramos por permisos del rol
        if (usuario !== null){
          oportunidadesPorRol = filtrarOportunidadesPorRol(oportunidades, usuario);
        }
        const oportunidadesFiltradas = oportunidadesPorRol.filter(oportunidad => {
          return (
            quitarTildes(oportunidad.nombreCliente.toLowerCase()).includes(busquedaSinTildes) || quitarTildes(oportunidad.estado.toLowerCase()).includes(busquedaSinTildes)
            || quitarTildes(oportunidad.idOportunidad.toLowerCase()).includes(busquedaSinTildes) || quitarTildes(oportunidad.nombreAbogadoAsignado.toLowerCase()).includes(busquedaSinTildes)
          )
        })
        if (montado) {
          guardarResultados(oportunidadesFiltradas)
          // const datosCSV = [] 
          //   oportunidades.map(oportunidad => {
          //   return(datosCSV.push(`{id: ${oportunidad.idOportunidad}, nombre: ${oportunidad.nombreCliente}}`))
          //   })
          // console.log('DatosCSV',datosCSV);
          guardarDatosCSV([])
          oportunidades.map(oportunidad => {
            // const fila = `{id: ${oportunidad.idOportunidad}, nombre: ${oportunidad.nombreCliente}}`
            // console.log({ id: oportunidad.idOportunidad, nombre: oportunidad.nombreCliente })
            return (
              guardarDatosCSV(
                datosCSV => [
                  ...datosCSV,
                  { id: oportunidad.idOportunidad, nombre: oportunidad.nombreCliente, telefono: oportunidad.telefonoCliente, estado: oportunidad.estado, abogado: oportunidad.nombreAbogadoAsignado, fechaOportunidad: format(oportunidad.creado, 'dd/MM/yyyy'), especialidad: oportunidad.especialidad, provincia: oportunidad.provinciaOportunidad, gestionado: oportunidad.creador.nombre }]
              )
            )

            // return(`{id: ${oportunidad.idOportunidad}, nombre: ${oportunidad.nombreCliente}}`)
          })
          console.log("ANTES")
          datosCSV.map(row => {
            // console.log('ROW', row)
            return (guardarDatosCSV(
              datosCSV => [
                ...datosCSV,
                row]
            ))
          })
          // console.log('DatosCSV', datosCSV);
        }
      }
    }
    return () => montado = false;

  }, [q, oportunidades, oportunidadesOrdenadas])

  //Quitar tildes de la palabra o frase.
  const quitarTildes = (palabra) => {
    const letras = { 'á': 'a', 'é': 'e', 'í': 'i', 'ó': 'o', 'ú': 'u' };
    palabra = palabra.replace(/[áéíóú]/g, m => letras[m]);
    return palabra;
  }

  //Ordenar tabla segun nombre de la tabla
  const ordenarTabla = (columna) => {
    console.log(columna)
    let oportunidadesOrdenadasFuncion = [...oportunidades];
    switch (columna) {
      case 'idOportunidad':
        oportunidadesOrdenadasFuncion.sort((a, b) => {
          if (a.idOportunidad < b.idOportunidad) {
            return 1;
          }
          if (a.idOportunidad > b.idOportunidad) {
            return -1;
          }
          return 0;
        })
        console.log('Oportunidades ordenadas: ', oportunidadesOrdenadasFuncion.map(oportunidad => oportunidad.idOportunidad))
        guardarOportunidadesOrdenadas(oportunidadesOrdenadasFuncion)
        break;
      case 'nombreCliente':
        oportunidadesOrdenadasFuncion.sort((a, b) => {
          if (a.nombreCliente < b.nombreCliente) {
            return -1;
          }
          if (a.nombreCliente > b.nombreCliente) {
            return 1;
          }
          return 0;
        })
        console.log('Oportunidades ordenadas: ', oportunidadesOrdenadasFuncion.map(oportunidad => oportunidad.nombreCliente))
        guardarOportunidadesOrdenadas(oportunidadesOrdenadasFuncion)
        break;
      case 'estado':
        oportunidadesOrdenadasFuncion.sort((a, b) => {
          if (a.estado < b.estado) {
            return -1;
          }
          if (a.estado > b.estado) {
            return 1;
          }
          return 0;
        })
        console.log('Oportunidades ordenadas: ', oportunidadesOrdenadasFuncion.map(oportunidad => oportunidad.estado))
        guardarOportunidadesOrdenadas(oportunidadesOrdenadasFuncion)
        break;
      case 'estado':
        oportunidadesOrdenadasFuncion.sort((a, b) => {
          if (a.estado < b.estado) {
            return -1;
          }
          if (a.estado > b.estado) {
            return 1;
          }
          return 0;
        })
        console.log('Oportunidades ordenadas: ', oportunidadesOrdenadasFuncion.map(oportunidad => oportunidad.estado))
        guardarOportunidadesOrdenadas(oportunidadesOrdenadasFuncion)
        break;
      case 'nombreAbogadoAsignado':
        oportunidadesOrdenadasFuncion.sort((a, b) => {
          if (a.nombreAbogadoAsignado < b.nombreAbogadoAsignado) {
            return -1;
          }
          if (a.nombreAbogadoAsignado > b.nombreAbogadoAsignado) {
            return 1;
          }
          return 0;
        })
        console.log('Oportunidades ordenadas: ', oportunidadesOrdenadasFuncion.map(oportunidad => oportunidad.nombreAbogadoAsignado))
        guardarOportunidadesOrdenadas(oportunidadesOrdenadasFuncion)
        break;
      case 'especialidad':
        oportunidadesOrdenadasFuncion.sort((a, b) => {
          if (a.especialidad < b.especialidad) {
            return -1;
          }
          if (a.especialidad > b.especialidad) {
            return 1;
          }
          return 0;
        })
        console.log('Oportunidades ordenadas: ', oportunidadesOrdenadasFuncion.map(oportunidad => oportunidad.especialidad))
        guardarOportunidadesOrdenadas(oportunidadesOrdenadasFuncion)
        break;
      case 'subEspecialidad':
        oportunidadesOrdenadasFuncion.sort((a, b) => {
          if (a.subEspecialidad < b.subEspecialidad) {
            return -1;
          }
          if (a.subEspecialidad > b.subEspecialidad) {
            return 1;
          }
          return 0;
        })
        console.log('Oportunidades ordenadas: ', oportunidadesOrdenadasFuncion.map(oportunidad => oportunidad.subEspecialidad))
        guardarOportunidadesOrdenadas(oportunidadesOrdenadasFuncion)
        break;
      case 'provinciaOportunidad':
        oportunidadesOrdenadasFuncion.sort((a, b) => {
          if (a.provinciaOportunidad < b.provinciaOportunidad) {
            return -1;
          }
          if (a.provinciaOportunidad > b.provinciaOportunidad) {
            return 1;
          }
          return 0;
        })
        console.log('Oportunidades ordenadas: ', oportunidadesOrdenadasFuncion.map(oportunidad => oportunidad.provinciaOportunidad))
        guardarOportunidadesOrdenadas(oportunidadesOrdenadasFuncion)
        break;
      case 'creador':
        oportunidadesOrdenadasFuncion.sort((a, b) => {
          if (a.creador.nombre < b.creador.nombre) {
            return -1;
          }
          if (a.creador.nombre > b.creador.nombre) {
            return 1;
          }
          return 0;
        })
        console.log('Oportunidades ordenadas: ', oportunidadesOrdenadasFuncion.map(oportunidad => oportunidad.creador.nombre))
        guardarOportunidadesOrdenadas(oportunidadesOrdenadasFuncion)
        break;
      case 'creado':
        oportunidadesOrdenadasFuncion.sort((a, b) => {
          if (a.creado < b.creado) {
            return -1;
          }
          if (a.creado > b.creado) {
            return 1;
          }
          return 0;
        })
        console.log('Oportunidades ordenadas: ', oportunidadesOrdenadasFuncion.map(oportunidad => oportunidad.creado))
        guardarOportunidadesOrdenadas(oportunidadesOrdenadasFuncion)
        break;

      default:
        return oportunidadesOrdenadasFuncion;
    }
  }

  //   const template_params = {
  //     "reply_to": "dcano@lawyoulegal.com",
  //     "destinatario": "dcano@lawyoulegal.com",
  //     "from_name": "Lawyou",
  //     "message_html": "<p>TIENES UNA NUEVA OPORTUNIDAD</p>"
  //  }
  //   const enviarMail = () => {
  //     emailjs.send('default_service', 'template_kZfiJX4o', template_params, 'user_xxs3Pu51cqPmbXqR7CsEW')
  //       .then((response) => {
  //         console.log('SUCCESS!', response.status, response.text);
  //       }, (err) => {
  //         console.log('FAILED...', err);
  //       });

  //   }


  return (
    <div>
      <Layout>

        {usuario && permiso ? (
          <>
            <h1 className="titulo">Oportunidades</h1>
            <div className="d-flex justify-content-between align-center col-12">
              <Boton
                className="btn btn-primary col-2"
                onClick={() => setModalShow(true)}
              >Nueva Oportunidad</Boton>
              <MyVerticallyCenteredModal
                show={modalShow}
                onHide={() => setModalShow(false)}
              />
              <div className="d-flex mt-4 justify-content-start">
                <InputTextOportunidades
                  type="text"
                  value={q}
                  onChange={e => guardarQ(e.target.value)}
                  placeholder="Busca algo..."
                  className="mt-2"
                  data-tip="Busca por Id Oportunidad, Abogado, Cliente o Estado"
                />
                <InputSubmit disabled>Buscar</InputSubmit>
                <ReactTooltip effect="solid" />
              </div>
              <CSVLink
                data={datosCSV}
                headers={headersCSV}
                filename={`Oportunidades_${format(new Date(), 'dd-MM')}.csv`}
                className="btn btn-exportar col-2 align-center"
                separator={";"}
              >
                Exportar CSV
          </CSVLink>

            </div>

            {oportunidades.length === 0 ? <p>Cargando...</p> : (

              <div className="tablaScroll">
                <table className="table table-bordered table-hover w-100">

                  <thead className="thead-dark text-center p-3">
                    <tr className="m-2">
                      <th className="align-middle cabecera" onClick={() => ordenarTabla('idOportunidad')}>Id Oportunidad</th>
                      <th className="align-middle cabecera" onClick={() => ordenarTabla('nombreCliente')}>Nombre Cliente</th>
                      <th className="align-middle">Telefono</th>
                      <th className="align-middle cabecera" onClick={() => ordenarTabla('estado')}>Estado</th>
                      <th className="align-middle cabecera" onClick={() => ordenarTabla('nombreAbogadoAsignado')}>Abogado Asignado</th>
                      <th className="align-middle cabecera" onClick={() => ordenarTabla('creado')}>Fecha Creación</th>
                      <th className="align-middle cabecera" onClick={() => ordenarTabla('especialidad')}>Especialidad</th>
                      <th className="align-middle cabecera" onClick={() => ordenarTabla('subEspecialidad')}>SubEspecialidad</th>
                      <th className="align-middle cabecera" onClick={() => ordenarTabla('provinciaOportunidad')}>Provincia</th>
                      <th className="align-middle cabecera" onClick={() => ordenarTabla('creador')}>Gestionado por</th>
                    </tr>
                  </thead>

                  <tbody>

                    {resultados.length === 0 ? <SinResultados className="text-center" ><td colSpan="8">La búsqueda no dió resultados</td></SinResultados> : (
                      resultados.map(oportunidad => {
                        return (
                          <DetallesOportunidad
                            key={oportunidad.id}
                            oportunidad={oportunidad}

                          />
                        )
                      })
                    )
                    }
                  </tbody>
                </table>
                
              </div>)}
          </>
        )
          : !permiso && usuario && cargando ? (
            <p>Cargando...</p>
          )
            : !permiso && usuario && !cargando ? (
              <PaginaError msg={`No puedes pasar.`}></PaginaError>
            )
              : !permiso && !usuario ? (
                <>
                  <PaginaError msg={`Tienes que estar logueado.`}></PaginaError>
                  <div className="text-center">
                    <Link href="/iniciar-sesion"><a><h1>Inicia Sesión</h1></a></Link>
                  </div>
                </>

              ) : (
                  <>
                    <PaginaError msg={`Tienes que estar logueado.`}></PaginaError>
                    <div className="text-center">
                      <Link href="/iniciar-sesion"><a><h1>Inicia Sesión</h1></a></Link>
                    </div>
                  </>
                )
        }
        <Footer />
      </Layout>
    </div>

  )
}

export default Home;

