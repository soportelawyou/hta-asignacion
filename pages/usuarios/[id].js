import React, { useContext, useState, useEffect } from 'react';
import Select from 'react-select';
import { useRouter } from 'next/router';
import Swal from 'sweetalert2';
import { ContenedorFormularioLg, BotonAceptar } from '../../components/ui/Formulario';
import {
    roles
} from '../../components/ui/OpcionesSelectores';

// Validaciones
import useValidacionEditar from '../../hooks/useValidacionEditar';
import validarEditarUsuarios from '../../validacion/validarEditarUsuarios';
import { Modal, Button } from 'react-bootstrap';
import { FirebaseContext } from '../../firebase/index';



export function MyVerticallyCenteredModal(props) {

    // State del componente
    const [usuarios, guardarUsuario] = useState({});
    const [error, guardarError] = useState(false);
    const [consultarDB, guardarConsultarDB] = useState(true)

    // Routing para obtener el ID actual
    const router = useRouter();
    var id = props.id;

    // Context de Firebase
    const { firebase, usuario } = useContext(FirebaseContext);

    useEffect(() => {
        // Declaramos variable para trackear
        let estaCancelado = false;
        // console.log('useEffect [ID].js')

        if (id && consultarDB) {
            const obtenerUsuario = async () => {
                const usuarioQuery = await firebase.db.collection('usuarios').doc(id);
                const user = await usuarioQuery.get();

                // Se ejecuta solo si esta montado
                if (!estaCancelado) {
                    if (user.exists) {
                        guardarUsuario(user.data());
                    } else {
                        guardarError(true);
                    }
                    guardarConsultarDB(false);
                }

            }
            obtenerUsuario();
        }
        return () => {
            // Ponemos la variable a true para evitar que intente actualizar un state inexistente
            estaCancelado = true;
        }
    }, [id, usuarios])


    // if (Object.keys(producto).length === 0) return <p>Cargando...</p>


    const { valores, errores, handleSubmit, handleChange,handleChangeRol} = useValidacionEditar(usuarios, null, validarEditarUsuarios, editarUsuario);


    // const { tipo, nombre, apellidos, idLawyou, dni, direccion, calle, provincia, ciudad, comunidadAutonoma, cp, creado, creador, contadorAsuntos, contadorRechazados } = abogado;
    

    // Destructuring de los valores
    var {  admitido,nombre,email,rol} = valores;
    if (rol === undefined){
        rol = ""
    }
    //valores de los fields
    const valorRol = roles.find(prov => prov.value === rol)

    async function editarUsuario() {
        if (!usuarios) {
            return router.push('/iniciar-sesion');
        }

        // Creamos el objeto de abogado
        const usuario = {
            nombre,
            email,
            rol,
            admitido
        }

        try {
            if (usuario) {
                // Actualiza en la bbdd
                await firebase.db.collection('usuarios').doc(id).update(usuario);
                console.log('Editando Usuario', usuario)
            }
            Swal.fire({
                icon: 'success',
                title: 'El Usuario se editó correctamente!',
                showConfirmButton: false,
                timer: 1200
            })

            router.push('/usuarios')
        } catch (error) {
            console.log(error)
            guardarError(error)
        }


    };


    return (
        <Modal
      {...props}
      size="xl"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          <h1> Editar Usuario <span>{nombre}</span></h1>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
      <div>

{error ? <PaginaError msg="Vaya... Parece que ha habido un error al editar el abogado, vuelve a intentarlo por favor"></PaginaError> : Object.keys(valores).length === 0 ? <p>Cargando...</p> : (

    usuario ? (
            <form
                onSubmit={handleSubmit}
            >
                <ContenedorFormularioLg>
                    <fieldset>
                        <legend>Datos Usuario</legend>
                        <div className="col-12 d-flex mt-2">
                            <div className="col-4">
                                <label htmlFor="nombre">Nombre</label>
                                <input readOnly
                                    type="text"
                                    className="form-control"
                                    name="nombre"
                                    onChange={handleChange}
                                    defaultValue={nombre}
                                    placeholder="Introduce el nombre"
                                />
                            </div>
                            <div className="col-4">
                                <label htmlFor="email">Email</label>
                                <input readOnly
                                    type="text"
                                    className="form-control"
                                    name="email"
                                    onChange={handleChange}
                                    defaultValue={email}
                                    placeholder="Introduce el email"
                                />
                            </div>
                        </div>

                        <div className="col-12 d-flex abajo">
                            <div className="col-4">
                                <label htmlFor="rol">Rol</label>
                                <Select
                                    onChange={handleChangeRol}
                                    options={roles}
                                    name="rol"
                                    placeholder="Selecciona Rol"
                                    defaultValue={valorRol}
                                />
                                {errores.rol && <p className="alert alert-danger text-center p-2 mt-2">{errores.rol}</p>}
                            </div>
                           </div>
                    </fieldset>
                    </ContenedorFormularioLg>
                

                <div className="col-12 d-flex justify-content-center">
                    {error && <p className="alert alert-danger text-center p2 mt-2 mb-2">{error}</p>}
                    <BotonAceptar
                        className="btn btn-primary"
                        type="submit"
                    >Guardar Cambios</BotonAceptar>
                </div>
                
            </form>
    ) :
        <>
        <h1> prueba fallo</h1>
        </>

)}


</div>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>

    )
}
export default MyVerticallyCenteredModal;
