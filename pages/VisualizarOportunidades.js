import React, { useContext, useState, useEffect } from "react";
import Router, { useRouter } from "next/router";
import Link from "next/link";
import Select from "react-select";
import Swal from "sweetalert2";
import emailjs from "emailjs-com";

import Layout from "../components/layout/Layout";
import {
  ContenedorFormularioLg,
  BotonAceptar,
  BotonCancelar,
} from "../components/ui/Formulario";
import PaginaError from "../components/layout/PaginaError";
import {
  Generales,
  Erte,
  Despidos,
  NegligenciaMedica,
  Herencia,
  Divorcio,
  Desahucio,
  LeySegundaOportunidad,
  ResponsabilidadPatrimonialEstado,
  Fijezas,
  PropiedadIntelectual,
  ViciosOcultos,
} from "../components/ui/Preguntas";
import {
  provincias,
  estados,
  especialidades,
  comunidadesConProvincias,
  Tipo_Servicio,
  Servicio,
  aTodo,
  Tipo,
  Civil,
  Penal,
  Laboral,
  Societario,
  Extranjeria,
  Internacional,
  Nuevas_Tecnologias,
  Mercantil,
  Fiscal_y_Tributario,
  Bancario,
  Constitucional,
  Compliance,
  Comunitario,
  Administrativo,
  Precios,
  SiNo,
  Canal,
  SubCanal,
  motivosRechazoAbogado,
  motivosRechazoCliente,
} from "../components/ui/OpcionesSelectores";
import useAbogadosSugerencia from "../hooks/useAbogadosSugerencia";
import { SinResultados } from "../components/ui/SinResultados";
import SeleccionarAbogado from "../components/layout/SeleccionarAbogado";
import SeleccionarAbogadoCita from "../components/layout/SeleccionarAbogadoCita";
import SeleccionarAbogadoPresupuesto from "../components/layout/SeleccionarAbogadoPresupuesto";
import { InputText, InputSubmit } from "../components/ui/Busqueda";
import { BotonEliminar } from "../components/ui/BotonEliminar";
import useContadorOportunidades from "../hooks/useContadorOportunidades";
import usePaises from "../hooks/usePaises";

// Validaciones
import useValidacionOportunidadesEditar from "../hooks/useValidacionOportunidadesEditar";
import validarEditarOportunidad from "../validacion/validarEditarOportunidad";
import {
  InputTextOportunidades,
  InputSubmitSugerencias,
} from "../components/ui/Busqueda";

// Firebase
import { FirebaseContext } from "../firebase/index";

//AXIOS libreria para llamada http

import axios from "axios";

//Funciones Propias

import { Convertir } from "./api/Funciones";

//estilos
import {
  Divp1,
  Divp2,
  Label1,
  Label2,
  Label4,
  DivFloat,
  H2Alerta,
  DivScroll,
} from "../components/ui/Visual";

import { Radio } from "../components/ui/Boton";

import { Modal, Button, InputGroup } from "react-bootstrap";

export function MyVerticallyCenteredModal(props) {

  const id = props.id;
  const router = useRouter();
  const [consultarDB, guardarConsultarDB] = useState(true);
  const [oportunidad, guardarOportunidad] = useState({});
  const [tipoUsuario, guardarTipoUsuario] = useState("");

  const [error, guardarError] = useState(false);
  const [cargado, guardarCargado] = useState(0);
  const [errorDB, guardarErrorDB] = useState("");
  const [contadorAbogado, guardarContadorAbogado] = useState(0);
  const [nombreClientec, guardarNombreCliente] = useState("");
  const [apellidoClientec, guardarApellidoCliente] = useState("");
  const [telefonoClientec, guardarTelefonoCliente] = useState("");
  const [contadorAceptadosAbogado, guardarContadorAceptadosAbogado] = useState(0);

  const [contadorDB, guardarContadorDB] = useState(0);
  const [contadorPreguntas, guardarContadorPreguntas] = useState(1);
  const [ventanaMostrada, guardarVentanaMostrada] = useState(1);
  const [entidadLegal, guardarEntidadLegal] = useState("Persona Física");
  const [idOportunidad, guardarIdOportunidad] = useState("");
  const [comunidadDeProvincia, guardarComunidadDeProvincia] = useState("");
  const [abogadosPorComunidadPrincipal, guardarAbogadosPorComunidadPrincipal] = useState([]);
  const [abogadosPorComunidadSecundario, guardarAbogadosPorComunidadSecundario] = useState([]);
  const [sociosComunidadPrincipal, guardarSociosComunidadPrincipal] = useState([]);
  const [sociosComunidadSecundario, guardarSociosComunidadSecundario] = useState([]);
  const [
    listaAbogadosSeleccionados,
    guardarListaAbogadosSeleccionados,
  ] = useState([]);
  
  //CheckFields
  const [Checked, setChecked] = useState(false);
  const [CheckedSms, setCheckedSms] = useState(false);
  const [CheckedEmail, setCheckedEmail] = useState(false);
  const [CheckedApp, setCheckedApp] = useState(false);
  const [CheckedSeguimiento, setCheckedSeguimiento] = useState(false);
  const [CheckedPasarela, setCheckedPasarela] = useState(false);
  const [
    CheckedPreguntarPresupuesto,
    setCheckedPreguntarPresupuesto,
  ] = useState(false);
  const [CheckedEmailPresupuesto, setCheckedEmailPresupuesto] = useState(false);
  const [CheckedLlamar, setCheckedLlamar] = useState(false);
  const [CheckedGestionarCita, setCheckedGestionarCita] = useState(false);

  const [colaboradoresComunidadPrincipal, guardarColaboradoresComunidadPrincipal] = useState([]);
  const [colaboradoresComunidadSecundario, guardarColaboradoresComunidadSecundario] = useState([]);
  const [emailAbogadoAsignado, guardarEmailAbogadoAsignado] = useState("");
  const [sEspecialidad, guardarEspecialidad] = useState("");

  // FILTRADO
  const [
    abogadosSugeridosPrincipal,
    guardarAbogadosSugeridosPrincipal,
  ] = useState([]);
  const [
    colaboradoresSugeridosPrincipal,
    guardarColaboradoresSugeridosPrincipal,
  ] = useState([]);
  const [
    abogadosSugeridosSecundario,
    guardarAbogadosSugeridosSecundario,
  ] = useState([]);
  const [
    colaboradoresSugeridosSecundario,
    guardarColaboradoresSugeridosSecundario,
  ] = useState([]);
  const [sinSugerencias, guardarSinSugerencias] = useState(false);
  const [busquedaManual, guardarBusquedaManual] = useState(false);
  const [otrasSugerenciasPrincipal, guardarOtrasSugerenciasPrincipal] = useState([]);
  const [otrasSugerenciasSecundario, guardarOtrasSugerenciasSecundario] = useState([]);

  const { firebase, usuario } = useContext(FirebaseContext);
  const { abogados } = useAbogadosSugerencia("ultimaOportunidad");
  const { contador } = useContadorOportunidades();
  const { Paises, Auth } = usePaises();
  const [opcionesProvincias, guardarOpcionesProvincias] = useState([]);
  const [opcionesCiudades, guardarOpcionesCiudades] = useState([]);

  // const {provincia, especialidadesAbogado} = abogados;
  // if(abogados.length !== 0){

  //     console.log(abogados)
  // }

  // Valores por defecto
  const sDefaultServicio = {
    value: "Consulta General",
    label: "Consulta General",
  };
  const sDefaultTipoServicio = { value: "Propio", label: "Propio" };
  const sPrecio = { value: "60", label: "60" };
  const sPais = { value: "Spain", label: "Spain" };

  // Funcion para buscar al usuario
  const buscarUsuario = async () => {
    await firebase.db
      .collection("usuarios")
      .where("email", "==", usuario.email)
      .onSnapshot(manejarSnapShotUsuario);
  };

  function manejarSnapShotUsuario(snapshot) {
    const usuarioBD = snapshot.docs.map((doc) => {
      return {
        id: doc.id,
        ...doc.data(),
      };
    });
    if (usuarioBD[0] !== undefined) {
      guardarTipoUsuario(usuarioBD[0].rol);
      console.log(usuarioBD[0].rol);
    }
  }

  // Funcion para Buscar Abogado en la bd
  const buscarAbogado = async (e) => {
    const abogadoQuery = await firebase.db
      .collection("abogados")
      .doc(e.id);
    return await abogadoQuery.get().data()
  };

  useEffect(() => {
    let montado = true;
    if (usuario !== undefined && usuario !== null) {
      buscarUsuario();
    }
    return () => (montado = false);
  }, [usuario]);

  useEffect(() => {
    // Declaramos variable para trackear
    let estaCancelado = false;
    // console.log('useEffect [ID].js')

    if (id && consultarDB) {
      const obtenerOportunidad = async () => {
        const oportunidadQuery = await firebase.db
          .collection("oportunidades")
          .doc(id);
        const oportunidad = await oportunidadQuery.get();

        // Se ejecuta solo si esta montado
        if (!estaCancelado) {
          if (oportunidad.exists) {
            guardarOportunidad(oportunidad.data());
          } else {
            guardarError(true);
          }
          guardarConsultarDB(false);
        }
      };
      obtenerOportunidad();
    }
    return () => {
      // Ponemos la variable a true para evitar que intente actualizar un state inexistente
      estaCancelado = true;
    };
  }, [id, oportunidad]);

  //Valores extraidos de la instancia principal de arriba
  const {
    valores,
    errores,
    handleSubmit,
    handleChange,
    handleChangeProvincia,
    handleChangeSubEspecialidad,
    handleChangeAbogadoAsignado,
    nombreAbogado,
    handleChangeTipo,
    handleChangeTipoServicio,
    handleChangeSiNo,
    handleChangePrProvincia,
    handleChangeCanal,
    handleChangeSubCanal,
    handleChangePais,
    handleChangeCiudad,
    handleChangeServicio,
    handleChangeEstado,
    handleChangeMotivo,
  } = useValidacionOportunidadesEditar(
    oportunidad,
    validarEditarOportunidad,
    editarOportunidad
  );

  var {
    entidadSocial,
    nombreCliente,
    abogadosSeleccionados,
    apellidoCliente,
    motivoRechazo,
    explicarMotivo,
    telefonoCliente,
    provincia,
    descripcion,
    subEspecialidad,
    ciudad,
    canales,
    email,
    dni,
    nif,
    cif,
    especialidad,
    provinciaOportunidad,
    abogadoAsignado,
    abogadoAsignadoOriginal,
    abogadosSeleccionados,
    estado,
    numeroReasignaciones,
    nombreAbogadoAsignado,
    servicio,
    precioServicio,
    precioPresupuesto,
    tipoServicio,
    descripcionOportunidad,
    registroLlamada,
    tipo,
    pais,
    cita,
    preguntasYRespuestas,
    horaCita,
    fechaCita,
    reasignada,
    logNoContestado,
    noContestadoFecha,
    noContestadoHora,
    notasGestion,
    canal,
    subcanal,
    respuestaProvin,
    respuestaText1,
    respuestaText2,
    respuestaText3,
    respuestaText4,
    respuestaText5,
    respuestaText6,
    respuestaText7,
    respuestaText8,
    respuestaText9,
    respuestaText10,
    respuestaText11,
    respuestaSiNo1,
    respuestaSiNo2,
    respuestaSiNo3,
    respuestaSiNo4,
    respuestaSiNo5,
    respuestaSiNo6,
    respuestaSiNo7,
    respuestaSiNo8,
    respuestaSiNo9,
    respuestaSiNo10,
    respuestaSiNo11,
    respuestaSiNo12,
    respuestaSiNo13,
    respuestaSiNo14,
    mEmail,
    mSms,
    mPasarela,
    mWhats,
    mSeguimiento,
    mEmailPresupuesto,
    mPreguntar,
    mLlamar,
    mGestionarCita,
  } = valores;

  //CheckFields
  const [Checked, setChecked] = useState(false);
  const [CheckedSms, setCheckedSms] = useState(false);
  const [CheckedEmail, setCheckedEmail] = useState(false);
  const [CheckedApp, setCheckedApp] = useState(false);
  const [CheckedSeguimiento, setCheckedSeguimiento] = useState(false);
  const [CheckedPasarela, setCheckedPasarela] = useState(false);
  const [
    CheckedPreguntarPresupuesto,
    setCheckedPreguntarPresupuesto,
  ] = useState(mPreguntar);
  const [CheckedEmailPresupuesto, setCheckedEmailPresupuesto] = useState(false);
  const [CheckedLlamar, setCheckedLlamar] = useState(false);
  const [CheckedGestionarCita, setCheckedGestionarCita] = useState(false);

  const [colaboradoresComunidadPrincipal, guardarColaboradoresComunidadPrincipal] = useState([]);
  const [colaboradoresComunidadSecundario, guardarColaboradoresComunidadSecundario] = useState([]);
  const [emailAbogadoAsignado, guardarEmailAbogadoAsignado] = useState("");
  const [sEspecialidad, guardarEspecialidad] = useState("");
  console.log("COMPROBAR EL CHECK: " + CheckedSms)

  useEffect(() => {
    let montado = true;
    console.log(montado);
    if (contador !== undefined) {
      if (montado) {
        guardarContadorDB(contador.contador);
        console.log("contadorDB", contador.contador);
      }
    }
    return () => (montado = false);
  }, [contador]);

  //Api PAISES

  useEffect(() => {
    axios(`https://www.universal-tutorial.com/api/states/${pais}`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${Auth}`,
        Accept: "application/json",
        Connection: "close",
      },
    }).then((response) => guardarOpcionesProvincias(Convertir(response.data)));
  }, [pais]);

  useEffect(() => {
    axios(
      `https://www.universal-tutorial.com/api/cities/${provinciaOportunidad}`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${Auth}`,
          Accept: "application/json",
          Connection: "close",
        },
      }
    ).then((response) => guardarOpcionesCiudades(Convertir(response.data)));
  }, [provinciaOportunidad]);

  // Sugerencias de SOCIOS y COLABORADORES
  useEffect(() => {
    let montado = true;
    if (montado && provinciaOportunidad !== "" && provinciaOportunidad !== undefined) {
      const comuna = comunidadesConProvincias.find((prov) =>
        prov.prov.includes(quitarTildes(provinciaOportunidad))
      );
      if (comuna === undefined) {
        guardarComunidadDeProvincia("");
      } else {
        console.log(comuna.com);
        guardarComunidadDeProvincia(comuna.com);
      }
    }

    if (abogados && provinciaOportunidad !== undefined) {
      // console.log('abogados', abogados)
      const provinciaMinuscula = provinciaOportunidad.toLowerCase();
      const provinciaSinTildes = quitarTildes(provinciaMinuscula);
      //const ciudadMinuscula = ciudad.toLowerCase();
      //const ciudadSinTildes = quitarTildes(ciudadMinuscula);
      // const especialidadMinuscula = especialidad.toLowerCase();
      // const especialidadSinTildes = quitarTildes(especialidadMinuscula);
      const abogadosFiltradosPrincipal = abogados.filter((abogado) => {
        if (
          subEspecialidad !== "" &&
          provinciaOportunidad !== "" &&
          abogado.lugaresEjercer !== undefined
        ) {
          if (abogado.lugaresEjercer.length > 0) {
            for (let i = 0; i < abogado.lugaresEjercer.length; i++) {
              return (
                quitarTildes(
                  abogado.lugaresEjercer[i].Provincia.toLowerCase()
                ).includes(provinciaSinTildes) &&
                abogado.subEspecialidadesAbogadosPrincipal.includes(
                  subEspecialidad
                ) &&
                abogado.tipo !== "Colaborador" &&
                abogado.activo === "Activo"
                //&&  quitarTildes(abogado.ciudad.toLowerCase()).includes(ciudadSinTildes)
              );
            }
          }
        }
      });
      const abogadosFiltradosSecundario = abogados.filter((abogado) => {
        if (
          subEspecialidad !== "" &&
          provinciaOportunidad !== "" &&
          abogado.lugaresEjercer !== undefined
        ) {
          if (abogado.lugaresEjercer.length > 0) {
            for (let i = 0; i < abogado.lugaresEjercer.length; i++) {
              return (
                quitarTildes(
                  abogado.lugaresEjercer[i].Provincia.toLowerCase()
                ).includes(provinciaSinTildes) &&
                abogado.subEspecialidadesAbogadosSecundario.includes(
                  subEspecialidad
                ) &&
                abogado.tipo !== "Colaborador" &&
                abogado.activo === "Activo"
                //&&  quitarTildes(abogado.ciudad.toLowerCase()).includes(ciudadSinTildes)
              );
            }
          }
        }
      });
      const colaboradoresFiltradosPrincipal = abogados.filter((abogado) => {
        if (subEspecialidad !== "" && provinciaOportunidad !== "") {
          if (abogado.lugaresEjercer.length > 0) {
            for (let i = 0; i < abogado.lugaresEjercer.length; i++) {
              return (
                quitarTildes(
                  abogado.lugaresEjercer[i].Provincia.toLowerCase()
                ).includes(provinciaSinTildes) &&
                abogado.subEspecialidadesAbogadosPrincipal.includes(
                  subEspecialidad
                ) &&
                abogado.tipo === "Colaborador" &&
                abogado.activo === "Activo"
                //&&  quitarTildes(abogado.ciudad.toLowerCase()).includes(ciudadSinTildes)
              );
            }
          }
        }
      });

      const colaboradoresFiltradosSecundario = abogados.filter((abogado) => {
        if (subEspecialidad !== "" && provinciaOportunidad !== "") {
          if (abogado.lugaresEjercer.length > 0) {
            for (let i = 0; i < abogado.lugaresEjercer.length; i++) {
              return (
                quitarTildes(
                  abogado.lugaresEjercer[i].Provincia.toLowerCase()
                ).includes(provinciaSinTildes) &&
                abogado.subEspecialidadesAbogadosSecundario.includes(
                  subEspecialidad
                ) &&
                abogado.tipo === "Colaborador" &&
                abogado.activo === "Activo"
                //&&  quitarTildes(abogado.ciudad.toLowerCase()).includes(ciudadSinTildes)
              );
            }
          }
        }
      });
      if (montado) {
        guardarAbogadosSugeridosPrincipal(abogadosFiltradosPrincipal);
        guardarColaboradoresSugeridosPrincipal(colaboradoresFiltradosPrincipal);
        guardarAbogadosSugeridosSecundario(abogadosFiltradosSecundario);
        guardarColaboradoresSugeridosSecundario(
          colaboradoresFiltradosSecundario
        );
        if (
          subEspecialidad !== "" &&
          provinciaOportunidad !== "" &&
          abogadosFiltradosPrincipal.length === 0 &&
          colaboradoresFiltradosPrincipal.length === 0 &&
          abogadosFiltradosSecundario.length === 0 &&
          colaboradoresFiltradosSecundario.length === 0
        ) {
          guardarSinSugerencias(true);
          guardarBusquedaManual(true);
          return;
        }
        guardarSinSugerencias(false);
        guardarBusquedaManual(false);
      }
    }
    return () => (montado = false);
  }, [provinciaOportunidad, subEspecialidad, ciudad]);

  async function verContador() {
    if(abogadoAsignado !== undefined){
      if (abogadoAsignado.id) {
        const abogadoSeleccionado = await firebase.db
          .collection("abogados")
          .doc(abogadoAsignado.id)
          .get();
        guardarContadorAbogado(abogadoSeleccionado.data().contadorAsuntos);
        nombreAbogado(
          `${abogadoSeleccionado.data().nombre} ${abogadoSeleccionado.data().apellidos
          }`
        );
        guardarEmailAbogadoAsignado(abogadoSeleccionado.data().emailAbogado);
      }
    } 
  }

  useEffect(() => {
    let montado = true;
    if (montado) {
      verContador();
    }
    return () => (montado = false);
  }, [abogadoAsignado]);

  const quitarTildes = (palabra) => {
    const letras = { á: "a", é: "e", í: "i", ó: "o", ú: "u" };
    palabra = palabra.replace(/[áéíóú]/g, (m) => letras[m]);
    return palabra;
  };

  // Funcion para checar el abogado seleccionado
  const seleccionarRadio = async (opcion) => {
    handleChangeAbogadoAsignado(opcion);
  };

  const seleccionarCheckBox = async (opcion, accion, nombre, apellidos) => {
    const aux = listaAbogadosSeleccionados;
    if (accion == true) {
      aux.push({ id: opcion, nombre: nombre, apellidos: apellidos });
    } else {
      const oBorrar = aux.filter((e) => e.id === opcion);
      if (oBorrar.length > 0) {
        const index = aux.indexOf(oBorrar[0]);
        if (index > -1) {
          aux.splice(index, 1);
        }
      }
    }
    guardarListaAbogadosSeleccionados(aux);
  };

  // Funcion de filtrado manual
  // FILTRADO
  const [q, guardarQ] = useState("");
  const [resultados, guardarResultados] = useState([]);

  const [qComunidad, guardarQComunidad] = useState("");

  // Cuando no hay abogados en la misma provincia, Abogados Principal
  useEffect(() => {
    let montado = true;
    if (otrasSugerenciasPrincipal) {
      const busqueda = q.toLowerCase();
      const busquedaSinTildes = quitarTildes(busqueda);
      const abogadosFiltrados = otrasSugerenciasPrincipal.filter((abogado) => {
        return `${quitarTildes(abogado.nombre.toLowerCase())} ${quitarTildes(
          abogado.apellidos.toLowerCase()
        )}`.includes(busquedaSinTildes);
      });

      if (montado) {
        if (comunidadDeProvincia !== "") {
          const busqueda = qComunidad.toLowerCase();
          const busquedaSinTildes = quitarTildes(busqueda);
          console.log(otrasSugerenciasPrincipal)
          const abogadosComunidadAutonoma = otrasSugerenciasPrincipal.filter(
            (abogado) => {
              return (
                quitarTildes(abogado.comunidadAutonoma).includes(
                  quitarTildes(comunidadDeProvincia)
                ) &&
                `${quitarTildes(abogado.nombre.toLowerCase())} ${quitarTildes(
                  abogado.apellidos.toLowerCase()
                )}`.includes(busquedaSinTildes)
              );
            }
          );
          // Guardamos los abogados de la CCAA en state
          guardarAbogadosPorComunidadPrincipal(abogadosComunidadAutonoma);
        }
        guardarResultados(abogadosFiltrados);
      }
    }
    return () => (montado = false);
  }, [
    q,
    qComunidad,
    otrasSugerenciasPrincipal,
    comunidadDeProvincia,
    provinciaOportunidad,
  ]);

  // Cuando no hay abogados en la misma provincia, Abogados Secundario
  useEffect(() => {
    let montado = true;
    if (otrasSugerenciasPrincipal) {
      const busqueda = q.toLowerCase();
      const busquedaSinTildes = quitarTildes(busqueda);
      const abogadosFiltrados = otrasSugerenciasSecundario.filter((abogado) => {
        return `${quitarTildes(abogado.nombre.toLowerCase())} ${quitarTildes(
          abogado.apellidos.toLowerCase()
        )}`.includes(busquedaSinTildes);
      });

      if (montado) {
        if (comunidadDeProvincia !== "") {
          const busqueda = qComunidad.toLowerCase();
          const busquedaSinTildes = quitarTildes(busqueda);
          console.log(otrasSugerenciasSecundario)
          const abogadosComunidadAutonoma = otrasSugerenciasSecundario.filter(
            (abogado) => {
              return (
                quitarTildes(abogado.comunidadAutonoma).includes(
                  quitarTildes(comunidadDeProvincia)
                ) &&
                `${quitarTildes(abogado.nombre.toLowerCase())} ${quitarTildes(
                  abogado.apellidos.toLowerCase()
                )}`.includes(busquedaSinTildes)
              );
            }
          );
          // Guardamos los abogados de la CCAA en state
          guardarAbogadosPorComunidadSecundario(abogadosComunidadAutonoma);
        }
        guardarResultados(abogadosFiltrados);
      }
    }
    return () => (montado = false);
  }, [
    q,
    qComunidad,
    otrasSugerenciasSecundario,
    comunidadDeProvincia,
    provinciaOportunidad,
  ]);

  // Diferenciamos Socios de Colaboradores (CCAA) cuando no haya sugerencias
  useEffect(() => {
    let montado = true;
    if (montado && abogadosPorComunidadPrincipal.length !== 0) {
      const sociosFiltrados = abogadosPorComunidadPrincipal.filter((abogado) => {
        return abogado.tipo !== "Colaborador";
      });
      guardarSociosComunidadPrincipal(sociosFiltrados);

      const colaboradoresFiltrados = abogadosPorComunidadPrincipal.filter((abogado) => {
        return abogado.tipo === "Colaborador";
      });
      guardarColaboradoresComunidadPrincipal(colaboradoresFiltrados);
    } else {
      guardarSociosComunidadPrincipal([]);
      guardarColaboradoresComunidadPrincipal([]);
    }

    return () => (montado = false);
  }, [abogadosPorComunidadPrincipal]);

  useEffect(() => {
    let montado = true;
    if (montado && abogadosPorComunidadSecundario.length !== 0) {
      const sociosFiltrados = abogadosPorComunidadSecundario.filter((abogado) => {
        return abogado.tipo !== "Colaborador";
      });
      guardarSociosComunidadSecundario(sociosFiltrados);

      const colaboradoresFiltrados = abogadosPorComunidadSecundario.filter((abogado) => {
        return abogado.tipo === "Colaborador";
      });
      guardarColaboradoresComunidadSecundario(colaboradoresFiltrados);
    } else {
      guardarSociosComunidadSecundario([]);
      guardarColaboradoresComunidadSecundario([]);
    }

    return () => (montado = false);
  }, [abogadosPorComunidadSecundario]);

  //Buscar por provincia con principal
  const resultadosProvinciaPrincipal = async () => {
    if(subEspecialidad !== undefined){
      await firebase.db
      .collection("abogados")
      .where("subEspecialidadesAbogadosPrincipal", "array-contains", subEspecialidad)
      .onSnapshot(manejarSnapShotPrincipal);
    }
  };

  function manejarSnapShotPrincipal(snapshot) {
    const abogadosDB = snapshot.docs.map((doc) => {
      return {
        id: doc.id,
        ...doc.data(),
      };
    });
    // const abogadosOrdenados = abogadosDB.sort((a, b) => (a.contadorAsuntos > b.contadorAsuntos) ? 1 : -1)
    const abogadosOrdenados = abogadosDB.sort((a, b) =>
      a.ultimaOportunidad > b.ultimaOportunidad ? 1 : -1
    );
    guardarOtrasSugerenciasPrincipal(abogadosOrdenados);
  }

  //Buscar por provincia con Secundario
  const resultadosProvinciaSecundario = async () => {
    if(subEspecialidad !== undefined){
      await firebase.db
      .collection("abogados")
      .where("subEspecialidadesAbogadosSecundario", "array-contains", subEspecialidad)
      .onSnapshot(manejarSnapShotSecundario);
    }
  };

  function manejarSnapShotSecundario(snapshot) {
    const abogadosDB = snapshot.docs.map((doc) => {
      return {
        id: doc.id,
        ...doc.data(),
      };
    });
    // const abogadosOrdenados = abogadosDB.sort((a, b) => (a.contadorAsuntos > b.contadorAsuntos) ? 1 : -1)
    const abogadosOrdenados = abogadosDB.sort((a, b) =>
      a.ultimaOportunidad > b.ultimaOportunidad ? 1 : -1
    );
    guardarOtrasSugerenciasSecundario(abogadosOrdenados);
  }

  useEffect(() => {
    resultadosProvinciaPrincipal();
    resultadosProvinciaSecundario();
  }, [subEspecialidad]);


  useEffect(() => {
    let montado = true;
    if (contadorDB !== 0) {
      const fecha = new Date();
      const año = fecha.getFullYear();
      if (montado) {
        guardarIdOportunidad(`LY${año}${contadorDB}`);
      }
    }
    return () => (montado = false);
  }, [contadorDB]);

  const template_params = {
    reply_to: "servicios.lawyou@lawyoulegal.com",
    emailAbogado: emailAbogadoAsignado,
    destinatario: nombreAbogadoAsignado,
    from_name: "Lawyou",
    mensaje_html: `<ul>
							<li><b>Nombre Cliente</b>:  ${nombreCliente}</li>
							<li><b>Telefono</b>:  ${telefonoCliente}</li>
							<li><b>Especialidad</b>:  ${especialidad}</li>
							<li><b>Provincia</b>:  ${provinciaOportunidad}</li>
						</ul><br>
						<p>En caso de no haber confirmado tu disponibilidad, no olvides hacerlo llamando al 667 60 66 11 o respondiendo a este mail</p>`,
  };

  // Funcion que a partir de las variables de respuestas y la subespecialidad seleccionada crea un objeto con las preguntas y respuestas para importar al firebase
  function crearObjetoPreguntasYRespuestas(subes) {
    let prYRes = [];

    if(respuestaText1 === undefined){
      return preguntasYRespuestas;
    }

    switch (subes) {
      case "ERTE":
        if (respuestaText1 !== ""){
          prYRes.push({ Pregunta: Erte["Text1"], Respuesta: respuestaText1 });
        }
        if (respuestaText2 !== ""){
          prYRes.push({ Pregunta: Erte["Text2"], Respuesta: respuestaText2 });
        }
        if (respuestaText3 !== ""){
          prYRes.push({ Pregunta: Erte["Text3"], Respuesta: respuestaText3 });
        }
        if (respuestaProvin !== ""){
          prYRes.push({ Pregunta: Erte["DespProv1"], Respuesta: respuestaProvin });
        }
        if (respuestaText4 !== ""){
          prYRes.push({ Pregunta: Erte["Text4"], Respuesta: respuestaText4 });
        } 
        if (respuestaText5 !== ""){
          prYRes.push({ Pregunta: Erte["Text5"], Respuesta: respuestaText5 });
        }
        if (respuestaSiNo1 !== ""){
          prYRes.push({ Pregunta: Erte["SiNo1"], Respuesta: respuestaSiNo1 });
        }
        break;
      case "Despidos":
        if (respuestaSiNo1 !== "")
          prYRes.push({
            Pregunta: Despidos["SiNo1"],
            Respuesta: respuestaSiNo1,
          });
        if (respuestaText1 !== "")
          prYRes.push({
            Pregunta: Despidos["Text1"],
            Respuesta: respuestaText1,
          });
        if (respuestaText2 !== "")
          prYRes.push({
            Pregunta: Despidos["Text2"],
            Respuesta: respuestaText2,
          });
        if (respuestaText3 !== "")
          prYRes.push({
            Pregunta: Despidos["Text3"],
            Respuesta: respuestaText3,
          });
        break;
      case "Negligencia Medica Civil":
        if (respuestaText1 !== "")
          prYRes.push({
            Pregunta: NegligenciaMedica["Text1"],
            Respuesta: respuestaText1,
          });
        if (respuestaSiNo1 !== "")
          prYRes.push({
            Pregunta: NegligenciaMedica["SiNo1"],
            Respuesta: respuestaSiNo1,
          });
        if (respuestaSiNo2 !== "")
          prYRes.push({
            Pregunta: NegligenciaMedica["SiNo2"],
            Respuesta: respuestaSiNo2,
          });
        if (respuestaText2 !== "")
          prYRes.push({
            Pregunta: NegligenciaMedica["Text2"],
            Respuesta: respuestaText2,
          });
        if (respuestaSiNo3 !== "")
          prYRes.push({
            Pregunta: NegligenciaMedica["SiNo3"],
            Respuesta: respuestaSiNo3,
          });
        if (respuestaSiNo4 !== "")
          prYRes.push({
            Pregunta: NegligenciaMedica["SiNo4"],
            Respuesta: respuestaSiNo4,
          });
        if (respuestaText3 !== "")
          prYRes.push({
            Pregunta: NegligenciaMedica["Text3"],
            Respuesta: respuestaText3,
          });
        if (respuestaText4 !== "")
          prYRes.push({
            Pregunta: NegligenciaMedica["Text4"],
            Respuesta: respuestaText4,
          });
        break;
      case "Herencia y Sucesiones":
        if (respuestaText1 !== "")
          prYRes.push({
            Pregunta: Herencia["Text1"],
            Respuesta: respuestaText1,
          });
        if (respuestaSiNo1 !== "")
          prYRes.push({
            Pregunta: Herencia["SiNo1"],
            Respuesta: respuestaSiNo1,
          });
        if (respuestaText2 !== "")
          prYRes.push({
            Pregunta: Herencia["Text2"],
            Respuesta: respuestaText2,
          });
        if (respuestaText3 !== "")
          prYRes.push({
            Pregunta: Herencia["Text3"],
            Respuesta: respuestaText3,
          });
        if (respuestaText4 !== "")
          prYRes.push({
            Pregunta: Herencia["Text4"],
            Respuesta: respuestaText4,
          });
        if (respuestaText5 !== "")
          prYRes.push({
            Pregunta: Herencia["Text5"],
            Respuesta: respuestaText5,
          });
        if (respuestaText6 !== "")
          prYRes.push({
            Pregunta: Herencia["Text6"],
            Respuesta: respuestaText6,
          });
        if (respuestaSiNo2 !== "")
          prYRes.push({
            Pregunta: Herencia["SiNo2"],
            Respuesta: respuestaSiNo2,
          });
        if (respuestaSiNo3 !== "")
          prYRes.push({
            Pregunta: Herencia["SiNo3"],
            Respuesta: respuestaSiNo3,
          });
        if (respuestaText7 !== "")
          prYRes.push({
            Pregunta: Herencia["Text7"],
            Respuesta: respuestaText7,
          });
        if (respuestaSiNo4 !== "")
          prYRes.push({
            Pregunta: Herencia["SiNo4"],
            Respuesta: respuestaSiNo4,
          });
        if (respuestaSiNo5 !== "")
          prYRes.push({
            Pregunta: Herencia["SiNo5"],
            Respuesta: respuestaSiNo5,
          });
        if (respuestaSiNo6 !== "")
          prYRes.push({
            Pregunta: Herencia["SiNo6"],
            Respuesta: respuestaSiNo6,
          });
        if (respuestaSiNo7 !== "")
          prYRes.push({
            Pregunta: Herencia["SiNo7"],
            Respuesta: respuestaSiNo7,
          });
        if (respuestaSiNo8 !== "")
          prYRes.push({
            Pregunta: Herencia["SiNo8"],
            Respuesta: respuestaSiNo8,
          });
        if (respuestaSiNo9 !== "")
          prYRes.push({
            Pregunta: Herencia["SiNo9"],
            Respuesta: respuestaSiNo9,
          });
        if (respuestaSiNo10 !== "")
          prYRes.push({
            Pregunta: Herencia["SiNo10"],
            Respuesta: respuestaSiNo10,
          });
        break;
      case "Ley de Segunda Oportunidad":
        if (respuestaText1 !== "")
          prYRes.push({
            Pregunta: LeySegundaOportunidad["Text1"],
            Respuesta: respuestaText1,
          });
        if (respuestaSiNo1 !== "")
          prYRes.push({
            Pregunta: LeySegundaOportunidad["SiNo1"],
            Respuesta: respuestaSiNo1,
          });
        if (respuestaSiNo2 !== "")
          prYRes.push({
            Pregunta: LeySegundaOportunidad["SiNo2"],
            Respuesta: respuestaSiNo2,
          });
        if (respuestaText2 !== "")
          prYRes.push({
            Pregunta: LeySegundaOportunidad["Text2"],
            Respuesta: respuestaText2,
          });
        if (respuestaSiNo3 !== "")
          prYRes.push({
            Pregunta: LeySegundaOportunidad["SiNo3"],
            Respuesta: respuestaSiNo3,
          });
        if (respuestaText3 !== "")
          prYRes.push({
            Pregunta: LeySegundaOportunidad["Text3"],
            Respuesta: respuestaText3,
          });
        if (respuestaSiNo4 !== "")
          prYRes.push({
            Pregunta: LeySegundaOportunidad["SiNo4"],
            Respuesta: respuestaSiNo4,
          });
        break;
      case "Responsabilidad Patrimonial":
        if (respuestaText1 !== "")
          prYRes.push({
            Pregunta: ResponsabilidadPatrimonialEstado["Text1"],
            Respuesta: respuestaText1,
          });
        if (respuestaText2 !== "")
          prYRes.push({
            Pregunta: ResponsabilidadPatrimonialEstado["Text2"],
            Respuesta: respuestaText2,
          });
        if (respuestaText3 !== "")
          prYRes.push({
            Pregunta: ResponsabilidadPatrimonialEstado["Text3"],
            Respuesta: respuestaText3,
          });
        if (respuestaSiNo1 !== "")
          prYRes.push({
            Pregunta: ResponsabilidadPatrimonialEstado["SiNo1"],
            Respuesta: respuestaSiNo1,
          });
        if (respuestaSiNo2 !== "")
          prYRes.push({
            Pregunta: ResponsabilidadPatrimonialEstado["SiNo2"],
            Respuesta: respuestaSiNo2,
          });
        if (respuestaSiNo3 !== "")
          prYRes.push({
            Pregunta: ResponsabilidadPatrimonialEstado["SiNo3"],
            Respuesta: respuestaSiNo3,
          });
        break;
      case "Propiedad Intelectual":
        if (respuestaSiNo1 !== "")
          prYRes.push({
            Pregunta: PropiedadIntelectual["SiNo1"],
            Respuesta: respuestaSiNo1,
          });
        if (respuestaText1 !== "")
          prYRes.push({
            Pregunta: PropiedadIntelectual["Text1"],
            Respuesta: respuestaText1,
          });
        if (respuestaSiNo2 !== "")
          prYRes.push({
            Pregunta: PropiedadIntelectual["SiNo2"],
            Respuesta: respuestaSiNo2,
          });
        if (respuestaText2 !== "")
          prYRes.push({
            Pregunta: PropiedadIntelectual["Text2"],
            Respuesta: respuestaText2,
          });
        break;
      default:
        prYRes.push("No ha preguntas específicas");
    }
    return prYRes;
  }
  async function Comprobar() {
    console.log("Funciona")
    var myHeaders = new Headers();
    var cuerpo = {
      "filterGroups": [
        {
          "filters": [
            {
              "propertyName": "email",
              "operator": "EQ",
              "value": email
            }
          ]
        }
      ],
      "limit": 100,
      "properties": ["email", "phone", "firstname", "lastname"]
    }
    myHeaders.append("Content-Type","application/json");
    myHeaders.append("Access-Control-Allow-Origin","*");
    const proxyurl = "https://cors-anywhere.herokuapp.com/";
    const url = `https://api.hubapi.com/crm/v3/objects/contacts/search?hapikey=17ffea9d-bfe1-420d-a87c-e0a49d5133eb`; // site that doesn’t send Access-Control-*

    fetch(proxyurl + url, {
      method: 'POST',
      mode: 'cors',
      headers: myHeaders,
      body: JSON.stringify(cuerpo)

    }).then((resp) => resp.json())
      .then(function (data) {
        if (data.results.length != 0) {
          guardarCargado(2)
          guardarNombreCliente(data.results[0].properties.firstname) 
          guardarApellidoCliente(data.results[0].properties.lastname) 
          guardarTelefonoCliente(data.results[0].properties.phone ? data.results[0].properties.phone : "")
          // El estado dos, cargara los datos y los pondra en readonly
          
        }
        else {
          //Podras introducir manualmente
          guardarCargado(1)
        }
      })
      .catch(function (error) {
        console.log(error);
      });

  }

  async function editarOportunidad() {
    if (!usuario) {
      return Router.push("/iniciar-sesion");
    }
    const oportunidad = {
      idOportunidad,
      reasignada,
      entidadLegal,
      entidadSocial,
      nombreCliente,
      apellidoCliente,
      telefonoCliente,
      ciudad,
      explicarMotivo,
      motivoRechazo,
      email,
      dni,
      nif,
      cif,
      especialidad,
      subEspecialidad,
      provinciaOportunidad,
      abogadoAsignado,
      estado,
      noContestadoFecha,
      noContestadoHora,
      logNoContestado,
      creador: {
        id: usuario.uid,
        nombre: usuario.displayName,
      },
      cita: {
        Hora: horaCita,
        Fecha: fechaCita,
      },
      numeroReasignaciones,
      nombreAbogadoAsignado,
      servicio,
      precioServicio,
      tipoServicio,
      descripcionOportunidad,
      notasGestion,
      registroLlamada,
      tipo,
      pais,
      canales: {
        canal: canal,
        subcanal: subcanal,
      },
      preguntasYRespuestas,
      mEmail,
      mSms,
      mPasarela,
      mWhats,
      mSeguimiento,
      mEmailPresupuesto,
      mPreguntar,
      mLlamar,
      mGestionarCita,
    }

    if (tipo == "Cita") {
      console.log("GUARDANDO EMAIL:")
      console.log(CheckedEmail)
      oportunidad.mEmail = CheckedEmail;
      oportunidad.mSms = CheckedSms;
      oportunidad.mPasarela = CheckedPasarela;
      oportunidad.mWhats = CheckedApp;
      oportunidad.mSeguimiento = CheckedSeguimiento;
    } else { // Tipo Presupuesto
      oportunidad.mEmailPresupuesto = CheckedEmailPresupuesto;
      oportunidad.mPreguntar = CheckedPreguntarPresupuesto;
      oportunidad.mLlamar = CheckedLlamar;
      oportunidad.mGestionarCita = CheckedGestionarCita;
      if (CheckedGestionarCita) {
        oportunidad.mEmail = CheckedEmail;
        oportunidad.mSms = CheckedSms;
        oportunidad.mPasarela = CheckedPasarela;
        oportunidad.mWhats = CheckedApp;
        oportunidad.mSeguimiento = CheckedSeguimiento;
      }
    }

    if (Checked == true) {
      oportunidad.estado = "No Contestado";
      oportunidad.logNoContestado.push({
        Fecha: oportunidad.noContestadoFecha,
        Hora: oportunidad.noContestadoHora,
      });
    }

    if (entidadLegal !== "Persona Jurídica") {
      oportunidad.entidadSocial = "";
    }

    oportunidad.especialidad = sEspecialidad;

    if (oportunidad.descripcion === undefined) {
      oportunidad.descripcion = "";
    }
    if (oportunidad.explicarMotivo === undefined) {
      oportunidad.explicarMotivo = "";
    }
    if (oportunidad.servicio === "") {
      oportunidad.servicio = sDefaultServicio.value;
    }
    if (oportunidad.tipoServicio === "") {
      oportunidad.tipoServicio = sDefaultTipoServicio.value;
    }
    if (oportunidad.precioServicio === "") {
      oportunidad.precioServicio = sPrecio;
    }
    if (oportunidad.pais === "") {
      oportunidad.pais = "Spain";
    }
    if (oportunidad.motivoRechazo === undefined) {
      oportunidad.motivoRechazo = "";
    }
    if (oportunidad.cita.Hora === undefined) {
      oportunidad.cita.Hora = "";
    }
    if (oportunidad.cita.Fecha === undefined) {
      oportunidad.cita.Fecha = "";
    }
    if (oportunidad.canales.canal === undefined) {
      oportunidad.canales.canal = "";
    }
    if (oportunidad.canales.subcanal === undefined) {
      oportunidad.canales.subcanal = "";
    }

    if (
      abogadoAsignadoOriginal !== abogadoAsignado &&
      abogadoAsignadoOriginal !== ""
    ) {
      console.log("ORIGInAL", abogadoAsignadoOriginal);
      Swal.fire({
        icon: "error",
        title: "No se puede cambiar el abogado asignado directamente",
        text:
          "Para ello, habría que rechazar la oportunidad y reasignarsela al abogado deseado",
        showConfirmButton: true,
      });
    } else {
      if (!usuario) {
        return router.push("/login");
      }
      // Updatear en firebase
      try {
        if (numeroReasignaciones >= 1) {
          await firebase.db
            .collection("oportunidades")
            .doc(id)
            .update({ ...oportunidad, reasignada: true });
          console.log("reasignada TRUE", oportunidad);
        } else {
          await firebase.db
            .collection("oportunidades")
            .doc(id)
            .update(oportunidad);
          console.log("Editando oportunidad...", oportunidad);
        }
      } catch (error) {
        console.log(error);
        guardarErrorDB({
          ...errorDB,
          error,
        });
      }

      // Si abogado rechaza, crear nueva oportunidad y sumar 1 al contador
      if (estado === "Rechazada por Abogado") {

        //eliminarOportunidad();

        // Crear una oportunidad "gemela" con la coletilla -R + crear numeroReasignaciones  idOportunidad: idOportunidad + numeroReasignaciones.toString()
        // la oportunidad debe de tener el estado pendiente de asignar
        const numeroReasignacionesActual = numeroReasignaciones + 1;
        console.log("numeroReasignaciones", numeroReasignaciones);
        console.log("numeroReasignacionesActual: ", numeroReasignacionesActual);

        if (numeroReasignaciones <= 0) {
          let abogadosRechazadores = [];
          abogadosRechazadores.push(abogadoAsignado);
          console.log("abogados RECHAZADORES: ", abogadosRechazadores);
          const oportunidadClonada = {
            idOportunidad: idOportunidad + "-" + numeroReasignacionesActual,
            entidadLegal,
            entidadSocial,
            nombreCliente,
            apellidoCliente,
            telefonoCliente,
            ciudad,
            explicarMotivo,
            email,
            dni,
            nif,
            cif,
            motivoRechazo,
            especialidad,
            subEspecialidad,
            provinciaOportunidad,
            abogadoAsignado,
            estado: "Pendiente Asignar",
            noContestadoFecha,
            noContestadoHora,
            logNoContestado,
            cita,
            creado: Date.now(),
            creador: {
              id: usuario.uid,
              nombre: usuario.displayName,
            },
            numeroReasignaciones: numeroReasignacionesActual,
            nombreAbogadoAsignado,
            servicio,
            precioServicio,
            tipoServicio,
            descripcionOportunidad,
            notasGestion,
            registroLlamada,
            tipo,
            pais,
            canales,
            preguntasYRespuestas,
          };
          console.log(oportunidadClonada.idOportunidad);
          if (oportunidadClonada.descripcion === undefined) {
            oportunidadClonada.descripcion = "";
          }
          if (oportunidadClonada.explicarMotivo === undefined) {
            oportunidadClonada.explicarMotivo = "";
          }
          if (estado === "No Contestado") {
            oportunidadClonada.logNoContestado.push({
              Fecha: oportunidadClonada.noContestadoFecha,
              Hora: oportunidadClonada.noContestadoHora,
            });
          }

          try {
            await firebase.db
              .collection("oportunidades")
              .add(oportunidadClonada);
            console.log("oportunidadClonada", oportunidadClonada);
          } catch (error) {
            console.log(error);
          }
        } else {
          abogadosRechazadores.push(abogadoAsignado);
          const idOportunidadnueva = idOportunidad.substring(
            0,
            idOportunidad.indexOf("-")
          );
          console.log("abogados RECHAZADORES: ", abogadosRechazadores);
          const oportunidadClonada = {
            idOportunidad: idOportunidadnueva + "-" + numeroReasignaciones,
            entidadLegal,
            entidadSocial,
            nombreCliente,
            apellidoCliente,
            telefonoCliente,
            ciudad,
            explicarMotivo,
            email,
            dni,
            nif,
            cif,
            creado: Date.now(),
            creador: {
              id: usuario.uid,
              nombre: usuario.displayName,
            },
            motivoRechazo,
            especialidad,
            subEspecialidad,
            provinciaOportunidad,
            abogadoAsignado,
            estado,
            noContestadoFecha,
            noContestadoHora,
            logNoContestado,
            cita,
            numeroReasignaciones: numeroReasignacionesActual,
            nombreAbogadoAsignado,
            servicio,
            precioServicio,
            tipoServicio,
            descripcionOportunidad,
            notasGestion,
            registroLlamada,
            tipo,
            pais,
            canales,
            preguntasYRespuestas,
          };

          if (oportunidadClonada.descripcion === undefined) {
            oportunidadClonada.descripcion = "";
          }
          if (estado === "No Contestado") {
            oportunidadClonada.logNoContestado.push({
              Fecha: oportunidadClonada.noContestadoFecha,
              Hora: oportunidadClonada.noContestadoHora,
            });
          }
          try {
            await firebase.db
              .collection("oportunidades")
              .add(oportunidadClonada);
            console.log("oportunidadClonada", oportunidadClonada);
          } catch (error) {
            console.log(error);
          }
        }

        try {
          await firebase.db
            .collection("abogados")
            .doc(abogadoAsignado)
            .update({ contadorRechazados: datosAbogado + 1 });
        } catch (error) {
          console.log(error);
        }
      }

      // Si aceptada sumamos 1 al contador
      if (estado === "Presupuesto Aceptada" || estado === "Cita Aceptada") {
        try {
          await firebase.db
            .collection("abogados")
            .doc(abogadoAsignado)
            .update({ contadorAceptados: contadorAceptadosAbogado + 1 });
          emailjs
            .send(
              "smtp_server",
              "template_kZfiJX4o",
              template_params,
              "user_xxs3Pu51cqPmbXqR7CsEW"
            )
            .then(
              (response) => {
                console.log("SUCCESS!", response.status, response.text);
              },
              (err) => {
                console.log("FAILED...", err);
              }
            );
        } catch (error) {
          console.log(error);
        }
      }
      Swal.fire({
        icon: "success",
        title: "La oportunidad se actualizó correctamente!",
        showConfirmButton: false,
        timer: 1200,
      });
      setTimeout(() => {
        router.push("/index");
      }, 1200);
    }
  }

  const eliminarOportunidad = async () => {
    if (!usuario) {
      return router.push("/login");
    }

    // Modal de SWAL
    Swal.fire({
      title: "¿Estás seguro?",
      text: "No podrás deshacer esta acción!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Sí, eliminar!",
      cancelButtonText: "Cancelar",
    }).then((result) => {
      if (result.value) {
        try {
          firebase.db
            .collection("oportunidades")
            .doc(idOportunidadOriginal)
            .delete();
          Swal.fire({
            icon: "success",
            title: "La Oportunidad ha sido eliminada!",
            showConfirmButton: false,
            timer: 1200,
          });
          setTimeout(() => {
            router.push("/");
          }, 1200);
        } catch (error) {
          console.log(error);
        }
      }
    });
  };

  // Funcion para navegar por selección
  function irASeleccion(n, id) {
    guardarVentanaMostrada(n);

    var selector = document.getElementById(id);
    if (typeof selector.scrollIntoView !== "undefined") {
      selector.scrollIntoView();
    }
  }

  return (
    <Modal
      {...props}
      size="xl"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          <h1 className="titulo">Editar Oportunidad {idOportunidad}</h1>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          {error ? (
            <PaginaError msg="Vaya... Parece que ha habido un error al editar el abogado, vuelve a intentarlo por favor"></PaginaError>
          ) : Object.keys(valores).length === 0 ? (
            <p>Cargando...</p>
          ) : usuario ? (
            <>
              <form onSubmit={handleSubmit}>
                <Divp2 className="mb-2 row">
                  <div className="col-12 col-lg-3">
                    <DivFloat>
                      <div className="row">
                        {/* Pestaña 1 */}
                        <div className="col-12 d-flex">
                          <Label1
                            className="d-block"
                            onClick={() => irASeleccion(1, "ven1")}
                          >
                            1. Datos Cliente
                          </Label1>
                        </div>
                      </div>

                      <hr />

                      <div className="row">
                        {/* Pestaña 2 */}
                        <div className="col-12 d-flex">
                          <Label2
                            className="d-block"
                            onClick={() => irASeleccion(2, "ven2")}
                          >
                            2. Registro de la Llamada
                          </Label2>
                        </div>
                      </div>

                      <hr />

                      {/* Pestaña 3 */}
                      {subEspecialidad !== "" && provinciaOportunidad !== "" ? (
                        <>
                          <div className="row">
                            <div className="col-12 d-flex">
                              <Label2
                                className="d-block"
                                onClick={() => irASeleccion(3, "ven3")}
                              >
                                3. Preguntas Específicas
                              </Label2>
                            </div>
                          </div>
                          <hr />
                        </>
                      ) : null}

                      {/* Pestaña 4 */}
                      {subEspecialidad !== "" && provinciaOportunidad !== "" ? (
                        <>
                          <div className="row">
                            <div className="col-12 d-flex">
                              <Label2
                                className="d-block"
                                onClick={() => irASeleccion(4, "ven4")}
                              >
                                4. Seleccionar Abogados
                              </Label2>
                            </div>
                          </div>
                          <hr />
                        </>
                      ) : null}

                      {/* Pestaña 5 */}
                      {subEspecialidad !== "" && provinciaOportunidad !== "" ? (
                        <>
                          <div className="row">
                            <div className="col-12 d-flex">
                              <Label2
                                className="d-block"
                                onClick={() => irASeleccion(5, "ven5")}
                              >
                                5. Datos Oportunidad
                              </Label2>
                            </div>
                          </div>
                          <hr />
                        </>
                      ) : null}

                      {/* Pestaña 6 */}
                      {subEspecialidad !== "" && provinciaOportunidad !== "" ? (
                        <>
                          <div className="row">
                            <div className="col-12 d-flex">
                              <Label2
                                className="d-block"
                                onClick={() => irASeleccion(6, "ven6")}
                              >
                                6. Gestión
                              </Label2>
                            </div>
                          </div>
                          <hr />
                        </>
                      ) : null}

                      {/* Pestaña 7 */}
                      {CheckedGestionarCita ? (
                        <>
                          <div className="row">
                            <div className="col-12 d-flex">
                              <Label2
                                className="d-block"
                                onClick={() => irASeleccion(7, "ven7")}
                              >
                                7. Gestion de cita
                              </Label2>
                            </div>
                          </div>
                        </>
                      ) : null}
                    </DivFloat>
                  </div>
                  <DivScroll className="col-12 col-lg-9">
                    {/* Ventana Completa */}
                    {/* V1 */}
                    <ContenedorFormularioLg id="ven1">
                      <fieldset className="mt-3">
                        <legend className="pl-5">Datos Cliente</legend>
                        <div className="col-12 d-flex mt-4">
                          <div className="col-12">
                            <label htmlFor="entidadLegal">Entidad Legal:</label>
                            <label>
                              <Radio
                                type="radio"
                                defaultChecked
                                name="persona"
                                onClick={() => {
                                  guardarEntidadLegal("Persona Física");
                                }}
                                onChange={handleChange}
                              />
                              Persona Física
                            </label>
                            <label>
                              <Radio
                                type="radio"
                                name="persona"
                                onClick={() => {
                                  guardarEntidadLegal("Persona Jurídica");
                                }}
                                onChange={handleChange}
                              />
                              Persona Jurídica
                            </label>
                          </div>
                        </div>
                        <div className="col-12 d-flex mt-4">
                          <div className="col-4">
                            <label htmlFor="email">Email</label>
                            <input
                              type="text"
                              className="form-control"
                              name="email"
                              onChange={handleChange}
                              value={email}
                              placeholder="Email del Cliente"
                            />
                          </div>
                        </div>
                        <div className="col-12 d-flex mt-4">
                          <div className="col-4">
                            <label htmlFor="nombreCliente">Nombre</label>
                            <input
                              type="text"
                              className="form-control"
                              name="nombreCliente"
                              onChange={handleChange}
                              value={nombreCliente}
                              placeholder="Nombre del cliente"
                            />
                          </div>

                          <div className="col-4">
                            <label htmlFor="apellidoCliente">Apellido</label>
                            <input
                              type="text"
                              className="form-control"
                              name="apellidoCliente"
                              onChange={handleChange}
                              value={apellidoCliente}
                              placeholder="Apellido del cliente"
                            />
                          </div>
                          {entidadLegal === "Persona Jurídica" ? (
                            <div className="col-4">
                              <label htmlFor="entidadSocial">
                                Entidad Social
                              </label>
                              <input
                                type="text"
                                className="form-control"
                                name="entidadSocial"
                                onChange={handleChange}
                                value={entidadSocial}
                                placeholder="Nombre de la Entidad Social"
                              />
                            </div>
                          ) : null}
                        </div>

                        <div className="col-12 d-flex abajo">
                          <div className="col-4">
                            <label htmlFor="telefonoCliente">
                              Teléfono del Cliente
                            </label>
                            <input
                              type="text"
                              className="form-control"
                              name="telefonoCliente"
                              onChange={handleChange}
                              value={telefonoCliente}
                              placeholder="Teléfono del cliente"
                            />
                          </div>
                        </div>
                      </fieldset>
                    </ContenedorFormularioLg>
                    {/* V2 */}
                    <ContenedorFormularioLg id="ven2">
                      <fieldset className="mt-3">
                        <legend className="pl-5">Registro de la Llamada</legend>
                        <div className="col-12 d-flex mt-2">
                          <div className="col-4">
                            <label>Llamada No Contestada: </label>
                            <input
                              type="checkbox"
                              defaultChecked={Checked}
                              onChange={() => {
                                setChecked(!Checked);
                              }}
                            />
                          </div>
                        </div>
                        <div className="col-12 d-flex abajo">
                          <div className="col-8">
                            <label>Registro Llamada</label>
                            <textarea
                              className="form-control"
                              type="text"
                              name="registroLlamada"
                              onChange={handleChange}
                              defaultValue={registroLlamada}
                            />
                          </div>
                        </div>
                        <div className="col-12 d-flex abajo">
                          <div className="col-8">
                            <p>
                              <Label4> {Generales[0]} </Label4>
                            </p>
                            <Select
                              name="Tipo"
                              options={Tipo}
                              onChange={handleChangeTipo}
                              defaultValue={{ label: tipo, value: tipo }}
                              placeholder="Selecciona que tipo de servicio busca el cliente"
                            />
                            {errores.tipo && (
                              <p className="alert alert-danger text-center p-2 mt-2">
                                {errores.tipo}
                              </p>
                            )}
                          </div>
                        </div>
                        <div className="col-12 d-flex abajo">
                          <div className="col-8 ">
                            <label htmlFor="subEspecialidad">
                              SubEspecialidad de la Oportunidad *
                            </label>
                            <Select
                              name="subEspecialidad"
                              options={aTodo}
                              onChange={handleChangeSubEspecialidad}
                              defaultValue={{ label: subEspecialidad, value: subEspecialidad }}
                              placeholder="Selecciona Subespecialidad"
                            />

                            {errores.subEspecialidad && (
                              <p className="alert alert-danger text-center p-2 mt-2">
                                {errores.subEspecialidad}
                              </p>
                            )}
                          </div>
                          <div className="col-4 ">
                            <label htmlFor="especialidad">Especialidad</label>
                            <input
                              type="text"
                              className="form-control"
                              name="especialidad"
                              readOnly
                              id="iEspecialidad"
                              placeholder="Especialidad"
                              defaultValue={especialidad}
                            ></input>
                          </div>
                        </div>
                        <div className="col-12 d-flex mt-2 justify-content-between">
                          <div className="col-4">
                            <label htmlFor="Pais">Pais *</label>
                            <Select
                              onChange={handleChangePais}
                              options={Paises}
                              name="pais"
                              placeholder="Selecciona Pais"
                              defaultValue={sPais}
                            />
                            {errores.pais && (
                              <p className="alert alert-danger text-center p-2 mt-2">
                                {errores.pais}
                              </p>
                            )}
                          </div>
                          {pais == "Spain" || pais == "" ? (
                            <div className="col-4">
                              <label htmlFor="provincia">Provincia *</label>
                              <Select
                                onChange={handleChangeProvincia}
                                options={provincias}
                                name="provincia"
                                defaultValue={{ label: provinciaOportunidad, value: provinciaOportunidad }}
                                placeholder="Selecciona Provincia"
                              />
                              {errores.provincia && (
                                <p className="alert alert-danger text-center p-2 mt-2">
                                  {errores.provincia}
                                </p>
                              )}
                            </div>
                          ) : (
                              <div className="col-4">
                                <label htmlFor="provincia">Provincia *</label>
                                <Select
                                  onChange={handleChangeProvincia}
                                  options={opcionesProvincias}
                                  name="provincia"
                                  placeholder="Selecciona Provincia"
                                />
                                {errores.provincia && (
                                  <p className="alert alert-danger text-center p-2 mt-2">
                                    {errores.provincia}
                                  </p>
                                )}
                              </div>
                            )}
                          {/*}
                                                    <div className="col-4">
                                                    <label htmlFor="ciudad">Ciudad *</label>
                                                    <Select
                                                        onChange={handleChangeCiudad}
                                                        options={opcionesCiudades}
                                                        name="ciudad"
                                                        placeholder="Selecciona Ciudad"
                                                    />
                                                    {errores.ciudad && <p className="alert alert-danger text-center p-2 mt-2">{errores.ciudad}</p>}
                                                    </div>{*/}
                        </div>
                      </fieldset>
                    </ContenedorFormularioLg>
                    {/* V3 */}
                    {subEspecialidad !== "" ? (
                      <ContenedorFormularioLg id="ven3">
                        <fieldset className="mt-3">
                          <legend className="pl-5">
                            Preguntas Especificas
                          </legend>
                          {
                            subEspecialidad === "ERTE" ? ( //  ERTE
                              <div>
                                <div className="col-12 d-flex abajo">
                                  {" "}
                                  {/* Pregunta Text1 */}
                                  <div className="col-8">
                                    <label>{Erte["Text1"]}</label>
                                    <input
                                      name="respuestaText1"
                                      className="form-control"
                                      type="text"
                                      onChange={handleChange}
                                      value={respuestaText1}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex abajo">
                                  {" "}
                                  {/* Pregunta Text2 */}
                                  <div className="col-8">
                                    <label>{Erte["Text2"]}</label>
                                    <input
                                      name="respuestaText2"
                                      className="form-control"
                                      type="text"
                                      onChange={handleChange}
                                      value={respuestaText2}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex abajo">
                                  {" "}
                                  {/* Pregunta Text3 */}
                                  <div className="col-8">
                                    <label>{Erte["Text3"]}</label>
                                    <input
                                      name="respuestaText3"
                                      className="form-control"
                                      type="text"
                                      onChange={handleChange}
                                      value={respuestaText3}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex abajo">
                                  {" "}
                                  {/* Pregunta DespProv1 */}
                                  <div className="col-8">
                                    <label>{Erte["DespProv1"]}</label>
                                    <Select
                                      name="respuestaProvin"
                                      options={provincias}
                                      onChange={handleChangePrProvincia}
                                      value={respuestaProvin}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex abajo">
                                  {" "}
                                  {/* Pregunta Text4(num) */}
                                  <div className="col-8">
                                    <label>{Erte["Text4"]}</label>
                                    <input
                                      name="respuestaText4"
                                      className="form-control"
                                      type="number"
                                      min="0.01"
                                      step="0.01"
                                      onChange={handleChange}
                                      value={respuestaText4}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex abajo">
                                  {" "}
                                  {/* Pregunta Text5(num) */}
                                  <div className="col-8">
                                    <label>{Erte["Text5"]}</label>
                                    <input
                                      name="respuestaText5"
                                      className="form-control"
                                      type="number"
                                      min="0.01"
                                      step="0.01"
                                      onChange={handleChange}
                                      value={respuestaText5}
                                    />
                                    {parseInt(respuestaText5) >= 25 ? (
                                      <label>Avisar al departamento.</label>
                                    ) : null}
                                  </div>
                                </div>
                                <div className="col-12 d-flex abajo">
                                  {" "}
                                  {/* Pregunta SiNo1 */}
                                  <div className="col-8">
                                    <label>{Erte["SiNo1"]}</label>
                                    <Select
                                      name="respuestaSiNo1"
                                      options={SiNo}
                                      onChange={(event) =>
                                        handleChangeSiNo(event, 1)
                                      }
                                      value={respuestaSiNo1}
                                    />
                                  </div>
                                </div>
                              </div>
                            ) : subEspecialidad === "Despidos" ? ( // DESPIDOS
                              <div>
                                <div className="col-12 d-flex abajo">
                                  {" "}
                                  {/* Pregunta SiNo1 */}
                                  <div className="col-8">
                                    <label>{Despidos["SiNo1"]}</label>
                                    <Select
                                      name="respuestaSiNo1"
                                      options={SiNo}
                                      onChange={(event) =>
                                        handleChangeSiNo(event, 1)
                                      }
                                      value={respuestaSiNo1}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex abajo">
                                  {" "}
                                  {/* Pregunta Text1 */}
                                  <div className="col-8">
                                    <label>{Despidos["Text1"]}</label>
                                    <input
                                      name="respuestaText1"
                                      className="form-control"
                                      type="date"
                                      onChange={handleChange}
                                      value={respuestaText1}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex abajo">
                                  {" "}
                                  {/* Pregunta Text2 */}
                                  <div className="col-8">
                                    <label>{Despidos["Text2"]}</label>
                                    <input
                                      name="respuestaText2"
                                      className="form-control"
                                      type="text"
                                      onChange={handleChange}
                                      value={respuestaText2}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex abajo">
                                  {" "}
                                  {/* Pregunta Text3 */}
                                  <div className="col-8">
                                    <label>{Despidos["Text3"]}</label>
                                    <input
                                      name="respuestaText3"
                                      className="form-control"
                                      type="text"
                                      onChange={handleChange}
                                      value={respuestaText3}
                                    />
                                  </div>
                                </div>
                              </div>
                            ) : subEspecialidad ===
                              "Negligencia Medica Civil" ? ( // NEGLIGENCIA MEDICA
                                    <div>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta Text1 */}
                                        <div className="col-8">
                                          <label>{NegligenciaMedica["Text1"]}</label>
                                          <input
                                            name="respuestaText1"
                                            className="form-control"
                                            type="text"
                                            onChange={handleChange}
                                            value={respuestaText1}
                                          />
                                        </div>
                                      </div>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta SiNo1 */}
                                        <div className="col-8">
                                          <label>{NegligenciaMedica["SiNo1"]}</label>
                                          <Select
                                            name="respuestaSiNo1"
                                            options={SiNo}
                                            onChange={(event) =>
                                              handleChangeSiNo(event, 1)
                                            }
                                          />
                                        </div>
                                      </div>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta SiNo2 */}
                                        <div className="col-8">
                                          <label>{NegligenciaMedica["SiNo2"]}</label>
                                          <Select
                                            name="respuestaSiNo2"
                                            options={SiNo}
                                            onChange={(event) =>
                                              handleChangeSiNo(event, 2)
                                            }
                                          />
                                        </div>
                                      </div>
                                      {respuestaSiNo2 === "Sí" ? (
                                        <div className="col-12 d-flex abajo">
                                          {" "}
                                          {/* Pregunta Text2 */}
                                          <div className="col-8">
                                            <label>
                                              {NegligenciaMedica["Text2"]}
                                            </label>
                                            <input
                                              name="respuestaText2"
                                              className="form-control"
                                              type="text"
                                              onChange={handleChange}
                                              value={respuestaText2}
                                            />
                                          </div>
                                        </div>
                                      ) : null}
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta SiNo3 */}
                                        <div className="col-8">
                                          <label>{NegligenciaMedica["SiNo3"]}</label>
                                          <Select
                                            name="respuestaSiNo3"
                                            options={SiNo}
                                            onChange={(event) =>
                                              handleChangeSiNo(event, 3)
                                            }
                                          />
                                        </div>
                                      </div>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta SiNo4 */}
                                        <div className="col-8">
                                          <label>{NegligenciaMedica["SiNo4"]}</label>
                                          <Select
                                            name="respuestaSiNo4"
                                            options={SiNo}
                                            onChange={(event) =>
                                              handleChangeSiNo(event, 4)
                                            }
                                          />
                                        </div>
                                      </div>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta Text3 */}
                                        <div className="col-8">
                                          <label>{NegligenciaMedica["Text3"]}</label>
                                          <input
                                            name="respuestaText3"
                                            className="form-control"
                                            type="date"
                                            onChange={handleChange}
                                            value={respuestaText3}
                                          />
                                        </div>
                                      </div>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta Text4 */}
                                        <div className="col-8">
                                          <label>{NegligenciaMedica["Text4"]}</label>
                                          <input
                                            name="respuestaText4"
                                            className="form-control"
                                            type="text"
                                            onChange={handleChange}
                                            value={respuestaText4}
                                          />
                                        </div>
                                      </div>
                                    </div>
                                  ) : subEspecialidad === "Herencia y Sucesiones" ? ( // HERENCIA
                                    <div>
                                      <p>
                                        <Label4>{Herencia["Titulo1"]}</Label4>
                                      </p>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta Text1 */}
                                        <div className="col-8">
                                          <label>{Herencia["Text1"]}</label>
                                          <input
                                            name="respuestaText1"
                                            className="form-control"
                                            type="date"
                                            onChange={handleChange}
                                            value={respuestaText1}
                                          />
                                        </div>
                                      </div>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta SiNo1 */}
                                        <div className="col-8">
                                          <label>{Herencia["SiNo1"]}</label>
                                          <Select
                                            name="respuestaSiNo1"
                                            options={SiNo}
                                            onChange={(event) =>
                                              handleChangeSiNo(event, 1)
                                            }
                                          />
                                        </div>
                                      </div>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta Text2 */}
                                        <div className="col-8">
                                          <label>{Herencia["Text2"]}</label>
                                          <input
                                            name="respuestaText2"
                                            className="form-control"
                                            type="number"
                                            min="0.01"
                                            step="0.01"
                                            onChange={handleChange}
                                            value={respuestaText2}
                                          />
                                        </div>
                                      </div>
                                      {parseInt(respuestaText2) > 0 ? (
                                        <div className="col-12 d-flex abajo">
                                          {" "}
                                          {/* Pregunta Text3 */}
                                          <div className="col-8">
                                            <label>{Herencia["Text3"]}</label>
                                            <input
                                              name="respuestaText3"
                                              className="form-control"
                                              type="text"
                                              onChange={handleChange}
                                              value={respuestaText3}
                                            />
                                          </div>
                                        </div>
                                      ) : null}
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta Text4 */}
                                        <div className="col-8">
                                          <label>{Herencia["Text4"]}</label>
                                          <input
                                            name="respuestaText4"
                                            className="form-control"
                                            type="text"
                                            onChange={handleChange}
                                            value={respuestaText4}
                                          />
                                        </div>
                                      </div>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta Text5 */}
                                        <div className="col-8">
                                          <label>{Herencia["Text5"]}</label>
                                          <input
                                            name="respuestaText5"
                                            className="form-control"
                                            type="text"
                                            onChange={handleChange}
                                            value={respuestaText5}
                                          />
                                        </div>
                                      </div>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta Text6 */}
                                        <div className="col-8">
                                          <label>{Herencia["Text6"]}</label>
                                          <input
                                            name="respuestaText6"
                                            className="form-control"
                                            type="text"
                                            onChange={handleChange}
                                            value={respuestaText6}
                                          />
                                        </div>
                                      </div>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta SiNo2 */}
                                        <div className="col-8">
                                          <label>{Herencia["SiNo2"]}</label>
                                          <Select
                                            name="respuestaSiNo2"
                                            options={SiNo}
                                            onChange={(event) =>
                                              handleChangeSiNo(event, 2)
                                            }
                                          />
                                        </div>
                                      </div>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta SiNo3 */}
                                        <div className="col-8">
                                          <label>{Herencia["SiNo3"]}</label>
                                          <Select
                                            name="respuestaSiNo3"
                                            options={SiNo}
                                            onChange={(event) =>
                                              handleChangeSiNo(event, 3)
                                            }
                                          />
                                        </div>
                                      </div>
                                      {respuestaSiNo3 === "Sí" ? (
                                        <div>
                                          <div className="col-12 d-flex abajo">
                                            {" "}
                                            {/* Pregunta Text7 */}
                                            <div className="col-8">
                                              <label>{Herencia["Text7"]}</label>
                                              <input
                                                name="respuestaText7"
                                                className="form-control"
                                                type="number"
                                                min="0.01"
                                                step="0.01"
                                                onChange={handleChange}
                                                value={respuestaText7}
                                              />
                                            </div>
                                          </div>
                                          <div className="col-12 d-flex abajo">
                                            {" "}
                                            {/* Pregunta SiNo4 */}
                                            <div className="col-8">
                                              <label>{Herencia["SiNo4"]}</label>
                                              <Select
                                                name="respuestaSiNo4"
                                                options={SiNo}
                                                onChange={(event) =>
                                                  handleChangeSiNo(event, 4)
                                                }
                                              />
                                            </div>
                                          </div>
                                        </div>
                                      ) : null}
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta SiNo5 */}
                                        <div className="col-8">
                                          <label>{Herencia["SiNo5"]}</label>
                                          <Select
                                            name="respuestaSiNo5"
                                            options={SiNo}
                                            onChange={(event) =>
                                              handleChangeSiNo(event, 5)
                                            }
                                          />
                                        </div>
                                      </div>
                                      <p>
                                        <Label4>{Herencia["Titulo2"]}</Label4>
                                      </p>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta SiNo6 */}
                                        <div className="col-8">
                                          <label>{Herencia["SiNo6"]}</label>
                                          <Select
                                            name="respuestaSiNo6"
                                            options={SiNo}
                                            onChange={(event) =>
                                              handleChangeSiNo(event, 6)
                                            }
                                          />
                                        </div>
                                      </div>
                                      <div className="col-12 d-flex abajo">
                                        {" "}
                                        {/* Pregunta SiNo7 */}
                                        <div className="col-8">
                                          <label>{Herencia["SiNo7"]}</label>
                                          <Select
                                            name="respuestaSiNo7"
                                            options={SiNo}
                                            onChange={(event) =>
                                              handleChangeSiNo(event, 7)
                                            }
                                          />
                                        </div>
                                      </div>
                                      {respuestaSiNo7 === "Sí" ? (
                                        <div>
                                          <div className="col-12 d-flex abajo">
                                            {" "}
                                            {/* Pregunta SiNo8 */}
                                            <div className="col-8">
                                              <label>{Herencia["SiNo8"]}</label>
                                              <Select
                                                name="respuestaSiNo8"
                                                options={SiNo}
                                                onChange={(event) =>
                                                  handleChangeSiNo(event, 8)
                                                }
                                              />
                                            </div>
                                          </div>
                                          <div className="col-12 d-flex abajo">
                                            {" "}
                                            {/* Pregunta SiNo9 */}
                                            <div className="col-8">
                                              <label>{Herencia["SiNo9"]}</label>
                                              <Select
                                                name="respuestaSiNo9"
                                                options={SiNo}
                                                onChange={(event) =>
                                                  handleChangeSiNo(event, 9)
                                                }
                                              />
                                            </div>
                                          </div>
                                          <div className="col-12 d-flex abajo">
                                            {" "}
                                            {/* Pregunta SiNo10 */}
                                            <div className="col-8">
                                              <label>{Herencia["SiNo10"]}</label>
                                              <Select
                                                name="respuestaSiNo10"
                                                options={SiNo}
                                                onChange={(event) =>
                                                  handleChangeSiNo(event, 10)
                                                }
                                              />
                                            </div>
                                          </div>
                                        </div>
                                      ) : null}
                                    </div>
                                  ) : subEspecialidad ===
                                    "Ley de Segunda Oportunidad" ? ( // LEY DE SEGUNDA OPORTUNIDAD
                                        <div>
                                          <div className="col-12 d-flex abajo">
                                            {" "}
                                            {/* Pregunta Text1 */}
                                            <div className="col-8">
                                              <label>
                                                {LeySegundaOportunidad["Text1"]}
                                              </label>
                                              <input
                                                name="respuestaText1"
                                                className="form-control"
                                                type="text"
                                                onChange={handleChange}
                                                value={respuestaText1}
                                              />
                                            </div>
                                          </div>
                                          <div className="col-12 d-flex abajo">
                                            <div className="col-8">
                                              <label>
                                                {LeySegundaOportunidad["SiNo1"]}
                                              </label>
                                              <Select
                                                name="respuestaSiNo1"
                                                options={SiNo}
                                                onChange={(event) =>
                                                  handleChangeSiNo(event, 1)
                                                }
                                              />
                                            </div>
                                          </div>
                                          <div className="col-12 d-flex abajo">
                                            <div className="col-8">
                                              <label>
                                                {LeySegundaOportunidad["SiNo2"]}
                                              </label>
                                              <Select
                                                name="respuestaSiNo2"
                                                options={SiNo}
                                                onChange={(event) =>
                                                  handleChangeSiNo(event, 2)
                                                }
                                              />
                                            </div>
                                          </div>
                                          {respuestaSiNo2 === "Sí" ? (
                                            <div className="col-12 d-flex abajo">
                                              <div className="col-8">
                                                <label>
                                                  {LeySegundaOportunidad["Text2"]}
                                                </label>
                                                <input
                                                  name="respuestaText2"
                                                  className="form-control"
                                                  type="text"
                                                  onChange={handleChange}
                                                  value={respuestaText2}
                                                />
                                              </div>
                                            </div>
                                          ) : null}
                                          <div className="col-12 d-flex abajo">
                                            <div className="col-8">
                                              <label>
                                                {LeySegundaOportunidad["SiNo3"]}
                                              </label>
                                              <Select
                                                name="respuestaSiNo3"
                                                options={SiNo}
                                                onChange={(event) =>
                                                  handleChangeSiNo(event, 3)
                                                }
                                              />
                                            </div>
                                          </div>
                                          {respuestaSiNo3 === "Sí" ? (
                                            <div className="col-12 d-flex abajo">
                                              <div className="col-8">
                                                <label>
                                                  {LeySegundaOportunidad["Text3"]}
                                                </label>
                                                <input
                                                  name="respuestaText3"
                                                  className="form-control"
                                                  type="text"
                                                  onChange={handleChange}
                                                  value={respuestaText3}
                                                />
                                              </div>
                                            </div>
                                          ) : null}
                                          <div className="col-12 d-flex abajo">
                                            <div className="col-8">
                                              <label>
                                                {LeySegundaOportunidad["SiNo4"]}
                                              </label>
                                              <Select
                                                name="respuestaSiNo3"
                                                options={SiNo}
                                                onChange={(event) =>
                                                  handleChangeSiNo(event, 3)
                                                }
                                              />
                                            </div>
                                          </div>
                                        </div>
                                      ) : subEspecialidad ===
                                        "Responsabilidad Patrimonial" ? ( // RESPONSABILIDAD PATRIMONIAL ESTADO
                                          <div>
                                            <div className="col-12 d-flex abajo">
                                              <div className="col-8">
                                                <label>
                                                  {
                                                    ResponsabilidadPatrimonialEstado[
                                                    "Text1"
                                                    ]
                                                  }
                                                </label>
                                                <input
                                                  name="respuestaText1"
                                                  className="form-control"
                                                  type="text"
                                                  onChange={handleChange}
                                                  value={respuestaText1}
                                                />
                                              </div>
                                            </div>
                                            <div className="col-12 d-flex abajo">
                                              <div className="col-8">
                                                <label>
                                                  {
                                                    ResponsabilidadPatrimonialEstado[
                                                    "Text2"
                                                    ]
                                                  }
                                                </label>
                                                <input
                                                  name="respuestaText2"
                                                  className="form-control"
                                                  type="text"
                                                  onChange={handleChange}
                                                  value={respuestaText2}
                                                />
                                              </div>
                                            </div>
                                            <div className="col-12 d-flex abajo">
                                              <div className="col-8">
                                                <label>
                                                  {
                                                    ResponsabilidadPatrimonialEstado[
                                                    "Text3"
                                                    ]
                                                  }
                                                </label>
                                                <input
                                                  name="respuestaText3"
                                                  className="form-control"
                                                  type="text"
                                                  onChange={handleChange}
                                                  value={respuestaText3}
                                                />
                                              </div>
                                            </div>
                                            <div className="col-12 d-flex abajo">
                                              <div className="col-8">
                                                <label>
                                                  {
                                                    ResponsabilidadPatrimonialEstado[
                                                    "SiNo1"
                                                    ]
                                                  }
                                                </label>
                                                <Select
                                                  name="respuestaSiNo1"
                                                  options={SiNo}
                                                  onChange={(event) =>
                                                    handleChangeSiNo(event, 1)
                                                  }
                                                />
                                              </div>
                                            </div>
                                            <div className="col-12 d-flex abajo">
                                              <div className="col-8">
                                                <label>
                                                  {
                                                    ResponsabilidadPatrimonialEstado[
                                                    "SiNo2"
                                                    ]
                                                  }
                                                </label>
                                                <Select
                                                  name="respuestaSiNo2"
                                                  options={SiNo}
                                                  onChange={(event) =>
                                                    handleChangeSiNo(event, 2)
                                                  }
                                                />
                                              </div>
                                            </div>
                                            <div className="col-12 d-flex abajo">
                                              <div className="col-8">
                                                <label>
                                                  {
                                                    ResponsabilidadPatrimonialEstado[
                                                    "SiNo3"
                                                    ]
                                                  }
                                                </label>
                                                <Select
                                                  name="respuestaSiNo3"
                                                  options={SiNo}
                                                  onChange={(event) =>
                                                    handleChangeSiNo(event, 3)
                                                  }
                                                />
                                              </div>
                                            </div>
                                          </div>
                                        ) : subEspecialidad === "Propiedad Intelectual" ? ( // PROPIEDAD INTELECTUAL
                                          <div>
                                            <div className="col-12 d-flex abajo">
                                              <div className="col-8">
                                                <label>
                                                  {PropiedadIntelectual["SiNo1"]}
                                                </label>
                                                <Select
                                                  name="respuestaSiNo1"
                                                  options={SiNo}
                                                  onChange={(event) =>
                                                    handleChangeSiNo(event, 1)
                                                  }
                                                />
                                              </div>
                                            </div>
                                            <div className="col-12 d-flex abajo">
                                              <div className="col-8">
                                                <label>
                                                  {PropiedadIntelectual["Text1"]}
                                                </label>
                                                <input
                                                  name="respuestaText1"
                                                  className="form-control"
                                                  type="text"
                                                  onChange={handleChange}
                                                  value={respuestaText1}
                                                />
                                              </div>
                                            </div>
                                            <div className="col-12 d-flex abajo">
                                              <div className="col-8">
                                                <label>
                                                  {PropiedadIntelectual["SiNo2"]}
                                                </label>
                                                <Select
                                                  name="respuestaSiNo2"
                                                  options={SiNo}
                                                  onChange={(event) =>
                                                    handleChangeSiNo(event, 2)
                                                  }
                                                />
                                              </div>
                                            </div>
                                            <div className="col-12 d-flex abajo">
                                              <div className="col-8">
                                                <label>
                                                  {PropiedadIntelectual["Text2"]}
                                                </label>
                                                <input
                                                  name="respuestaText2"
                                                  className="form-control"
                                                  type="text"
                                                  onChange={handleChange}
                                                  value={respuestaText2}
                                                />
                                              </div>
                                            </div>
                                          </div>
                                        ) : (
                                            <label>
                                              No hay preguntas específicas para esta
                                              subespecialidad.
                                            </label>
                                          ) // NO HAY PREGUNTAS
                          }
                        </fieldset>
                      </ContenedorFormularioLg>
                    ) : null}
                    {/* V4 */}
                    {tipo === "Cita" ? (
                      <>
                        <div id="ven4"></div>
                        {subEspecialidad !== "" &&
                          provinciaOportunidad !== "" ? (
                            <>
                              {/* Abogados Principal */}
                              {sinSugerencias != true ? (
                                <>
                                  <>
                                    <div className="col-12 d-flex abajo">
                                      <div className="col-8">
                                        <h2>Socios Principal Sugeridos</h2>
                                        <table
                                          className="table table-bordered table-hover"
                                        >
                                          <thead className="thead-dark text-center p-3">
                                            <tr className="m-2">
                                              <th className="align-middle">
                                                Seleccionar
                                          </th>
                                              <th className="align-middle">
                                                Nombre y Apellidos
                                          </th>
                                              <th className="align-middle">
                                                Comentarios
                                          </th>
                                              <th className="align-middle">
                                                Dirección
                                          </th>
                                              <th className="align-middle">
                                                Última Oportunidad
                                          </th>
                                            </tr>
                                          </thead>
                                          {abogadosSugeridosPrincipal.length !== 0 ? (
                                            <tbody>
                                              {abogadosSugeridosPrincipal.map(
                                                (abogado) => {
                                                  return (
                                                    <SeleccionarAbogado
                                                      key={abogado.id}
                                                      abogado={abogado}
                                                      seleccionarRadio={
                                                        seleccionarRadio
                                                      }
                                                    />
                                                  );
                                                }
                                              )}
                                            </tbody>
                                          ) : (
                                              <SinResultados className="text-center">
                                                <td colSpan="8">
                                                  La búsqueda no dió resultados
                                      </td>
                                              </SinResultados>)}
                                        </table>
                                      </div>
                                    </div>
                                  </>


                                  {/* Abogados Secundarios */}

                                  <>
                                    <div className="col-12 d-flex abajo">
                                      <div className="col-8">
                                        <h2>Socios Secundario Sugeridos</h2>
                                        <table
                                          className="table table-bordered table-hover"
                                        >
                                          <thead className="thead-dark text-center p-3">
                                            <tr className="m-2">
                                              <th className="align-middle">
                                                Seleccionar
                                          </th>
                                              <th className="align-middle">
                                                Nombre y Apellidos
                                          </th>
                                              <th className="align-middle">
                                                Comentarios
                                          </th>
                                              <th className="align-middle">
                                                Dirección
                                          </th>
                                              <th className="align-middle">
                                                Última Oportunidad
                                          </th>
                                            </tr>
                                          </thead>
                                          {abogadosSugeridosSecundario.length !== 0 ? (
                                            <tbody>
                                              {abogadosSugeridosSecundario.map(
                                                (abogado) => {
                                                  return (
                                                    <SeleccionarAbogado
                                                      key={abogado.id}
                                                      abogado={abogado}
                                                      seleccionarRadio={
                                                        seleccionarRadio
                                                      }
                                                    />
                                                  );
                                                }
                                              )}
                                            </tbody>
                                          ) : (
                                              <SinResultados className="text-center">
                                                <td colSpan="8">
                                                  La búsqueda no dió resultados
                                      </td>
                                              </SinResultados>
                                            )}
                                        </table>
                                      </div>
                                    </div>
                                  </>


                                  {/* Colaboradores Principales*/}

                                  <>
                                    <div className="col-12 d-flex abajo">
                                      <div className="col-8">
                                        <h2 className="mt-4">
                                          Colaboradores Sugeridos
                                </h2>
                                        <table className="table table-bordered table-hover w-100 ">
                                          <thead className="thead-dark text-center p-3">
                                            <tr className="m-2">
                                              <th className="align-middle">
                                                Seleccionar
                                      </th>
                                              <th className="align-middle">
                                                Nombre y Apellidos
                                      </th>
                                              <th className="align-middle">
                                                Comentarios
                                      </th>
                                              <th className="align-middle">
                                                Dirección
                                      </th>
                                              <th className="align-middle">
                                                Última Oportunidad
                                      </th>
                                            </tr>
                                          </thead>
                                          {colaboradoresSugeridosPrincipal.length !== 0 ? (
                                            <tbody>
                                              {colaboradoresSugeridosPrincipal.map(
                                                (abogado) => {
                                                  return (
                                                    <SeleccionarAbogado
                                                      key={abogado.id}
                                                      abogado={abogado}
                                                      seleccionarRadio={seleccionarRadio}
                                                    />
                                                  );
                                                }
                                              )}
                                            </tbody>
                                          ) : (
                                              <SinResultados className="text-center">
                                                <td colSpan="8">
                                                  La búsqueda no dió resultados
                                    </td>
                                              </SinResultados>
                                            )}
                                        </table>
                                      </div>
                                    </div>
                                  </>


                                  {/* Colaboradores Secundarios*/}

                                  <>
                                    <div className="col-12 d-flex abajo">
                                      <div className="col-8">
                                        <h2 className="mt-4">
                                          Colaboradores Sugeridos
                                </h2>
                                        <table className="table table-bordered table-hover w-100 ">
                                          <thead className="thead-dark text-center p-3">
                                            <tr className="m-2">
                                              <th className="align-middle">
                                                Seleccionar
                                      </th>
                                              <th className="align-middle">
                                                Nombre y Apellidos
                                      </th>
                                              <th className="align-middle">
                                                Comentarios
                                      </th>
                                              <th className="align-middle">
                                                Dirección
                                      </th>
                                              <th className="align-middle">
                                                Última Oportunidad
                                      </th>
                                            </tr>
                                          </thead>
                                          {colaboradoresSugeridosSecundario.length !== 0 ? (
                                            <tbody>
                                              {colaboradoresSugeridosSecundario.map(
                                                (abogado) => {
                                                  return (
                                                    <SeleccionarAbogado
                                                      key={abogado.id}
                                                      abogado={abogado}
                                                      seleccionarRadio={seleccionarRadio}
                                                    />
                                                  );
                                                }
                                              )}
                                            </tbody>
                                          ) : (
                                              <SinResultados className="text-center">
                                                <td colSpan="8">
                                                  La búsqueda no dió resultados
                                    </td>
                                              </SinResultados>
                                            )}
                                        </table>
                                      </div>
                                    </div>
                                  </>
                                </>
                              ) : null}


                              {/* Sin Sugerencias */}
                              {sinSugerencias && (
                                <>
                                  <H2Alerta className="text-center mb-3">
                                    No hay resultados de socios ni colaboradores
                                    en la provincia, elige otra opción:
                                </H2Alerta>

                                  <div>Selecciona</div>

                                  {/* Socios y Colaboradores de la CCAA */}
                                  <div className="d-flex justify-content-between mt-5">
                                    <h2 className="font-weight-bold">
                                      Comunidad Autonoma
                                  </h2>
                                    <div className="d-flex justify-content-between">
                                      <InputTextOportunidades
                                        type="text"
                                        value={qComunidad}
                                        onChange={(e) =>
                                          guardarQComunidad(e.target.value)
                                        }
                                        placeholder="Introduce el nombre del abogado..."
                                      // className="mt-2"
                                      />
                                      <InputSubmitSugerencias disabled>
                                        Buscar
                                    </InputSubmitSugerencias>
                                    </div>
                                  </div>

                                  <br />

                                  {/* <div className="d-flex mt-4 mb-4"> */}

                                  <div className="col-4">
                                    <label htmlFor="provincia">
                                      Cambiar Provincia:
                                  </label>
                                    <Select
                                      onChange={handleChangeProvincia}
                                      options={provincias}
                                      name="provincia"
                                      placeholder="Selecciona Provincia"
                                    />
                                    {errores.provincia && (
                                      <p className="alert alert-danger text-center p-2 mt-2">
                                        {errores.provincia}
                                      </p>
                                    )}
                                  </div>

                                  <br />

                                  <h2>Socios Principal</h2>
                                  <table className="table table-bordered table-hover w-100">
                                    <thead className="thead-dark text-center p-3">
                                      <tr className="m-2">
                                        <th className="align-middle">
                                          Seleccionar
                                      </th>
                                        <th className="align-middle">
                                          Nombre y Apellidos
                                      </th>
                                        <th className="align-middle">
                                          Comentarios
                                      </th>
                                        <th className="align-middle">
                                          Dirección
                                      </th>
                                        <th className="align-middle">
                                          Última Oportunidad
                                      </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      {sociosComunidadPrincipal.length === 0 ? (
                                        <SinResultados className="text-center">
                                          <td colSpan="8">
                                            La búsqueda no dió resultados
                                        </td>
                                        </SinResultados>
                                      ) : (
                                          sociosComunidadPrincipal.map((abogado) => {
                                            return (
                                              <SeleccionarAbogado
                                                key={abogado.id}
                                                abogado={abogado}
                                                seleccionarRadio={seleccionarRadio}
                                              />
                                            );
                                          })
                                        )}
                                    </tbody>
                                  </table>

                                  <h2>Socios Secundario</h2>
                                  <table className="table table-bordered table-hover w-100">
                                    <thead className="thead-dark text-center p-3">
                                      <tr className="m-2">
                                        <th className="align-middle">
                                          Seleccionar
                                      </th>
                                        <th className="align-middle">
                                          Nombre y Apellidos
                                      </th>
                                        <th className="align-middle">
                                          Comentarios
                                      </th>
                                        <th className="align-middle">
                                          Dirección
                                      </th>
                                        <th className="align-middle">
                                          Última Oportunidad
                                      </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      {sociosComunidadSecundario.length === 0 ? (
                                        <SinResultados className="text-center">
                                          <td colSpan="8">
                                            La búsqueda no dió resultados
                                        </td>
                                        </SinResultados>
                                      ) : (
                                          sociosComunidadSecundario.map((abogado) => {
                                            return (
                                              <SeleccionarAbogado
                                                key={abogado.id}
                                                abogado={abogado}
                                                seleccionarRadio={seleccionarRadio}
                                              />
                                            );
                                          })
                                        )}
                                    </tbody>
                                  </table>

                                  <h2>Colaboradores Principal</h2>
                                  <table className="table table-bordered table-hover w-100">
                                    <thead className="thead-dark text-center p-3">
                                      <tr className="m-2">
                                        <th className="align-middle">
                                          Seleccionar
                                      </th>
                                        <th className="align-middle">
                                          Nombre y Apellidos
                                      </th>
                                        <th className="align-middle">
                                          Comentarios
                                      </th>
                                        <th className="align-middle">
                                          Dirección
                                      </th>
                                        <th className="align-middle">
                                          Última Oportunidad
                                      </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      {colaboradoresComunidadPrincipal.length === 0 ? (
                                        <SinResultados className="text-center">
                                          <td colSpan="8">
                                            La búsqueda no dió resultados
                                        </td>
                                        </SinResultados>
                                      ) : (
                                          colaboradoresComunidadPrincipal.map((abogado) => {
                                            return (
                                              <SeleccionarAbogado
                                                key={abogado.id}
                                                abogado={abogado}
                                                seleccionarRadio={seleccionarRadio}
                                                abogadoAsignado={
                                                  listaAbogadosSeleccionados
                                                }
                                              />
                                            );
                                          })
                                        )}
                                    </tbody>
                                  </table>

                                  <h2>Colaboradores Secundario</h2>
                                  <table className="table table-bordered table-hover w-100">
                                    <thead className="thead-dark text-center p-3">
                                      <tr className="m-2">
                                        <th className="align-middle">
                                          Seleccionar
                                      </th>
                                        <th className="align-middle">
                                          Nombre y Apellidos
                                      </th>
                                        <th className="align-middle">
                                          Comentarios
                                      </th>
                                        <th className="align-middle">
                                          Dirección
                                      </th>
                                        <th className="align-middle">
                                          Última Oportunidad
                                      </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      {colaboradoresComunidadSecundario.length === 0 ? (
                                        <SinResultados className="text-center">
                                          <td colSpan="8">
                                            La búsqueda no dió resultados
                                        </td>
                                        </SinResultados>
                                      ) : (
                                          colaboradoresComunidadSecundario.map((abogado) => {
                                            return (
                                              <SeleccionarAbogado
                                                key={abogado.id}
                                                abogado={abogado}
                                                seleccionarRadio={seleccionarRadio}
                                                abogadoAsignado={
                                                  listaAbogadosSeleccionados
                                                }
                                              />
                                            );
                                          })
                                        )}
                                    </tbody>
                                  </table>

                                  {abogadoAsignado === ""
                                    ? (estado = "Pendiente Asignar")
                                    : null}
                                </>
                              )}

                              {/* Errores */}
                              {errores.abogadoAsignado ? (
                                <p className="alert alert-danger text-center p-2 mt-2">
                                  {errores.abogadoAsignado}
                                </p>
                              ) : null}

                              {error && (
                                <p className="alert alert-danger text-center p2 mt-2 mb-2">
                                  {error}
                                </p>
                              )}
                            </>
                          ) : null}
                      </>
                    ) : (
                        <>
                          <div id="ven4"></div>
                          {subEspecialidad !== "" &&
                            provinciaOportunidad !== "" ? (
                              <>
                                {/* Abogados Principal */}
                                {sinSugerencias != true ? (
                                  <>
                                    <>
                                      <div className="col-12 d-flex abajo">
                                        <div className="col-8">
                                          <h2>Socios Principal Sugeridos</h2>
                                          <table
                                            className="table table-bordered table-hover"
                                          >
                                            <thead className="thead-dark text-center p-3">
                                              <tr className="m-2">
                                                <th className="align-middle">
                                                  Seleccionar
                                          </th>
                                                <th className="align-middle">
                                                  Nombre y Apellidos
                                          </th>
                                                <th className="align-middle">
                                                  Comentarios
                                          </th>
                                                <th className="align-middle">
                                                  Dirección
                                          </th>
                                                <th className="align-middle">
                                                  Última Oportunidad
                                          </th>
                                              </tr>
                                            </thead>
                                            {abogadosSugeridosPrincipal.length !== 0 ? (
                                              <tbody>
                                                {abogadosSugeridosPrincipal.map(
                                                  (abogado) => {
                                                    return (
                                                      <SeleccionarAbogadoPresupuesto
                                                        key={abogado.id}
                                                        abogado={abogado}
                                                        abogadoAsignado={
                                                          abogadosSeleccionados
                                                        }
                                                        seleccionarCheckBox={
                                                          seleccionarCheckBox
                                                        }
                                                      />
                                                    );
                                                  }
                                                )}
                                              </tbody>
                                            ) : (
                                                <SinResultados className="text-center">
                                                  <td colSpan="8">
                                                    La búsqueda no dió resultados
                                      </td>
                                                </SinResultados>)}
                                          </table>
                                        </div>
                                      </div>
                                    </>


                                    {/* Abogados Secundarios */}

                                    <>
                                      <div className="col-12 d-flex abajo">
                                        <div className="col-8">
                                          <h2>Socios Secundario Sugeridos</h2>
                                          <table
                                            className="table table-bordered table-hover"
                                          >
                                            <thead className="thead-dark text-center p-3">
                                              <tr className="m-2">
                                                <th className="align-middle">
                                                  Seleccionar
                                          </th>
                                                <th className="align-middle">
                                                  Nombre y Apellidos
                                          </th>
                                                <th className="align-middle">
                                                  Comentarios
                                          </th>
                                                <th className="align-middle">
                                                  Dirección
                                          </th>
                                                <th className="align-middle">
                                                  Última Oportunidad
                                          </th>
                                              </tr>
                                            </thead>
                                            {abogadosSugeridosSecundario.length !== 0 ? (
                                              <tbody>
                                                {abogadosSugeridosSecundario.map(
                                                  (abogado) => {
                                                    return (
                                                      <SeleccionarAbogadoPresupuesto
                                                        key={abogado.id}
                                                        abogado={abogado}
                                                        abogadoAsignado={
                                                          abogadoSeleccionado
                                                        }
                                                        seleccionarCheckBox={
                                                          seleccionarCheckBox
                                                        }
                                                      />
                                                    );
                                                  }
                                                )}
                                              </tbody>
                                            ) : (
                                                <SinResultados className="text-center">
                                                  <td colSpan="8">
                                                    La búsqueda no dió resultados
                                      </td>
                                                </SinResultados>
                                              )}
                                          </table>
                                        </div>
                                      </div>
                                    </>


                                    {/* Colaboradores Principales*/}

                                    <>
                                      <div className="col-12 d-flex abajo">
                                        <div className="col-8">
                                          <h2 className="mt-4">
                                            Colaboradores Sugeridos
                                </h2>
                                          <table className="table table-bordered table-hover w-100 ">
                                            <thead className="thead-dark text-center p-3">
                                              <tr className="m-2">
                                                <th className="align-middle">
                                                  Seleccionar
                                      </th>
                                                <th className="align-middle">
                                                  Nombre y Apellidos
                                      </th>
                                                <th className="align-middle">
                                                  Comentarios
                                      </th>
                                                <th className="align-middle">
                                                  Dirección
                                      </th>
                                                <th className="align-middle">
                                                  Última Oportunidad
                                      </th>
                                              </tr>
                                            </thead>
                                            {colaboradoresSugeridosPrincipal.length !== 0 ? (
                                              <tbody>
                                                {colaboradoresSugeridosPrincipal.map(
                                                  (abogado) => {
                                                    return (
                                                      <SeleccionarAbogadoPresupuesto
                                                        key={abogado.id}
                                                        abogado={abogado}
                                                        abogadoAsignado={
                                                          abogadosSeleccionados
                                                        }
                                                        seleccionarCheckBox={
                                                          seleccionarCheckBox
                                                        }
                                                      />
                                                    );
                                                  }
                                                )}
                                              </tbody>
                                            ) : (
                                                <SinResultados className="text-center">
                                                  <td colSpan="8">
                                                    La búsqueda no dió resultados
                                    </td>
                                                </SinResultados>
                                              )}
                                          </table>
                                        </div>
                                      </div>
                                    </>


                                    {/* Colaboradores Secundarios*/}

                                    <>
                                      <div className="col-12 d-flex abajo">
                                        <div className="col-8">
                                          <h2 className="mt-4">
                                            Colaboradores Sugeridos
                                </h2>
                                          <table className="table table-bordered table-hover w-100 ">
                                            <thead className="thead-dark text-center p-3">
                                              <tr className="m-2">
                                                <th className="align-middle">
                                                  Seleccionar
                                      </th>
                                                <th className="align-middle">
                                                  Nombre y Apellidos
                                      </th>
                                                <th className="align-middle">
                                                  Comentarios
                                      </th>
                                                <th className="align-middle">
                                                  Dirección
                                      </th>
                                                <th className="align-middle">
                                                  Última Oportunidad
                                      </th>
                                              </tr>
                                            </thead>
                                            {colaboradoresSugeridosSecundario.length !== 0 ? (
                                              <tbody>
                                                {colaboradoresSugeridosSecundario.map(
                                                  (abogado) => {
                                                    return (
                                                      <SeleccionarAbogadoPresupuesto
                                                        key={abogado.id}
                                                        abogado={abogado}
                                                        abogadoAsignado={
                                                          abogadosSeleccionados
                                                        }
                                                        seleccionarCheckBox={
                                                          seleccionarCheckBox
                                                        }
                                                      />
                                                    );
                                                  }
                                                )}
                                              </tbody>
                                            ) : (
                                                <SinResultados className="text-center">
                                                  <td colSpan="8">
                                                    La búsqueda no dió resultados
                                    </td>
                                                </SinResultados>
                                              )}
                                          </table>
                                        </div>
                                      </div>
                                    </>
                                  </>
                                ) : null}


                                {/* Sin Sugerencias */}
                                {sinSugerencias && (
                                  <>
                                    <H2Alerta className="text-center mb-3">
                                      No hay resultados de socios ni colaboradores
                                      en la provincia, elige otra opción:
                                </H2Alerta>

                                    <div>Selecciona</div>

                                    {/* Socios y Colaboradores de la CCAA */}
                                    <div className="d-flex justify-content-between mt-5">
                                      <h2 className="font-weight-bold">
                                        Comunidad Autonoma
                                  </h2>
                                      <div className="d-flex justify-content-between">
                                        <InputTextOportunidades
                                          type="text"
                                          value={qComunidad}
                                          onChange={(e) =>
                                            guardarQComunidad(e.target.value)
                                          }
                                          placeholder="Introduce el nombre del abogado..."
                                        // className="mt-2"
                                        />
                                        <InputSubmitSugerencias disabled>
                                          Buscar
                                    </InputSubmitSugerencias>
                                      </div>
                                    </div>

                                    <br />

                                    {/* <div className="d-flex mt-4 mb-4"> */}

                                    <div className="col-4">
                                      <label htmlFor="provincia">
                                        Cambiar Provincia:
                                  </label>
                                      <Select
                                        onChange={handleChangeProvincia}
                                        options={provincias}
                                        name="provincia"
                                        placeholder="Selecciona Provincia"
                                      />
                                      {errores.provincia && (
                                        <p className="alert alert-danger text-center p-2 mt-2">
                                          {errores.provincia}
                                        </p>
                                      )}
                                    </div>

                                    <br />

                                    <h2>Socios Principal</h2>
                                    <table className="table table-bordered table-hover w-100">
                                      <thead className="thead-dark text-center p-3">
                                        <tr className="m-2">
                                          <th className="align-middle">
                                            Seleccionar
                                      </th>
                                          <th className="align-middle">
                                            Nombre y Apellidos
                                      </th>
                                          <th className="align-middle">
                                            Comentarios
                                      </th>
                                          <th className="align-middle">
                                            Dirección
                                      </th>
                                          <th className="align-middle">
                                            Última Oportunidad
                                      </th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        {sociosComunidadPrincipal.length === 0 ? (
                                          <SinResultados className="text-center">
                                            <td colSpan="8">
                                              La búsqueda no dió resultados
                                        </td>
                                          </SinResultados>
                                        ) : (
                                            sociosComunidadPrincipal.map((abogado) => {
                                              return (
                                                <SeleccionarAbogadoPresupuesto
                                                  key={abogado.id}
                                                  abogado={abogado}
                                                  abogadoAsignado={
                                                    abogadosSeleccionados
                                                  }
                                                  seleccionarCheckBox={
                                                    seleccionarCheckBox
                                                  }
                                                />
                                              );
                                            })
                                          )}
                                      </tbody>
                                    </table>

                                    <h2>Socios Secundario</h2>
                                    <table className="table table-bordered table-hover w-100">
                                      <thead className="thead-dark text-center p-3">
                                        <tr className="m-2">
                                          <th className="align-middle">
                                            Seleccionar
                                      </th>
                                          <th className="align-middle">
                                            Nombre y Apellidos
                                      </th>
                                          <th className="align-middle">
                                            Comentarios
                                      </th>
                                          <th className="align-middle">
                                            Dirección
                                      </th>
                                          <th className="align-middle">
                                            Última Oportunidad
                                      </th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        {sociosComunidadSecundario.length === 0 ? (
                                          <SinResultados className="text-center">
                                            <td colSpan="8">
                                              La búsqueda no dió resultados
                                        </td>
                                          </SinResultados>
                                        ) : (
                                            sociosComunidadSecundario.map((abogado) => {
                                              return (
                                                <SeleccionarAbogadoPresupuesto
                                                  key={abogado.id}
                                                  abogado={abogado}
                                                  abogadoAsignado={
                                                    abogadosSeleccionados
                                                  }
                                                  seleccionarCheckBox={
                                                    seleccionarCheckBox
                                                  }
                                                />
                                              );
                                            })
                                          )}
                                      </tbody>
                                    </table>

                                    <h2>Colaboradores Principal</h2>
                                    <table className="table table-bordered table-hover w-100">
                                      <thead className="thead-dark text-center p-3">
                                        <tr className="m-2">
                                          <th className="align-middle">
                                            Seleccionar
                                      </th>
                                          <th className="align-middle">
                                            Nombre y Apellidos
                                      </th>
                                          <th className="align-middle">
                                            Comentarios
                                      </th>
                                          <th className="align-middle">
                                            Dirección
                                      </th>
                                          <th className="align-middle">
                                            Última Oportunidad
                                      </th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        {colaboradoresComunidadPrincipal.length === 0 ? (
                                          <SinResultados className="text-center">
                                            <td colSpan="8">
                                              La búsqueda no dió resultados
                                        </td>
                                          </SinResultados>
                                        ) : (
                                            colaboradoresComunidadPrincipal.map((abogado) => {
                                              return (
                                                <SeleccionarAbogadoPresupuesto
                                                  key={abogado.id}
                                                  abogado={abogado}
                                                  seleccionarCheckBox={
                                                    seleccionarCheckBox
                                                  }
                                                  abogadoAsignado={
                                                    abogadosSeleccionados
                                                  }
                                                />
                                              );
                                            })
                                          )}
                                      </tbody>
                                    </table>

                                    <h2>Colaboradores Secundario</h2>
                                    <table className="table table-bordered table-hover w-100">
                                      <thead className="thead-dark text-center p-3">
                                        <tr className="m-2">
                                          <th className="align-middle">
                                            Seleccionar
                                      </th>
                                          <th className="align-middle">
                                            Nombre y Apellidos
                                      </th>
                                          <th className="align-middle">
                                            Comentarios
                                      </th>
                                          <th className="align-middle">
                                            Dirección
                                      </th>
                                          <th className="align-middle">
                                            Última Oportunidad
                                      </th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        {colaboradoresComunidadSecundario.length === 0 ? (
                                          <SinResultados className="text-center">
                                            <td colSpan="8">
                                              La búsqueda no dió resultados
                                        </td>
                                          </SinResultados>
                                        ) : (
                                            colaboradoresComunidadSecundario.map((abogado) => {
                                              return (
                                                <SeleccionarAbogadoPresupuesto
                                                  key={abogado.id}
                                                  abogado={abogado}
                                                  seleccionarCheckBox={
                                                    seleccionarCheckBox
                                                  }
                                                  abogadoAsignado={
                                                    abogadosSeleccionados
                                                  }
                                                />
                                              );
                                            })
                                          )}
                                      </tbody>
                                    </table>

                                    {abogadoAsignado === ""
                                      ? (estado = "Pendiente Asignar")
                                      : null}
                                  </>
                                )}

                                {/* Errores */}
                                {errores.abogadoAsignado ? (
                                  <p className="alert alert-danger text-center p-2 mt-2">
                                    {errores.abogadoAsignado}
                                  </p>
                                ) : null}

                                {error && (
                                  <p className="alert alert-danger text-center p2 mt-2 mb-2">
                                    {error}
                                  </p>
                                )}
                              </>

                            ) : null}
                        </>

                      )}

                    {/* V5 */}
                    {subEspecialidad !== "" && provinciaOportunidad !== "" ? (
                      <ContenedorFormularioLg id="ven5">
                        <fieldset className="mt-3">
                          <legend className="pl-8">Datos Oportunidad</legend>

                          <div className="col-12 d-flex abajo">
                            <div className="col-8">
                              <label>Datos de la oportunidad</label>
                              <textarea
                                className="form-control"
                                type="text"
                                name="descripcionOportunidad"
                                onChange={handleChange}
                                value={descripcionOportunidad}
                              />
                            </div>
                          </div>

                          <div className="col-12 d-flex abajo">
                            <div className="col-4">
                              <label htmlFor="idOportunidad">
                                ID de Oportunidad
                              </label>
                              <input
                                type="text"
                                className="form-control"
                                name="idOportunidad"
                                disabled
                                value={
                                  contadorDB !== undefined ? idOportunidad : ""
                                }
                              />
                            </div>
                          </div>
                          <fieldset>
                            <div className="col-12">
                              <Label4>Información del canal de entrada</Label4>
                            </div>
                            <div className="col-12 d-flex abajo">
                              <div className="col-5">
                                <label>Canal</label>
                                <Select
                                  name="canal"
                                  options={Canal}
                                  placeholder="Lawyou"
                                  defaultValue={{ label: canales.canal, label: canales.canal }}
                                  onChange={handleChangeCanal}
                                />
                              </div>
                              <div className="col-5">
                                <label>SubCanal</label>
                                <Select
                                  name="subcanal"
                                  options={SubCanal}
                                  defaultValue={{ label: canales.subcanal, label: canales.subcanal }}
                                  placeholder="Lawyou"
                                  onChange={handleChangeSubCanal}
                                />
                              </div>
                            </div>
                          </fieldset>
                          <div className="col-12 d-flex abajo">
                            <div className="col-6 flex">
                              <label>Estado De La Oportunidad</label>
                              <Select
                                name="estado"
                                options={estados}
                                placeholder="Lawyou"
                                defaultValue={{ label: estado, label: estado }}
                                onChange={handleChangeEstado}
                              />
                            </div>
                            {estado === "Rechazada por Abogado" ? (
                              <div className="col-6 flex">
                                <label>Motivo Rechazo</label>
                                <Select
                                  name="motivoRechazo"
                                  options={motivosRechazoAbogado}
                                  placeholder="Motivo del rechazo"
                                  onChange={handleChangeMotivo}
                                />
                              </div>
                            ) : estado === "Rechazada por Cliente" ? (
                              <div className="col-6 flex">
                                <label>Motivo Rechazo</label>
                                <Select
                                  name="motivoRechazo"
                                  options={motivosRechazoCliente}
                                  placeholder="Motivo del rechazo"
                                  onChange={handleChangeMotivo}
                                />
                              </div>
                            ) : null}
                          </div>
                          {motivoRechazo === "Otro" ? (
                            <div className="col-12 d-flex abajo">
                              <div className="col-6 flex">
                                <label htmlFor="explicarMotivo">
                                  ID de Oportunidad
                              </label>
                                <input
                                  type="text"
                                  className="form-control"
                                  name="explicarMotivo"
                                  value={
                                    explicarMotivo
                                  }
                                  onChange={handleChange}
                                />
                              </div>
                            </div>
                          ) : null

                          }
                        </fieldset>
                      </ContenedorFormularioLg>
                    ) : null}

                    {/*GESTION OPORTUNIDAD CITA O PRESUPUESTO VEN6 */}
                    {subEspecialidad !== "" && provinciaOportunidad !== "" ? (
                      <>
                        <div id="ven6"></div>{" "}
                        {/* Div vacio para redireccionar aqui */}
                        {tipo === "Cita" ? (
                          <ContenedorFormularioLg id="ven6">
                            <fieldset className="mt-3">
                              <legend className="pl-5">Gestion</legend>
                              <div className="col-12 d-flex abajo">
                                <div className="col-4">
                                  <label htmlFor="email">
                                    GESTION DE LA CITA
                                  </label>
                                </div>
                              </div>
                              <div className="col-12 d-flex abajo">
                                <div className="col-12">
                                  <label>Notas</label>
                                  <textarea
                                    className="form-control"
                                    type="text"
                                    name="notasGestion"
                                    onChange={handleChange}
                                    value={notasGestion}
                                  />
                                </div>
                              </div>
                              <div className="col-12 d-flex abajo">
                                <div className="col-4">
                                  <label>Fecha Cita</label>
                                  <input
                                    type="date"
                                    className="form-control"
                                    name="fechaCita"
                                    onChange={handleChange}
                                    defaultValue={cita.Fecha}
                                  />
                                </div>
                                <div className="col-4">
                                  <label>Hora Cita</label>
                                  <input
                                    type="time"
                                    className="form-control"
                                    name="horaCita"
                                    onChange={handleChange}
                                    defaultValue={cita.Hora}
                                  />
                                </div>
                              </div>
                              <div className="col-12 d-flex abajo">
                                <div className="col-4">
                                  <label htmlFor="email">Email</label>
                                  <input
                                    type="text"
                                    className="form-control"
                                    name="email"
                                    onChange={handleChange}
                                    value={email}
                                    placeholder="Email del Cliente"
                                  />

                                  {
                                    //errores.email && <p className="alert alert-danger text-center p-2 mt-2">{errores.email}</p>
                                  }
                                </div>
                                {entidadLegal === "Persona Física" ? (
                                  <div className="col-4">
                                    <label>DNI</label>
                                    <input
                                      type="text"
                                      className="form-control"
                                      name="dni"
                                      onChange={handleChange}
                                      value={dni}
                                      placeholder="DNI del Cliente"
                                    />
                                  </div>
                                ) : (
                                    <div className="col-4">
                                      <label>NIF</label>
                                      <input
                                        type="text"
                                        className="form-control"
                                        name="nif"
                                        onChange={handleChange}
                                        value={nif}
                                        placeholder="DNI del Cliente"
                                      />
                                    </div>
                                  )}
                              </div>

                              {entidadLegal === "Persona Jurídica" ? (
                                <div className="col-12 d-flex abajo">
                                  <div className="col-8">
                                    <label>CIF de la Empresa</label>
                                    <input
                                      type="text"
                                      className="form-control"
                                      name="cif"
                                      onChange={handleChange}
                                      value={cif}
                                      placeholder="CIF de la Entidad Social"
                                    />
                                  </div>
                                </div>
                              ) : null}
                              <div className="col-12 d-flex abajo">
                                <div className="col-4">
                                  <label htmlFor="servicio">Servicio *</label>
                                  <Select
                                    name="servicio"
                                    options={Servicio}
                                    placeholder="Servicios"
                                    defaultValue={sDefaultServicio}
                                    onChange={handleChangeServicio}
                                  />
                                </div>
                                <div className="col-4">
                                  <label htmlFor="precio">
                                    Precio del Servicio
                                  </label>
                                  <InputGroup className="mb-6">
                                    <input
                                      type="number"
                                      min="0.01"
                                      step="0.01"
                                      className="col-6"
                                      name="precioServicio"
                                      onChange={handleChange}
                                      placeholder="Precio del servicio"
                                      defaultValue={precioServicio}
                                    />
                                    <InputGroup.Prepend>
                                      <InputGroup.Text>€</InputGroup.Text>
                                    </InputGroup.Prepend>
                                  </InputGroup>
                                </div>
                              </div>
                              <div className="col-12 d-flex abajo">
                                <div className="col-4">
                                  <label>Tipo de Servicio *</label>
                                  <Select
                                    name="tipoServicio"
                                    options={Tipo_Servicio}
                                    placeholder="Tipo de Servicio"
                                    defaultValue={sDefaultTipoServicio}
                                  />
                                </div>
                              </div>
                              <div className="col-12 d-flex abajo">
                                <div className="col-6">
                                  <label htmlFor="precio">
                                    Abogado Asignado
                                  </label>
                                  <input
                                    readOnly
                                    type="text"
                                    className="form-control"
                                    name="abogadoAsignado"
                                    id="abogadoAsignado"
                                    onChange={handleChange}
                                    placeholder="Email del Cliente"
                                    value={abogadoAsignado.nombre}
                                  />
                                </div>
                              </div>
                              <div className="col-12 d-flex mt-2">
                                <div className="col-4">
                                  <label>MandarEmail: </label>
                                  <input
                                    type="checkbox"
                                    defaultChecked={mEmail}
                                    onChange={() => {
                                      setCheckedEmail(!CheckedEmail);
                                    }}
                                  />
                                </div>
                              </div>
                              <div className="col-12 d-flex mt-2">
                                <div className="col-4">
                                  <label>Mandar SMS: </label>
                                  <input
                                    type="checkbox"
                                    defaultChecked={mSms}
                                    onChange={() => {
                                      setCheckedSms(!CheckedSms);
                                    }}
                                  />
                                </div>
                              </div>
                              <div className="col-12 d-flex mt-2">
                                <div className="col-4">
                                  <label>Pasarela: </label>
                                  <input
                                    type="checkbox"
                                    defaultChecked={mPasarela}
                                    onChange={() => {
                                      setCheckedPasarela(!CheckedPasarela);
                                    }}
                                  />
                                </div>
                              </div>
                              <div className="col-12 d-flex mt-2">
                                <div className="col-4">
                                  <label>WhatsApp: </label>
                                  <input
                                    type="checkbox"
                                    defaultChecked={mWhats}
                                    onChange={() => {
                                      setCheckedApp(!CheckedApp);
                                    }}
                                  />
                                </div>
                              </div>
                              <div className="col-12 d-flex mt-2">
                                <div className="col-4">
                                  <label>Tarea de Seguimiento: </label>
                                  <input
                                    type="checkbox"
                                    defaultChecked={mSeguimiento}
                                    onChange={() => {
                                      setCheckedSeguimiento(
                                        !CheckedSeguimiento
                                      );
                                    }}
                                  />
                                </div>
                              </div>
                            </fieldset>
                          </ContenedorFormularioLg>
                        ) : (
                            <ContenedorFormularioLg id="ven6">
                              <fieldset className="mt-3">
                                <legend className="pl-5">Gestion</legend>
                                <div className="col-12 d-flex abajo">
                                  <div className="col-12">
                                    <label htmlFor="email">
                                      GESTION DEL PRESUPUESTO
                                  </label>
                                  </div>
                                </div>
                                <div className="col-12 d-flex abajo">
                                  <div className="col-12">
                                    <label>Notas</label>
                                    <textarea
                                      className="form-control"
                                      type="text"
                                      name="notasGestion"
                                      onChange={handleChange}
                                      value={notasGestion}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex abajo">
                                  <div className="col-8">
                                    <label htmlFor="precio">
                                      Presupuesto Orientativo
                                  </label>
                                    <InputGroup className="mb-6">
                                      <input
                                        type="number"
                                        min="0.01"
                                        step="0.01"
                                        className="col-10"
                                        name="precioPresupuesto"
                                        defaultValue={precioPresupuesto}
                                        onChange={handleChange}
                                        placeholder="Precio del presupuesto"
                                      />
                                      <InputGroup.Prepend>
                                        <InputGroup.Text>€</InputGroup.Text>
                                      </InputGroup.Prepend>
                                    </InputGroup>
                                  </div>
                                </div>

                                {listaAbogadosSeleccionados.length !== 0 ? (
                                  <>
                                    <fieldset>
                                      {" "}
                                      <legend>Abogados Seleccionados</legend>
                                      {listaAbogadosSeleccionados.map(
                                        (e, index) => {
                                          return (
                                            <div className="col-12 d-flex abajo">
                                              <div className="col-6">
                                                <label htmlFor="abogadoSeleccionado">
                                                  Abogado Seleccionado {index + 1}
                                                </label>
                                                <p>
                                                  <strong>
                                                    {e.nombre} {e.apellidos}
                                                  </strong>
                                                </p>
                                              </div>
                                            </div>
                                          );
                                        }
                                      )}
                                    </fieldset>
                                  </>
                                ) : null}

                                <div className="col-12 d-flex abajo">
                                  <div className="col-4">
                                    <label htmlFor="servicio">Servicio *</label>
                                    <Select
                                      name="servicio"
                                      options={Servicio}
                                      placeholder="Servicios"
                                      defaultValue={servicio}
                                      onChange={handleChangeServicio}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex abajo">
                                  <div className="col-4">
                                    <label>Tipo de Servicio *</label>
                                    <Select
                                      name="tipoServicio"
                                      options={Tipo_Servicio}
                                      placeholder="Tipo de Servicio"
                                      defaultValue={tipoServicio}
                                      onChange={handleChangeTipoServicio}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex abajo">
                                  <div className="col-4">
                                    <label htmlFor="email">Email</label>
                                    <input
                                      type="text"
                                      className="form-control"
                                      name="email"
                                      onChange={handleChange}
                                      value={email}
                                      placeholder="Email del Cliente"
                                    />

                                    {
                                      //errores.email && <p className="alert alert-danger text-center p-2 mt-2">{errores.email}</p>
                                    }
                                  </div>
                                  {entidadLegal === "Persona Física" ? (
                                    <div className="col-4">
                                      <label>DNI</label>
                                      <input
                                        type="text"
                                        className="form-control"
                                        name="dni"
                                        onChange={handleChange}
                                        value={dni}
                                        placeholder="DNI del Cliente"
                                      />
                                    </div>
                                  ) : (
                                      <div className="col-4">
                                        <label>nif</label>
                                        <input
                                          type="text"
                                          className="form-control"
                                          name="nif"
                                          onChange={handleChange}
                                          value={nif}
                                          placeholder="DNI del Cliente"
                                        />
                                      </div>
                                    )}
                                </div>

                                {entidadLegal === "Persona Jurídica" ? (
                                  <div className="col-12 d-flex abajo">
                                    <div className="col-8">
                                      <label>CIF de la Empresa</label>
                                      <input
                                        type="text"
                                        className="form-control"
                                        name="cif"
                                        onChange={handleChange}
                                        value={cif}
                                        placeholder="CIF de la Entidad Social"
                                      />
                                    </div>
                                  </div>
                                ) : null}
                                <div className="col-12 d-flex mt-2">
                                  <div className="col-12">
                                    <label>Preguntar Abogado Presupuesto: </label>
                                    <input
                                      type="checkbox"
                                      defaultChecked={mPreguntar}
                                      onChange={() => {
                                        setCheckedPreguntarPresupuesto(
                                          !CheckedPreguntarPresupuesto
                                        );
                                      }}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex mt-2">
                                  <div className="col-12">
                                    <label>
                                      Enviar Email con el Presupuesto:{" "}
                                    </label>
                                    <input
                                      type="checkbox"
                                      defaultChecked={mEmailPresupuesto}
                                      onChange={() => {
                                        setCheckedEmailPresupuesto(
                                          !CheckedEmailPresupuesto
                                        );
                                      }}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex mt-2">
                                  <div className="col-12">
                                    <label>
                                      Llamar al Cliente con el Presupuesto:{" "}
                                    </label>
                                    <input
                                      type="checkbox"
                                      defaultChecked={mLlamar}
                                      onChange={() => {
                                        setCheckedLlamar(!CheckedLlamar);
                                      }}
                                    />
                                  </div>
                                </div>
                                <div className="col-12 d-flex mt-2">
                                  <div className="col-12">
                                    <label>Gestionar Cita: </label>
                                    <input
                                      type="checkbox"
                                      defaultChecked={mGestionarCita}
                                      onChange={() => {
                                        setCheckedGestionarCita(
                                          !CheckedGestionarCita
                                        );
                                      }}
                                    />
                                  </div>
                                </div>
                              </fieldset>
                            </ContenedorFormularioLg>
                          )}
                      </>
                    ) : null}

                    {CheckedGestionarCita || mGestionarCita ? (
                      <ContenedorFormularioLg id="ven7">
                        <fieldset className="mt-3">
                          <legend className="pl-5">Gestion de la cita</legend>
                          <div className="col-12 d-flex abajo">
                            <div className="col-12">
                              <label>Notas</label>
                              <textarea
                                className="form-control"
                                type="text"
                                name="notasGestion"
                                onChange={handleChange}
                                value={notasGestion}
                              />
                            </div>
                          </div>
                          <div className="col-12 d-flex abajo">
                            <div className="col-4">
                              <label>Fecha Cita</label>
                              <input
                                type="date"
                                className="form-control"
                                name="fechaCita"
                                onChange={handleChange}
                                defaultValue={cita.Fecha}
                              />
                            </div>
                            <div className="col-4">
                              <label>Hora Cita</label>
                              <input
                                type="time"
                                className="form-control"
                                name="horaCita"
                                onChange={handleChange}
                                defaultValue={cita.Hora}
                              />
                            </div>
                          </div>
                          <div className="col-12 d-flex abajo">
                            <div className="col-4">
                              <label htmlFor="email">Email</label>
                              <input
                                type="text"
                                className="form-control"
                                name="email"
                                onChange={handleChange}
                                value={email}
                                placeholder="Email del Cliente"
                              />

                              {
                                //errores.email && <p className="alert alert-danger text-center p-2 mt-2">{errores.email}</p>
                              }
                            </div>
                            {entidadLegal === "Persona Física" ? (
                              <div className="col-4">
                                <label>DNI</label>
                                <input
                                  type="text"
                                  className="form-control"
                                  name="dni"
                                  onChange={handleChange}
                                  value={dni}
                                  placeholder="DNI del Cliente"
                                />
                              </div>
                            ) : (
                                <div className="col-4">
                                  <label>NIF</label>
                                  <input
                                    type="text"
                                    className="form-control"
                                    name="nif"
                                    onChange={handleChange}
                                    value={nif}
                                    placeholder="DNI del Cliente"
                                  />
                                </div>
                              )}
                          </div>

                          {entidadLegal === "Persona Jurídica" ? (
                            <div className="col-12 d-flex abajo">
                              <div className="col-8">
                                <label>CIF de la Empresa</label>
                                <input
                                  type="text"
                                  className="form-control"
                                  name="cif"
                                  onChange={handleChange}
                                  value={cif}
                                  placeholder="CIF de la Entidad Social"
                                />
                              </div>
                            </div>
                          ) : null}
                          <div className="col-12 d-flex abajo">
                            <div className="col-4">
                              <label htmlFor="servicio">Servicio *</label>
                              <Select
                                name="servicio"
                                options={Servicio}
                                placeholder="Servicios"
                                defaultValue={{ label: servicio, value: servicio }}
                              />
                            </div>
                            <div className="col-4">
                              <label htmlFor="precio">
                                Precio del Servicio
                              </label>
                              <InputGroup className="mb-6">
                                <input
                                  type="number"
                                  min="0.01"
                                  step="0.01"
                                  className="col-6"
                                  name="precioServicio"
                                  onChange={handleChange}
                                  placeholder="Precio del servicio"
                                  defaultValue={precioServicio}
                                />
                                <InputGroup.Prepend>
                                  <InputGroup.Text>€</InputGroup.Text>
                                </InputGroup.Prepend>
                              </InputGroup>
                            </div>
                          </div>
                          <div className="col-12 d-flex abajo">
                            <div className="col-4">
                              <label>Tipo de Servicio *</label>
                              <Select
                                name="tipoServicio"
                                options={Tipo_Servicio}
                                placeholder="Tipo de Servicio"
                                onChange={handleChangeTipoServicio}
                                defaultValue={{ label: tipoServicio, value: tipoServicio }}
                              />
                            </div>
                          </div>
                          {abogadosSeleccionados.length > 0 ? (
                            <>
                              <h2 className="mt-4">
                                SELECCIONA ABOGADO
                                </h2>
                              <table className="table table-bordered table-hover w-100 ">
                                <thead className="thead-dark text-center p-3">
                                  <tr className="m-2">
                                    <th className="align-middle">
                                      Seleccionar
                                      </th>
                                    <th className="align-middle">
                                      Nombre y Apellidos
                                      </th>
                                  </tr>
                                </thead>

                                <tbody>
                                  {abogadosSeleccionados.map(
                                    (abogado) => {                                     
                                     return (
                                      <SeleccionarAbogadoCita
                                        key={abogado.id}
                                        abogado={abogado}
                                        seleccionarRadio={
                                          seleccionarRadio
                                        }
                                      />
                                    );

                                    }
                                  )}
                                </tbody>
                              </table>
                            </>
                          ) : null}
                          <div className="col-12 d-flex mt-2">
                            <div className="col-4">
                              <label>MandarEmail: </label>
                              <input
                                type="checkbox"
                                defaultChecked={mEmail}
                                onChange={() => {
                                  setCheckedEmail(!CheckedEmail);
                                }}
                              />
                            </div>
                          </div>
                          <div className="col-12 d-flex mt-2">
                            <div className="col-4">
                              <label>Mandar SMS: </label>
                              <input
                                type="checkbox"
                                defaultChecked={mSms}
                                onChange={() => {
                                  setCheckedSms(!CheckedSms);
                                }}
                              />
                            </div>
                          </div>
                          <div className="col-12 d-flex mt-2">
                            <div className="col-4">
                              <label>Pasarela: </label>
                              <input
                                type="checkbox"
                                defaultChecked={mPasarela}
                                onChange={() => {
                                  setCheckedPasarela(!CheckedPasarela);
                                }}
                              />
                            </div>
                          </div>
                          <div className="col-12 d-flex mt-2">
                            <div className="col-4">
                              <label>WhatsApp: </label>
                              <input
                                type="checkbox"
                                defaultChecked={mWhats}
                                onChange={() => {
                                  setCheckedApp(!CheckedApp);
                                }}
                              />
                            </div>
                          </div>
                          <div className="col-12 d-flex mt-2">
                            <div className="col-4">
                              <label>Tarea de Seguimiento: </label>
                              <input
                                type="checkbox"
                                defaultChecked={mSeguimiento}
                                onChange={() => {
                                  setCheckedSeguimiento(!CheckedSeguimiento);
                                }}
                              />
                            </div>
                          </div>
                        </fieldset>
                      </ContenedorFormularioLg>
                    ) : null}

                    {Checked == true ? (
                      <>
                        {/* Boton Crear Llamada contestada*/}
                        <div className="col-12 d-flex justify-content-center">
                          <BotonAceptar
                            className="btn btn-primary"
                            type="button"
                            onClick={() => editarOportunidad()}
                          >
                            Actualizar Oportunidad
                          </BotonAceptar>
                        </div>
                      </>
                    ) : (
                        <>
                          {/* Boton Crear Llamada contestada*/}
                          <div className="col-12 d-flex justify-content-center">
                            <BotonAceptar
                              className="btn btn-primary"
                              type="submit"
                            >
                              Actualizar Oportunidad
                          </BotonAceptar>
                          </div>
                        </>
                      )}
                  </DivScroll>
                </Divp2>
              </form>
            </>
          ) : (
                  <>
                    <PaginaError
                      msg={`Tienes que estar logueado para acceder a esta sección.`}
                    ></PaginaError>
                    <div className="text-center">
                      <Link href="/iniciar-sesion">
                        <a>
                          <h1>Inicia Sesión</h1>
                        </a>
                      </Link>
                    </div>
                  </>
                )}
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}
export default MyVerticallyCenteredModal;

