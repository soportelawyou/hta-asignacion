import React, { useContext, useState, useEffect } from 'react';
import Layout from '../components/layout/Layout';
import Link from 'next/link';
import ReactTooltip from 'react-tooltip';
import Footer from '../components/layout/Footer'


import PaginaError from '../components/layout/PaginaError';
import { Boton, Boton2 } from '../components/ui/Boton';
import DetallesAbogado from '../components/layout/DetallesAbogado';
import useAbogados from '../hooks/useAbogados';
import { SinResultados } from '../components/ui/SinResultados';
import { InputText, InputSubmit } from '../components/ui/Busqueda';
import ordenarTabla from '../components/helpers/ordenarTabla'; 
import { MyVerticallyCenteredModal } from './nuevo-abogado-modal'
import { MyVerticallyCenteredModal2 } from './abogadoImportar'
import { FirebaseContext } from '../firebase/index';
import { Divp5, Filtro } from '../components/ui/Visual';



const Abogados = () => {

  //

  // const [columnaSeleccionada, guardarColumnaSeleccionada] = useState('nombre');
  const [abogadosOrdenados, guardarAbogadosOrdenados] = useState([]);
  const [permiso, guardarPermiso] = useState(false);
  const [cargando, guardarCargando] = useState(false);
  const [modalShow, setModalShow] = React.useState(false);
  const [modalShow2, setModalShow2] = React.useState(false);
  const [mostrarFiltro, guardarMostrarFiltro] = useState(false);

  const { usuario, firebase } = useContext(FirebaseContext);
  const { abogados } = useAbogados('nombre');

  ////////////////

  // buscar usuario
  const busqueda = async (email) => {
    await firebase.db.collection('usuarios').where("email", "==", email).onSnapshot(manejarSnapShot);
  }

  function manejarSnapShot(snapshot) {
    const usuarioBD = snapshot.docs.map(doc => {
      return {
        id: doc.id,
        ...doc.data()
      }
    })
    if (usuarioBD[0] !== undefined) {
      // console.log("USUARIO DB", usuarioBD[0])
      if (usuarioBD[0].admitido) {
        guardarPermiso(true);
      } else {
        guardarPermiso(false)
      }
    }
    guardarCargando(false);
  }

  useEffect(() => {
    guardarCargando(true);
    if (usuario !== undefined && usuario !== null) {
      busqueda(usuario.email)
    }
  }, [usuario])

  ////////////
  // FILTRADO
  const [q, guardarQ] = useState('');
  const [w, guardarW] = useState('');
  const [e, guardarE] = useState('');
  const [resultados, guardarResultados] = useState([])


  useEffect(() => {
    if (abogados) {
      const busqueda = q.toLowerCase();
      const busquedaSinTildes = quitarTildes(busqueda);
      const busqueda2 = w.toLowerCase();
      const busquedaSinTildes2 = quitarTildes(busqueda2);
      const busqueda3 = e.toLowerCase();
      const busquedaSinTildes3 = quitarTildes(busqueda3);
      if (abogadosOrdenados.length !== 0) {
        const abogadosFiltrados = abogadosOrdenados.filter(abogado => {
          return (
            `${quitarTildes(abogado.nombre.toLowerCase())} ${quitarTildes(abogado.apellidos.toLowerCase())}`.includes(busquedaSinTildes)
            || quitarTildes(abogado.tipo.toLowerCase()).includes(busquedaSinTildes) || (arrayMinusculas(abogado.subEspecialidadesAbogadosPrincipal).some(function (v) { return v.indexOf(busquedaSinTildes) >= 0 }) ? abogado.subEspecialidadesAbogadosPrincipal : null)
          )
        })
        const abogadosFiltrados2 = abogadosFiltrados.filter(abogado => {
          return (
            quitarTildes(abogado.direccion.toLowerCase()).includes(busquedaSinTildes2)
          )
        })
        const abogadosFiltrados3 = abogadosFiltrados2.filter(abogado => {
          return (
            (arrayMinusculas(abogado.idioma).some(function (v) { return v.indexOf(busquedaSinTildes3) >= 0 }) ? abogado.idioma : null)
          )
        })
        guardarResultados(abogadosFiltrados3)
      } else {
        // console.log('TEST', abogados.filter(abogado=> (arrayMinusculas(abogado.especialidadesAbogado).some(function(v){ return v.indexOf(busquedaSinTildes)>=0 }) ? abogado.especialidadesAbogado : null)))
        const abogadosFiltrados = abogados.filter(abogado => {
          return (
            `${quitarTildes(abogado.nombre.toLowerCase())} ${quitarTildes(abogado.apellidos.toLowerCase())}`.includes(busquedaSinTildes)
            || quitarTildes(abogado.tipo.toLowerCase()).includes(busquedaSinTildes)
            || (arrayMinusculas(abogado.subEspecialidadesAbogadosPrincipal).some(function (v) { return v.indexOf(busquedaSinTildes) >= 0 }) ? abogado.subEspecialidadesAbogadosPrincipal : null)

          )
        })
        const abogadosFiltrados2 = abogadosFiltrados.filter(abogado => {
          return (
            quitarTildes(abogado.direccion.toLowerCase()).includes(busquedaSinTildes2)
          )
        })
        const abogadosFiltrados3 = abogadosFiltrados2.filter(abogado => {
          return (
            (arrayMinusculas(abogado.idioma).some(function (v) { return v.indexOf(busquedaSinTildes3) >= 0 }) ? abogado.idioma : null)
          )
        })
        guardarResultados(abogadosFiltrados3)
      }
    }

  }, [w, q, e, abogados, abogadosOrdenados])

  const quitarTildes = (palabra) => {
    const letras = { 'á': 'a', 'é': 'e', 'í': 'i', 'ó': 'o', 'ú': 'u' };
    palabra = palabra.replace(/[áéíóú]/g, m => letras[m]);
    return palabra;
  }

  const arrayMinusculas = (array) => {
    const especialidadesMinusculas = []
    if (array !== undefined) {
      // console.log('array', array)
      for (let i = 0; i < array.length; i++) {
        const especialidad = quitarTildes(array[i])
        const especialidadMinuscula = especialidad.toLowerCase()
        // console.log('especialidadMinuscula', especialidadMinuscula)
        especialidadesMinusculas.push(especialidadMinuscula)
      }
      // console.log('especialidadesMinusculas', especialidadesMinusculas)
      return especialidadesMinusculas;
    }
    else {
      return [];
    }

  }

  const ordenarTabla = (columna) => {
    let abogadosOrdenadosFuncion = [...abogados];
    switch (columna) {
      case 'tipo':
        abogadosOrdenadosFuncion.sort((a, b) => {
          if (a.tipo < b.tipo) {
            return -1;
          }
          if (a.tipo > b.tipo) {
            return 1;
          }
          return 0;
        })
        console.log('Abogados ordenados: ', abogadosOrdenadosFuncion.map(abogado => abogado.tipo))
        guardarAbogadosOrdenados(abogadosOrdenadosFuncion)
        break;
      case 'nombre':
        abogadosOrdenadosFuncion.sort((a, b) => {
          if (a.nombre < b.nombre) {
            return -1;
          }
          if (a.nombre > b.nombre) {
            return 1;
          }
          return 0;
        })
        console.log('Abogados ordenados: ', abogadosOrdenadosFuncion.map(abogado => abogado.nombre))
        guardarAbogadosOrdenados(abogadosOrdenadosFuncion)
        break;
      case 'telefono':
        abogadosOrdenadosFuncion.sort((a, b) => {
          if (a.numeroTelefonoPrincipal < b.numeroTelefonoPrincipal) {
            return -1;
          }
          if (a.numeroTelefonoPrincipal > b.numeroTelefonoPrincipal) {
            return 1;
          }
          return 0;
        })
        console.log('Abogados ordenados: ', abogadosOrdenadosFuncion.map(abogado => abogado.numeroTelefonoPrincipal))
        guardarAbogadosOrdenados(abogadosOrdenadosFuncion)
        break;
      case 'especialidadesAbogado':
        abogadosOrdenadosFuncion.sort((a, b) => {
          if (a.especialidadesAbogado < b.especialidadesAbogado) {
            return -1;
          }
          if (a.especialidadesAbogado > b.especialidadesAbogado) {
            return 1;
          }
          return 0;
        })
        console.log('Abogados ordenados: ', abogadosOrdenadosFuncion.map(abogado => abogado.especialidadesAbogado))
        guardarAbogadosOrdenados(abogadosOrdenadosFuncion)
        break;
        case 'subEspecialidadesAbogados':
          abogadosOrdenadosFuncion.sort((a, b) => {
            if (a.subEspecialidadesAbogados < b.subEspecialidadesAbogados) {
              return -1;
            }
            if (a.subEspecialidadesAbogados > b.subEspecialidadesAbogados) {
              return 1;
            }
            return 0;
          })
          console.log('Abogados ordenados: ', abogadosOrdenadosFuncion.map(abogado => abogado.subEspecialidadesAbogados))
          guardarAbogadosOrdenados(abogadosOrdenadosFuncion)
          break;
      case 'direccion':
        abogadosOrdenadosFuncion.sort((a, b) => {
          if (a.direccion < b.direccion) {
            return -1;
          }
          if (a.direccion > b.direccion) {
            return 1;
          }
          return 0;
        })
        console.log('Abogados ordenados: ', abogadosOrdenadosFuncion.map(abogado => abogado.direccion))
        guardarAbogadosOrdenados(abogadosOrdenadosFuncion)
        break;
      case 'contadorAsuntos':
        abogadosOrdenadosFuncion.sort((a, b) => {
          if (a.contadorAsuntos < b.contadorAsuntos) {
            return -1;
          }
          if (a.contadorAsuntos > b.contadorAsuntos) {
            return 1;
          }
          return 0;
        })
        console.log('Abogados ordenados: ', abogadosOrdenadosFuncion.map(abogado => abogado.contadorAsuntos))
        guardarAbogadosOrdenados(abogadosOrdenadosFuncion)
        break;
      case 'contadorAsuntos':
        abogadosOrdenadosFuncion.sort((a, b) => {
          if (a.contadorAsuntos < b.contadorAsuntos) {
            return -1;
          }
          if (a.contadorAsuntos > b.contadorAsuntos) {
            return 1;
          }
          return 0;
        })
        console.log('Abogados ordenados: ', abogadosOrdenadosFuncion.map(abogado => abogado.contadorAsuntos))
        guardarAbogadosOrdenados(abogadosOrdenadosFuncion)
        break;
      case 'contadorRechazados':
        abogadosOrdenadosFuncion.sort((a, b) => {
          if (a.contadorRechazados < b.contadorRechazados) {
            return -1;
          }
          if (a.contadorRechazados > b.contadorRechazados) {
            return 1;
          }
          return 0;
        })
        console.log('Abogados ordenados: ', abogadosOrdenadosFuncion.map(abogado => abogado.contadorRechazados))
        guardarAbogadosOrdenados(abogadosOrdenadosFuncion)
        break;

      case 'activo':
        abogadosOrdenadosFuncion.sort((a, b) => {
          if (a.activo < b.activo) {
            return -1;
          }
          if (a.activo > b.activo) {
            return 1;
          }
          return 0;
        })
        console.log('Abogados ordenados: ', abogadosOrdenadosFuncion.map(abogado => abogado.activo))
        guardarAbogadosOrdenados(abogadosOrdenadosFuncion)
        break;
      default:
        return abogadosOrdenadosFuncion;
    }
  }


  return (
    <div>
      <Layout>

        {usuario && permiso ? (

          <>
            <h1 className="titulo">Abogados</h1>
            <div className="d-flex justify-content-between align-center col-12">
              <Boton
                onClick={() => setModalShow(true)}
                className="btn btn-primary col-2"
              >Nuevo Abogado
                </Boton>
              <MyVerticallyCenteredModal
                show={modalShow}
                onHide={() => setModalShow(false)}
              />
              <Boton2
                onClick={() => setModalShow2(true)}
                className="btn btn-primary col-2"
              >Importar Abogados
                </Boton2>
                <MyVerticallyCenteredModal2
                show={modalShow2}
                onHide={() => setModalShow2(false)}
              />
            </div>
            <Filtro
              onClick={() => guardarMostrarFiltro(!mostrarFiltro)}
            ></Filtro>
            {mostrarFiltro ?
              (
                <Divp5 className="row">
                  <div className="d-flex mt-4">
                    <InputText
                      type="text"
                      value={q}
                      onChange={e => guardarQ(e.target.value)}
                      placeholder="Busca por Nombre, Tipo o Especialidades..."
                      className="mt-2"
                      data-tip="Busca por Nombre, Tipo o subEspecialidad"
                    />
                    <InputSubmit disabled>Buscar</InputSubmit>
                    <ReactTooltip effect="solid" />
                  </div>
                  <div className="d-flex mt-4">
                    <InputText
                      type="text"
                      value={w}
                      onChange={e => guardarW(e.target.value)}
                      placeholder="Busca por direccion..."
                      className="mt-2"
                      data-tip="Busca por Direccion"
                    />
                    <InputSubmit disabled>Buscar</InputSubmit>
                    <ReactTooltip effect="solid" />
                  </div>

                  <div className="d-flex mt-4">
                    <InputText
                      type="text"
                      value={e}
                      onChange={e => guardarE(e.target.value)}
                      placeholder="Busca por idioma..."
                      className="mt-2"
                      data-tip="Busca por idioma"
                    />
                    <InputSubmit disabled>Buscar</InputSubmit>
                    <ReactTooltip effect="solid" />
                  </div>
                </Divp5>
              ) : null}

            <div classname="row">

              {abogados.length === 0 ? <p>Cargando...</p> : (

                <div className="tablaScroll">
                  <table className="table table-bordered table-hover w-100">

                    <thead className="thead-dark text-center p-3">
                      <tr className="m-2">
                        <th className="align-middle cabecera" onClick={() => ordenarTabla('nombre')}>Nombre y Apellidos</th>
                        <th className="align-middle cabecera" onClick={() => ordenarTabla('tipo')}>Tipo</th>
                        <th className="align-middle cabecera" onClick={() => ordenarTabla('especialidadesAbogado')}>Especialidades</th>
                        <th className="align-middle cabecera" onClick={() => ordenarTabla('subEspecialidadesAbogados')}>SubEspecialidades</th>
                        <th className="align-middle cabecera" onClick={() => ordenarTabla('telefono')}>Telefono</th>
                        <th className="align-middle cabecera" onClick={() => ordenarTabla('direccion')}>Dirección</th>
                        <th className="align-middle cabecera" onClick={() => ordenarTabla('contadorAsuntos')}>Asuntos Llevados</th>
                        <th className="align-middle cabecera" onClick={() => ordenarTabla('contadorAceptados')}>Asuntos Aceptados</th>
                        <th className="align-middle cabecera" onClick={() => ordenarTabla('contadorRechazados')}>Asuntos Rechazados</th>
                        <th className="align-middle cabecera" onClick={() => ordenarTabla('activo')}>Activos</th>
                        <th className="align-middle cabecera" onClick={() => ordenarTabla('idioma')}>Idioma</th>
                      </tr>
                    </thead>

                    <tbody>
                      {/* {Object.keys(resultados).length === 0 ? <tr className="text-center font-weight-bold" ><td colSpan="6">Cargando...</td></tr> : ( */}

                      {resultados.length === 0 ? <SinResultados className="text-center" ><td colSpan="6">La búsqueda no dió resultados</td></SinResultados> : (

                        resultados.map(abogado => {

                          return (
                            <DetallesAbogado
                              key={abogado.id}
                              abogado={abogado}

                            />
                          )
                        })

                      )
                      }


                      {/* {abogados.length === 0 ? <tr><th>No hay abogados</th></tr> : (

                  
                    abogados.map(abogado => {

                      return (
                        <Abogado
                          key={abogado.id}
                          abogado={abogado}

                        />
                      )
                    })
                  
                )} */}

                    </tbody>

                  </table>
                  <Footer />
                </div>

              )}
            </div>
          </>
        ) : !permiso && usuario && cargando ? (
          <p>Cargando...</p>
        )
            : !permiso && usuario && !cargando ? (
              <PaginaError msg={`No puedes pasar.`}></PaginaError>
            )
              : !permiso && !usuario ? (
                <>
                  <PaginaError msg={`Tienes que estar logueado.`}></PaginaError>
                  <div className="text-center">
                    <Link href="/iniciar-sesion"><a><h1>Inicia Sesión</h1></a></Link>
                  </div>
                </>

              ) : (
                  <>
                    <PaginaError msg={`Tienes que estar logueado.`}></PaginaError>
                    <div className="text-center">
                      <Link href="/iniciar-sesion"><a><h1>Inicia Sesión</h1></a></Link>
                    </div>
                  </>
                )}
      </Layout>
    </div>
  )
}

export default Abogados;
