import React, { useContext, useState, useEffect } from 'react';
import Link from 'next/link';
import Select from 'react-select';
import { useRouter } from 'next/router';
import Swal from 'sweetalert2';

import PaginaError from '../components/layout/PaginaError';
import Layout from '../components/layout/Layout';
import { ContenedorFormularioLg, BotonCancelar, BotonAceptar } from '../components/ui/Formulario';
import { BotonEliminar } from '../components/ui/BotonEliminar';
import {
    provincias,
    tipoterceros,
    comunidades,
    actividad,
    idiomas
} from '../components/ui/OpcionesSelectores';

// Validaciones
import useValidacionEditar from '../hooks/useValidacionEditar';
import validarEditarTerceros from '../validacion/validarEditarTerceros';
import { Modal, Button } from 'react-bootstrap';
import { FirebaseContext } from '../firebase/index';



export function MyVerticallyCenteredModal(props) {

    // State del componente
    const [terceros, guardarTerceros] = useState({});
    const [error, guardarError] = useState(false);
    const [consultarDB, guardarConsultarDB] = useState(true)
    const [rol, guardarRol] = useState("");
    const [cargando, guardarCargando] = useState(false);
    // Routing para obtener el ID actual
    const router = useRouter();
    var id = props.id;

    // Context de Firebase
    const { firebase, usuario } = useContext(FirebaseContext);

    useEffect(() => {
        // Declaramos variable para trackear
        let estaCancelado = false;
        // console.log('useEffect [ID].js')

        if (id && consultarDB) {
            const obtenerTerceros = async () => {
                const tercerosQuery = await firebase.db.collection('terceros').doc(id);
                const terceros = await tercerosQuery.get();

                // Se ejecuta solo si esta montado
                if (!estaCancelado) {
                    if (terceros.exists) {
                        guardarTerceros(terceros.data());
                    } else {
                        guardarError(true);
                    }
                    guardarConsultarDB(false);
                }

            }
            obtenerTerceros();
        }
        return () => {
            // Ponemos la variable a true para evitar que intente actualizar un state inexistente
            estaCancelado = true;
        }
    }, [id, terceros])

    const busqueda = async (email) => {
        await firebase.db.collection('usuarios').where("email", "==", email).onSnapshot(manejarSnapShot);
    }

    function manejarSnapShot(snapshot) {
        const usuarioBD = snapshot.docs.map(doc => {
            return {
                id: doc.id,
                ...doc.data()
            }
        })
        if (usuarioBD[0] !== undefined) {
            // console.log("USUARIO DB", usuarioBD[0])
            console.log("entre en el guardar usuarios");
            console.log(usuarioBD)
            guardarRol(usuarioBD[0].rol);
        }
        guardarCargando(false)
    }

    useEffect(() => {
        guardarCargando(true)
        if (usuario !== undefined && usuario !== null) {
            console.log(usuario.email)
            busqueda(usuario.email)
        }
    }, [usuario])

    // if (Object.keys(producto).length === 0) return <p>Cargando...</p>

    const dniOriginal = terceros.dni;

    const { valores, errores, handleSubmit, handleChange, handleChangeTipo, handleChangeProvincia, handleChangeComunidad, handleChangeActividad,handleChangeIdioma } = useValidacionEditar(terceros, dniOriginal, validarEditarTerceros, editarTerceros);


    // const { tipo, nombre, apellidos, idLawyou, dni, direccion, calle, provincia, ciudad, comunidadAutonoma, cp, creado, creador, contadorAsuntos, contadorRechazados } = abogado;


    // Destructuring de los valores
    var { tipo, nombre, apellidos, idLawyou, dni, direccion, calle,idioma, provincia, ciudad, comunidadAutonoma, cp, activo, creado, creador, contadorAsuntos, contadorRechazados, ultimaOportunidad, emailTerceros, numeroTelefono } = valores;




    const valorProvincia = provincias.find(prov => prov.value === provincia);
    const valorTipoTerceros = tipoterceros.find(tip => tip.value === tipo);
    const valorComunidad = comunidades.find(comunidad => comunidad.value === comunidadAutonoma);
    const valoractivo = actividad.find(act => act.value === activo);
    // const valorEspecialidades = especialidades.filter(especialidad => especialidad.value === especialidadesAbogado);
    const valorIdioma = [];
    if (idioma !== undefined) {
        // console.log(especialidadesAbogado)
        for (let i = 0; i < idioma.length; i++) {
            valorIdioma.push(idiomas.find(idio => idio.value === idioma[i]))
        }
    }
    else {
        idioma = [];
    }


   

    async function editarTerceros() {
        if (!usuario) {
            return router.push('/iniciar-sesion');
        }

        // Creamos el objeto de abogado
        const terceros = {
            tipo,
            nombre,
            apellidos,
            idLawyou,
            dni,
            calle,
            provincia,
            ciudad,
            emailTerceros,
            numeroTelefono,
            direccion: `${calle}, ${ciudad}, ${provincia}, ${comunidadAutonoma} - ${cp}`,
            comunidadAutonoma,
            cp,
            idioma,
            activo,
            actualizado: Date.now(),
            actualizaUsuario: {
                id: usuario.uid,
                nombre: usuario.displayName
            },
            creado,
            creador,
            contadorAsuntos,
            contadorRechazados,
            ultimaOportunidad
        };

        try {
            if (terceros) {
                // Actualiza en la bbdd
                await firebase.db.collection('terceros').doc(id).update(terceros);
                console.log('Editando Terceros', terceros)
            }
            Swal.fire({
                icon: 'success',
                title: 'El abogado se editó correctamente!',
                showConfirmButton: false,
                timer: 1200
            })

            router.push('/terceros')
            window.location.reload(true);
        } catch (error) {
            console.log(error)
            guardarError(error)
        }


    };

    return (
        <div>

            {error ? <PaginaError msg="Vaya... Parece que ha habido un error al editar el tercero, vuelve a intentarlo por favor"></PaginaError> : Object.keys(valores).length === 0 ? <p>Cargando...</p> : (

                usuario ? (
                    <>
                        <Modal
                            {...props}
                            size="xl"
                            aria-labelledby="contained-modal-title-vcenter"
                            centered
                        >
                            <Modal.Header closeButton>
                                <Modal.Title id="contained-modal-title-vcenter">
                                    <h1 className="titulo">Editar Datos Terceros</h1>
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <form
                                    onSubmit={handleSubmit}
                                >
                                    <ContenedorFormularioLg>
                                        <fieldset>
                                            <legend>Datos Personales *</legend>
                                            <div className="col-12 d-flex mt-2">
                                                <div className="col-4">
                                                    <label htmlFor="tipo">Tipo *</label>
                                                    <Select
                                                        onChange={handleChangeTipo}
                                                        options={tipoterceros}
                                                        name="tipo"
                                                        placeholder="Selecciona Tipo"
                                                        defaultValue={valorTipoTerceros}
                                                    />
                                                </div>
                                                <div className="col-4">
                                                    <label htmlFor="idLawyou">ID Lawyou *</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="idLawyou"
                                                        onChange={handleChange}
                                                        defaultValue={idLawyou}
                                                        placeholder="Ej. A050"
                                                    />
                                                </div>
                                                <div className="col-4">
                                                    <label htmlFor="dni">DNI *</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="dni"
                                                        onChange={handleChange}
                                                        defaultValue={dni}
                                                        placeholder="DNI del Abogado"
                                                    />
                                                    <small>Ej. 12345678A</small>
                                                    {errores.dni && <p className="alert alert-danger text-center p-2 mt-2">{errores.dni}</p>}
                                                    {errores.dniDuplicado && <p className="alert alert-danger text-center p-2 mt-2">{errores.dniDuplicado}</p>}
                                                </div>

                                            </div>

                                            <div className="col-12 d-flex abajo">
                                                <div className="col-4">
                                                    <label htmlFor="nombre">Nombre *</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="nombre"
                                                        onChange={handleChange}
                                                        defaultValue={nombre}
                                                        placeholder="Nombre del abogado"
                                                    />
                                                    {errores.nombre && <p className="alert alert-danger text-center p-2 mt-2">{errores.nombre}</p>}
                                                </div>
                                                <div className="col-4">
                                                    <label htmlFor="apellidos">Apellidos *</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="apellidos"
                                                        onChange={handleChange}
                                                        defaultValue={apellidos}
                                                        placeholder="Apellidos del abogado"
                                                    />
                                                    {errores.apellidos && <p className="alert alert-danger text-center p-2 mt-2">{errores.apellidos}</p>}
                                                </div>

                                            </div>

                                            <div className="col-12 d-flex abajo">
                                                <div className="col-4">
                                                    <label htmlFor="numeroTelefono">Numero Telefono *</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="numeroTelefono"
                                                        onChange={handleChange}
                                                        defaultValue={numeroTelefono}
                                                        placeholder="Numero de Telefono"
                                                    />
                                                    {errores.numeroTelefono && <p className="alert alert-danger text-center p-2 mt-2">{errores.numeroTelefono}</p>}
                                                </div>
                                                <div className="col-4">
                                                    <label htmlFor="idioma"> Idioma *</label>
                                                    <Select
                                                        name="idioma"
                                                        options={idiomas}
                                                        isMulti
                                                        className="basic-multi-select"
                                                        classNamePrefix="select"
                                                        onChange={handleChangeIdioma}
                                                        placeholder="Selecciona Idioma"
                                                        defaultValue={valorIdioma}
                                                    />
                                                    {errores.idioma && <p className="alert alert-danger text-center p-2 mt-2">{errores.idioma}</p>}
                                                </div>
                                            </div>

                                            <div className="col-12 d-flex abajo">
                                                <div className="col-4">
                                                    <label htmlFor="emailTerceros">Email *</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="emailterceros"
                                                        onChange={handleChange}
                                                        defaultValue={emailTerceros}
                                                        placeholder="Email"
                                                    />
                                                    {errores.emailterceros && <p className="alert alert-danger text-center p-2 mt-2">{errores.emailTerceros}</p>}
                                                </div>
                                            </div>
                                        </fieldset>

                                        <fieldset className="mt-5">
                                            <legend>Dirección</legend>
                                            <div className="col-12 d-flex mt-2 justify-content-between">
                                                <div className="col-6">
                                                    <label htmlFor="calle">Calle *</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="calle"
                                                        onChange={handleChange}
                                                        defaultValue={calle}
                                                        placeholder="Ej. Calle de Miramon Nº9, 3-A"
                                                    />
                                                    {errores.calle && <p className="alert alert-danger text-center p-2 mt-2">{errores.calle}</p>}
                                                </div>
                                                <div className="col-6">
                                                    <label htmlFor="ciudad">Ciudad *</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="ciudad"
                                                        onChange={handleChange}
                                                        defaultValue={ciudad}
                                                        placeholder="Ej. Sevilla"
                                                    />
                                                    {errores.ciudad && <p className="alert alert-danger text-center p-2 mt-2">{errores.ciudad}</p>}
                                                </div>
                                            </div>

                                            <div className="col-12 d-flex  abajo">
                                                <div className="col-4">
                                                    <label htmlFor="provincia">Provincia *</label>
                                                    <Select
                                                        onChange={handleChangeProvincia}
                                                        options={provincias}
                                                        name="provincia"
                                                        placeholder="Selecciona Provincia"
                                                        defaultValue={valorProvincia}
                                                    />
                                                </div>
                                                <div className="col-4">
                                                    <label htmlFor="comunidadAutonoma">Comunidad Autónoma *</label>
                                                    <Select
                                                        onChange={handleChangeComunidad}
                                                        options={comunidades}
                                                        name="comunidadAutonoma"
                                                        placeholder="Selecciona Comunidad Autonoma"
                                                        defaultValue={valorComunidad}

                                                    />
                                                </div>
                                                <div className="col-4">
                                                    <label htmlFor="cp">Código Postal *</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="cp"
                                                        onChange={handleChange}
                                                        defaultValue={cp}
                                                        placeholder="Ej. 20532"
                                                    />
                                                    {errores.cp && <p className="alert alert-danger text-center p-2 mt-2">{errores.cp}</p>}
                                                </div>
                                            </div>
                                        </fieldset>

                                        <fieldset className="mt-5">
                                            <legend>Opciones</legend>
                                            <div className="col-12 d-flex mt-2 justify-content-between">
                                                <div className="col-4">
                                                    <label htmlFor="activo">Activo *</label>
                                                    <Select
                                                        onChange={handleChangeActividad}
                                                        options={actividad}
                                                        name="activo"
                                                        defaultValue={valoractivo}
                                                        placeholder="actividad"
                                                    />
                                                </div>
                                            </div>
                                        </fieldset>

                                    </ContenedorFormularioLg>

                                    <div className="col-12 d-flex mt-2 justify-content-between">
                                        <label> * marca los campos obligatorios</label>
                                    </div>

                                    <div className="col-12 d-flex justify-content-center">
                                        {error && <p className="alert alert-danger text-center p2 mt-2 mb-2">{error}</p>}
                                        <BotonAceptar
                                            className="btn btn-primary"
                                            type="submit"
                                        >Guardar Cambios</BotonAceptar>
                                    </div>
                                </form>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button onClick={props.onHide}>Close</Button>
                            </Modal.Footer>
                        </Modal>


                    </>
                ) :
                    <>
                        <PaginaError msg={`Tienes que estar logueado para acceder a esta sección.`}></PaginaError>
                        <div className="text-center">
                            <Link href="/iniciar-sesion"><a><h1>Inicia Sesión</h1></a></Link>
                        </div>
                    </>

            )}


        </div >
    )
}
export default MyVerticallyCenteredModal;
