import React, { useContext, useState, useEffect } from 'react';
import Layout from '../components/layout/Layout';
import Link from 'next/link';
import styled from '@emotion/styled';
import ReactTooltip from 'react-tooltip';
import Footer from '../components/layout/Footer'


import PaginaError from '../components/layout/PaginaError';
import { Boton } from '../components/ui/Boton';
import DetallesTerceros from '../components/layout/DetallesTerceros';
import useTerceros from '../hooks/useTerceros';
import { SinResultados } from '../components/ui/SinResultados';
import { InputText, InputSubmit } from '../components/ui/Busqueda';
import { MyVerticallyCenteredModal } from './nuevo-terceros-modal'
import { FirebaseContext } from '../firebase/index';
import { Divp5,Filtro } from '../components/ui/Visual';



const Terceros = () => {

  const [modalShow, setModalShow] = React.useState(false);
  // const [columnaSeleccionada, guardarColumnaSeleccionada] = useState('nombre');
  const [tercerosOrdenados, guardarTercerosOrdenados] = useState([]);
  const [permiso, guardarPermiso] = useState(false);
  const [cargando, guardarCargando] = useState(false);
  const [mostrarFiltro, guardarMostrarFiltro] = useState(false);

  const { usuario, firebase } = useContext(FirebaseContext);
  const { terceros } = useTerceros('nombre');

  ////////////////

  // buscar usuario
  const busqueda = async (email) => {
    await firebase.db.collection('usuarios').where("email", "==", email).onSnapshot(manejarSnapShot);
  }

  function manejarSnapShot(snapshot) {
    const usuarioBD = snapshot.docs.map(doc => {
      return {
        id: doc.id,
        ...doc.data()
      }
    })
    //Comprobar si elusuario no existe
    if (usuarioBD[0] !== undefined) {
      // console.log("USUARIO DB", usuarioBD[0])
      //Comprobar si elusuario, tiene permisos
      if (usuarioBD[0].admitido) {
        guardarPermiso(true);
      } else {
        guardarPermiso(false)
      }
    }
    guardarCargando(false);
  }

  useEffect(() => {
    guardarCargando(true);
    if (usuario !== undefined && usuario !== null) {
      busqueda(usuario.email)
    }
  }, [usuario])

  ////////////
  // FILTRADO  

  const [q, guardarQ] = useState('');
  const [w, guardarW] = useState('');
  const [e, guardarE] = useState('');
  const [resultados, guardarResultados] = useState([])


  useEffect(() => {
    if (terceros) {
      const busqueda = q.toLowerCase();
      const busquedaSinTildes = quitarTildes(busqueda);
      const busqueda2 = w.toLowerCase();
      const busquedaSinTildes2 = quitarTildes(busqueda2);
      const busqueda3 = e.toLowerCase();
      const busquedaSinTildes3 = quitarTildes(busqueda3);
      if (tercerosOrdenados.length !== 0) {
        const tercerosFiltrados = tercerosOrdenados.filter(terceros => {
          return (
            `${quitarTildes(terceros.nombre.toLowerCase())} ${quitarTildes(terceros.apellidos.toLowerCase())}`.includes(busquedaSinTildes)
            || quitarTildes(terceros.tipo.toLowerCase()).includes(busquedaSinTildes) || quitarTildes(terceros.numeroTelefono).includes(busquedaSinTildes)
          )
        })
        const tercerosFiltrados2 = tercerosFiltrados.filter(terceros => {
          return (
            quitarTildes(terceros.direccion.toLowerCase()).includes(busquedaSinTildes2)

          )
        })

        const tercerosFiltrados3 = tercerosFiltrados2.filter(terceros => {
          return (
            (arrayMinusculas(terceros.idioma).some(function (v) { return v.indexOf(busquedaSinTildes3) >= 0 }) ? terceros.idioma : null)
          )
        })
        //guardarResultados(tercerosFiltrados2)
        guardarResultados(tercerosFiltrados3)
        //}

      } else {
        // console.log('TEST', abogados.filter(abogado=> (arrayMinusculas(abogado.especialidadesAbogado).some(function(v){ return v.indexOf(busquedaSinTildes)>=0 }) ? abogado.especialidadesAbogado : null)))
        const tercerosFiltrados = terceros.filter(terceros => {
          return (
            `${quitarTildes(terceros.nombre.toLowerCase())} ${quitarTildes(terceros.apellidos.toLowerCase())}`.includes(busquedaSinTildes)
            || quitarTildes(terceros.tipo.toLowerCase()).includes(busquedaSinTildes) || quitarTildes(terceros.numeroTelefono).includes(busquedaSinTildes)
          )
        })
        const tercerosFiltrados2 = tercerosFiltrados.filter(terceros => {
          return (
            quitarTildes(terceros.direccion.toLowerCase()).includes(busquedaSinTildes2)
          )
        })
        const tercerosFiltrados3 = tercerosFiltrados2.filter(terceros => {
          return (
            (arrayMinusculas(terceros.idioma).some(function (v) { return v.indexOf(busquedaSinTildes3) >= 0 }) ? terceros.idioma : null)
          )
        })
        //guardarResultados(tercerosFiltrados2)
        guardarResultados(tercerosFiltrados3)
      }
    }

  }, [q, w, e, terceros, tercerosOrdenados])

  const quitarTildes = (palabra) => {
    const letras = { 'á': 'a', 'é': 'e', 'í': 'i', 'ó': 'o', 'ú': 'u' };
    palabra = palabra.replace(/[áéíóú]/g, m => letras[m]);

    return palabra;
  }

  const arrayMinusculas = (array) => {
    const especialidadesMinusculas = []
    if (array !== undefined) {
      // console.log('array', array)
      for (let i = 0; i < array.length; i++) {
        const especialidad = quitarTildes(array[i])
        const especialidadMinuscula = especialidad.toLowerCase()
        // console.log('especialidadMinuscula', especialidadMinuscula)
        especialidadesMinusculas.push(especialidadMinuscula)
      }
      // console.log('especialidadesMinusculas', especialidadesMinusculas)
      return especialidadesMinusculas;
    }
    else {
      return [];
    }

  }

  const ordenarTabla = (columna) => {
    let tercerosOrdenadosFuncion = [...resultados];
    switch (columna) {
      case 'tipo':
        tercerosOrdenadosFuncion.sort((a, b) => {
          if (a.tipo < b.tipo) {
            return -1;
          }
          if (a.tipo > b.tipo) {
            return 1;
          }
          return 0;
        })
        console.log('Terceros ordenados: ', tercerosOrdenadosFuncion.map(terceros => terceros.tipo))
        guardarTercerosOrdenados(tercerosOrdenadosFuncion)
        break;
      case 'dni':
        tercerosOrdenadosFuncion.sort((a, b) => {
          if (a.dni < b.dni) {
            return -1;
          }
          if (a.dni > b.dni) {
            return 1;
          }
          return 0;
        })
        console.log('Terceros ordenados: ', tercerosOrdenadosFuncion.map(terceros => terceros.dni))
        guardarTercerosOrdenados(tercerosOrdenadosFuncion)
        break;
      case 'nombre':
        tercerosOrdenadosFuncion.sort((a, b) => {
          if (a.nombre < b.nombre) {
            return -1;
          }
          if (a.nombre > b.nombre) {
            return 1;
          }
          return 0;
        })
        console.log('Terceros ordenados: ', tercerosOrdenadosFuncion.map(terceros => terceros.nombre))
        guardarTercerosOrdenados(tercerosOrdenadosFuncion)
        break;
      case 'telefono':
        tercerosOrdenadosFuncion.sort((a, b) => {
          if (a.numeroTelefono < b.numeroTelefono) {
            return -1;
          }
          if (a.numeroTelefono > b.numeroTelefono) {
            return 1;
          }
          return 0;
        })
        console.log('Terceros ordenados: ', tercerosOrdenadosFuncion.map(terceros => terceros.numeroTelefono))
        guardarTercerosOrdenados(tercerosOrdenadosFuncion)
        break;
      case 'direccion':
        tercerosOrdenadosFuncion.sort((a, b) => {
          if (a.direccion < b.direccion) {
            return -1;
          }
          if (a.direccion > b.direccion) {
            return 1;
          }
          return 0;
        })
        console.log('Terceros ordenados: ', tercerosOrdenadosFuncion.map(terceros => terceros.direccion))
        guardarTercerosOrdenados(tercerosOrdenadosFuncion)
        break;
      case 'activos':
        tercerosOrdenadosFuncion.sort((a, b) => {
          if (a.activo < b.activo) {
            return -1;
          }
          if (a.activo > b.activo) {
            return 1;
          }
          return 0;
        })
        console.log('Terceros ordenados: ', tercerosOrdenadosFuncion.map(terceros => terceros.activo))
        guardarTercerosOrdenados(tercerosOrdenadosFuncion)
        break;
      default:
        return tercerosOrdenadosFuncion;
    }


  }


  return (
    <div>
      <Layout>

        {usuario && permiso ? (

          <>
            <h1 className="titulo">Terceros</h1>
            <div className="row">
              <Boton className="btn btn-primary col-2" onClick={() => setModalShow(true)}>
                Nuevo Tercero
              </Boton>

              <MyVerticallyCenteredModal
                show={modalShow}
                onHide={() => setModalShow(false)}
              />
            </div>
            <Filtro
              onClick={() => guardarMostrarFiltro(!mostrarFiltro)}
            ></Filtro>
            {mostrarFiltro ?
              (
                <Divp5 className="row">
                  <div className="d-flex mt-4">
                    <InputText
                      type="text"
                      value={q}
                      onChange={e => guardarQ(e.target.value)}
                      placeholder="Busca algo..."
                      className="mt-2"
                      data-tip="Busca por Nombre, Tipo..."
                    />
                    <InputSubmit disabled>Buscar</InputSubmit>
                    <ReactTooltip effect="solid" />
                  </div>
                  <div className="d-flex mt-4">
                    <InputText
                      id='refrescar'
                      type="text"
                      value={w}
                      onChange={e => guardarW(e.target.value)}
                      placeholder="Busca por direccion..."
                      className="mt-2"
                      data-tip="Busca por Direccion"
                    />
                    <InputSubmit disabled>Buscar</InputSubmit>
                    <ReactTooltip effect="solid" />
                  </div>
                  <div className="d-flex mt-4">
                    <InputText
                      type="text"
                      value={e}
                      onChange={e => guardarE(e.target.value)}
                      placeholder="Busca por idioma..."
                      className="mt-2"
                      data-tip="Busca por idioma"
                    />
                    <InputSubmit disabled>Buscar</InputSubmit>
                    <ReactTooltip effect="solid" />
                  </div>
                </Divp5>
              ) : null}
              {/* <div className="d-flex mt-4">
                <InputCheckBox type="checkbox"
                checked={checked}
                onChange={() => botonMostrarActivos(!checked)}
              /> */}
            {/* <CheckBoxLabel>
              Ocultar inactivos
              </CheckBoxLabel>
              </div> */}


            {terceros.length === 0 ? <p>Cargando...</p> : (

              <div className="tablaScroll">
                <table className="table table-bordered table-hover w-100">

                  <thead className="thead-dark text-center p-3">
                    <tr className="m-2">
                      <th className="align-middle cabecera" onClick={() => ordenarTabla('nombre')}>Nombre y Apellidos</th>
                      <th className="align-middle cabecera" onClick={() => ordenarTabla('tipo')}>Tipo</th>
                      <th className="align-middle cabecera" onClick={() => ordenarTabla('dni')}>DNI</th>
                      <th className="align-middle cabecera" onClick={() => ordenarTabla('telefono')}>Telefono</th>
                      <th className="align-middle cabecera" onClick={() => ordenarTabla('direccion')}>Dirección</th>
                      <th className="align-middle cabecera" onClick={() => ordenarTabla('idioma')}>Idioma</th>
                      <th className="align-middle cabecera" onClick={() => ordenarTabla('activos')}>Activo</th>
                    </tr>
                  </thead>

                  <tbody>
                    {/* {Object.keys(resultados).length === 0 ? <tr className="text-center font-weight-bold" ><td colSpan="6">Cargando...</td></tr> : ( */}

                    {resultados.length === 0 ? <SinResultados className="text-center" ><td colSpan="6">La búsqueda no dió resultados</td></SinResultados> : (


                      resultados.map(terceros => {

                        return (
                          <DetallesTerceros
                            key={terceros.id}
                            terceros={terceros}
                          />
                        )


                      })


                    )
                    }

                    {/* {abogados.length === 0 ? <tr><th>No hay abogados</th></tr> : (

                  
                    abogados.map(abogado => {

                      return (
                        <Abogado
                          key={abogado.id}
                          abogado={abogado}

                        />
                      )
                    })
                  
                )} */}

                  </tbody>

                </table>

                <Footer />
              </div>

            )}
          </>
        ) : !permiso && usuario && cargando ? (
          <p>Cargando...</p>
        )
            : !permiso && usuario && !cargando ? (
              <PaginaError msg={`No puedes pasar.`}></PaginaError>
            )
              : !permiso && !usuario ? (
                <>
                  <PaginaError msg={`Tienes que estar logueado.`}></PaginaError>
                  <div className="text-center">
                    <Link href="/iniciar-sesion"><a><h1>Inicia Sesión</h1></a></Link>
                  </div>
                </>

              ) : (
                  <>
                    <PaginaError msg={`Tienes que estar logueado.`}></PaginaError>
                    <div className="text-center">
                      <Link href="/iniciar-sesion"><a><h1>Inicia Sesión</h1></a></Link>
                    </div>
                  </>
                )}


      </Layout>
    </div>
  )
}

export default Terceros;
