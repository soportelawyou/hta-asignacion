import React, { useContext, useEffect, useState } from 'react';

import Layout from '../components/layout/Layout';
import useUsuarios from '../hooks/useUsuarios';
import SeleccionarUsuario from '../components/layout/SeleccionarUsuario';
import PaginaError from '../components/layout/PaginaError';
import Link from 'next/link';
import Footer from '../components/layout/Footer'

import { FirebaseContext } from '../firebase/index';


const Usuarios = () => {

    const [permiso, guardarPermiso] = useState(false);
    const [cargando, guardarCargando] = useState(false);

    const { usuarios } = useUsuarios('nombre');
    const { usuario, firebase } = useContext(FirebaseContext);

    // const{id, rol, email} = usuarios;

    ////
    // buscar usuario
    const busqueda = async (email) => {
        await firebase.db.collection('usuarios').where("email", "==", email).onSnapshot(manejarSnapShot);
    }

    function manejarSnapShot(snapshot) {
        const usuarioBD = snapshot.docs.map(doc => {
            return {
                id: doc.id,
                ...doc.data()
            }
        })
        if (usuarioBD[0] !== undefined) {
            // console.log("USUARIO DB", usuarioBD[0])
            if (usuarioBD[0].admitido) {
                guardarPermiso(true);
            } else {
                guardarPermiso(false)
            }
        }
        guardarCargando(false);
    }

    useEffect(() => {
        guardarCargando(true);
        if (usuario !== undefined && usuario !== null) {
            console.log(usuario.email)
            busqueda(usuario.email)
        }
    }, [usuario])
    ////

    return (
        <>
            <Layout>

                {usuario && permiso ? (
                    usuarios.length !== 0 ?
                        <>
                            <h1 className="titulo">Usuarios</h1>
                            <div className="tablaScroll">
                            <table className="table table-bordered table-hover w-100">

                                <thead className="thead-dark text-center p-3">
                                    <tr className="m-2">
                                        <th className="align-middle">Nombre</th>
                                        <th className="align-middle">Email</th>
                                        <th className="align-middle">Rol</th>
                                        <th className="align-middle">Estado</th>
                                        <th className="align-middle">Acciones</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    {usuarios.map(usuario => {
                                        return (
                                            <SeleccionarUsuario
                                                key={usuario.email}
                                                usuario={usuario}
                                            />
                                        )
                                    })}



                                </tbody>
                            </table>
                            <Footer/>
                            </div>

                        </>

                        : <p>No hay usuarios</p>
                )
                    : !permiso && usuario && cargando ? (
                        <p>Cargando...</p>
                    )
                        : !permiso && usuario && !cargando ? (
                            <PaginaError msg={`No puedes pasar.`}></PaginaError>
                        )
                            : !permiso && !usuario ? (
                                <>
                                    <PaginaError msg={`Tienes que estar logueado.`}></PaginaError>
                                    <div className="text-center">
                                        <Link href="/iniciar-sesion"><a><h1>Inicia Sesión</h1></a></Link>
                                    </div>
                                </>

                            ) : (
                                    <>
                                        <PaginaError msg={`Tienes que estar logueado.`}></PaginaError>
                                        <div className="text-center">
                                            <Link href="/iniciar-sesion"><a><h1>Inicia Sesión</h1></a></Link>
                                        </div>
                                    </>
                                )}

            </Layout>
            
        </>
    );
}

export default Usuarios;