import React, { useState, useContext, useEffect } from 'react';
import Layout from '../components/layout/Layout';
import Router from 'next/router';
import { css } from '@emotion/core';
import { ContenedorFormulario, Error } from '../components/ui/Formulario';
import firebase, {FirebaseContext} from '../firebase/index';


// Validaciones
import useValidacion from '../hooks/useValidacion';
import validarCrearCuenta from '../validacion/validarNuevoUsuario';

const STATE_INICIAL = {
    nombre: '',
    email: '',
    password: '',
    repetirPassword: '',
    rol: 'Estandar'
}

const Registro = () => {

    // const { usuario, firebase } = useContext(FirebaseContext);

    // useEffect(()=>{
    //     if(usuario !== null){
    //         console.log(usuario.uid)
    //     }
    // },[usuario])

    const [error, guardarError] = useState(false);

    const { valores, errores, handleSubmit, handleChange } = useValidacion(STATE_INICIAL, validarCrearCuenta, crearUsuario);

    const { nombre, email, password, repetirPassword } = valores;

    async function crearUsuario() {
        guardarError(false)
        try {
            await firebase.registrar(nombre, email, password);
            console.log('usuario registrado')
            await firebase.login(email, password)
            console.log("UID",firebase.auth.uid)
            await firebase.db.collection('usuarios')
                .add({
                    nombre: valores.nombre,
                    email: valores.email,
                    rol: 'Estandar',
                    admitido: false
                });
            console.log('usuario introducido bbdd')
            Router.push('/');
        } catch (error) {
            if (error.code === "auth/email-already-in-use") {
                console.log(error)
                guardarError('Ocurrió un error con el Email introducido, intenta con otro')
            }else{
                guardarError(error.message)
            }
        }
    }

    return (
        <div>
            <Layout>
                <>
                    <h1
                        css={css`
                            text-align: center;
                        `}
                    >Registrate</h1>
                    <form
                        onSubmit={handleSubmit}
                        noValidate
                    >
                        <ContenedorFormulario >
                            <fieldset>
                                <div className="form-group col-12">
                                    <label htmlFor="nombre">Nombre</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="nombre"
                                        placeholder="Introduce tu Nombre"
                                        name="nombre"
                                        value={nombre}
                                        onChange={handleChange}
                                    />
                                </div>

                                {errores.nombre && (
                                    <div className="form-group col-12">
                                        <label></label>
                                        <p className="alert alert-danger text-center w-100">{errores.nombre}</p>
                                    </div>)}


                                <div className="form-group col-12">
                                    <label htmlFor="email">Email</label>
                                    <input
                                        type="email"
                                        className="form-control"
                                        placeholder="Introduce tu Email"
                                        id="email"
                                        name="email"
                                        value={email}
                                        onChange={handleChange}
                                    />
                                </div>

                                {errores.email && (
                                    <div className="form-group col-12">
                                        <label></label>
                                        <p className="alert alert-danger text-center w-100">{errores.email}</p>
                                    </div>)}

                                <div className="form-group col-12">
                                    <label htmlFor="password">Password</label>
                                    <input
                                        type="password"
                                        className="form-control"
                                        id="password"
                                        name="password"
                                        value={password}
                                        placeholder="Introduce tu Contraseña"
                                        onChange={handleChange}
                                    />
                                </div>

                                {errores.password && (
                                    <div className="form-group col-12">
                                        <label></label>
                                        <p className="alert alert-danger text-center w-100">{errores.password}</p>
                                    </div>)}

                                <div className="form-group col-12">
                                    <label htmlFor="repetirPassword">Repite tu Password</label>
                                    <input
                                        type="password"
                                        className="form-control"
                                        id="repetirPassword"
                                        name="repetirPassword"
                                        value={repetirPassword}
                                        placeholder="Repite tu Contraseña"
                                        onChange={handleChange}
                                    />
                                </div>

                                {errores.repetirPassword && (
                                    <div className="form-group col-12">
                                        <label></label>
                                        <p className="alert alert-danger text-center w-100">{errores.repetirPassword}</p>
                                    </div>)}
                                {error && (
                                    <div className="form-group col-12">
                                        <label></label>
                                        <p className="alert alert-danger text-center w-100">{error}</p>
                                    </div>)}

                                <button
                                    type="submit"
                                    className="btn btn-primary btn-block"
                                >Registrarse</button>
                            </fieldset>

                        </ContenedorFormulario>
                    </form>
                </>
            </Layout>
        </div>
    )
}

export default Registro;
