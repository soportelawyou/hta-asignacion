import React, { useContext, useState, useEffect } from 'react';
import Link from 'next/link';
import Select from 'react-select';
import Router from 'next/router';
import Swal from 'sweetalert2';
import { Modal, Button } from 'react-bootstrap'
import PaginaError from '../components/layout/PaginaError';
import { ContenedorFormularioLg, BotonAceptar } from '../components/ui/Formulario';
import { Radio } from "../components/ui/Boton";
import usePaises from "../hooks/usePaises";
import { Convertir } from "./api/Funciones";
import axios from 'axios'
import {
    tipos,
    comunidadesConProvincias,
    especialidades,
    actividad,
    idiomas,
    Civil,
    Penal,
    Laboral,
    Societario,
    Extranjeria,
    Internacional,
    Nuevas_Tecnologias,
    Mercantil,
    Fiscal_y_Tributario,
    Bancario,
    Constitucional,
    Compliance,
    Comunitario,
    Administrativo
} from '../components/ui/OpcionesSelectores';
import { BotonRemove } from '../components/ui/Boton';

// Validaciones
import useValidacion from '../hooks/useValidacion';
import validarNuevoAbogado from '../validacion/validarNuevoAbogado';
import { BotonMas,imagenInformacion } from '../components/ui/Visual';

import { FirebaseContext } from '../firebase/index';
import firebase from '../firebase/index';

const STATE_INICIAL = {
    tipo: '',
    nombre: '',
    apellidos: '',
    idLawyou: '',
    entidadLegal: "Persona Física",
    dni: '',
    pais: '',
    calle: '',
    provincia: '',
    comunidadAutonoma: '',
    ciudad: '',
    cp: '',
    idioma: [],
    cif: '',
    entidadSocial: '',
    activo: '',
    emailAbogado: '',
    emailAbogadoSecundario: '',
    numeroTelefonoPrincipal: '',
    numeroTelefonoSecundario: '',
    especialidadesAbogadoPrincipal: [],
    subEspecialidadesAbogadosPrincipal: [],
    especialidadesAbogadoSecundario: [],
    subEspecialidadesAbogadosSecundario: [],
    especialidadesAbogadoDeseados: [],
    subEspecialidadesAbogadosDeseados: [],
    dniDuplicado: false,
    contadorAsuntos: 0,
    contadorAceptados: 0,
    contadorRechazados: 0,
    ultimaOportunidad: '',
    comentarios: '',
    lugaresEjercer: []
}

export function MyVerticallyCenteredModal(props) {

    const [inputList, setInputList] = useState([{ Provincia: "", Ciudad: [] }]);

    const [error, guardarError] = useState(false);
    const [grupoSubEspecialidadesPrincipal, guardarGrupoSubEspecialidadesPrincipal] = useState([]);
    const [grupoSubEspecialidadesSecundario, guardarGrupoSubEspecialidadesSecundario] = useState([]);
    const [grupoSubEspecialidadesDeseados, guardarGrupoSubEspecialidadesDeseados] = useState([]);
    const [opcionesProvincias, guardarOpcionesProvincias] = useState([]);
    const [opcionesCiudades, guardarOpcionesCiudades] = useState([]);

    const { valores, errores, handleSubmit, handleChange, handleChangeTipo, handleChangePais, handleChangeProvincia, handleChangeCiudad, handleChangeEspecialidadPrincipal, handleChangeSubEspecialidadPrincipal, handleChangeEspecialidadSecundario, handleChangeSubEspecialidadSecundario, handleChangeEspecialidadDeseados, handleChangeSubEspecialidadDeseados, handleChangeActividad, handleChangeIdioma } = useValidacion(STATE_INICIAL, validarNuevoAbogado, nuevoAbogado);


    // Destructuring de los valores
    const { tipo, nombre, comunidadAutonoma, cif, apellidos, comentarios, idLawyou, dni, calle, entidadSocial, provincia, especialidadesAbogadoPrincipal, especialidadesAbogadoSecundario, especialidadesAbogadoDeseados, subEspecialidadesAbogadosPrincipal, subEspecialidadesAbogadosSecundario, subEspecialidadesAbogadosDeseados, ciudad, pais, cp, activo, contadorAsuntos, contadorAceptados, emailAbogado, emailAbogadoSecundario, numeroTelefonoPrincipal, numeroTelefonoSecundario, contadorRechazados, ultimaOportunidad, idioma } = valores;
    const { Paises, Auth } = usePaises()

    const { usuario } = useContext(FirebaseContext);
    const [entidadLegal, guardarEntidadLegal] = useState("Persona Física");


    // handle input change
    const handleInputChange = (e, index, name) => {

        const list = [...inputList];
        if (name.indexOf("Ciudad") != -1) {
            let resultados = e.map(res => res.value)
            list[index][name] = resultados;

        }
        else {
            axios(`https://www.universal-tutorial.com/api/cities/${e.value}`, {
                "method": 'GET',
                "headers": {
                    "Authorization": `Bearer ${Auth}`,
                    "Accept": "application/json",
                    "Connection": "close"
                },
            })
                .then(response => guardarOpcionesCiudades(Convertir(response.data)))

            list[index][name] = e.value;
        }
        setInputList(list);
        console.log(list)
    };

    // handle click event of the Remove button
    const handleRemoveClick = index => {
        const list = [...inputList];
        list.splice(index, 1);
        setInputList(list);
    };

    // handle click event of the Add button
    const handleAddClick = () => {
        setInputList([...inputList, { Provincia: "", Ciudad: [] }]);
    };


    //Metodo para unir las Subespecialidades
    //Datos entrada: especialidades Abogado
    //Datos Salida: un Array Con los nombres de las subespecialidades
    useEffect(() => {
        var asubespecialidades = []
        if (especialidadesAbogadoPrincipal.length != 0) {
            especialidadesAbogadoPrincipal.map(e => {
                if (e == "Civil") asubespecialidades = asubespecialidades.concat(Civil)
                else if (e == "Penal") asubespecialidades = asubespecialidades.concat(Penal)
                else if (e == "Societario") asubespecialidades = asubespecialidades.concat(Societario)
                else if (e == "Laboral") asubespecialidades = asubespecialidades.concat(Laboral)
                else if (e == "Extranjeria") asubespecialidades = asubespecialidades.concat(Extranjeria)
                else if (e == "Internacional") asubespecialidades = asubespecialidades.concat(Internacional)
                else if (e == "Nuevas_Tecnologias") asubespecialidades = asubespecialidades.concat(Nuevas_Tecnologias)
                else if (e == "Mercantil") asubespecialidades = asubespecialidades.concat(Mercantil)
                else if (e == "Fiscal_y_Tributario") asubespecialidades = asubespecialidades.concat(Fiscal_y_Tributario)
                else if (e == "Bancario") asubespecialidades = asubespecialidades.concat(Bancario)
                else if (e == "Constitucional") asubespecialidades = asubespecialidades.concat(Constitucional)
                else if (e == "Compliance") asubespecialidades = asubespecialidades.concat(Compliance)
                else if (e == "Comunitario") asubespecialidades = asubespecialidades.concat(Comunitario)
                else if (e == "Administrativo") asubespecialidades = asubespecialidades.concat(Administrativo)

            })
            guardarGrupoSubEspecialidadesPrincipal(asubespecialidades.sort())
        }
    }, [especialidadesAbogadoPrincipal])

    useEffect(() => {
        var asubespecialidades = []
        if (especialidadesAbogadoSecundario.length != 0) {
            especialidadesAbogadoSecundario.map(e => {
                if (e == "Civil") asubespecialidades = asubespecialidades.concat(Civil)
                else if (e == "Penal") asubespecialidades = asubespecialidades.concat(Penal)
                else if (e == "Societario") asubespecialidades = asubespecialidades.concat(Societario)
                else if (e == "Laboral") asubespecialidades = asubespecialidades.concat(Laboral)
                else if (e == "Extranjeria") asubespecialidades = asubespecialidades.concat(Extranjeria)
                else if (e == "Internacional") asubespecialidades = asubespecialidades.concat(Internacional)
                else if (e == "Nuevas_Tecnologias") asubespecialidades = asubespecialidades.concat(Nuevas_Tecnologias)
                else if (e == "Mercantil") asubespecialidades = asubespecialidades.concat(Mercantil)
                else if (e == "Fiscal_y_Tributario") asubespecialidades = asubespecialidades.concat(Fiscal_y_Tributario)
                else if (e == "Bancario") asubespecialidades = asubespecialidades.concat(Bancario)
                else if (e == "Constitucional") asubespecialidades = asubespecialidades.concat(Constitucional)
                else if (e == "Compliance") asubespecialidades = asubespecialidades.concat(Compliance)
                else if (e == "Comunitario") asubespecialidades = asubespecialidades.concat(Comunitario)
                else if (e == "Administrativo") asubespecialidades = asubespecialidades.concat(Administrativo)

            })
            if (subEspecialidadesAbogadosPrincipal.length !== 0) {
                subEspecialidadesAbogadosPrincipal.map(e => {
                    var index = asubespecialidades.findIndex(x => x.value == e)
                    if (index != -1) {
                        asubespecialidades.splice(index, 1);
                    }
                })
                guardarGrupoSubEspecialidadesSecundario(asubespecialidades.sort())
            }
            else { guardarGrupoSubEspecialidadesSecundario(asubespecialidades.sort()) }

        }
    }, [especialidadesAbogadoSecundario])

    useEffect(() => {
        var asubespecialidades = []
        if (especialidadesAbogadoDeseados.length != 0) {
            especialidadesAbogadoDeseados.map(e => {
                if (e == "Civil") asubespecialidades = asubespecialidades.concat(Civil)
                else if (e == "Penal") asubespecialidades = asubespecialidades.concat(Penal)
                else if (e == "Societario") asubespecialidades = asubespecialidades.concat(Societario)
                else if (e == "Laboral") asubespecialidades = asubespecialidades.concat(Laboral)
                else if (e == "Extranjeria") asubespecialidades = asubespecialidades.concat(Extranjeria)
                else if (e == "Internacional") asubespecialidades = asubespecialidades.concat(Internacional)
                else if (e == "Nuevas_Tecnologias") asubespecialidades = asubespecialidades.concat(Nuevas_Tecnologias)
                else if (e == "Mercantil") asubespecialidades = asubespecialidades.concat(Mercantil)
                else if (e == "Fiscal_y_Tributario") asubespecialidades = asubespecialidades.concat(Fiscal_y_Tributario)
                else if (e == "Bancario") asubespecialidades = asubespecialidades.concat(Bancario)
                else if (e == "Constitucional") asubespecialidades = asubespecialidades.concat(Constitucional)
                else if (e == "Compliance") asubespecialidades = asubespecialidades.concat(Compliance)
                else if (e == "Comunitario") asubespecialidades = asubespecialidades.concat(Comunitario)
                else if (e == "Administrativo") asubespecialidades = asubespecialidades.concat(Administrativo)

            })
            if (subEspecialidadesAbogadosPrincipal.length !== 0) {
                subEspecialidadesAbogadosPrincipal.map(e => {
                    var index = asubespecialidades.findIndex(x => x.value == e)
                    if (index != -1) {
                        asubespecialidades.splice(index, 1);
                    }
                })
            }
            if (subEspecialidadesAbogadosSecundario.length !== 0) {
                subEspecialidadesAbogadosSecundario.map(e => {
                    var index = asubespecialidades.findIndex(x => x.value == e)
                    if (index != -1) {
                        asubespecialidades.splice(index, 1);
                    }
                })
            }
            guardarGrupoSubEspecialidadesDeseados(asubespecialidades.sort())

        }
    }, [especialidadesAbogadoDeseados])

    useEffect(() => {
        axios(`https://www.universal-tutorial.com/api/states/${pais}`, {
            "method": 'GET',
            "headers": {
                "Authorization": `Bearer ${Auth}`,
                "Accept": "application/json",
                "Connection": "close"
            },
        })
            .then(response => guardarOpcionesProvincias(Convertir(response.data)))


    }, [pais]);

    useEffect(() => {
        axios(`https://www.universal-tutorial.com/api/cities/${provincia}`, {
            "method": 'GET',
            "headers": {
                "Authorization": `Bearer ${Auth}`,
                "Accept": "application/json",
                "Connection": "close"
            },
        })
            .then(response => guardarOpcionesCiudades(Convertir(response.data)))


    }, [provincia]);

    async function nuevoAbogado() {
        if (!usuario) {
            return Router.push('/iniciar-sesion');
        }
        // const direccion = `${calle}, ${ciudad}, ${provincia}, ${comunidadAutonoma} - ${cp}`;

        // Creamos el objeto de abogado
        var abogado = {
            tipo,
            nombre,
            apellidos,
            idLawyou,
            dni,
            entidadLegal: "Persona Física",
            calle,
            pais,
            provincia,
            comunidadAutonoma,
            ciudad,
            emailAbogado,
            emailAbogadoSecundario,
            numeroTelefonoPrincipal,
            numeroTelefonoSecundario,
            direccion: `${calle}, ${ciudad}, ${provincia}, ${pais} - ${cp}`,
            cp,
            cif,
            idioma,
            entidadSocial,
            activo,
            especialidadesAbogadoPrincipal,
            subEspecialidadesAbogadosPrincipal,
            especialidadesAbogadoSecundario,
            subEspecialidadesAbogadosSecundario,
            especialidadesAbogadoDeseados,
            subEspecialidadesAbogadosDeseados,
            creado: Date.now(),
            creador: {
                id: usuario.uid,
                nombre: usuario.displayName
            },
            contadorAsuntos,
            contadorAceptados,
            contadorRechazados,
            ultimaOportunidad,
            comentarios
        };

        abogado.entidadLegal = entidadLegal
        abogado.lugaresEjercer = inputList
        for (let i = 0; i < comunidadesConProvincias.length; i++) {
            let comunidad = comunidadesConProvincias[i].prov
            if (comunidad.indexOf(provincia) != -1) {
                abogado.comunidadAutonoma = comunidadesConProvincias[i].com
            }

        }
        console.log(abogado.pais)
        try {
            await firebase.db.collection('abogados').add(abogado);
            Swal.fire({
                // position: 'top-end',
                icon: 'success',
                title: 'El abogado se creó correctamente!',
                showConfirmButton: false,
                timer: 1200
            })
            setTimeout(() => {
                Router.push('/abogados');
                location.reload();
            }, 1200);
        } catch (error) {
            guardarError(error);
        }
    };

    return (
        <Modal
            {...props}
            size="xl"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    <h1>Nuevo Abogado</h1>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div>
                    {usuario ? (
                        <>
                            <form
                                onSubmit={handleSubmit}
                            >
                                <ContenedorFormularioLg>
                                    <fieldset>
                                        <legend>Datos Personales</legend>
                                        <div className="col-12">
                                            <label htmlFor="entidadLegal">
                                                Entidad Legal:
                                </label>

                                            <label>
                                                <Radio
                                                    type="radio"
                                                    defaultChecked
                                                    name="persona"
                                                    onClick={() => { guardarEntidadLegal("Persona Física") }}
                                                />Persona Física
                                </label>
                                            <label>
                                                <Radio
                                                    type="radio"
                                                    name="persona"
                                                    onClick={() => { guardarEntidadLegal("Persona Jurídica") }}
                                                />Persona Jurídica
                                </label>
                                        </div>
                                        <div className="col-12 d-flex mt-3">
                                            <div className="col-4">
                                                <label htmlFor="tipo">Tipo *</label>
                                                <Select
                                                    onChange={handleChangeTipo}
                                                    options={tipos}
                                                    name="tipo"
                                                    placeholder="Selecciona Tipo"
                                                />
                                                {errores.tipo && <p className="alert alert-danger text-center p-2 mt-2">{errores.tipo}</p>}
                                            </div>
                                            <div className="col-4">
                                                <label htmlFor="idLawyou">ID Lawyou *</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="idLawyou"
                                                    onChange={handleChange}
                                                    value={idLawyou}
                                                    placeholder="Ej. A050"
                                                />
                                                {errores.idLawyou && <p className="alert alert-danger text-center p-2 mt-2">{errores.idLawyou}</p>}
                                            </div>
                                            <div className="col-4">
                                                {entidadLegal === "Persona Física" ? (
                                                    <>
                                                        <label htmlFor="dni">DNI *</label>
                                                        <input
                                                            type="text"
                                                            className="form-control"
                                                            name="dni"
                                                            onChange={handleChange}
                                                            value={dni}
                                                            placeholder="DNI del Abogado"
                                                        />
                                                        <small>Ej. 12345678A</small>
                                                        {errores.dni && <p className="alert alert-danger text-center p-2 mt-2">{errores.dni}</p>}
                                                        {errores.dniDuplicado && <p className="alert alert-danger text-center p-2 mt-2">{errores.dniDuplicado}</p>}
                                                    </>
                                                ) : (
                                                        <>
                                                            <label htmlFor="dni">NIF *</label>
                                                            <input
                                                                type="text"
                                                                className="form-control"
                                                                name="dni"
                                                                onChange={handleChange}
                                                                value={dni}
                                                                placeholder="NIF de la empresa"
                                                            />
                                                            <small>Ej. 12345678A</small>
                                                            {errores.dni && <p className="alert alert-danger text-center p-2 mt-2">{errores.dni}</p>}
                                                            {errores.dniDuplicado && <p className="alert alert-danger text-center p-2 mt-2">{errores.dniDuplicado}</p>}
                                                        </>)}



                                            </div>

                                        </div>

                                        <div className="col-12 d-flex abajo">
                                            <div className="col-4">
                                                <label htmlFor="nombre">Nombre *</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="nombre"
                                                    onChange={handleChange}
                                                    value={nombre}
                                                    placeholder="Nombre del abogado"
                                                />
                                                {errores.nombre && <p className="alert alert-danger text-center p-2 mt-2">{errores.nombre}</p>}
                                            </div>
                                            <div className="col-4">
                                                <label htmlFor="apellidos">Apellidos *</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="apellidos"
                                                    onChange={handleChange}
                                                    value={apellidos}
                                                    placeholder="Apellidos del abogado"
                                                />
                                                {errores.apellidos && <p className="alert alert-danger text-center p-2 mt-2">{errores.apellidos}</p>}
                                            </div>
                                        </div>
                                        <div className="col-12 d-flex abajo">
                                            <div className="col-6">
                                                <label htmlFor="especialidades">Especialidades Primarias* (Maximo 3)</label>
                                                <Select
                                                    name="especialidadesPrincipales"
                                                    options={especialidades}
                                                    isMulti
                                                    className="basic-multi-select"
                                                    classNamePrefix="select"
                                                    onChange={handleChangeEspecialidadPrincipal}
                                                    placeholder="Selecciona Especialidades Primarias"
                                                />
                                                {errores.especialidadesPrincipales && <p className="alert alert-danger text-center p-2 mt-2">{errores.especialidadesPrincipales}</p>}
                                            </div>

                                            <div className="col-6">
                                                <label htmlFor="SubEspecialidades">Sub-Especialidades Primarias* (Maximo 3)</label>
                                                <Select
                                                    name="SubEspecialidadesPrincipales"
                                                    options={grupoSubEspecialidadesPrincipal}
                                                    isMulti
                                                    className="basic-multi-select"
                                                    classNamePrefix="select"
                                                    onChange={handleChangeSubEspecialidadPrincipal}
                                                    placeholder="Selecciona Especialidades Primarias"
                                                />
                                                {errores.SubEspecialidadesPrincipales && <p className="alert alert-danger text-center p-2 mt-2">{errores.SubEspecialidadesPrincipales}</p>}
                                            </div>
                                        </div>
                                        {subEspecialidadesAbogadosPrincipal.length !== 0 ? (
                                            <div className="col-12 d-flex abajo">
                                                <div className="col-6">
                                                    <label htmlFor="especialidades">Especialidades Secundarias*</label>
                                                    <Select
                                                        name="especialidadesSecundario"
                                                        options={especialidades}
                                                        isMulti
                                                        className="basic-multi-select"
                                                        classNamePrefix="select"
                                                        onChange={handleChangeEspecialidadSecundario}
                                                        placeholder="Selecciona Especialidades Secundarias"
                                                    />
                                                   
                                                </div>

                                                <div className="col-6">
                                                    <label htmlFor="SubEspecialidades">Sub-Especialidades Secundarias*</label>
                                                    <Select
                                                        name="SubEspecialidadesSecundario"
                                                        options={grupoSubEspecialidadesSecundario}
                                                        isMulti
                                                        className="basic-multi-select"
                                                        classNamePrefix="select"
                                                        onChange={handleChangeSubEspecialidadSecundario}
                                                        placeholder="Selecciona Especialidades Secundarias"
                                                    />
                                                </div>
                                            </div>
                                        ) : null}

                                        {subEspecialidadesAbogadosPrincipal.length !== 0 && subEspecialidadesAbogadosSecundario.length !== 0 ? (
                                            <div className="col-12 d-flex abajo">
                                                <div className="col-6">
                                                    <label htmlFor="especialidades">Especialidades Deseados</label>
                                                    <Select
                                                        name="especialidadesDeseados"
                                                        options={especialidades}
                                                        isMulti
                                                        className="basic-multi-select"
                                                        classNamePrefix="select"
                                                        onChange={handleChangeEspecialidadDeseados}
                                                        placeholder="Selecciona Especialidades Deseados"
                                                    />
                                                   
                                                </div>

                                                <div className="col-6">
                                                    <label htmlFor="SubEspecialidades">Sub-Especialidades Deseados</label>
                                                    <Select
                                                        name="SubEspecialidadesDeseados"
                                                        options={grupoSubEspecialidadesDeseados}
                                                        isMulti
                                                        className="basic-multi-select"
                                                        classNamePrefix="select"
                                                        onChange={handleChangeSubEspecialidadDeseados}
                                                        placeholder="Selecciona Especialidades Deseados"
                                                    />
                                                    {errores.subEspecialidades && <p className="alert alert-danger text-center p-2 mt-2">{errores.subEspecialidades}</p>}
                                                </div>
                                            </div>
                                        ) : null}




                                        <div className="col-12 d-flex abajo">

                                            <div className="col-4">
                                                <label htmlFor="numeroTelefonoPrincipal">Numero Telefono Principal *</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="numeroTelefonoPrincipal"
                                                    onChange={handleChange}
                                                    value={numeroTelefonoPrincipal}
                                                    placeholder="Numero de telefono"
                                                />
                                                {errores.numeroTelefonoPrincipal && <p className="alert alert-danger text-center p-2 mt-2">{errores.numeroTelefonoPrincipal}</p>}
                                            </div>

                                            <div className="col-4">
                                                <label htmlFor="numeroTelefonoSecundario">Numero Telefono Secundario</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="numeroTelefonoSecundario"
                                                    onChange={handleChange}
                                                    value={numeroTelefonoSecundario}
                                                    placeholder="Numero de telefono"
                                                />
                                                {errores.numeroTelefonoSecundario && <p className="alert alert-danger text-center p-2 mt-2">{errores.numeroTelefonoSecundario}</p>}
                                            </div>
                                            <div className="col-4">
                                                <label htmlFor="idioma"> Idiomas *</label>
                                                <Select
                                                    name="idioma"
                                                    options={idiomas}
                                                    isMulti
                                                    className="basic-multi-select"
                                                    classNamePrefix="select"
                                                    onChange={handleChangeIdioma}
                                                    placeholder="Selecciona Idiomas hablados"
                                                />
                                                {errores.idioma && <p className="alert alert-danger text-center p-2 mt-2">{errores.idioma}</p>}
                                            </div>

                                        </div>
                                        <div className="col-12 d-flex abajo">
                                            <div className="col-4">
                                                <label htmlFor="emailAbogado">Email LawYou *</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="emailAbogado"
                                                    onChange={handleChange}
                                                    value={emailAbogado}
                                                    defaultValue="@lawyoulegal.com"
                                                    placeholder="Email de Lawyou"
                                                />
                                                {errores.emailAbogado && <p className="alert alert-danger text-center p-2 mt-2">{errores.emailAbogado}</p>}
                                            </div>

                                            <div className="col-4">
                                                <label htmlFor="emailAbogadoSecundario">Email Secundario</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="emailAbogadoSecundario"
                                                    onChange={handleChange}
                                                    value={emailAbogadoSecundario}
                                                    placeholder="Email"
                                                />
                                                {errores.emailAbogadoSecundario && <p className="alert alert-danger text-center p-2 mt-2">{errores.emailAbogadoSecundario}</p>}
                                            </div>
                                        </div>
                                    </fieldset>

                                    <fieldset className="mt-5">
                                        <legend>Datos Facturación</legend>
                                        <div className="col-12 d-flex mt-2 justify-content-between">
                                            {entidadLegal === "Persona Jurídica" ? (

                                                <div className="col-6">
                                                    <label htmlFor="entidadSocial">Entidad Social *</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="entidadSocial"
                                                        onChange={handleChange}
                                                        value={entidadSocial}
                                                        placeholder="Ej. Sevilla"
                                                    />
                                                    {errores.entidadSocial && <p className="alert alert-danger text-center p-2 mt-2">{errores.entidadSocial}</p>}
                                                </div>

                                            ) : null}

                                            {entidadLegal === "Persona Jurídica" ? (

                                                <div className="col-6">
                                                    <label htmlFor="cif">CIF *</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="cif"
                                                        onChange={handleChange}
                                                        value={cif}
                                                        placeholder="Ej. Sevilla"
                                                    />
                                                    {errores.cif && <p className="alert alert-danger text-center p-2 mt-2">{errores.cif}</p>}
                                                </div>

                                            ) : null}
                                        </div>
                                        <div className="col-12 d-flex mt-2 justify-content-between">
                                            <div className="col-4">
                                                <label htmlFor="Pais">Pais *</label>
                                                <Select
                                                    onChange={handleChangePais}
                                                    options={Paises}
                                                    name="provincia"
                                                    placeholder="Selecciona Pais"
                                                />
                                                {errores.pais && <p className="alert alert-danger text-center p-2 mt-2">{errores.pais}</p>}
                                            </div>
                                            <div className="col-4">
                                                <label htmlFor="Provincia">Provincia *</label>
                                                <Select
                                                    onChange={handleChangeProvincia}
                                                    options={opcionesProvincias}
                                                    name="provincia"
                                                    placeholder="Selecciona Pais"
                                                />
                                                {errores.provincia && <p className="alert alert-danger text-center p-2 mt-2">{errores.provincia}</p>}
                                            </div>
                                            <div className="col-4">
                                                <label htmlFor="ciudad">Ciudad *</label>
                                                <Select
                                                    onChange={handleChangeCiudad}
                                                    options={opcionesCiudades}
                                                    name="ciudad"
                                                    placeholder="Selecciona Pais"
                                                />
                                                {errores.ciudad && <p className="alert alert-danger text-center p-2 mt-2">{errores.ciudad}</p>}
                                            </div>
                                        </div>
                                        <div className="col-12 d-flex mt-2 justify-content-between">
                                            <div className="col-6">
                                                <label htmlFor="calle">Calle *</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="calle"
                                                    onChange={handleChange}
                                                    value={calle}
                                                    placeholder="Ej. Calle de Miramon Nº9, 3-A"
                                                />
                                                {errores.calle && <p className="alert alert-danger text-center p-2 mt-2">{errores.calle}</p>}
                                            </div>

                                            <div className="col-4">
                                                <label htmlFor="cp">Código Postal *</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="cp"
                                                    onChange={handleChange}
                                                    value={cp}
                                                    placeholder="Ej. 20532"
                                                />
                                                {errores.cp && <p className="alert alert-danger text-center p-2 mt-2">{errores.cp}</p>}
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset className="mt-5">
                                        <legend> Zona Geografica de Actuación </legend>
                                        <div className="col-12 d-flex mt-2 justify-content-between">
                                            <div className="col-12">
                                                <BotonMas
                                                    type="button"
                                                    onClick={handleAddClick}
                                                > Añadir Localización
                                                </BotonMas>
                                            </div>
                                        </div>

                                        {inputList.map((x, i) => {
                                            return (
                                                <>
                                                    <div className="col-12 d-flex mt-2 justify-content-between">
                                                        <div className="col-4">
                                                            <label htmlFor="Provincia">Provincia</label>
                                                            <Select
                                                                options={opcionesProvincias}
                                                                name="provincia"
                                                                placeholder="Introduce provincia"
                                                                onChange={e => handleInputChange(e, i, `Provincia`)}
                                                            />
                                                        </div>
                                                        <div className="col-4">
                                                            <label htmlFor="Ciudad" title="Solo Rellenar en caso de que no actues en toda la provincia">Ciudad <img src="/static/img/Alerta.png"></img></label>
                                                            <Select
                                                                options={opcionesCiudades}
                                                                name="ciudad"
                                                                isMulti
                                                                className="basic-multi-select"
                                                                classNamePrefix="select"
                                                                placeholder="Introduce Ciudad"
                                                                onChange={e => handleInputChange(e, i, `Ciudad`)}
                                                            />
                                                        </div>
                                                        <div className="col-4">
                                                            {inputList.length !== 1 && <BotonRemove
                                                                type="button"
                                                                onClick={() => handleRemoveClick(i)}>Remove</BotonRemove>}
                                                        </div>
                                                    </div>
                                                </>
                                            );
                                        })}


                                    </fieldset>

                                    <fieldset className="mt-5">
                                        <legend>Opciones</legend>
                                        <div className="col-12 d-flex mt-2 justify-content-between">
                                            <div className="col-4">
                                                <label htmlFor="activo">Activo *</label>
                                                <Select

                                                    onChange={handleChangeActividad}
                                                    options={actividad}
                                                    name="activo"
                                                    placeholder="estado"

                                                />
                                                {errores.activo && <p className="alert alert-danger text-center p-2 mt-2">{errores.activo}</p>}
                                            </div>
                                        </div>
                                        <div className="col-12 d-flex mt-2 justify-content-between">
                                            <div className="col-8">
                                                <label htmlFor="comentarios">Comentarios</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    type="text"
                                                    name="comentarios"
                                                    onChange={handleChange}
                                                />
                                            </div>
                                        </div>
                                    </fieldset>

                                </ContenedorFormularioLg>

                                <div className="col-12 d-flex justify-content-center">
                                    {error && <p className="alert alert-danger text-center p2 mt-2 mb-2">{error}</p>}
                                    <BotonAceptar
                                        className="btn btn-primary"
                                        type="submit"
                                    >Crear</BotonAceptar>
                                </div>
                            </form>

                        </>
                    ) :
                        <>
                            <PaginaError msg={`Tienes que estar logueado para acceder a esta sección.`}></PaginaError>
                            <div className="text-center">
                                <Link href="/iniciar-sesion"><a><h1>Inicia Sesión</h1></a></Link>
                            </div>
                        </>
                    }
                </div>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={props.onHide}>Close</Button>
            </Modal.Footer>
        </Modal >
    );
}
export default MyVerticallyCenteredModal;


