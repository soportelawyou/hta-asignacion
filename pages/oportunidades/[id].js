//CORE DEL PROGRAMA

import React, { useContext, useState, useEffect } from 'react';
// import { useRouter } from 'next/router';
import { useRouter } from 'next/router';
import Link from 'next/link';
import Swal from 'sweetalert2';
import PaginaError from '../../components/layout/PaginaError';

// Array de opciones


//Estilos
import { Divp1, Divp2, Label, Span1, BotonEditar, BotonOportunidad1, BotonOportunidad2, LabelPregunta, LabelRespuesta } from '../../components/ui/Visual'
import Footer from '../../components/layout/Footer';
import Layout from '../../components/layout/Layout';
import { PendienteAsignar, Aceptado, Rechazado, PendienteAceptar } from '../../components/ui/Estados';

// Validaciones
import useValidacionOportunidadesEditar from '../../hooks/useValidacionOportunidadesEditar';
import validarEditarOportunidad from '../../validacion/validarEditarOportunidad';
import { MyVerticallyCenteredModal } from '../VisualizarOportunidades'

// Firebase
import { FirebaseContext } from '../../firebase/index';

function copiarAlPortapapeles(id_elemento){
    // Crea un input para poder copiar el texto dentro      
    let copyText = document.getElementById(id_elemento).innerText
    const textArea = document.createElement('textarea');
    textArea.textContent = copyText;
    document.body.append(textArea);      
    textArea.select();      
    document.execCommand("copy");      
    // Delete created Element      
    textArea.remove()
}

function copiarAlPortapapeles2(datos){
    const textArea = document.createElement('textarea');
    if(datos !== null){
        let info = "Nombre: " + datos.nombreCliente + " | Apellidos: " + datos.apellidoCliente + "\n"
                    + "Telefono: " + datos.telefonoCliente + " | Email: " + datos.email + "\n"
                    + "Especialidad: " + datos.especialidad + " | Subespecialidad: " + datos.subEspecialidad + "\n"
                    + "Registro Llamada: " + datos.registroLlamada + "\n";
        if(datos.estado === "Cita"){
            info = info + "Presupuesto: " + datos.precioServicio + "\n"
                    + "Hora y Fecha: " + datos.cita.Hora + " | " + datos.cita.Fecha + "\n";
        } else { //Presupuesto
            info = info + "Presupuesto: " + datos.precioPresupuesto + "\n";
        }
        info = info + "Estado: " + datos.estado + "\n"
                + "Abogado Asignado: " + datos.nombreAbogadoAsignado;
        textArea.textContent = info;
        document.body.append(textArea);
    }
    textArea.select();
    document.execCommand("copy");
    textArea.remove();
    Swal.fire({
        icon: "success",
        title: "Copiado al Portapapeles!",
        showConfirmButton: false,
        timer: 1200,
    });
}

const Oportunidad = () => {

    // Routing para obtener el ID actual
    const router = useRouter();
    const { query: { id } } = router;
    const [modalShow, setModalShow] = React.useState(false);
    const [vista, actualizarVista] = useState(1);

    const [error, guardarError] = useState(false);
    const [consultarDB, guardarConsultarDB] = useState(true)
    const [oportunidad, guardarOportunidad] = useState({});
    const [nombreAbogado, guardarNombreAbogado] = useState('');
    const [telefonoAbogado, guardarTelefonoAbogado] = useState('');
    const [emailAbogadoAsignado, guardarEmailAbogadoAsignado] = useState('');
    const [comentarioAbogadoAsignado, guardarComentarioAbogado] = useState('');
    const [tipoUsuario, guardarTipoUsuario] = useState('');
    const { firebase, usuario } = useContext(FirebaseContext);

    // Funcion para buscar al usuario
    const buscarUsuario = async () => {
        await firebase.db.collection("usuarios").where("email", "==", usuario.email).onSnapshot(manejarSnapShotUsuario);
    }

    function manejarSnapShotUsuario(snapshot) {
        const usuarioBD = snapshot.docs.map(doc => {
            return {
                id: doc.id,
                ...doc.data()
            }
        })
        if (usuarioBD[0] !== undefined) {
            guardarTipoUsuario(usuarioBD[0].rol)
            console.log(usuarioBD[0].rol)
        }
    }

    useEffect(() => {
        let montado = true;
        if (usuario !== undefined && usuario !== null) {
            buscarUsuario();
        }
        return () => montado = false;
    }, [usuario])



    //Conseguir la oportunidad que queremos visualizar
    useEffect(() => {
        // Declaramos variable para trackear
        let estaCancelado = false;
        // console.log('useEffect [ID].js')

        if (id && consultarDB) {
            const obtenerOportunidad = async () => {
                const oportunidadQuery = await firebase.db.collection('oportunidades').doc(id);
                const oportunidad = await oportunidadQuery.get();

                // Se ejecuta solo si esta montado
                if (!estaCancelado) {
                    if (oportunidad.exists) {
                        guardarOportunidad(oportunidad.data());
                    } else {
                        guardarError(true);
                    }
                    guardarConsultarDB(false);
                }

            }
            obtenerOportunidad();
        }
        return () => {
            // Ponemos la variable a true para evitar que intente actualizar un state inexistente
            estaCancelado = true;
        }
    }, [id, oportunidad])

    // FILTRADO

    const { valores } = useValidacionOportunidadesEditar(oportunidad, validarEditarOportunidad,);
    const { nombreCliente, apellidoCliente, telefonoCliente, especialidad, subEspecialidad, logNoContestado, ciudad, email, registroLlamada, idOportunidad, provinciaOportunidad, abogadoAsignado, estado, preguntasYRespuestas, notasGestion, tipo, canales, cita, precioServicio, precioPresupuesto, nombreAbogadoAsignado} = valores;

    // console.log("valorMotivoRechazo",valorMotivoRechazo )

    // Sugerencias de SOCIOS y Colaboradores

    async function verContador() {
        if (abogadoAsignado) {
            const abogadoSeleccionado = await firebase.db.collection('abogados').doc(abogadoAsignado).get()
            guardarNombreAbogado(`${abogadoSeleccionado.data().nombre} ${abogadoSeleccionado.data().apellidos}`);
            guardarEmailAbogadoAsignado(abogadoSeleccionado.data().emailAbogado);
            guardarTelefonoAbogado(abogadoSeleccionado.data().numeroTelefonoPrincipal);
            guardarComentarioAbogado(abogadoSeleccionado.data().comentarios);
        }
    }

    useEffect(() => {
        verContador();
    }, [abogadoAsignado]);


    // Funcion para checar el abogado seleccionado
    // Funcion de filtrado manual

    const eliminarOportunidad = async () => {
        if (!usuario) {
            return router.push("/login");
        }

        // Modal de SWAL
        Swal.fire({
            title: '¿Estás seguro?',
            text: "No podrás deshacer esta acción!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, eliminar!',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value) {
                try {
                    firebase.db.collection('oportunidades').doc(id).delete();
                    Swal.fire({
                        icon: 'success',
                        title: 'La Oportunidad ha sido eliminada!',
                        showConfirmButton: false,
                        timer: 1200
                    })
                    setTimeout(() => {
                        router.push("/")
                    }, 1200);
                } catch (error) {
                    console.log(error)
                }

            }
        })

    }


    const hacerSwitch = (estado) => {
        switch (estado) {
            case "Pendiente Aceptar":
                return <PendienteAceptar>{estado}</PendienteAceptar>;
            case "Aceptada":
                return <Aceptado>{estado}</Aceptado>;
            case "Rechazada":
                return <Rechazado>{estado}</Rechazado>;
            case "Pendiente Asignar":
                return <PendienteAsignar>{estado}</PendienteAsignar>;
            default:
                return estado;
        }
    }

    return (
        <div>
            <Layout>

                {error ? <PaginaError msg="Vaya... Parece que ha habido un error al editar el tercero, vuelve a intentarlo por favor"></PaginaError> : Object.keys(valores).length === 0 ? <p>Cargando...</p> : (

                    usuario ? (
                        <>
                            <div className="row">
                                <div className="col-12">
                                    <div className="row">
                                        <div className="col-12">
                                            <h1 className="display-5 my-2">Detalles <span>{idOportunidad}</span>  - Oportunidad</h1>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-12">
                                            <BotonOportunidad1
                                                className="col-2 btn"
                                                type="button"
                                                onClick={() => actualizarVista(1)}
                                            > Perfil Oportunidad</BotonOportunidad1>
                                            <BotonOportunidad2
                                                className="col-2 btn"
                                                type="button"
                                                onClick={() => actualizarVista(2)}
                                            > Formulario Preguntas</BotonOportunidad2>
                                            <BotonOportunidad2
                                                className="col-2 btn"
                                                type="button"
                                                onClick={() => actualizarVista(3)}
                                            > Historial llamadas</BotonOportunidad2>
                                        </div>
                                    </div>
                                    {vista === 1 ?
                                        (
                                            <>
                                            <Divp1 className="mb-2 row" id='textCopy'>
                                                <div className="col-12 col-lg-4">
                                                    <div className="row">
                                                        <div className="col-12">
                                                            <Label className="d-block">Estado</Label>
                                                            <Span1>{hacerSwitch(estado)}</Span1>
                                                        </div>
                                                    </div>
                                                    <hr />
                                                    <div className="row">
                                                        <div className="col-6">
                                                            <Label className="d-block">Nombre Cliente</Label>
                                                            <Span1>{nombreCliente}</Span1>
                                                        </div>
                                                        <div className="col-6">
                                                            <Label className="d-block">Apellido Cliente</Label>
                                                            <Span1>{apellidoCliente}</Span1>
                                                        </div>
                                                    </div>
                                                    <hr />
                                                    <div className="row">
                                                        <div className="col-6">
                                                            <Label className="d-block">Telefono</Label>
                                                            <Span1>{telefonoCliente}</Span1>
                                                        </div>
                                                        <div className="col-6">
                                                            <Label className="d-block">Email Cliente</Label>
                                                            <Span1>{email}</Span1>
                                                        </div>
                                                    </div>
                                                    <hr />
                                                    <div className="row">
                                                        <div className="col-6">
                                                            <Label className="d-block">Especialidad</Label>
                                                            <Span1>{especialidad}</Span1>
                                                        </div>
                                                        <div className="col-6">
                                                            <Label className="d-block">Subespecialidad</Label>
                                                            <Span1>{subEspecialidad}</Span1>
                                                        </div>
                                                    </div>
                                                    <hr />
                                                    <div className="row">
                                                        <div className="col-6">
                                                            <Label className="d-block">Registro Llamada</Label>
                                                            <Span1>{registroLlamada}</Span1>
                                                        </div>
                                                        <div className="col-6">
                                                            <Label className="d-block">Canal</Label>
                                                            <Span1>{canales.canal}</Span1>
                                                        </div>
                                                        <div className="col-6">
                                                            <Label className="d-block"></Label>
                                                            <Span1></Span1>
                                                        </div>
                                                        <div className="col-6">
                                                            <Label className="d-block">Subcanal</Label>
                                                            <Span1>{canales.subcanal}</Span1>
                                                        </div>
                                                    </div>
                                                    <hr />

                                                    {(tipo === "Cita") ?
                                                        (
                                                            <>
                                                                <div className="row">
                                                                    <div className="col-12">
                                                                        <Label className="d-block">Fecha y Hora Cita</Label>
                                                                        <Span1>{cita.Fecha}</Span1>
                                                                        <Span1> / </Span1>
                                                                        <Span1>{cita.Hora}</Span1>
                                                                    </div>
                                                                </div>
                                                                <hr />
                                                                <div className="row">
                                                                    <div className="col-12">
                                                                        <Label className="d-block">Precio Cita</Label>
                                                                        <Span1>{precioServicio}</Span1>
                                                                    </div>
                                                                </div>
                                                                <hr />
                                                            </>
                                                        ) : 
                                                        (
                                                            <>
                                                                <div className="row">
                                                                    <div className="col-12">
                                                                        <Label className="d-block">Presupuesto</Label>
                                                                        <Span1>{precioPresupuesto}</Span1>
                                                                    </div>
                                                                </div>
                                                                <hr />
                                                            </>
                                                        )
                                                    }

                                                    <div className="row">
                                                        <div className="col-12">
                                                            <BotonEditar
                                                                className="col-4 btn"
                                                                onClick={() => setModalShow(true)}>
                                                                Editar Oportunidad
                                                </BotonEditar>
                                                            <MyVerticallyCenteredModal
                                                                show={modalShow}
                                                                id={id}
                                                                onHide={() => setModalShow(false)}
                                                            />
                                                            {tipoUsuario !== "Estandar" ? (
                                                                <BotonEditar
                                                                    className="col-4 btn"
                                                                    type="button"
                                                                    onClick={() => eliminarOportunidad()}
                                                                >Eliminar Oportunidad</BotonEditar>

                                                            ) : null}
                                                        </div>
                                                    </div>
                                                    <hr />
                                                </div>
                                                <Divp2 className="con-12 col-lg-7">
                                                    <div className="row">
                                                        <div className="col-12">
                                                            <Label>Informacion Abogado</Label>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-12">
                                                            <table className="table table-bordered table-hover w-100">
                                                                <thead className="thead-dark text-center p-3">
                                                                    <tr className="m-2">
                                                                        <th className="align-middle cabecera">Nombre y Apellidos</th>
                                                                        <th className="align-middle cabecera">Telefono</th>
                                                                        <th className="align-middle cabecera">Email</th>
                                                                        <th className="align-middle cabecera">Comentario</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr className="text-center">
                                                                        <th className="p-3 align-middle">
                                                                            <Link href="/abogados/[id]" as={`/abogados/${abogadoAsignado}`}>
                                                                                <a href={`/abogados/${abogadoAsignado}`}><span className="mr-1">{nombreAbogado}</span></a>
                                                                            </Link>
                                                                        </th>
                                                                        <th className="p-3 align-middle">{telefonoAbogado}</th>
                                                                        <th className="p-3 align-middle">{emailAbogadoAsignado}</th>
                                                                        <th className="p-3 align-middle">{comentarioAbogadoAsignado}</th>
                                                                    </tr>
                                                                </tbody>

                                                            </table>
                                                        </div>
                                                    </div>
                                                    <hr />
                                                </Divp2>
                                            </Divp1>
                                            <div>
                                                <BotonEditar
                                                    onClick={() => copiarAlPortapapeles2(valores)}
                                                >
                                                    Copiar Datos
                                                </BotonEditar>
                                            </div>
                                            </>
                                        )
                                        : vista === 2 ?
                                            (
                                                <>
                                                    <Divp1 className="mb-2 row">
                                                        <div className="row">
                                                            <div className="col-12">
                                                                <h1 className="col-12">FORMULARIO PREGUNTAS</h1>
                                                            </div>
                                                        </div>
                                                    </Divp1>
                                                    <Divp1 className="mb-2 row">
                                                        <div className="col-12" >
                                                            {preguntasYRespuestas.map(e =>
                                                                <>
                                                                    {e == "No ha preguntas específicas" ?
                                                                        (
                                                                            <div className="row">
                                                                                <div className="col-12 ">
                                                                                    <LabelPregunta className="d-block">{e}</LabelPregunta>
                                                                                </div>
                                                                            </div>
                                                                        ) : null
                                                                    }
                                                                    
                                                                    <div className="row">
                                                                        <div className="col-12 ">
                                                                            <LabelPregunta className="d-block">{e['Pregunta']}</LabelPregunta>
                                                                        </div>
                                                                    </div>
                                                                    <div className="row">
                                                                        <div className="col-12 ">
                                                                            <LabelRespuesta className="d-block">{e['Respuesta']}</LabelRespuesta>
                                                                        </div>
                                                                    </div>
                                                                    <hr />
                                                                </>
                                                            )}

                                                        </div>
                                                    </Divp1>

                                                </>
                                            ) : vista === 3 ? (
                                                <>
                                                    <Divp1 className="mb-2 row">
                                                        <div className="row">
                                                            <div className="col-12">
                                                                <h1 className="col-12">HISTORIAL LLAMADAS</h1>
                                                            </div>
                                                        </div>
                                                    </Divp1>
                                                    <Divp1 className="mb-2 row">
                                                        <div className="col-12" >
                                                            {logNoContestado.map(e =>
                                                                <>
                                                                    <div className="row">
                                                                        <div className="col-12 ">
                                                                            <LabelPregunta className="d-block">{e.Fecha} / {e.Hora}</LabelPregunta>
                                                                        </div>
                                                                    </div>
                                                                    <hr />
                                                                </>
                                                            )}

                                                        </div>
                                                    </Divp1>

                                                </>

                                            ) : null}


                                </div>
                            </div>
                        </>
                    ) :
                        <>
                            <PaginaError msg={`Tienes que estar logueado para acceder a esta sección.`}></PaginaError>
                            <div className="text-center">
                                <Link href="/iniciar-sesion"><a><h1>Inicia Sesión</h1></a></Link>
                            </div>
                        </>

                )}

                <Footer />
            </Layout>
        </div>
    );
}

export default Oportunidad;
